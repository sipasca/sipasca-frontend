import postRequest from '../pages/api/post-request';
import storage from '../components/storage';
import { ref, uploadBytes } from "firebase/storage";
import deleteDokumenFromStorage from './DeleteDokumenFromStorage';

async function editTopikHandler(router, listAllDokumen, body, listUrlDeletedFile) {
    for (let urlFile of listUrlDeletedFile) {
        deleteDokumenFromStorage(urlFile)
      }
    for (const [key, value] of Object.entries(listAllDokumen)) {
        body[key] = ""
        const dokumen = value[2]
        if (dokumen!=null){
            const nama = dokumen.name
            const dokumen_directory = 'production/dokumenTopik/' + body['id_topik'] + '/' + key + '_' + nama
            const filePath = ref(storage, dokumen_directory)
            await uploadBytes(filePath, dokumen)
            body[key] = {'nama_file':nama, 'url':dokumen_directory}
        }
    }

    const res = await postRequest('/editTopikDosen/'+ body['id_topik'] +'/', body);
    const data = res.data;  
    if (data.status === 'Success') {
        if (window != 'undefined') {
            window.localStorage.setItem('edit', 'Success')
        }
        router.push('/topik/daftarTopik')
    }
}
export default editTopikHandler