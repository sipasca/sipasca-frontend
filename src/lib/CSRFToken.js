import React, { useState, useEffect } from "react";
import axios from "axios";
import Cookies from 'js-cookie'

const CSRFToken = (props) => {
  const [csrftoken, setcsrftoken] = useState('');

  const getCookie = (name) => {
    let cookieValue = null;
    if (document.cookie && document.cookie !== "") {
      let cookies = document.cookie.split(";");
      for (let item of cookies) {
        let cookie = item.trim();
        if (cookie.substring(0, name.length + 1) === name + "=") {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  };

  useEffect(() => {
    let backendURL = ``;
    if (props.type.toUpperCase() === 'LOCAL') {
      backendURL = `http://localhost:8000/auth/token`;
    }
    else {
      backendURL = `https://simkuspa-backend.herokuapp.com/auth/token`;
    }
    const fetchToken = async () => {
      const CSRFRequest = await axios.get(backendURL, {withCredentials: true, credentials: 'include'});
      Cookies.set('csrftoken', CSRFRequest.data.csrftoken, { sameSite:'None', secure: true});
    }
    
    fetchToken();

    setcsrftoken(getCookie("csrftoken"));
  }, [props.type]);

  return <input type="hidden" name="csrfmiddlewaretoken" value={csrftoken} />;
};

export default CSRFToken;
