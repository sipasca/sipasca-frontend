import { ref, deleteObject } from "firebase/storage";
import storage from "../components/storage";

async function deleteDokumenFromStorage(url) {
    const storageRef = ref(storage, url);
    try{
        deleteObject(storageRef);
    }
    catch(err){
        console.error(err.message);
        alert("Terjadi kesalahan dalam menghapus file");
    }
}
export default deleteDokumenFromStorage