import { createContext, useContext, useState } from "react"

const TermContext = createContext()

export const TermProvider = ({children}) => {
    const [activeTerm, setActiveTerm] = useState(null)

    const getTerm = async () => {
        const res = await fetch("https://simkuspa-backend.herokuapp.com/getTermAktif/")
        const data = await res.json()
        const term = data.term_aktif[0][0]
        setActiveTerm(term)
    }

    const updateTerm = async () => {
        if (activeTerm === null) {
            getTerm()
        }

        setTimeout(() => {
            setActiveTerm(null)
        }, 1000 * 60 * 60 * 24)
    }

    return (
        <TermContext.Provider
          value={{
              activeTerm, 
              updateTerm
          }}
        >
          {children}
        </TermContext.Provider>
      )
}

export const useTerm = () => useContext(TermContext)