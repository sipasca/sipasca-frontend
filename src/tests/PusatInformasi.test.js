import React from "react";
import {describe, expect, test} from '@jest/globals'
import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom'
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import PusatInformasi from '../pages/pusatInformasi';
import { getServerSideProps } from '../pages/pusatInformasi';
import Heading from "../components/Heading"
import * as nextRouter from "next/router";

import axios from 'axios';
jest.mock('axios');

library.add(fas);

const berkasPentingDummy = {'berkas_penting_list' : [[1, 'dummy.firebase.com', 'Umum', 'dummy']]};
const pengumumanDummy = {"pengumuman_list": [[10, "C", "DCCC", "2022-03-18"]]}
const pusatInformasiPage = <PusatInformasi berkasPenting = {berkasPentingDummy} pengumuman = {pengumumanDummy}/>

// Mocking getting userID and role with axios
const res = {
  data : {
    userID: 'staf123',
    role: 'staf'
  }
}
axios.get.mockResolvedValue(res);

// Mocking useState for setting role
const state = 'staf';
React.useState = (initial) => [state, () => {}]

test("renders pengumuman tittle in Pusat Informasi", () => {
    render(pusatInformasiPage)
    const pengumumanTittle = screen.getByText("| Pengumuman");
    expect(pengumumanTittle).toBeInTheDocument();
})

test("Page contains Component Heading with text '| Pengumuman'", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Heading text = "| Pengumuman" type="pusatInformasi"></Heading>)
  const text = screen.getByText("| Pengumuman");
  expect(text).toBeInTheDocument()
})

test("Page contains Component Heading with text '| Berkas Penting'", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Heading text = "| Berkas Penting" type="pusatInformasi"></Heading>)
  const text = screen.getByText("| Berkas Penting");
  expect(text).toBeInTheDocument()
})

test("renders icon angle-down in Pusat Informasi", () => {
    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({ prefetch: async () => {} }));
    
    const { container } = render(pusatInformasiPage)

    const iconAngleDown = container.querySelector("[class*='fa-angle-down']")
    expect(iconAngleDown).not.toBeNull();
})

test("renders link to berkas penting in Pusat Informasi", () => {
    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({ prefetch: async () => {} }));

    const { container } = render(pusatInformasiPage)
    expect(
        container.querySelector('[data-testid="link-berkas-penting"]').getAttribute("href")
      ).toEqual("/berkasPenting");
})

test("renders link to pengumuman in Pusat Informasi", () => {
    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({ prefetch: async () => {} }));

    const { container } = render(pusatInformasiPage)
    expect(
        container.querySelector('[data-testid="link-pengumuman"]').getAttribute("href")
      ).toEqual("/pengumuman");
})

describe("getServerSideProps", () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ status:'success' })
      })
    );
  
    beforeEach(() => {
      fetch.mockClear();
    });
  
    it("should call api", async () => {
      await getServerSideProps();
      expect(fetch).toHaveBeenCalledTimes(2)
    });
  });