import CariDosen from '../pages/cariDosen/index';
import BidangKepakaran from "../pages/cariDosen/bidangKepakaran/[bidangKepakaranID]"
import {expect, test} from '@jest/globals'
import {render, screen, fireEvent } from "@testing-library/react";
import '@testing-library/jest-dom'
import userEvent from "@testing-library/user-event";

import React from "react";
import { getServerSideProps } from "../pages/cariDosen/index";
import { getServerSideProps as getServerSideProps2 } from "../pages/cariDosen/bidangKepakaran/[bidangKepakaranID]";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
library.add(fas);

const dummy = {
    list_bidang_kepakaran: [
        [ 'AIG', 'Artificial Intelligence' ],
        [ 'DSA', 'Data Science & Analytics' ],
    ],
    list_dosen: [["D111", "Dosen A", "dosenA@email.com", "url"]]
}

it("check on getServerSideProps list bidang kepakaran with context", async () => {
    global.fetch = jest.fn(() =>
    Promise.resolve({
            json: () => Promise.resolve({})
        })
    );
    
    await getServerSideProps();
});


it("check on getServerSideProps daftar dosen with context", async () => {
    const context = {
        query: { bidangKepakaranID: "SEG", namaBidang:"Software Engineering"}
    };
    

    global.fetch = jest.fn(() =>
    Promise.resolve({
            json: () => Promise.resolve({})
        })
    );
    
    await getServerSideProps2(context);
});

test("list bidang kepakaran contains heading", () => {
    render(<CariDosen data={dummy}/>)
    const title= screen.getByText("| Cari Dosen");
    expect(title).toBeInTheDocument();
})

test("daftar dosen contains heading", () => {
    render(<BidangKepakaran data={dummy}/>)
    const title= screen.getByText("| Daftar Dosen");
    expect(title).toBeInTheDocument();
})

test("list bidang kepakaran contains sub heading", () => {
    render(<CariDosen data={dummy}/>)
    const sub = screen.getByText("Daftar Dosen Berdasarkan Bidang Kepakaran");
    expect(sub).toBeInTheDocument();
})

test("click daftar dosen button go to request page", () => {
    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
      route: '/cariDosen/requestDospem',
      push: jest.fn(),
    }));

    render(<BidangKepakaran data={dummy}/>)
    const bidangButton = screen.getAllByRole('button')
    fireEvent.click(bidangButton[0])
    expect(useRouter).toHaveBeenCalled();
})

test("click list bidang kepakaran go to daftar dosen", () => {
    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
      route: '/cariDosen/bidangKepakaran/AIG?namaBidang=Artificial+Intelligence',
      push: jest.fn(),
    }));

    
    const { container } = render(<CariDosen data={dummy}/>);
    const mkBox = container.querySelector("[id*='cardClick']")
    userEvent.click(mkBox)
    expect(useRouter).toHaveBeenCalled();
})

test("search dosen in daftar dosen", () => {
    render(<BidangKepakaran data={dummy}/>)
    const searchButton = screen.getByPlaceholderText("Cari nama dosen");
    fireEvent.change(searchButton, {target: {value: 'D'}})
})