import React from "react";
import { expect, test} from '@jest/globals'
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";

import * as nextRouter from "next/router";
import MKLayout from '../components/MKLayout';

import axios from 'axios';
jest.mock('axios');

jest.spyOn(window.localStorage.__proto__, 'getItem');
window.localStorage.__proto__.getItem = jest.fn(() => {
    return 'm'
});

// Mocking getting userID and role with axios
const res = {
    data : {
      userID: 'mahasiswa123',
      role: 'mahasiswa'
    }
  }
axios.get.mockResolvedValue(res);


test("renders navigation name correctly", () => {
    // Mocking getting userID and role with axios
    const res = {
        data : {
        userID: 'staf123',
        role: 'staf'
        }
    }
    axios.get.mockResolvedValue(res);

    // Mocking useState
    const state = 'staf'
    React.useState = (initial) => [state, () => {}]

    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: "CSGE" }, prefetch: async () => {} }));

    render(<MKLayout namaMK="Tesis"> </MKLayout>)
    const navigationName = screen.getByText("Administrasi");
    expect(navigationName).toBeInTheDocument();
})

test("text inside MataKuliahLayout appeared", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/bimbingan", query: { mataKuliahID: "CSGE" } }));
    
    render(<MKLayout namaMK="Tesis"> Text </MKLayout>)

    const text = screen.getByText("Text");
    expect(text).toBeInTheDocument();
})

test("style is activated when pathname is same", () => {
    // Mocking getting userID and role with axios
    const res = {
        data : {
        userID: 'dosen123',
        role: 'dosen'
        }
    }
    axios.get.mockResolvedValue(res);

    // Mocking useState
    const state = 'dosen'
    React.useState = (initial) => [state, () => {}]
    
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ 
        pathname: "/mataKuliahSpesial/CSGE/d/bimbingan/detail", 
        query: { mataKuliahID: "CSGE" },
        prefetch: async () => {}
    }));
    
    const { container } = render(<MKLayout namaMK="Tesis"> Text </MKLayout>);
    const styleActivated = container.querySelector("[class*='active']")
    expect(styleActivated).not.toBeNull();
})

test("style is not activated when pathname is different", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ 
        pathname: "/dummy/a/b/c/d", 
        query: { mataKuliahID: "CSGE" } 
    }));
    
    const { container } = render(<MKLayout namaMK="Tesis"> Text </MKLayout>);
    const styleActivated = container.querySelector("[class*='active']")
    expect(styleActivated).toBeNull();
})