import DetailPengumuman from "../pages/detailPengumuman/[detailPengumumanID]";
import {describe, expect, test} from '@jest/globals'
import { render, screen } from "@testing-library/react";
import Heading from "../components/Heading"
import * as nextRouter from "next/router";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { getServerSideProps } from "../pages/detailPengumuman/[detailPengumumanID]";
library.add(fas);

import '@testing-library/jest-dom'
import FormatTanggal from "../components/FormatTanggal";

const detail = {
    "pengumuman_detail": [
        [
            6,
            "Pelayanan Loket Sekretariat Akademik Depok BARU",
            "Yth Bapak/Ibu Staf Pengajar dan Mahasiswa Fasilkom",
            "2022-03-14"
        ]
    ] 
}
test("Page contains Component Heading with text '| Detail Pengumuman'", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));
  
    render(<Heading text = "| DetailPengumuman" type="kecil"></Heading>)
    const text = screen.getByText("| DetailPengumuman");
    expect(text).toBeInTheDocument()
})

test("renders judul in Detail Pengumuman", () => {
    render(<DetailPengumuman detailPengumuman = {detail}/>)

    const judul = screen.getByText(detail.pengumuman_detail[0][1]);
    expect(judul).toBeInTheDocument();
})

test("renders konten in Detail Pengumuman", () => {
    render(<DetailPengumuman detailPengumuman = {detail}/>)

    const konten = screen.getByText(detail.pengumuman_detail[0][2]);
    expect(konten).toBeInTheDocument();
})

test("renders tanggal in Detail Pengumuman", () => {
    render(<DetailPengumuman detailPengumuman = {detail}/>)

    const tanggal = screen.getByText("Terakhir dimodifikasi: 14 Maret 2022");
    expect(tanggal).toBeInTheDocument();
})

test("renders back button in Detail Pengumuman", () => {
    render(<DetailPengumuman detailPengumuman = {detail}/>)

    const backButton = screen.getByRole("button");
    expect(backButton).toBeInTheDocument();
})

it("check on getServerSideProps with context", async () => {
    const context = {
        query: { detailPengumumanID: "6"}
    
    };
    global.fetch = jest.fn(() =>
    Promise.resolve({
            json: () => Promise.resolve({})
        })
    );
    
    await getServerSideProps(context);
  });
