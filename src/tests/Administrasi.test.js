import React from "react";
import Administrasi from "../pages/administrasi/index";
import {describe, expect, test} from '@jest/globals'
import { render, screen, fireEvent } from "@testing-library/react";
import '@testing-library/jest-dom'
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
library.add(far, fas);

import { getServerSideProps } from "../pages/administrasi/index";
import * as nextRouter from "next/router";

import { GetCookie } from "./Helper";

import * as TermContext from "../context/termContext"

import axios from 'axios';
jest.mock('axios');

const mata_kuliah_mahasiswa = {"mk_list": [["Publikasi Ilmiah", "2022-2", "CSGE1"],["Studi Mandiri", "2022-2", "CSGE2"]]}
const enroled_mata_kuliah = <Administrasi data = {mata_kuliah_mahasiswa} />

// Mock cookie in getServerSideProps
const { cookieUserID: cookieUserStaf, cookieRole: cookieRoleStaf } = GetCookie('staf123', 'staf')
const { cookieUserID: cookieUserMahasiswa , cookieRole: cookieRoleMahasiswa } = GetCookie('mahasiswa123', 'mahasiswa')

const activeTerm = '2022-2'
const contextValues = { activeTerm: activeTerm };
jest.spyOn(TermContext, 'useTerm').mockImplementation(() => contextValues);

it("check on getServerSideProps with context", async () => {
  const context = {
      req: { 
        cookies: {
          "userID" : cookieUserMahasiswa,
          "role": cookieRoleMahasiswa
        }
      },
  };
  global.fetch = jest.fn(() =>
  Promise.resolve({
          json: () => Promise.resolve({})
      })
  );
  
  await getServerSideProps(context);
});

it("check on getServerSideProps with context staf", async () => {
  const context = {
      req: { 
        cookies: {
          "userID" : cookieUserStaf,
          "role": cookieRoleStaf
        }
      },
  };
  global.fetch = jest.fn(() =>
  Promise.resolve({
          json: () => Promise.resolve({
            'mk_tersedia': [['CS', 'Tahun', 'Nama']]
          })
      })
  );
  
  await getServerSideProps(context);
});

test("renders title in administrasi Page", () => {
    // Mocking getting userID and role with axios
    const res = {
      data : {
        userID: 'test',
        role: 'mahasiswa'
      }
    }
    axios.get.mockResolvedValue(res);

    render(enroled_mata_kuliah)
    const title = screen.getByText("| Mata Kuliah Spesial");
    expect(title).toBeInTheDocument();
})

test("Page with tanda : SUCCESS contains Alert", () => {
    // Mocking getting userID and role with axios
    const res = {
      data : {
        userID: 'test',
        role: 'mahasiswa'
      }
    }
    axios.get.mockResolvedValue(res);
    
    jest.spyOn(window.localStorage.__proto__, 'getItem');
    window.localStorage.__proto__.getItem = jest.fn(() => {
      return 'SUCCESS'
    });
    
    render(enroled_mata_kuliah)
  
    const buttonTambah = screen.getByText("Mata Kuliah SUCCESS berhasil dibuat");
    expect(buttonTambah).toBeInTheDocument();
  
  })

  test("Page with tanda : FAILED contains Alert", () => {
    // Mocking getting userID and role with axios
    const res = {
      data : {
        userID: 'test',
        role: 'mahasiswa'
      }
    }
    axios.get.mockResolvedValue(res);


    jest.spyOn(window.localStorage.__proto__, 'getItem');
    window.localStorage.__proto__.getItem = jest.fn(() => {
      return 'FAILED'
    });
    
    render(enroled_mata_kuliah)
  
    const buttonTambah = screen.getByText("Mata Kuliah FAILED tidak berhasil ditambahkan");
    expect(buttonTambah).toBeInTheDocument();
  })
  
test("function gotoMataKuliah run in mahasiswa", () => {
  // Mocking getting userID and role with axios
  const res = {
    data : {
      userID: 'test',
      role: 'mahasiswa'
    }
  }
  axios.get.mockResolvedValue(res);

  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ push: jest.fn()}));
  
  const { container } = render(enroled_mata_kuliah);
  const goToMataKuliahButton = container.querySelector('[id="cardClick"]');
  fireEvent.click(goToMataKuliahButton);
  
});

test("function gotoMataKuliah run in staf", () => {
  // Mocking getting userID and role with axios
  const res = {
    data : {
      userID: 'test',
      role: 'staf'
    }
  }
  axios.get.mockResolvedValue(res);

  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ push: jest.fn(), prefetch: async () => {} }));
  
  const { container } = render(enroled_mata_kuliah);
  const goToMataKuliahButton = container.querySelector('[id="cardClick"]');
  fireEvent.click(goToMataKuliahButton);
  
});

test("function gotoMataKuliah run in dosen", () => {
  // Mocking getting userID and role with axios
  const res = {
    data : {
      userID: 'test',
      role: 'dosen'
    }
  }
  axios.get.mockResolvedValue(res);

  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ push: jest.fn(), prefetch: async () => {} }));
  
  const { container } = render(enroled_mata_kuliah);
  const goToMataKuliahButton = container.querySelector('[id="cardClick"]');
  fireEvent.click(goToMataKuliahButton);
  
});