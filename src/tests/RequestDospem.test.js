import RequestDospem from "../pages/cariDosen/requestDospem";
import {expect, test} from '@jest/globals'
import { render, screen, fireEvent } from "@testing-library/react";
import '@testing-library/jest-dom'
import React from "react";
import { getServerSideProps } from "../pages/cariDosen/requestDospem";
import userEvent from "@testing-library/user-event";
import { GetCookie } from "./Helper";

const mata_kuliah_mahasiswa = {
    "mk_list": [["Publikasi Ilmiah", "2022-2", "CSGE802097"]],
    "idDosen": "D999999999",
    "judulTopik": "Topik A",
    "deskripsiTopik": "Deskripsi A",
    "detailMahasiswa": ["M999999999", "Chaehun", "test@gmail.com", "Magister", "1906999999", "img/link", "cv/link", "namaCV.pdf", "Chaehun S.Kom"],
    "detailDosen": ["D999999999", "profileURL.com", "Dosen A", "Available", "dosen@mail.com", "Biografi Dosen A", "Minat Penelitian Dosen A", "Jenis Kelamin Dosen A", "999999999", "Dosen A, S.Kom., M.Sc., Ph.D.", "Foto Dosen A"]
}
const request_dospem = <RequestDospem data = {mata_kuliah_mahasiswa} />


// Mocking axios for CSRF Token
const axios = require('axios');
jest.mock('axios');
const csrf = {
    'data': {'csrftoken': 'test'}
}
axios.get.mockResolvedValue(csrf);

// Mocking setting cookie for CSRF Token
const cookie = 'csrftoken=test;';
global.document.cookie = cookie;

test("renders title in mendaftarMK Page", () => {
    render(request_dospem)
    const title = screen.getByText("| Request Dosen Pembimbing");
    expect(title).toBeInTheDocument();
})

// Mock cookie in getServerSideProps
const { cookieUserID: cookieUserMahasiswa } = GetCookie('mahasiswa123', 'mahasiswa')

it("check on getServerSideProps with context", async () => {
    const context = {
        req: { 
            cookies: { "userID" : cookieUserMahasiswa }
        },
        query: { id: "D2432150536", judulTopik: '', deskripsiTopik: '', idTopik:''}
    };
    global.fetch = jest.fn(() =>
    Promise.resolve({
            json: () => Promise.resolve({
                "mk_list": [["Studi Mandiri", "2022-2", "CSGE801092"]],
                "profile_mahasiswa": [["M999999999", "Chaehun", "test@gmail.com", "Magister", "1906999999", "img/link", "cv/link", "namaCV.pdf", "Chaehun S.Kom"]],
                "profile_dosen": [["D999999999", "profileURL.com", "Dosen A", "Available", "dosen@mail.com", "Biografi Dosen A", "Minat Penelitian Dosen A", "Jenis Kelamin Dosen A", "999999999", "Dosen A, S.Kom., M.Sc., Ph.D.", "Foto Dosen A"]]
            })
        })
    );
    
    await getServerSideProps(context);
});

test("submit request with empty input will failed", async () => {    
    // Test
    const { container } = render(request_dospem);
    const submitButton = container.querySelector("[type='submit']");
    fireEvent.click(submitButton);
}) 

test("submit request successfully", async () => {
    // Mocking PostRequest
    const response = {
        'data': {
            'status' : 'SUCCESS',
            'id_Mk': 'testIdMK',
            'id_dosen' : 'testIdDosen',
            'id_mahasiswa': 'testIdMahasiswa',
            'judul' : 'testJudul',
            'deskripsi' : 'testDeskripsi',
            'motivasi' : 'testMotivasi',
            'batas_waktu' : 'testBatasWaktu',
            'pesan' : 'testPesan',
        }
    }

    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });

    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
      route: '/cariDosen/',
      push: jest.fn(),
    }));

    // Test
    const { container } = render(request_dospem);

    const mataKuliahInput = container.querySelector("[id='mk']");
    fireEvent.change(mataKuliahInput, { target: { selectedIndex: 0 }});

    const judulInput = container.querySelector("[id='judulTopik']");
    userEvent.type(judulInput, 'Topik B')

    const deskripsiInput = container.querySelector("[id='deskripsiTopik']");
    userEvent.type(deskripsiInput, 'Deskripsi B')

    const batasWaktuInput = container.querySelector("[id='batasWaktu']");
    fireEvent.change(batasWaktuInput, { target: { value: '2022-05-12' } });
    
    const motivasiInput = container.querySelector("[id='motivasiTopik']");
    userEvent.type(motivasiInput, 'Motivasi B')
    
    const submitButton = container.querySelector("[type='submit']");
    fireEvent.click(submitButton);
    expect(useRouter).toHaveBeenCalled();
}) 

test("change mata kuliah successfully", () => {
    const { container } = render(request_dospem);
    const mataKuliahInput = container.querySelector("[id='mk']");
    fireEvent.change(mataKuliahInput, { target: { selectedIndex: 0 }});
})

test("change judul topik successfully", () => {
    const { container } = render(request_dospem);
    const judulInput = container.querySelector("[id='judulTopik']");
    userEvent.type(judulInput, 'Topik B')
})

test("change deskripsi successfully", () => {
    const { container } = render(request_dospem);
    const deskripsiInput = container.querySelector("[id='deskripsiTopik']");
    userEvent.type(deskripsiInput, 'Deskripsi B')
})

test("change date successfully", () => {
    const { container } = render(request_dospem);
    const batasWaktuInput = container.querySelector("[id='batasWaktu']");
    fireEvent.change(batasWaktuInput, { target: { value: '2022-05-12' } });
})

test("change motivasi successfully", () => {
    const { container } = render(request_dospem);
    const motivasiInput = container.querySelector("[id='motivasiTopik']");
    userEvent.type(motivasiInput, 'Motivasi B')
})

test("change pesan successfully", () => {
    const { container } = render(request_dospem);
    const pesanInput = container.querySelector("[id='pesan']");
    userEvent.type(pesanInput, 'Pesan B')
})

test("popup appears successfully", () => {
    const { container } = render(request_dospem);
    const popupRequestButton = container.querySelector("[id='popupButton']");
    fireEvent.click(popupRequestButton);
    const popupInfo = screen.getByText("SIPASCA akan mengirimkan email kepada dosen pembimbing berisi detail permohonan yang Anda ajukan. Jika Dosen membalas email permohonan tersebut, maka balasan Dosen akan langsung masuk ke email Anda.");
    expect(popupInfo).toBeInTheDocument();
})
