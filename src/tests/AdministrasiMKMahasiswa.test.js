import {expect, test} from '@jest/globals'
import { render, screen, fireEvent,getByTestId} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import '@testing-library/jest-dom';

import React from "react";
import * as nextRouter from "next/router";
import Administrasi from "../pages/mataKuliahSpesial/[mataKuliahID]/m/administrasi";
import { getServerSideProps, getDataFormPersyaratan } from "../pages/mataKuliahSpesial/[mataKuliahID]/m/administrasi";
import { GetCookie } from "./Helper";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";

library.add(fas);

import * as TermContext from "../context/termContext"

const activeTerm = '2022-2'
const contextValues = { activeTerm: activeTerm };
jest.spyOn(TermContext, 'useTerm').mockImplementation(() => contextValues);

nextRouter.useRouter = jest.fn();
nextRouter.useRouter.mockImplementation(() => ({ 
    reload: jest.fn(),
    query: { mataKuliahID: 'CSGE802099' }
}));

// Mock cookie in getServerSideProps
const { cookieUserID: cookieUserMahasiswa } = GetCookie('mahasiswa123', 'mahasiswa')

const mkID = 'CSGE802099'
const mhsIDVerified= ["M9636831678"]
const mhsIDVerified2= "M9636831678"

const detailMK = {'detail_mk': [
      'CSGE802099',
      '2022-2',
      'Tesis',
      29,
      6,
      2,
      '03.00.12.01-2020',
      true
    ]}
const profileMahasiswa = {
    profile_mahasiswa: [
      [
        'M9636831678',
        'Choi Chaehun',
        'choichaehun@gmail.com',
        'Magister Ilmu Komputer',
        '1909293044',
        'production/profileImage/mahasiswa/M9636831678',
        'production/CV/M9636831678',
        'test.pdf',
        null
      ]
    ]
  }

const dataListFormPersyaratan = {
    list_form_persyaratan: [
        ['Form Persetujuan Pembimbing MK Spesial', 'Prasyarat'],
        ['Form Pernyataan Integritas', 'Keluaran'],
    ]
}
const dataFormPersyaratan = {
    'Form Persetujuan Pembimbing MK Spesial': [
      [
        'production/FormPersyaratan/CSGE802099/Form Persetujuan Pembimbing MK Spesial/1906293045_Form Persetujuan Pembimbing MK Spesial.pdf',
        '1909293044_Form Persetujuan Pembimbing MK Spesial'
      ]
    ],
    'Form Pernyataan Integritas': [],
    'Form Persyaratan Sidang ': []
  }

  const dataFormPersyaratan2 = {
    'Form Persetujuan Pembimbing MK Spesial': [],
    'Form Pernyataan Integritas': [],
    'Form Persyaratan Sidang ': []
  }

  const dosenPembimbing = {"dospem_list": [["Menunggu persetujuan dosen", "Dosen1, S.Kom., M.Sc., Ph.D."]]}
  const dosenPembimbing2 = {"dospem_list": [["Disetujui dosen pembimbing", "Dosen1, S.Kom., M.Sc., Ph.D."]]}
  const dosenPembimbing3 = {"dospem_list": []}

  const detail_migrasi_per_mahasiswa_req = {
    "detail_migrasi_per_mahasiswa": [[
      100,'CSGE802099','M9636831678','2022-2','dummy','REQ'
    ]]
  }

  const detail_migrasi_per_mahasiswa_disetujui = {
    "detail_migrasi_per_mahasiswa": [[
      100,'CSGE802099','M9636831678','2022-2','dummy','Disetujui'
    ]]
  }

  const detail_migrasi_per_mahasiswa_kosong = {
    "detail_migrasi_per_mahasiswa": []
  }
    

const axios = require('axios');
jest.mock('axios');
const csrf = {
      'data': {'csrftoken': 'test'}
}
axios.get.mockResolvedValue(csrf);
  
const cookie = 'csrftoken=test;';
global.document.cookie = cookie;
const administrasiMKPage = <Administrasi mkID={mkID} mhsIDVerified={mhsIDVerified} detailMK={detailMK} profileMahasiswa={profileMahasiswa} dataFormPersyaratan={dataFormPersyaratan} dosenPembimbing={dosenPembimbing} dataDetailMigrasiMahasiswa={detail_migrasi_per_mahasiswa_req} listFormPersyaratan={dataListFormPersyaratan}/>
const administrasiMKPage2 = <Administrasi mkID={mkID} mhsIDVerified={mhsIDVerified} detailMK={detailMK} profileMahasiswa={profileMahasiswa} dataFormPersyaratan={dataFormPersyaratan2} dosenPembimbing={dosenPembimbing2} dataDetailMigrasiMahasiswa={detail_migrasi_per_mahasiswa_req} listFormPersyaratan={dataListFormPersyaratan}/>
const administrasiMKPage3 = <Administrasi mkID={mkID} mhsIDVerified={mhsIDVerified} detailMK={detailMK} profileMahasiswa={profileMahasiswa} dataFormPersyaratan={dataFormPersyaratan2} dosenPembimbing={dosenPembimbing3} dataDetailMigrasiMahasiswa={detail_migrasi_per_mahasiswa_req} listFormPersyaratan={dataListFormPersyaratan}/>
const administrasiMKPage4 = <Administrasi mkID={mkID} mhsIDVerified={mhsIDVerified2} detailMK={detailMK} profileMahasiswa={profileMahasiswa} dataFormPersyaratan={dataFormPersyaratan2} dosenPembimbing={dosenPembimbing3} dataDetailMigrasiMahasiswa={detail_migrasi_per_mahasiswa_req} listFormPersyaratan={dataListFormPersyaratan}/>

const administrasiMKPageMigrasiKosong = <Administrasi mkID={mkID} mhsIDVerified={mhsIDVerified} detailMK={detailMK} profileMahasiswa={profileMahasiswa} dataFormPersyaratan={dataFormPersyaratan} dosenPembimbing={dosenPembimbing} dataDetailMigrasiMahasiswa={detail_migrasi_per_mahasiswa_kosong} listFormPersyaratan={dataListFormPersyaratan}/>
const administrasiMKPageMigrasiDisetujui = <Administrasi mkID={mkID} mhsIDVerified={mhsIDVerified} detailMK={detailMK} profileMahasiswa={profileMahasiswa} dataFormPersyaratan={dataFormPersyaratan} dosenPembimbing={dosenPembimbing} dataDetailMigrasiMahasiswa={detail_migrasi_per_mahasiswa_disetujui} listFormPersyaratan={dataListFormPersyaratan}/>

it("check on getServerSideProps with context", async () => {
    const context = {
        req: { 
            cookies: { "userID" : cookieUserMahasiswa }
        },
        query: { mataKuliahID: "CSGE802099"}
    };
    

    global.fetch = jest.fn(() =>
    Promise.resolve({
            json: () => Promise.resolve({})
        })
    );
    
    await getServerSideProps(context)
});

it("check on getData with data", async () => {
    const urlFormPersyaratan = 'https://simkuspa-backend.herokuapp.com/fileFormPersyaratan/'+ mkID + '/' + mhsIDVerified +'/'
    const listFormPersyaratan = {
        list_form_persyaratan: [
          [ 'Form Persetujuan Pembimbing MK Spesial', 'Tidak Ada' ],
          [ 'Form Pernyataan Integritas', 'Tidak Ada' ],
          [ 'Form Persyaratan Sidang ', 'Prasyarat' ]
        ]
      }    

    global.fetch = jest.fn(() =>
    Promise.resolve({
            json: () => Promise.resolve({})
        })
    );
    
    await getDataFormPersyaratan(listFormPersyaratan,urlFormPersyaratan)
    expect(fetch).toHaveBeenCalledTimes(3)
});

test("Prasyarat Mata Kuliah is in administrasi page", () => {
    render(administrasiMKPage)
    const prasyarat = screen.getByText("Prasyarat Mata Kuliah");
    expect(prasyarat).toBeInTheDocument();
})

test("Prasyarat Mata Kuliah is in administrasi page by login with sso", () => {
    render(administrasiMKPage4)
    const prasyarat = screen.getByText("Prasyarat Mata Kuliah");
    expect(prasyarat).toBeInTheDocument();
})

test("upload form persyaratan with empty dokumen", () => {
    const response = {
        'data': {
            'status' : 'SUCCESS',
        }
    }
    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });
    
    const initialState = true;
    React.useState = (initial) => [initialState, () => {}]
    
    render(administrasiMKPage)
    const buttonElement = screen.getAllByRole("button", { name: "Unggah Dokumen" })
    userEvent.click(buttonElement[0])
    const { container } =  render(administrasiMKPage)
    const buttonElement3 = container.querySelector('[id="yesUnggah"]')
    fireEvent.click(buttonElement3)
})


test("cancel upload form persyaratan successfully", () => {
    const response = {
        'data': {
            'status' : 'SUCCESS',
        }
    }
    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });
    
    const initialState = true;
    React.useState = (initial) => [initialState, () => {}]
    
    render(administrasiMKPage)
    const buttonElement = screen.getAllByRole("button", { name: "Unggah Dokumen" })
    userEvent.click(buttonElement[0])
    const { container } =  render(administrasiMKPage)
    const buttonElement2 = container.querySelector('[id="dokumen"]')
    const file = new File(['dummy'], 'values.json', {
        type: 'application/JSON'
    })
    File.prototype.text = jest.fn().mockResolvedValueOnce("dummy")
    userEvent.upload(buttonElement2, file)
    const buttonElement3 = container.querySelector('[id="cancelUnggah"]')
    userEvent.click(buttonElement3)
})

test("upload form persyaratan successfully test", () => {
    render(administrasiMKPage2)
    const response = {
        'data': {
            'status' : 'SUCCESS',
        }
    }
    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });

    const initialState = true;
    React.useState = (initial) => [initialState, () => {}]
    
    render(administrasiMKPage2)
    const buttonElement = screen.getAllByRole("button", { name: "Unggah Dokumen" })
    userEvent.click(buttonElement[0])
    const { container } =  render(administrasiMKPage2)
    const buttonElement2 = container.querySelector('[id="dokumen"]')
    const file = new File(['dummy'], 'values.json', {
        type: 'application/pdf'
    })
    File.prototype.text = jest.fn().mockResolvedValueOnce("dummy")
    userEvent.upload(buttonElement2, file)
    const buttonElement3 = container.querySelector('[id="yesUnggah"]')
    userEvent.click(buttonElement3)
    expect(nextRouter.useRouter).toHaveBeenCalled();
})

test("hapus form persyaratan successfully test", () => {
    render(administrasiMKPage)
    const useRouter = jest.spyOn(require("next/router"), "useRouter");
    useRouter.mockImplementation(() => ({
        route: '/',
        pathname:'/mataKuliahSpesial/CSGE802099/m/administrasi',
        push: jest.fn(),
        reload: jest.fn(),
        query: { mataKuliahID: "CSGE802099" }
    }));
    const { container } =  render(administrasiMKPage);
    const buttonDeleteTopik = container.querySelector('[id="hapusDokumen"]');
    userEvent.click(buttonDeleteTopik);
    const buttonYa = container.querySelector('[id="tombolYa"]')
    userEvent.click(buttonYa);
    expect(useRouter).toHaveBeenCalled();
})

test("cancel hapus form persyaratan successfully test", () => {
    render(administrasiMKPage)
    const useRouter = jest.spyOn(require("next/router"), "useRouter");
    useRouter.mockImplementation(() => ({
        route: '/',
        pathname:'/mataKuliahSpesial/CSGE802099/m/administrasi',
        push: jest.fn(),
        reload: jest.fn(),
        query: { mataKuliahID: "CSGE802099" }
    }));
    const { container } =  render(administrasiMKPage);
    const buttonDeleteTopik = container.querySelector('[id="hapusDokumen"]');
    userEvent.click(buttonDeleteTopik);
    const buttonTidak = container.querySelector('[id="tombolTidak"]')
    userEvent.click(buttonTidak);
    expect(useRouter).toHaveBeenCalled();
})

test("can display status belum terpenuhi when mahasiswa doesn't have dosen pembimbing", () => {
    render(administrasiMKPage3)
    const text = screen.getAllByText("Belum Terpenuhi")
    expect(text[0]).toBeInTheDocument()
})

test("Komponen Migrasi is in administrasi page when Migrasi Kosong", () => {
    render(administrasiMKPageMigrasiKosong)
    const migrasiText1 = screen.getByTestId('migrasiText1',"Migrasi ke Semester Selanjutnya");
    const migrasiText2 = screen.getByTestId('migrasiText2',"Migrasi data-data mata kuliah ini ke semester selanjutnya jika Anda belum menyelesaikannya di semester ini.");
    const migrasiButton =screen.getByTestId('migrasiButton1',"Migrasi");
    expect(migrasiText1).toBeInTheDocument();
    expect(migrasiText2).toBeInTheDocument();
    expect(migrasiButton).toBeInTheDocument();
})

test("Komponen Migrasi is in administrasi page when Migrasi Status Req", () => {
    render(administrasiMKPage)
    const migrasiText1 = screen.getByTestId('migrasiText1',"Migrasi ke Semester Selanjutnya");
    const migrasiText2 = screen.getByTestId('migrasiText2',"Migrasi data-data mata kuliah ini ke semester selanjutnya jika Anda belum menyelesaikannya di semester ini.");
    const migrasiText3 =screen.getByTestId('prosesText',"Proses pengajuan migrasi Mata Kuliah Tesis sedang berjalan");
    expect(migrasiText1).toBeInTheDocument();
    expect(migrasiText2).toBeInTheDocument();
    expect(migrasiText3).toBeInTheDocument();
})

test("Komponen Migrasi is in administrasi page when Migrasi Status Disetujui", () => {
    render(administrasiMKPage)
    const migrasiText1 = screen.getByTestId('migrasiText1',"Migrasi ke Semester Selanjutnya");
    const migrasiText2 = screen.getByTestId('migrasiText2',"Migrasi data-data mata kuliah ini ke semester selanjutnya jika Anda belum menyelesaikannya di semester ini.");
    const migrasiText3 =screen.getByTestId('prosesText',"Permohonan migrasi Mata Kuliah Tesis disetujui");
    expect(migrasiText1).toBeInTheDocument();
    expect(migrasiText2).toBeInTheDocument();
    expect(migrasiText3).toBeInTheDocument();
})



test("PopUp when Click Migrasi Button", () => {
    render(administrasiMKPageMigrasiKosong)
    const migrasiButton =screen.getByTestId('migrasiButton1');
    userEvent.click(migrasiButton);
    const migrasiPopUpText = screen.getByText("Apakah Anda yakin untuk melakukan migrasi mata kuliah Tesis ?")
    expect(migrasiPopUpText).toBeInTheDocument();
})

test("Ketika menekan tidak di PopUp", () => {
    render(administrasiMKPageMigrasiKosong)
    const { container } =  render(administrasiMKPageMigrasiKosong);
    const migrasiButton =getByTestId(container,'migrasiButton1');
    userEvent.click(migrasiButton);
    const buttonTidak = container.querySelector('[id="tombolTidak"]')
    userEvent.click(buttonTidak);
})

test("Sukses Membuat Request Migrasi",() => {
    render(administrasiMKPageMigrasiKosong)
    const response = {
        'detail_migrasi_per_mahasiswa': {
            'id' : '100',
            'id_mk':'dummy',
            'id_mahasiswa' : 'dummy',
            'periode_lama' :'dummy',
            'periode_baru' : 'dummy',
            'status' : 'REQ'
        }
    }
    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });

    const initialState = true;
    React.useState = (initial) => [initialState, () => {}]
    
    render(administrasiMKPageMigrasiKosong)
    const { container } =  render(administrasiMKPageMigrasiKosong);
    const migrasiButton =getByTestId(container,'migrasiButton1');
    userEvent.click(migrasiButton);
    const buttonYa = container.querySelector('[id="tombolYa"]')
    userEvent.click(buttonYa);
    expect(nextRouter.useRouter).toHaveBeenCalled();
})