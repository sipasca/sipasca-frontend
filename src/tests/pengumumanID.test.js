import PengumumanID from "../pages/pengumuman/edit/[pengumumanID]";
import {expect, test} from '@jest/globals'
import { render, screen, fireEvent } from "@testing-library/react";
import '@testing-library/jest-dom'
import { getServerSideProps } from "../pages/pengumuman/edit/[pengumumanID]";

import React from "react";

const pengumuman_detail = {"pengumuman_detail": [[10, "C", "DCCC", "2022-03-18"]]}
const pengumuman_detail_lain = <PengumumanID data = {pengumuman_detail} />
const pengumuman_detail_null = {"pengumuman_detail": [[10, "C", "", "2022-03-18"]]}
const pengumuman_detail_null_lain = <PengumumanID data = {pengumuman_detail_null} />

test("renders buat title in form buat pengumuman", () => {
    render(pengumuman_detail_lain)
    const editTitle = screen.getByText("| Edit Pengumuman");
    expect(editTitle).toBeInTheDocument(); 
})

test("renders buat judul in form buat pengumuman", () => {
    render(pengumuman_detail_lain)
    const editJudul = screen.getByText("Judul");
    expect(editJudul).toBeInTheDocument();
})

test("renders buat konten in form buat pengumuman", () => {
    render(pengumuman_detail_lain)
    const editKonten = screen.getByText("Konten");
    expect(editKonten).toBeInTheDocument();
})



  // Mocking setting cookie for CSRF Token
const cookie = 'csrftoken=test;';
global.document.cookie = cookie;

test("edit successfully", async () => {
  // Mocking PostRequest
  const response = {
      'data': {
          'status' : 'SUCCESS',
          'judul': 'testJudul',
          'konten': 'testKonten'
      }
  }

  const postRequest = require('../pages/api/post-request');
  postRequest.default = jest.fn(() => {
      return response
  });

   // Mocking useRouter
   const useRouter = jest.spyOn(require("next/router"), "useRouter");

   useRouter.mockImplementation(() => ({
     route: '/pusatInformasi',
     push: jest.fn(),
   }));

    // Test
    render(pengumuman_detail_lain)

    const formHandler = screen.getByText("Simpan");
    fireEvent.click(formHandler);
    expect(useRouter).toHaveBeenCalled();
  })

  test("edit pengumuman unsuccessfully", async () => {
    // Mocking PostRequest
    const response = {
        'data': {
            'status' : 'SUCCESS',
            'judul': 'testJudul',
            'konten': 'testKonten'
        }
    }

    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });

    jest.spyOn(window, 'alert').mockImplementation(() => {});

    // Test
    render(pengumuman_detail_null_lain)
    const formHandler = screen.getByText("Simpan");
    fireEvent.click(formHandler);
    expect(window.alert).toHaveBeenCalled();
})

  it("check on getServerSideProps with context", async () => {
    const context = {
        query: { pengumumanID: "25"}
    
    };
    global.fetch = jest.fn(() =>
    Promise.resolve({
            json: () => Promise.resolve({})
        })
    );
    
    await getServerSideProps(context);
  });