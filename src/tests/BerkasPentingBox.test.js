import React from "react";
import { expect, test } from '@jest/globals'
import { render, screen} from "@testing-library/react";
import '@testing-library/jest-dom'
import userEvent from "@testing-library/user-event";
import BerkasPentingBox from "../components/BerkasPentingBox";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import storage from "../components/storage";
library.add(fas);
import { ref, getDownloadURL } from "firebase/storage";

test("nama appear in berkas penting box", () => {
    render(<BerkasPentingBox nama='berkas test' url='berkastest.com' />);
    const nama = screen.getByText("berkas test");
    expect(nama).toBeInTheDocument();
})

test("nama appear in berkas penting box mode edit", () => {
    render(<BerkasPentingBox mode='edit' nama='berkas test 2' url='berkastest2.com' />);
    const nama = screen.getByText("berkas test 2");
    expect(nama).toBeInTheDocument();
})

test("function handleDeleteId run when click x button", () => {
    const handleDeleteId = jest.fn();
    const { container } = render(<BerkasPentingBox mode='edit' handleId={handleDeleteId} />)
    const buttonElement2 = container.querySelector('[id="tombolX"]')
    userEvent.click(buttonElement2)
})
