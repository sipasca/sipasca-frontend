import Daftar from "../pages/topik/daftarTopik";
import { expect, test } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import React from "react";
import { getServerSideProps } from "../pages/topik/daftarTopik";
import Heading from "../components/Heading";
import * as nextRouter from "next/router";
import { GetCookie } from "./Helper";

const daftarTopikDummy1 = {
  list_topik_by_dosen: [[1, "topik1", "dosen1", "deksrips1"]],
};
const daftarTopikDummy2 = {
  list_topik_by_dosen: [
    [
      2,
      "topik2",
      "dosen2",
      "Scelerisque in dictum non consectetur a erat nam at. Amet mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan",
      -1,
      3,
    ],
  ],
};
const topikPage1 = <Daftar dataDaftarTopik={daftarTopikDummy1} role="dosen" />;
const topikPage2 = <Daftar dataDaftarTopik={daftarTopikDummy2} role="dosen" />;

// Mock cookie in getServerSideProps
const { cookieUserID: cookieUserDosen , cookieRole: cookieRoleDosen } = GetCookie('dosen123', 'dosen')

it("check on getServerSideProps with context", async () => {
  const context = {
    req: {
      cookies: { userID: cookieUserDosen },
    },
  };

  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve({}),
    })
  );

  await getServerSideProps(context);
});

test("Page contains Component Heading with text '| Daftar Topik'", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Heading text="| Daftar Topik" type="kecil"></Heading>);
  const text = screen.getByText("| Daftar Topik");
  expect(text).toBeInTheDocument();
});

test("Daftar Topik Pages contains title", () => {
  jest.spyOn(window.localStorage.__proto__, "getItem");
  window.localStorage.__proto__.getItem = jest.fn(() => {
    return "dosen";
  });

  render(topikPage1);
  const title = screen.getByText("| Daftar Topik");
  expect(title).toBeInTheDocument();
});

test("description text length more than 120", () => {
  jest.spyOn(window.localStorage.__proto__, "getItem");
  window.localStorage.__proto__.getItem = jest.fn(() => {
    return "dosen";
  });

  render(topikPage2);
  const title = screen.getByText("| Daftar Topik");
  expect(title).toBeInTheDocument();
});

test("Page contains Alert", () => {
  // works:
  jest.spyOn(window.localStorage.__proto__, "getItem");
  window.localStorage.__proto__.getItem = jest.fn(() => {
    return "Success";
  });

  render(topikPage1);
  const button1 = screen.getByText("Topik berhasil dihapus");
  expect(button1).toBeInTheDocument();
});