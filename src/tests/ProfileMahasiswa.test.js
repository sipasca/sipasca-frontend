import { expect, test } from '@jest/globals'
import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom'
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import ProfileMahasiswa from '../pages/profile/profileMahasiswa';
import { getServerSideProps } from '../pages/profile/profileMahasiswa';
import { GetCookie } from "./Helper";

library.add(fas);

const profile_mahasiswa = {"profile_mahasiswa": [["M9636831678", "Choi Chaehun", "caturyudishtira@gmail.com", "Magister Ilmu Komputer", "1906293045", null, null, null]]}
const profile_mahasiswa_cv_not_null = {"profile_mahasiswa" : [["M9636831678", "Choi Chaehun", "caturyudishtira@gmail.com", "Magister Ilmu Komputer", "1906293045", 'dummy.firebase.com', null, null]]}
const profile_page = <ProfileMahasiswa data = {profile_mahasiswa} />
const profile_page_not_null = <ProfileMahasiswa data ={profile_mahasiswa_cv_not_null} />

// Mock cookie in getServerSideProps
const { cookieUserID: cookieUserMahasiswa } = GetCookie('mahasiswa123', 'mahasiswa')

test("renders title in profile mahasiswa Page", () => {
    render(profile_page)
    const title = screen.getByText("| Profil");
    expect(title).toBeInTheDocument();
  })

test("renders title in profile mahasiswa Page with cv not null", () => {
    render(profile_page_not_null)
    const other_title = screen.getByText("| Profil");
    expect(other_title).toBeInTheDocument();
  })

it("check on getServerSideProps with context", async () => {
    const context = {
        req: { 
            cookies: { "userID" : cookieUserMahasiswa }
        },
        query: { id: "M9636831678"}
    };
    global.fetch = jest.fn(() =>
    Promise.resolve({
            json: () => Promise.resolve({})
        })
    );
    
    await getServerSideProps(context);
});

it("check on getServerSideProps with context id undefined", async () => {
  const context = {
      req: { 
          cookies: { "userID" : cookieUserMahasiswa }
      },
      query: { id: undefined}
  };
  global.fetch = jest.fn(() =>
  Promise.resolve({
          json: () => Promise.resolve({})
      })
  );
  
  await getServerSideProps(context);
});

test("renders Nama in profile mahasiswa Page", () => {
    render(profile_page)
    const nama = screen.getByText("Nama");
    expect(nama).toBeInTheDocument();
  })
