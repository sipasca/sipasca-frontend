import Buat from "../pages/pengumuman/buat";
import {expect, test} from '@jest/globals'
import { render, screen, fireEvent } from "@testing-library/react";
import '@testing-library/jest-dom'
import React from "react";
import * as nextRouter from "next/router";
import PopUp from "../components/PopUp";
import userEvent from "@testing-library/user-event"

// Mocking axios for CSRF Token
const axios = require('axios');
jest.mock('axios');
const csrf = {
    'data': {'csrftoken': 'test'}
}
axios.get.mockResolvedValue(csrf);

// Mocking setting cookie for CSRF Token
const cookie = 'csrftoken=test;';
global.document.cookie = cookie;

test("Form Buat Pengumuman Contains '| Buat Pengumuman'", () => {
    render(<Buat />)
    const buatTitle = screen.getByText("| Buat Pengumuman");
    expect(buatTitle).toBeInTheDocument(); 
})

test("Form Buat Pengumuman Contains 'Judul'", () => {
    render(<Buat />)
    const buatJudul = screen.getByText("Judul");
    expect(buatJudul).toBeInTheDocument();
})

test("Form Buat Pengumuman Contains 'Konten'", () => {
    render(<Buat />)
    const buatkonten = screen.getByText("Konten");
    expect(buatkonten).toBeInTheDocument();
})

test("Form Buat Pengumuman Contains Button", () => {
    render(<Buat />)
    const button = screen.getByRole("button");
    expect(button).toBeInTheDocument()
})

test("Form Buat Pengumuman Contains Placeholder Judul", () => {
    render(<Buat />)
    const placeholderJudul = screen.getByPlaceholderText("Required judul");
    expect(placeholderJudul).toBeInTheDocument()
})

test("Form Buat Pengumuman Contains Placeholder Konten", () => {
    render(<Buat />)
    const placeholderKonten = screen.getByPlaceholderText("Required konten");
    expect(placeholderKonten).toBeInTheDocument()
})

test("Form Buat Pengumuman Contains Heading", () => {
    render(<Buat />)
    const heading = screen.getByRole("heading");
    expect(heading).toBeInTheDocument()
})

test("Form Buat Pengumuman Contains Required TextBox", () => {
    const { container } = render(<Buat />);
    const required = container.getAttributeNames("required")
    expect(required).not.toBeNull()
})

test("Form Buat Pengumuman Contains Paragraph", () => {
    const { container } = render(<Buat />);
    const paragraf = container.getElementsByTagName("p")
    expect(paragraf).not.toBeNull()
})

test("Form Buat Pengumuman Contains TextArea", () => {
    const { container } = render(<Buat />);
    const textarea = container.getElementsByTagName("textarea")
    expect(textarea).not.toBeNull()
})

test("buat pengumuman successfully", async () => {
    // Mocking PostRequest
    const response = {
        'data': {
            'status' : 'SUCCESS',
            'judul': 'testJudul',
            'konten': 'testKonten'
        }
    }

    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });

    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
      route: '/pengumumanEdit',
      push: jest.fn(),
    }));

    // Test
    render(<Buat />)
    userEvent.type(screen.getByPlaceholderText("Required judul"), "judul")
    userEvent.type(screen.getByPlaceholderText("Required konten"), "konten")
    const loginButton = screen.getByRole("button");
    userEvent.click(loginButton);
    expect(useRouter).toHaveBeenCalled();
})

test("buat pengumuman unsuccessfully", async () => {
    // Mocking PostRequest
    const response = {
        'data': {
            'status' : 'SUCCESS',
            'judul': 'testJudul',
            'konten': 'testKonten'
        }
    }

    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });

    jest.spyOn(window, 'alert').mockImplementation(() => {});

    // Test
    render(<Buat />)
    userEvent.type(screen.getByPlaceholderText("Required judul"), " ")
    userEvent.type(screen.getByPlaceholderText("Required konten"), "konten")
    const loginButton = screen.getByRole("button");
    userEvent.click(loginButton);
    expect(window.alert).toHaveBeenCalled();
})

test("Form Buat Pengumuman Contains PopUp Batal Buat Pengumuman with Text and URL", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));
  
    render(<PopUp text="Apakah Anda yakin untuk batal?" url="pengumumanEdit"></PopUp>);
    const text = screen.getByText("Apakah Anda yakin untuk batal?");
    const url = document.querySelector("a").getAttribute("href");
    const button = document.querySelector("a").getElementsByTagName("button");
    expect(button).not.toBeNull()
    expect(url).toBe("pengumumanEdit");
    expect(text).toBeInTheDocument();
})

