import React from "react";
import { expect, test } from '@jest/globals'
import '@testing-library/jest-dom'
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
library.add(fas);
import deleteDokumenFromStorage from "../lib/DeleteDokumenFromStorage";

describe("Delete Dokumen where url empty", () => {  
    it("should return alert", async () => {
        global.alert = jest.fn();

      await deleteDokumenFromStorage("");
      expect(global.alert).toHaveBeenCalledTimes(1);
    });
  });