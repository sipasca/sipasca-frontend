import MendaftarMK from "../pages/administrasi/mendaftarMK";
import {describe, expect, test} from '@jest/globals'
import { render, screen, fireEvent } from "@testing-library/react";
import '@testing-library/jest-dom'
import '@testing-library/jest-dom'
import React from "react";
import * as nextRouter from "next/router";
import { GetCookie } from "./Helper";

import { getServerSideProps } from "../pages/administrasi/mendaftarMK";

const daftarMataKuliah = {"mk_tersedia": [["CSGE801092", "2022-2", "Studi Mandiri", 30, 2, 2, "03.00.12.01-2020", false]]}
const kumpulanMK = <MendaftarMK data = {daftarMataKuliah} />

// Mocking axios for CSRF Token
// Mocking getting userID and role with axios
const axios = require('axios');
jest.mock('axios');
const csrf = {
    data: {'csrftoken': 'test'},
    userID: 'test',
    role: 'mahasiswa'
}
axios.get.mockResolvedValue(csrf);

nextRouter.useRouter = jest.fn();
nextRouter.useRouter.mockImplementation(() => ({ 
    prefetch: async () => {},
}));

// Mocking setting cookie for CSRF Token
const cookie = 'csrftoken=test;';
global.document.cookie = cookie;


// Mock cookie in getServerSideProps
const { cookieUserID: cookieUserMahasiswa , cookieRole: cookieRoleMahasiswa } = GetCookie('mahasiswa123', 'mahasiswa')

describe("getServerSideProps", () => {
  const context = {
    req: { 
        cookies: { "userID" : cookieUserMahasiswa }
    },
};
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ status:'success' })
      })
    );
  
    beforeEach(() => {
      fetch.mockClear();
    });
  
    it("should call api", async () => {
      
      await getServerSideProps(context);
      expect(fetch).toHaveBeenCalledTimes(1)
    });
  });

test("renders title in mendaftarMK Page", () => {
    render(kumpulanMK)
    const title = screen.getByText("| Daftar Mata Kuliah Spesial");
    expect(title).toBeInTheDocument();
  })

  
test("tambah mata kuliah successfully", async () => { 
    // Mocking PostRequest
    const response = {
        'data': {
            'status' : 'SUCCESS',
            'id_Mk': 'testIdMK',
            'id_mahasiswa': 'testIdMahasiswa'
        }
    }

    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });

    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
      route: '/administrasi',
      push: jest.fn(),
      prefetch: jest.fn()
    }));

    // Test
    render(kumpulanMK)

    const tambahMK = screen.getAllByRole("button");
    fireEvent.click(tambahMK[1]);
    expect(useRouter).toHaveBeenCalled();
}) 

test("test set IdMK in mendaftarMK Page successfully", () => {
    const { container } = render(kumpulanMK);
    const input = container.querySelector('[id="flexCheckDefault"]');
    fireEvent.click(input);
    
})

test("test set IdMK in mendaftarMK Page successfully", () => {
  const { container } = render(kumpulanMK);
  const input = container.querySelector('[class="matkulName"]');
  fireEvent.click(input);
  
})