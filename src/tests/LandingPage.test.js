import React from "react";
import LandingPage from "../pages/index";
import {describe, expect, test} from '@jest/globals'
import { render, screen } from "@testing-library/react";

import '@testing-library/jest-dom'
import { getServerSideProps } from "../pages/index"
import { TermProvider } from "../context/termContext"
import axios from 'axios';
import userEvent from "@testing-library/user-event";
jest.mock('axios');

const landingPage= <LandingPage data={{"pengumuman_list": [[6, "judul", "konten", "2022-03-13"]]}} />

// Mocking getting userID and role with axios
const res = {
  data : {
    userID: 'pengguna123',
    role: 'pengguna'
  }
}
axios.get.mockResolvedValue(res);

describe("getServerSideProps", () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ status:'success' })
      })
    );
  
    beforeEach(() => {
      fetch.mockClear();
    });
  
    it("should call api", async () => {
      await getServerSideProps();
      expect(fetch).toHaveBeenCalledTimes(1)
    });
});

test("useContext works", () => {  
  React.useState = (initial) => [null, () => {}]
  render(<TermProvider />)
})


test("renders title in Landing Page", () => {
    render(landingPage)
    const title = screen.getByText("SIPASCA");
    expect(title).toBeInTheDocument();
})
test("renders subtitle in Landing Page", () => {
    render(landingPage)
    const subtitle = screen.getByText("Sistem Informasi Mata Kuliah Spesial Pasca");
    expect(subtitle).toBeInTheDocument();
})


test("renders login button in Landing Page", () => {
  render(landingPage)
  const loginButton = screen.getByRole("button");
  expect(loginButton).toBeInTheDocument();
})

test("renders login in Landing Page", () => {
  render(landingPage)
  const loginText = screen.getByText("Login");
  expect(loginText).toBeInTheDocument();
})


test("renders pengumuman title in Landing Page", ()=>{
    render(landingPage)
    const pengumumanTitle = screen.getByText("| Pengumuman Terkini")
    expect(pengumumanTitle).toBeInTheDocument()
})

test("click alur umum in Landing Page", ()=>{
  const { container } = render(landingPage)
  const buttonUmum = container.querySelector("[id*='alurUmum']")
  userEvent.click(buttonUmum)
  expect(buttonUmum).toBeInTheDocument()
})

test("click alur mahasiswa in Landing Page", ()=>{
  const { container } = render(landingPage)
  const buttonMahasiwa = container.querySelector("[id*='alurMahasiswa']")
  userEvent.click(buttonMahasiwa)
  expect(buttonMahasiwa).toBeInTheDocument()
})

test("click alur dosen in Landing Page", ()=>{
  const { container } = render(landingPage)
  const buttonDosen = container.querySelector("[id*='alurDosen']")
  userEvent.click(buttonDosen)
  expect(buttonDosen).toBeInTheDocument()
})

test("renders alur umum in Landing Page", ()=>{
  // Mocking useState
  const state = 'umum';
  React.useState = (initial) => [state, () => {}]

  render(landingPage)
  const alurTitle = screen.getByText("| Alur Umum")
  expect(alurTitle).toBeInTheDocument()
})

test("renders alur mahasiswa in Landing Page", ()=>{
  // Mocking useState
  const state = 'mahasiswa';
  React.useState = (initial) => [state, () => {}]

  render(landingPage)
  const alurTitle = screen.getByText("| Alur Mahasiswa")
  expect(alurTitle).toBeInTheDocument()
})

test("renders alur dosen in Landing Page", ()=>{
  // Mocking useState
  const state = 'dosen';
  React.useState = (initial) => [state, () => {}]

  render(landingPage)
  const alurTitle = screen.getByText("| Alur Dosen")
  expect(alurTitle).toBeInTheDocument()
})
