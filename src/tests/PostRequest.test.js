import CSRFToken from "../lib/CSRFToken";
import { render } from "@testing-library/react";
import postRequest from "../pages/api/post-request";
import {test} from '@jest/globals';
import axios from 'axios';

jest.mock('axios');

const cookie = 'csrftoken=test;';
global.document.cookie = cookie;

const req = {
    'username': 'user1',
    'password': 'pass1'
}
const res = {
    'status': 'success',
}

test("CSRFToken renders correctly", async () => {
    const res = {
        'data': {'csrftoken': 'test'}
    }
    axios.get.mockResolvedValue(res);

    render(<CSRFToken type="LOCAL"/>)
});

test("CSRFToken renders correctly", async () => {
    const res = {
        'data': {'csrftoken': 'test'}
    }
    axios.get.mockResolvedValue(res);

    render(<CSRFToken type="PRODUCTION"/>)
});

test("POST request local function successfully", async () => {
    axios.create.mockReturnThis();
    axios.post.mockResolvedValue(res);

    postRequest('/', req, 'LOCAL');
})

test("POST request production function successfully", () => {
    axios.create.mockReturnThis();
    axios.post.mockResolvedValue(res);

    postRequest('/', req);
})