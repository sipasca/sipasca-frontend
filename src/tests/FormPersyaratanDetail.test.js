import DetailAdministrasi from "../pages/mataKuliahSpesial/[mataKuliahID]/s/administrasi/detailAdministrasi/[detailAdministrasiID]"
import {expect, test} from '@jest/globals';
import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom';
import * as nextRouter from "next/router";
import { getServerSideProps } from "../pages/mataKuliahSpesial/[mataKuliahID]/s/administrasi/detailAdministrasi/[detailAdministrasiID]";

const mataKuliahID = "dummy"
const detailAdministrasiID = "dummyForm"
const detailMK_data = {'detail_mk':["CSGE802097", "2022-2", "Publikasi Ilmiah", 30, 2, 2, "03.00.12.01-2020", false]}
const data = {"detail_form_persyaratan":[["Gilang","1906293045","production/berkasPenting"],["Gilang","1906293045","production/berkasPenting"]]}
const detailAdministrasiPage = <DetailAdministrasi data={data} detailMK_data={detailMK_data} mataKuliahID={mataKuliahID} detailAdministrasiID={detailAdministrasiID} />

import axios from 'axios';
jest.mock('axios');

// Mocking getting userID and role with axios
const res = {
  data : {
    userID: 'staf123',
    role: 'staf'
  }
}
axios.get.mockResolvedValue(res);

jest.spyOn(window.localStorage.__proto__, 'getItem');
window.localStorage.__proto__.getItem = jest.fn(() => {
    return 's'
});

describe("getServerSideProps", () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ status:'success' })
      })
    );
  
    beforeEach(() => {
      fetch.mockClear();
    });
  
    it("should call api", async () => {
        const context = {
            query: { mataKuliahID: mataKuliahID, detailAdministrasiID:detailAdministrasiID} ,
        };
      await getServerSideProps(context);
      expect(fetch).toHaveBeenCalledTimes(2)
    });
  });

test("renders text title in Detail Administrasi", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: "CSGE" } }));
    render(detailAdministrasiPage);
    const title = screen.getByText("Administrasi");
    expect(title).toBeInTheDocument();
})


test("renders label of Button Kembali in Detail Administrasi", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: "CSGE" } }));
    render(detailAdministrasiPage)
    const buttonKembali = screen.getByText("Kembali");
    expect(buttonKembali).toBeInTheDocument();
})


test("renders text unduh in Detail Administrasi", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: "CSGE" } }));
    render(detailAdministrasiPage)
    const unduh = screen.getAllByText("Unduh Semua");
    expect(unduh.length).toBeGreaterThan(0);
})

