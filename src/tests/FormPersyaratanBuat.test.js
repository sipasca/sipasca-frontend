import BuatAdministrasi from "../pages/mataKuliahSpesial/[mataKuliahID]/s/administrasi/BuatForm"
import {describe, expect, test} from '@jest/globals'
import { render, screen, fireEvent} from "@testing-library/react";
import '@testing-library/jest-dom'
import * as nextRouter from "next/router";
import React from "react";
import { getServerSideProps } from "../pages/mataKuliahSpesial/[mataKuliahID]/s/administrasi/BuatForm";


const mataKuliahID = "dummy"
const detailMK_data = {'detail_mk':["CSGE802097", "2022-2", "Publikasi Ilmiah", 30, 2, 2, "03.00.12.01-2020", false]}
const buatAdministrasi = <BuatAdministrasi mataKuliahID={mataKuliahID} detailMK_data={detailMK_data} />

// Mocking axios for CSRF Token
const axios = require('axios');
jest.mock('axios');
const csrf = {
    'data': {'csrftoken': 'test'}
}
axios.get.mockResolvedValue(csrf);

// Mocking setting cookie for CSRF Token
const cookie = 'csrftoken=test;';
global.document.cookie = cookie;

jest.spyOn(window.localStorage.__proto__, 'getItem');
window.localStorage.__proto__.getItem = jest.fn(() => {
    return 's'
});

describe("getServerSideProps", () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ status:'success' })
      })
    );
  
    beforeEach(() => {
      fetch.mockClear();
    });
  
    it("should call api", async () => {
        const context = {
            query: { mataKuliahID: mataKuliahID}
        };
      await getServerSideProps(context);
      expect(fetch).toHaveBeenCalledTimes(1)
    });
  });
  

test("renders text title in Buat Administrasi", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: "CSGE" } }));
    render(buatAdministrasi)
    const title = screen.getByText("Administrasi");
    expect(title).toBeInTheDocument();
})

test("renders form label in Buat Administrasi", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: "CSGE" } }));
    render(buatAdministrasi)
    const formLabelNama = screen.getByText("Nama Form");
    const formLabelJenis = screen.getByText("Jenis Form");
    
    expect(formLabelNama).toBeInTheDocument();
    expect(formLabelJenis).toBeInTheDocument();
})


test("renders label of Button Simpan in Buat Administrasi", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: "CSGE" } }));
    render(buatAdministrasi)
    const buttonSimpan = screen.getByText("Simpan");
    expect(buttonSimpan).toBeInTheDocument();
})

test("renders label of Button Batal in Buat Administrasi", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: "CSGE" } }));
    render(buatAdministrasi)
    const buttonBatal = screen.getByText("Batal");
    expect(buttonBatal).toBeInTheDocument();
})


test("renders batal button in Buat Administrasi",  () => {
    const {container} = render(buatAdministrasi)
    const buttonElement = container.querySelector('[id="batalButton"]')
    fireEvent.click(buttonElement);
})


test("renders function buatHandler in Buat Administrasi", () => {
    // Mocking PostRequest
    const response = {
        'data': {
            'Success' : 'Success',
            'jenis_form': 'test form',
        }
    }

    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
         return response
     });

    // // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");
    const mataKuliahID = "CS"
    useRouter.mockImplementation(() => ({
       pathname:'/administrasi/', query : mataKuliahID,
       push: jest.fn(),
     }));

    // Test
    
    const {container} = render(buatAdministrasi)
    const buttonElement = container.querySelector('[id="simpanButton"]')
    fireEvent.click(buttonElement);
    expect(useRouter).toHaveBeenCalled();
})







