import Cari from "../pages/topik/cariTopik";
import { expect, test } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import React from "react";
import { getServerSideProps } from "../pages/topik/cariTopik";
import Heading from "../components/Heading";
import * as nextRouter from "next/router";

const cariTopikDummy1 = {
  daftar_topik_list: [[1, "topik1", "dosen1", "deksripsi1"]],
};
const cariTopikDummy2 = {
  daftar_topik_list: [
    [
      2,
      "topik2",
      "dosen2",
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cras fermentum odio eu feugiat pretium nibh ipsum consequat nisl.",
      -1,
      3,
    ],
  ],
};
const topikPage1 = <Cari dataCariTopik={cariTopikDummy1} role="mahasiswa" />;
const topikPage2 = <Cari dataCariTopik={cariTopikDummy2} role="mahasiswa" />;

describe("getServerSideProps", () => {
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve({ status: "success" }),
    })
  );

  beforeEach(() => {
    fetch.mockClear();
  });

  it("should call api", async () => {
    await getServerSideProps();
    expect(fetch).toHaveBeenCalledTimes(1);
  });
});

test("Page contains Component Heading with text '| Daftar Topik'", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Heading text="| Daftar Topik" type="kecil"></Heading>);
  const text = screen.getByText("| Daftar Topik");
  expect(text).toBeInTheDocument();
});

test("Cari Topik Pages contains title", () => {
  jest.spyOn(window.localStorage.__proto__, "getItem");
  window.localStorage.__proto__.getItem = jest.fn(() => {
    return "mahasiswa";
  });
  render(topikPage1);
  const title = screen.getByText("| Daftar Topik");
  expect(title).toBeInTheDocument();
});

test("description text length more than 120", () => {
  jest.spyOn(window.localStorage.__proto__, "getItem");
  window.localStorage.__proto__.getItem = jest.fn(() => {
    return "mahasiswa";
  });
  render(topikPage2);
  const title = screen.getByText("| Daftar Topik");
  expect(title).toBeInTheDocument();
});
