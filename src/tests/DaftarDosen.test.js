
import DaftarDosen from '../pages/daftarDosen/index';
import {expect, test} from '@jest/globals'
import {render, screen, fireEvent} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import '@testing-library/jest-dom'

import { GetCookie } from "./Helper";
import * as nextRouter from "next/router";
import React from "react";
import { getServerSideProps } from "../pages/daftarDosen/index";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
library.add(fas);

// Mocking axios for CSRF Token
const axios = require('axios');
jest.mock('axios');

// Mocking setting cookie for CSRF Token
const cookie = 'csrftoken=test;';
global.document.cookie = cookie;

// Mock cookie in getServerSideProps
const { cookieUserID: cookieUserMahasiswa } = GetCookie('mahasiswa123', 'mahasiswa')
const { cookieUserID: cookieUserDosen } = GetCookie('dosen123', 'dosen')
const { cookieUserID: cookieUserStaf } = GetCookie('staf123', 'staf')

const dummy= {
    "list_all_dosen": [["D111", "Dosen A", "dosenA@email.com", "url","A,B"]]
}


it("check on getServerSideProps list all dosen with context role staf", async () => {
    const context = {
        req: { 
            cookies: { "userID" : cookieUserStaf }
        }
    };
    global.fetch = jest.fn(() =>
    Promise.resolve({
            json: () => Promise.resolve({})
        })
    );
    
    await getServerSideProps(context);
});

  
  it("check on getServerSideProps list all dosen with context role dosen", async () => {
    const context = {
        req: { 
            cookies: { "userID" : cookieUserDosen }
        }
    };
    global.fetch = jest.fn(() =>
    Promise.resolve({
            json: () => Promise.resolve({})
        })
    );
    
    await getServerSideProps(context);
  });

  it("check on getServerSideProps list all dosen with context role mahasiswa", async () => {
    const context = {
        req: { 
            cookies: { "userID" : cookieUserMahasiswa }
        }
    };
    global.fetch = jest.fn(() =>
    Promise.resolve({
            json: () => Promise.resolve({})
        })
    );
    
    await getServerSideProps(context);
  });
  
test("Daftar Dosen is in Halaman Daftar Dosen as Role Staff", () => {
    // Mocking getting userID and role with axios
    const res = {
        data : {
        userID: 'staf',
        role: 'staf'
        }
    }
    axios.get.mockResolvedValue(res);
    
    // Mocking useState for setting role
    function changeState(initial) {
        if (initial === 'pengguna') {
            return 'staf'
        }
        else {
            return [ {
                'Nama Dosen' : 'Dosen A', 
                'ID': 'D111', 
                'Kontak': 'dosenA@email.com',
                'Bidang Kepakaran' : 'A,B'
                
            } ]
        }
    }
    React.useState = (initial) => [changeState(initial), () => {}]

    render(<DaftarDosen dataListAllDosen={dummy}/>)
    const profil= screen.getByText("| Daftar Dosen");
    expect(profil).toBeInTheDocument();
    })

test("Daftar Dosen is in Halaman Daftar Dosen as Role Dosen", () => {
    // Mocking getting userID and role with axios
    const res = {
        data : {
        userID: 'dosen',
        role: 'dosen'
        }
    }
    axios.get.mockResolvedValue(res);
    
    // Mocking useState for setting role
    function changeState(initial) {
        if (initial === 'pengguna') {
            return 'dosen'
        }
        else {
            return [ {
                'Nama Dosen' : 'Dosen A', 
                'ID': 'D111', 
                'Kontak': 'dosenA@email.com',
                'Bidang Kepakaran' : 'A,B'
                
            } ]
        }
    }
    React.useState = (initial) => [changeState(initial), () => {}]

    render(<DaftarDosen dataListAllDosen={dummy}/>)
    const profil= screen.getByText("| Daftar Dosen");
    expect(profil).toBeInTheDocument();
})

test("Daftar Dosen is in Halaman Daftar Dosen as Role Mahasiswa", () => {
    // Mocking getting userID and role with axios
    const res = {
        data : {
        userID: 'mahasiswa',
        role: 'mahasiswa'
        }
    }
    axios.get.mockResolvedValue(res);
    
    // Mocking useState for setting role
    function changeState(initial) {
        if (initial === 'pengguna') {
            return 'mahasiswa'
        }
        else {
            return [ {
                'Nama Dosen' : 'Dosen A', 
                'ID': 'D111', 
                'Kontak': 'dosenA@email.com',
                'Bidang Kepakaran' : 'A,B',
                'Action' : <button data-test-id="request-button">Request</button>
                
            } ]
        }
    }
    React.useState = (initial) => [changeState(initial), () => {}]

    render(<DaftarDosen dataListAllDosen={dummy}/>)
    const profil= screen.getByText("| Daftar Dosen");
    expect(profil).toBeInTheDocument();
})

test("Komponen Staff is in Halaman Daftar Dosen as Role Staf", () => {
    // Mocking getting userID and role with axios
    const res = {
        data : {
        userID: 'staf',
        role: 'staf'
        }
    }
    axios.get.mockResolvedValue(res);
    
    // Mocking useState for setting role
    function changeState(initial) {
        if (initial === 'pengguna') {
            return 'staf'
        }
        else {
            return [ {
                'Nama Dosen' : 'Dosen A', 
                'ID': 'D111', 
                'Kontak': 'dosenA@email.com',
                'Bidang Kepakaran' : 'A,B'
                
            } ]
        }
    }
    React.useState = (initial) => [changeState(initial), () => {}]
  
    render(<DaftarDosen dataListAllDosen={dummy}/>)
    const namaDosen= screen.getByText("Nama Dosen");
    const kontak= screen.getByText("Kontak");
    const bidang= screen.getByText("Bidang Kepakaran");
    expect(namaDosen).toBeInTheDocument();
    expect(kontak).toBeInTheDocument();
    expect(bidang).toBeInTheDocument();
})

test("Komponen Dosen is in Halaman Daftar Dosen as Role Dosen", () => {
    // Mocking getting userID and role with axios
    const res = {
        data : {
        userID: 'dosen',
        role: 'dosen'
        }
    }
    axios.get.mockResolvedValue(res);
    
    // Mocking useState for setting role
    function changeState(initial) {
        if (initial === 'pengguna') {
            return 'dosen'
        }
        else {
            return [ {
                'Nama Dosen' : 'Dosen A', 
                'ID': 'D111', 
                'Kontak': 'dosenA@email.com',
                'Bidang Kepakaran' : 'A,B'
                
            } ]
        }
    }
    React.useState = (initial) => [changeState(initial), () => {}]
  
    render(<DaftarDosen dataListAllDosen={dummy}/>)
    const namaDosen= screen.getByText("Nama Dosen");
    const kontak= screen.getByText("Kontak");
    const bidang= screen.getByText("Bidang Kepakaran");
    expect(namaDosen).toBeInTheDocument();
    expect(kontak).toBeInTheDocument();
    expect(bidang).toBeInTheDocument();
})

test("Komponen Mahasiswa is in Halaman Daftar Dosen as Role Mahasiswa", () => {
    // Mocking getting userID and role with axios
    const res = {
        data : {
        userID: 'mahasiswa',
        role: 'mahasiswa'
        }
    }
    axios.get.mockResolvedValue(res);
    
    // Mocking useState for setting role
    function changeState(initial) {
        if (initial === 'pengguna') {
            return 'mahasiswa'
        }
        else {
            return [ {
                'Nama Dosen' : 'Dosen A', 
                'ID': 'D111', 
                'Kontak': 'dosenA@email.com',
                'Bidang Kepakaran' : 'A,B',
                'Action' : <button data-test-id="request-button">Request</button>
                
            } ]
        }
    }
    React.useState = (initial) => [changeState(initial), () => {}]
  
    render(<DaftarDosen dataListAllDosen={dummy}/>)
    const namaDosen= screen.getByText("Nama Dosen");
    const kontak= screen.getByText("Kontak");
    const bidang= screen.getByText("Bidang Kepakaran");
    const action= screen.getByText("Action");
    expect(namaDosen).toBeInTheDocument();
    expect(kontak).toBeInTheDocument();
    expect(bidang).toBeInTheDocument();
    expect(action).toBeInTheDocument();
})

test("click daftar dosen button run request handler", () => {
    // Mocking getting userID and role with axios
    const res = {
        data : {
        userID: 'mahasiswa',
        role: 'mahasiswa'
        }
    }
    axios.get.mockResolvedValue(res);

    const requestHandler = jest.fn();

    // Mocking useState for setting role
    function changeState(initial) {
        if (initial === 'pengguna') {
            return 'mahasiswa'
        }
        else {
            return [ {
                'Nama Dosen' : 'Dosen A', 
                'ID': 'D111', 
                'Kontak': 'dosenA@email.com',
                'Bidang Kepakaran' : 'A,B',
                'Action' : <button onClick={event => requestHandler(event, 'dosen123')} data-test-id="request-button">Request</button>
            } ]
        }
    }
    React.useState = (initial) => [changeState(initial), () => {}]


    const { container } =  render(<DaftarDosen dataListAllDosen={dummy}/>);
    const requestButton = container.querySelector('[data-test-id="request-button"]');
    fireEvent.click(requestButton)
    expect(requestHandler).toBeCalled();
})

test("click daftar dosen button go to request page", () => {
    // Mocking getting userID and role with axios
    const res = {
        data : {
        userID: 'mahasiswa',
        role: 'mahasiswa'
        }
    }
    axios.get.mockResolvedValue(res);

    const requestHandler = jest.fn();

    // Mocking useState for setting role
    function changeState(initial) {
        if (initial === 'pengguna') {
            return 'mahasiswa'
        }
        else {
            return [ {
                'Nama Dosen' : 'Dosen A', 
                'ID': 'D111', 
                'Kontak': 'dosenA@email.com',
                'Bidang Kepakaran' : 'A,B',
                'Action' : <button onClick={event => requestHandler(event, 'dosen123')} data-test-id="request-button">Request</button>
            } ]
        }
    }
    React.useState = (initial) => [changeState(initial), () => {}]

    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
      route: '/cariDosen/requestDospem',
      push: jest.fn(),
    }));

    const { container } =  render(<DaftarDosen dataListAllDosen={dummy}/>);
    const requestButton = container.querySelector('[data-test-id="request-button"]');
    fireEvent.click(requestButton)
    expect(useRouter).toHaveBeenCalled();
})

test("search dosen in daftar dosen", () => {
    render(<DaftarDosen dataListAllDosen={dummy}/>)
    const searchButton = screen.getByPlaceholderText("Cari nama dosen");
    fireEvent.change(searchButton, {target: {value: 'D'}})
})

