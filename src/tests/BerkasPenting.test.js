import  BerkasPenting from "../pages/berkasPenting/index";
import {describe, expect, test} from '@jest/globals'
import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom'
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
library.add(fas);
import { getServerSideProps } from "../pages/berkasPenting/index";
import Heading from "../components/Heading"
import * as nextRouter from "next/router";
import axios from 'axios';
jest.mock('axios');

const berkasPentingUmumDummy = {'berkas_penting_list' : [[1, 'dummy.firebase.com', 'Umum', 'dummy']]};
const berkasPentingTemplateDummy = {'berkas_penting_list' : [[2, 'dummy.firebase.com', 'Template', 'dummy']]};
const berkasPentingLainLainDummy = {'berkas_penting_list' : [[3, 'dummy.firebase.com', 'Lain-lain', 'dummy']]};
const berkasPentingPage = <BerkasPenting umum={berkasPentingUmumDummy} template={berkasPentingTemplateDummy} lainlain={berkasPentingLainLainDummy}/>;

describe("getServerSideProps", () => {
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve({ status:'success' })
    })
  );

  beforeEach(() => {
    fetch.mockClear();
  });

  it("should call api", async () => {
    await getServerSideProps();
    expect(fetch).toHaveBeenCalledTimes(3)
  });
});

// Mocking getting role with axios
const res = {
  data : {
    userID: 'test',
    role: 'staf'
  }
}
axios.get.mockResolvedValue(res);

test("Page contains Component Heading with text '| Berkas Penting'", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Heading text = "| Berkas Penting" type="pusatInformasi"></Heading>)
  const text = screen.getByText("| Berkas Penting");
  expect(text).toBeInTheDocument()
})

test("renders umum section in Berkas Penting Page", () => {
  render(berkasPentingPage)
  const umum = screen.getByText("Umum");
  expect(umum).toBeInTheDocument();
})

test("renders template section in Berkas Penting Page", () => {
    render(berkasPentingPage)
    const template = screen.getByText("Template");
    expect(template).toBeInTheDocument();
})

test("renders lain-lain section in Berkas Penting Page", () => {
    render(berkasPentingPage)
    const lainLain = screen.getByText("Lain-lain");
    expect(lainLain).toBeInTheDocument();
})