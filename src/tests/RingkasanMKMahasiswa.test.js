import {expect, test} from '@jest/globals'
import { render, screen} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import '@testing-library/jest-dom';

import React from "react";
import * as nextRouter from "next/router";
import Ringkasan from "../pages/mataKuliahSpesial/[mataKuliahID]/m/ringkasan";
import DosenPembimbing from '../components/DosenPembimbing';
import { getServerSideProps } from "../pages/mataKuliahSpesial/[mataKuliahID]/m/ringkasan";
import { GetCookie } from "./Helper";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";

library.add(fas);

import * as TermContext from "../context/termContext"

const activeTerm = '2022-2'
const contextValues = { activeTerm: activeTerm };
jest.spyOn(TermContext, 'useTerm').mockImplementation(() => contextValues);

nextRouter.useRouter = jest.fn();
nextRouter.useRouter.mockImplementation(() => ({ 
    route: '/',
    push: jest.fn(),
    pathname:'/mataKuliahSpesial/CSGE/m/ringkasan',
    prefetch: async () => {},
    reload: jest.fn(),
    query: { mataKuliahID: "CSGE" }
}));

// Mock cookie in getServerSideProps
const { cookieUserID: cookieUserMahasiswa , cookieRole: cookieRoleMahasiswa } = GetCookie('mahasiswa123', 'mahasiswa')

const dummyData = {
    'topik_mahasiswa':[],
    'dospem_list':[],
    'detail_mk':["CSGE", "2020-2", "Mata Kuliah A", 1, 1, 1, "00.00.00.00-0000", false]
}

const dummyTopikDospem = {
    'topik_mahasiswa':[['Sebuah Topik', 'Deskripsi Topik 1', '2022-12-31']],
    'dospem_list':[['Menunggu persetujuan dosen', 'Dosen A'], ['Menunggu persetujuan dosen', 'Dosen B']],
    'detail_mk':["CSGE", "2021-2", "Mata Kuliah A", 1, 1, 1, "00.00.00.00-0000", false]
}

const dummyTopikDospem2 = {
    'topik_mahasiswa':[['Sebuah Topik', 'Deskripsi Topik 1', '2022-12-31']],
    'dospem_list':[['Disetujui', 'Dosen A'], ['Disetujui', 'Dosen B']],
    'detail_mk':["CSGE", "2022-2", "Mata Kuliah A", 1, 1, 1, "00.00.00.00-0000", false]
}

// Mocking axios for CSRF Token
const axios = require('axios');
jest.mock('axios');
const csrf = {
    'data': {'csrftoken': 'test'}
}
axios.get.mockResolvedValue(csrf);

// Mocking setting cookie for CSRF Token
const cookie = 'csrftoken=test;';
global.document.cookie = cookie;

it("check on getServerSideProps with context", async () => {
    const context = {
        req: { 
            cookies: { "userID" : cookieUserMahasiswa }
        },
        query: { mataKuliahID: "CSGE802097"}
    };
    

    global.fetch = jest.fn(() =>
    Promise.resolve({
            json: () => Promise.resolve({})
        })
    );
    
    await getServerSideProps(context);
});

test("topik is in ringkasan page", () => {
    render(<Ringkasan data={dummyData} />)
    const topic= screen.getByText("Topik");
    expect(topic).toBeInTheDocument();
})

test("nama, deskripsi, and batas waktu is in ringkasan page", () => {
    render(<Ringkasan data={dummyData} />)
    const namaTopik = screen.getByText("Judul Topik");
    expect(namaTopik).toBeInTheDocument();
    
    const deskripsiTopik = screen.getByText("Deskripsi Topik");
    expect(deskripsiTopik).toBeInTheDocument();

    const batasWaktu = screen.getByText("Batas Waktu Penyelesaian Yang Diharapkan");
    expect(batasWaktu).toBeInTheDocument();
})

test("dosen pembimbing is in ringkasan page", () => {
    render(<Ringkasan data={dummyData} />)
    const dospemTitle = screen.getAllByText("Dosen Pembimbing");
    expect(dospemTitle[0]).toBeInTheDocument();

    const dospem1 = screen.getByText("Dosen Pembimbing 1");
    expect(dospem1).toBeInTheDocument();
})

test("nama dosen pembimbing exist", () => {
    render(<DosenPembimbing dospem1="Dosen A" dospem2="Dosen B"/>)
    const dospem1 = screen.getByText("Dosen A");
    expect(dospem1).toBeInTheDocument();
})

test("nama dosen pembimbing not exist", () => {
    render(<DosenPembimbing dospem1={null} dospem2={null}/>)
    const dospem1 = screen.getByText("Anda belum memiliki Dosen Pembimbing 1");
    expect(dospem1).toBeInTheDocument();
})

test("topik successfully set", () => {
    // Mocking useState
    const setState = jest.fn();
    const useStateSpy = jest.spyOn(React, 'useState');

    useStateSpy.mockImplementation((init) => [init, setState]);
    
    render(<Ringkasan data={dummyTopikDospem} />)
})

test("dosen 1 successfully set", () => {
    // Mocking useState
    const mockSetState = jest.fn();
    jest.mock('react', () => ({
        ...jest.requireActual('react'),
        useState: initial => [initial, mockSetState]
    }));
    render(<Ringkasan data={dummyTopikDospem2} />)
})

test("edit mode actived successfully", () => {
    render(<Ringkasan data={dummyTopikDospem2} />);
    const initialButtons = screen.getAllByRole('button')
    
    userEvent.click(initialButtons[0])
})

test("edit batas waktu successfully", () => {    
    // Mocking useState
    const initialState = true;
    React.useState = (initial) => [initialState, () => {}]
    
    // Activating edit mode
    render(<Ringkasan data={dummyTopikDospem2} />);
    const initialButtons = screen.getAllByRole('button')
    
    userEvent.click(initialButtons[0])

    // Button in edit mode
    const batasWaktuButton = screen.getByLabelText('Batas Waktu Penyelesaian Yang Diharapkan:')
    userEvent.type(batasWaktuButton, '2022-12-31')
})

test("save updated topik successfully", () => {
    // Mocking PostRequest
    const response = {
        'data': {
            'status' : 'SUCCESS',
        }
    }

    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });
    
    // Mocking useState
    const initialState = true;
    React.useState = (initial) => [initialState, () => {}]
    
    // Activating edit mode
    render(<Ringkasan data={dummyTopikDospem2} />);
    const initialButtons = screen.getAllByRole('button')
    
    userEvent.click(initialButtons[0])

    // Button in edit mode
    const editModeButtons = screen.getAllByRole('button')
    userEvent.click(editModeButtons[0])

    expect(nextRouter.useRouter).toHaveBeenCalled();
})

test("cancel edit successfully", () => {    
    // Mocking useState
    const initialState = true;
    React.useState = (initial) => [initialState, () => {}]
    
    // Activating edit mode
    render(<Ringkasan data={dummyTopikDospem2} />);
    const initialButtons = screen.getAllByRole('button')
    
    userEvent.click(initialButtons[0])

    // Button in edit mode
    const editModeButtons = screen.getAllByRole('button')
    userEvent.click(editModeButtons[1])
})
