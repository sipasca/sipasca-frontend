import DetailStatus, { getServerSideProps } from "../pages/mataKuliahSpesial/[mataKuliahID]/s/administrasi/detailStatus/[detailStatusID]";
import { expect, test } from "@jest/globals";
import { render, screen, fireEvent} from "@testing-library/react";
import "@testing-library/jest-dom";
import * as nextRouter from "next/router";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
library.add(fas);
import React from "react";

import axios from 'axios';
jest.mock('axios');

// Mocking getting userID and role with axios
const res = {
  data : {
    userID: 'staf123',
    role: 'staf'
  }
}
axios.get.mockResolvedValue(res);

const mataKuliahID = "CSGE802097";
const detailStatusID = "Disetujui";
const detailMK_data = {
  detail_mk: ["CSGE802097", "2022-2", "Publikasi Ilmiah", 30, 2, 2, "03.00.12.01-2020", false]
};
const data = {"detail_mahasiswa_mk": [["M9636831678", "Choi Chaehun", null, "Disetujui dosen pembimbing"], [["url", "nama", "PI-Form Persetujuan Akademis"]], [["dosen1"]]]}
const detailStatusPage = (
  <DetailStatus
    detailMK_data={detailMK_data}
    mataKuliahID={mataKuliahID}
    detailStatusID={detailStatusID}
    data={data}
    role="staf"
  />
);

it("check on getServerSideProps with context", async () => {
  const context = {
    query: {
      mataKuliahID: mataKuliahID,
      detailStatusID: detailStatusID,
      detailMK_data: detailMK_data,
    },
  };
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve({}),
    })
  );

  await getServerSideProps(context);
});

test("Page contains Component Heading with text '| Mata Kuliah ..'", () => {
  jest.spyOn(window.localStorage.__proto__, "getItem");
  window.localStorage.__proto__.getItem = jest.fn(() => {
    return "staf";
  });

  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({
    pathname: "/administrasi",
    query: { mataKuliahID: "CSGE802097", detailStatusID: "M9636831678"},
  }));

  render(detailStatusPage);
  const title = screen.getByText("| Mata Kuliah Publikasi Ilmiah");
  expect(title).toBeInTheDocument();
});

test("onchange in form-select works'", () => {
  jest.spyOn(window.localStorage.__proto__, "getItem");
  window.localStorage.__proto__.getItem = jest.fn(() => {
    return "staf";
  });

  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({
    pathname: "/administrasi",
    query: { mataKuliahID: "CSGE802097", detailStatusID: "M9636831678"},
    push: jest.fn(),
  }));

  render(detailStatusPage);

  fireEvent.change(screen.getByTestId("validationCustom04"), {
    target: { value: "MasaBimbingan" },
  });
  expect(screen.getByText("Status")).toBeInTheDocument();
});