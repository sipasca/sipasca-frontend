import React from "react";
import DetailTopik, {getServerSideProps} from "../pages/topik/detail/[detailTopikID]";
import {expect, test} from '@jest/globals'
import { render, screen, fireEvent } from "@testing-library/react";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
library.add(fas, far);

import '@testing-library/jest-dom'
import axios from 'axios';
jest.mock('axios');

const detail1 = {
    "detail_topik_dosen": [
        [
            154,
            "D1234567890",
            "lolll",
            "jfdjfjd",
            "dj",
            "dsldls",
            "",
            "belum diambil"
        ],
        []
    ]
}

const detail2 = {
    "detail_topik_dosen": [
        [
            154,
            "D1234567890",
            "lolll",
            "jfdjfjd",
            "",
            "",
            "",
            "belum diambil"
        ],
        []
    ]
}

// Mocking getting userID and role with axios
const res = {
    data : {
      userID: 'mahasiswa123',
      role: 'mahasiswa'
    }
  }
axios.get.mockResolvedValue(res);

test("renders title in Detail Topik", () => {
    render(<DetailTopik detailTopik={detail1} role="dosen"/>)

    const title = screen.getByText("| Detail Topik");
    expect(title).toBeInTheDocument();
})

test("renders nama in Detail Topik", () => {
    render(<DetailTopik detailTopik={detail1} role="dosen"/>)

    const nama = screen.getByText("Nama Topik");
    expect(nama).toBeInTheDocument();
})

test("renders deskripsi in Detail Topik", () => {
    render(<DetailTopik detailTopik={detail1} role="dosen"/>)

    const deskripsi = screen.getByText("Deskripsi Topik");
    expect(deskripsi).toBeInTheDocument();
})

test("renders dokumen pendukung in Detail Topik", () => {
    render(<DetailTopik detailTopik={detail1} role="dosen"/>)

    const dokumen = screen.getByText("Dokumen Pendukung");
    expect(dokumen).toBeInTheDocument();
})

test("renders prasyarat in Detail Topik", () => {
    render(<DetailTopik detailTopik={detail1} role="dosen"/>)

    const prasyarat = screen.getByText("Prasyarat");
    expect(prasyarat).toBeInTheDocument();
})

test("renders kembali button in Detail Topik", () => {
    render(<DetailTopik detailTopik={detail1} role="mahasiswa"/>)

    const backButton = screen.getByText("Kembali");
    expect(backButton).toBeInTheDocument();
})

test("renders Prasyarat in Detail Topik if there is no prasyarat", () => {
    render(<DetailTopik detailTopik={detail2} role="mahasiswa"/>)

    const prasyarat2 = screen.getByText("Prasyarat");
    expect(prasyarat2).toBeInTheDocument();
})

test("request request button works", () => {
    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
      route: '/cariDosen/',
      push: jest.fn(),
      prefetch: async () => {}
    }));

    // Mocking useState for setting role
    const state = 'mahasiswa';
    React.useState = (initial) => [state, () => {}]

    const { container } = render(<DetailTopik detailTopik={detail2} role="mahasiswa"/>)

    const requestButton = container.querySelector("[id='request']");
    fireEvent.click(requestButton);
    expect(useRouter).toHaveBeenCalled();
})

it("check on getServerSideProps with context", async () => {
    const context = {
        query: { detailTopikID: "1"}
    
    };
    global.fetch = jest.fn(() =>
    Promise.resolve({
            json: () => Promise.resolve({})
        })
    );
    
    await getServerSideProps(context);
  });
