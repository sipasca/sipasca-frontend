import Buat from "../pages/topik/buat";
import {expect, test} from '@jest/globals'
import {render, screen} from "@testing-library/react";
import '@testing-library/jest-dom'
import userEvent from "@testing-library/user-event";
import axios from 'axios';
jest.mock('axios');

// Mocking getting userID and role with axios
const res = {
    data : {
      userID: 'dosen123',
      role: 'dosen'
    }
  }
axios.get.mockResolvedValue(res);

test("Buat Topik Pages contains title", () => {
    render(<Buat />)
    const title= screen.getByText("| Buat Topik");
    expect(title).toBeInTheDocument();
})

test("Buat Topik Pages contains 'Deskripsi Topik'", () => {
    render(<Buat />)
    const deskripsiTopik = screen.getByText("Deskripsi Topik");
    expect(deskripsiTopik).toBeInTheDocument();
})

test("Buat Topik Pages contains 'Tambahkan Prasyarat'", () => {
    render(<Buat />)
    const tambahkanPrasyarat = screen.getByText("Tambahkan Prasyarat");
    expect(tambahkanPrasyarat).toBeInTheDocument();
})

test("Buat Topik Pages contains 'Tambahkan Dokumen Pendukung'", () => {
    render(<Buat />)
    const tambahkanDokumenPendukung = screen.getByText("Tambahkan Dokumen Pendukung");
    expect(tambahkanDokumenPendukung).toBeInTheDocument();
})

test("Buat Topik Pages contains title Button", () => {
    render(<Buat />)
    const button = screen.getByRole("button");
    expect(button).toBeInTheDocument()
})

test("onchange in Batas Terima works", () => {
    const { container } = render(<Buat/>)
    const buttonElement1 = container.querySelector('[id="button1"]')
    userEvent.click(buttonElement1)
    const buttonElement2 = container.querySelector('[id="button2"]')
    userEvent.click(buttonElement2)
    const buttonElement3 = container.querySelector('[id="button3"]')
    userEvent.click(buttonElement3)
});

