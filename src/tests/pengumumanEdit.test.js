import React from "react";
import PengumumanEdit from "../pages/pengumuman/pengumumanEdit";
import { expect, test} from '@jest/globals'
import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom';
import * as nextRouter from "next/router";
import userEvent from "@testing-library/user-event"
import Heading from "../components/Heading"

import Alert from "../components/SuccessAlert";

import '@testing-library/jest-dom'
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
library.add(fas);
import { getServerSideProps } from "../pages/pengumuman/pengumumanEdit";

import axios from 'axios';
jest.mock('axios');

// Mocking getting userID and role with axios
const res = {
  data : {
    userID: 'staf123',
    role: 'staf'
  }
}
axios.get.mockResolvedValue(res);

// Mocking useState for setting role
const state = 'staf';
React.useState = (initial) => [state, () => {}]

const pengumuman_list = {"pengumuman_list": [[10, "C", "DCCC", "2022-03-18"]]}
const pengumuman_list_all = <PengumumanEdit data = {pengumuman_list} />

test("renders lihat selengkapnya in Edit Pengumuman Page", () => {
    render(pengumuman_list_all)

    const lihatSelengkapnya = screen.getByText("Lihat Selengkapnya");
    expect(lihatSelengkapnya).toBeInTheDocument();
})

test("Page with role : Staf contains button Tambah", () => {
  // works:
  jest.spyOn(window.localStorage.__proto__, 'getItem');
  window.localStorage.__proto__.getItem = jest.fn(() => {
    return 'staf'
  });
  render(pengumuman_list_all)

    const buttonTambah = screen.getByText("Tambah");
    expect(buttonTambah).toBeInTheDocument();

})

test("Page with role : Staf contains button Batal", () => {
  // works:
  jest.spyOn(window.localStorage.__proto__, 'getItem');
  window.localStorage.__proto__.getItem = jest.fn(() => {
    return 'staf'
  });
  
  render(pengumuman_list_all)

    const buttonBatal = screen.getByText("Batal");
    expect(buttonBatal).toBeInTheDocument();

})

test("Page contains Alert Success Buat Pengumuman", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));
  
    render(<Alert text="Pengumuman berhasil dibuat" />);
    const text = screen.getByText("Pengumuman berhasil dibuat");
    expect(text).toBeInTheDocument();
})



describe("getServerSideProps", () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ status:'success' })
      })
    );
  
    beforeEach(() => {
      fetch.mockClear();
    });
  
    it("should call api", async () => {
      await getServerSideProps();
      expect(fetch).toHaveBeenCalledTimes(1)
    });
  });

test("function handleDelete run when click Ya in popup delete", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ prefetch: async () => {} }));
    render(pengumuman_list_all);

    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
      route: '/pengumuman/pengumumanEdit/',
      push: jest.fn(),
    }));

    const { container } = render(pengumuman_list_all);
    const buttonElement2 = container.querySelector('[id="tombolX"]');
    userEvent.click(buttonElement2);
    const buttonElement3 = container.querySelector('[id="tombolYa"]')
    userEvent.click(buttonElement3)
    expect(useRouter).toHaveBeenCalled();
});

test("function handleDelete run when click Tidak in popup", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ prefetch: async () => {} }));

    render(pengumuman_list_all);
    const { container } = render(pengumuman_list_all);
    const buttonElement2 = container.querySelector('[id="tombolX"]');
    userEvent.click(buttonElement2);
    const buttonElement3 = container.querySelector('[id="tombolTidak"]')
    userEvent.click(buttonElement3)
});

test("Page with tanda : SUCCESS contains Alert", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ prefetch: async () => {} }));

  // works:
  jest.spyOn(window.localStorage.__proto__, 'getItem');
  window.localStorage.__proto__.getItem = jest.fn(() => {
    return 'SUCCESS'
  });
  
  render(pengumuman_list_all)

    const buttonBatal = screen.getByText("Pengumuman berhasil dibuat");
    expect(buttonBatal).toBeInTheDocument();

})

describe("hapusHandler", () => {
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve({ status:'SUCCESS' })
    })
  );
  test("Page contains Component Heading with text '| Pengumuman'", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));
  
    render(<Heading text = "| Pengumuman" type="pengumuman"></Heading>);
    const text = screen.getByText("| Pengumuman");
    expect(text).toBeInTheDocument();
  })

  beforeEach(() => {
    fetch.mockClear();
  });

  it("should call api", async () => {
    await getServerSideProps();
    expect(fetch).toHaveBeenCalledTimes(1)
  });
});