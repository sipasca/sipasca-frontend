import Tambah from "../pages/mataKuliahSpesial/tambah";
import {expect, test} from '@jest/globals'
import { render, screen, fireEvent, getByTestId} from "@testing-library/react";
import '@testing-library/jest-dom'
import React from "react";
import * as nextRouter from "next/router";
import PopUp from "../components/PopUp";
import Heading from "../components/Heading"
import userEvent from "@testing-library/user-event" 

// Mocking axios for CSRF Token
const axios = require('axios');
jest.mock('axios');
const csrf = {
    'data': {'csrftoken': 'test'}
}
axios.get.mockResolvedValue(csrf);

// Mocking setting cookie for CSRF Token
const cookie = 'csrftoken=test;';
global.document.cookie = cookie;

test("Form Tambah Mata Kuliah Spesial Contains '| Tambah Mata Kuliah Spesial'", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));
  
    render(<Heading text = "| Tambah Mata Kuliah Spesial" type="kecil"></Heading>)
    const text = screen.getByText("| Tambah Mata Kuliah Spesial");
    expect(text).toBeInTheDocument()

})

test("Form Tambah Mata Kuliah Spesial Contains Button", () => {
    render(<Tambah />)
    const button = screen.getByRole("button",{ name: "Simpan" });
    expect(button).toBeInTheDocument()
})

test("Form Tambah Mata Kuliah Spesial Contains Heading", () => {
    render(<Tambah />)
    const heading = screen.getByRole("heading", {name:"| Tambah Mata Kuliah Spesial"});
    expect(heading).toBeInTheDocument()
})

test("Form Tambah Mata Kuliah Spesial Contains Button Type Button", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

    render(<Tambah />);
    const button = document.querySelector("button").getAttribute("type")
    expect(button).toBe("submit");
})

test("Form Tambah Mata Kuliah Spesial Contains Paragraph Type Button", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

    render(<Tambah />);
    const paragraf = screen.getByTestId("button").getAttribute("type")
    expect(paragraf).toBe("button");
})

test("Form Tambah Mata Kuliah Spesial Contains PopUp Batal Tambah Mata Kuliah Spesial with Text and URL", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));
  
    render(<PopUp text="Perubahan belum disimpan. Apakah Anda yakin untuk batal?" url="/administrasi/"></PopUp>);
    const text = screen.getByText("Perubahan belum disimpan. Apakah Anda yakin untuk batal?");
    const url = document.querySelector("a").getAttribute("href");
    const button = document.querySelector("a").getElementsByTagName("button");
    expect(button).not.toBeNull()
    expect(url).toBe("/administrasi/");
    expect(text).toBeInTheDocument();
})

test("Render Click Batal in Form Tambah Mata Kuliah Spesial", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

    const { container } = render(<Tambah />);
    const buttonElement2 = container.querySelector('[id="tombolBatal"]');
    userEvent.click(buttonElement2);

    const text = screen.getByText("Perubahan belum disimpan. Apakah Anda yakin untuk batal?");
    expect(text).toBeInTheDocument()
    const button = document.querySelector("a").getElementsByTagName("button",{name:"Ya"});
    expect(button).not.toBeNull()
})

// New Code Tambah Form
test("Form Tambah Mata Kuliah Spesial Contains Input Form Kode Mata Kuliah", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

    const { container } = render(<Tambah />);
    const input = container.querySelector("input").getAttributeNames("required")
    expect(input).not.toBeNull()
    const label = container.querySelector("label",{name:"Kode Mata Kuliah"})
    expect(label).toBeInTheDocument()
})

test("Form Tambah Mata Kuliah Spesial Contains Input Form Nama Mata Kuliah", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

    const { container } = render(<Tambah/>);
    const input = container.querySelector("input").getAttributeNames("required")
    expect(input).not.toBeNull()
    const label = container.querySelector("label",{name:"Nama Mata Kuliah"})
    expect(label).toBeInTheDocument()
})

test("Form Tambah Mata Kuliah Spesial Contains Input Form Kurikulum Mata Kuliah", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

    const { container } = render(<Tambah />);
    const input = container.querySelector("input").getAttributeNames("required")
    expect(input).not.toBeNull()
    const label = container.querySelector("label",{name:"Kurikulum"})
    expect(label).toBeInTheDocument()
})

test("Form Tambah Mata Kuliah Spesial Contains Input Form Kapasitas Mata Kuliah", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

    const { container } = render(<Tambah />);
    const input = container.querySelector("input").getAttributeNames("required")
    expect(input).not.toBeNull()
    const label = container.querySelector("label",{name:"Kapasitas"})
    expect(label).toBeInTheDocument()
})

test("Form Tambah Mata Kuliah Spesial Contains Select Form Periode Mata Kuliah", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

    const { container } = render(<Tambah />);
    const select = container.querySelector("select").getAttributeNames("required")
    expect(select).not.toBeNull()
    const placeholder = container.querySelector("option",{name:"Pilih Periode Mata Kuliah Spesial"})
    expect(placeholder).toBeInTheDocument()
    const label = container.querySelector("label",{name:"Periode"})
    expect(label).toBeInTheDocument()
})

test("Form Tambah Mata Kuliah Spesial Contains Select Form Kredit Mata Kuliah", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

    const { container } = render(<Tambah />);
    const select = container.querySelector("select").getAttributeNames("required")
    expect(select).not.toBeNull()
    const placeholder = container.querySelector("option",{name:"Pilih Jumlah Kredit Mata Kuliah Spesial"})
    expect(placeholder).toBeInTheDocument()
    const label = container.querySelector("label",{name:"Kredit"})
    expect(label).toBeInTheDocument()
})

// Test for Use Router

test("Test for Tambah Mata Kuliah Click Button Batal Not Simpan", async () => {
    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
      route: '/administrasi/',
      push: jest.fn(),
    }));

    // Test
    const { container } = render(<Tambah />);
    const batalButton = container.querySelector('[id="tombolBatal"]');
    fireEvent.click(batalButton);
    expect(useRouter).toHaveBeenCalled();
    const text = screen.getByText("Perubahan belum disimpan. Apakah Anda yakin untuk batal?");
    expect(text).toBeInTheDocument()
})

test("Test for Tambah Mata Kuliah Click Button Simpan Succesfully", async () => {
    // Mocking PostRequest
    const response = {
        'data': {
            'status' : 'SUCCESS',
            'kode' : 'testKode',
            'periode' : 'testPeriode',
            'nama' : 'testNama',
            'jumlah_mahasiswa' : 1,
            'kredit' : 1,
            'term' : 1,
            'kurikulum' : 'testKurikulum',
            'tugas_akhir' : 'true',
            'prasyarat' : 'prasyarat'
        }
    }

    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });

    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
      route: '/administrasi/',
      push: jest.fn(),
    }));

    // Test
    render(<Tambah />)

    const simpanButton = screen.getByRole("button",{name:"Simpan"});
    fireEvent.click(simpanButton);
    expect(useRouter).toHaveBeenCalled();
})

describe("Test for Tambah Mata Kuliah Click Button Simpan SUCCESS", () => {
    // Mocking PostRequest
    const response = {
        'data': {
            'status' : 'SUCCESS',
            'kode' : 'testKode',
            'periode' : 'testPeriode',
            'nama' : 'testNama',
            'jumlah_mahasiswa' : 1,
            'kredit' : 1,
            'term' : 1,
            'kurikulum' : 'testKurikulum',
            'tugas_akhir' : 'true',
            'prasyarat' : 'prasyarat'
        }
    }

    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });

    // Mocking local storage
    jest.spyOn(window.localStorage.__proto__, 'setItem');
    Object.setPrototypeOf(window.localStorage.setItem, jest.fn())
  
    it('calls localStorage.setItem when clicked', () => {
        render(<Tambah />);
        const simpanButton = screen.getByRole("button",{name:"Simpan"});
        fireEvent.click(simpanButton);
        expect(window.localStorage.setItem).toHaveBeenCalledWith('SuccessCreateForm', 'SUCCESS')
        expect(window.localStorage.setItem).toHaveBeenCalledWith('matKul', '')
    })
 })

test("Test for Tambah Mata Kuliah Click Button Simpan FAILED", async () => {
    // Mocking PostRequest
    const response = {
        'data': {
            'status' : 'FAILED',
            'kode' : 'testKode',
            'periode' : 'testPeriode',
            'nama' : 'testNama',
            'jumlah_mahasiswa' : 1,
            'kredit' : 1,
            'term' : 1,
            'kurikulum' : 'testKurikulum',
            'tugas_akhir' : 'true',
            'prasyarat' : 'prasyarat'
        }
    }

    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });

    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
      route: '/administrasi/',
      push: jest.fn(),
    }));

    // Test
    render(<Tambah />)

    const simpanButton = screen.getByRole("button",{name:"Simpan"});
    fireEvent.click(simpanButton);
    expect(useRouter).toHaveBeenCalled();
})

describe("Test for Tambah Mata Kuliah Click Button Simpan Failed", () => {
    // Mocking PostRequest
    const response = {
        'data': {
            'status' : 'FAILED',
            'kode' : 'testKode',
            'periode' : 'testPeriode',
            'nama' : 'testNama',
            'jumlah_mahasiswa' : 1,
            'kredit' : 1,
            'term' : 1,
            'kurikulum' : 'testKurikulum',
            'tugas_akhir' : 'true',
            'prasyarat' : 'prasyarat'
        }
    }

    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });

    // Mocking local storage
    jest.spyOn(window.localStorage.__proto__, 'setItem');
    Object.setPrototypeOf(window.localStorage.setItem, jest.fn())
  
    it('calls localStorage.setItem when clicked', () => {
        render(<Tambah />);
        const simpanButton = screen.getByRole("button",{name:"Simpan"});
        fireEvent.click(simpanButton);
        expect(window.localStorage.setItem).toHaveBeenCalledWith('SuccessCreateForm', 'FAILED')
        expect(window.localStorage.setItem).toHaveBeenCalledWith('matKul', '')
    })
 })

test("Form Tambah Mata Kuliah Spesial Contains Red Mark for Required Forms", () => {
    render(<Tambah />);
    const tandaWajib = screen.getAllByText("*");
    expect(tandaWajib).not.toBeNull();
    expect(tandaWajib).toHaveLength(6);
    const kalimatWajib = screen.getByText("* Kolom dengan tanda ini wajib diisi");
    expect(kalimatWajib).toBeInTheDocument();
})
