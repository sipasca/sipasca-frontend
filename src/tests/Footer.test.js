import {expect, test} from '@jest/globals'
import { render, screen } from "@testing-library/react";
import Footer from '../components/Footer';
import "@testing-library/jest-dom";

test("renders kontak in footer", () =>{
    render(<Footer />);
    const title = screen.getByText("KONTAK");
    expect(title).toBeInTheDocument();

    const facebook = screen.getByText("Facebook");
    expect(facebook).toBeInTheDocument();

    const linkedin = screen.getByText("LinkedIn");
    expect(linkedin).toBeInTheDocument();

    const instagram = screen.getByText("Instagram");
    expect(instagram).toBeInTheDocument();

    const twitter = screen.getByText("Twitter");
    expect(twitter).toBeInTheDocument();

    const youtube = screen.getByText("Youtube");
    expect(youtube).toBeInTheDocument();
})

