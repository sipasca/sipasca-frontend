import React from "react";
import Pengumuman from "../pages/pengumuman/index";
import {describe, expect, test} from '@jest/globals'
import { render, screen } from "@testing-library/react";
import Alert from "../components/SuccessAlert";
import '@testing-library/jest-dom'
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import Heading from "../components/Heading";

import axios from 'axios';
jest.mock('axios');

library.add(fas);
import { getServerSideProps } from "../pages/pengumuman/index";
import * as nextRouter from "next/router";

const pengumuman_All = {"pengumuman_list": [[10, "C", "DCCC", "2022-03-18"]]}
const kumpulan_pengumuman_All = <Pengumuman data = {pengumuman_All} />

describe("getServerSideProps", () => {
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve({ status:'success' })
    })
  );

  beforeEach(() => {
    fetch.mockClear();
  });

  it("should call api", async () => {
    await getServerSideProps();
    expect(fetch).toHaveBeenCalledTimes(1)
  });
});

// Mocking getting userID and role with axios
const res = {
  data : {
    userID: 'staf123',
    role: 'staf'
  }
}
axios.get.mockResolvedValue(res);

// Mocking useState for setting role
const state = 'staf';
React.useState = (initial) => [state, () => {}]

test("Page contains Component Heading with text '| Pengumuman'", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Heading text = "| Pengumuman" type="pengumuman"></Heading>);
  const text = screen.getByText("| Pengumuman");
  expect(text).toBeInTheDocument();
})

test("renders lihat selengkapnya in Pengumuman Page", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ prefetch: async () => {} }));

    render(kumpulan_pengumuman_All)

    const lihatSelengkapnya = screen.getByText("Lihat Selengkapnya");
    expect(lihatSelengkapnya).toBeInTheDocument();
})

test("Page with role : Staf contains button Edit", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ prefetch: async () => {} }));

    render(kumpulan_pengumuman_All)

    const buttonEdit = screen.getByText("Edit");
    expect(buttonEdit).toBeInTheDocument();
    
})

test("Page contains Alert Success Hapus Pengumuman", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Alert text="Pengumuman berhasil dihapus" />);
  const text = screen.getByText("Pengumuman berhasil dihapus");
  expect(text).toBeInTheDocument();
})

test("Page with tanda_hapus : SUCCESS contains Alert", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ prefetch: async () => {} }));
  
  render(kumpulan_pengumuman_All)

    const buttonBatal = screen.getByText("Pengumuman berhasil dihapus");
    expect(buttonBatal).toBeInTheDocument();

})


  



