import DeletePopUp from "../components/DeletePopUp";
import { expect, test } from "@jest/globals";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";

it('calls "onDialog = true" prop on button click', () => {
  const onDialog = jest.fn();
  const { getByText } = render(<DeletePopUp onDialog={onDialog} />);

  fireEvent.click(getByText("Tidak"));
  expect(onDialog).toHaveBeenCalled();
});

it('calls "onDialog = false" prop on button click', () => {
  const onDialog = jest.fn();
  const { getByText } = render(<DeletePopUp onDialog={onDialog} />);

  fireEvent.click(getByText("Ya"));
  expect(onDialog).toHaveBeenCalled();
});
