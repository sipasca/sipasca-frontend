import Login from "../pages/login";
import { expect, test} from '@jest/globals'
import { render, screen, fireEvent } from "@testing-library/react";
import '@testing-library/jest-dom'
import React from "react";
import * as nextRouter from "next/router";
import Heading from "../components/Heading";

// Mocking axios for CSRF Token
const axios = require('axios');
jest.mock('axios');
const csrf = {
    'data': {'csrftoken': 'test'}
}
axios.get.mockResolvedValue(csrf);

// Mocking setting cookie for CSRF Token
const cookie = 'csrftoken=test;';
global.document.cookie = cookie;

test("renders simkuspa tittle in Login", () => {
    render(<Login />)
    const simkuspaTittle = screen.getByText("SIPASCA");
    expect(simkuspaTittle).toBeInTheDocument();
})

test("Page contains Component Heading with text '| Login'", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));
  
    render(<Heading text = "| Login" type="login"></Heading>)
    const text = screen.getByText("| Login");
    expect(text).toBeInTheDocument()
})

test("renders username text in Login", () => {
    render(<Login />)
    const usernameText= screen.getByText("Username");
    expect(usernameText).toBeInTheDocument();
})

test("renders password text in Login", () => {
    render(<Login />)
    const passwordText = screen.getByText("Password");
    expect(passwordText).toBeInTheDocument();
})

test("renders button in Login", () => {
    render(<Login />)
    const buttonLogin = screen.getByRole("button");
    expect(buttonLogin).toBeInTheDocument();
})

test("renders textbox in Login", () => {
    render(<Login />)
    const textBox = screen.getByRole("textbox");
    expect(textBox).toBeInTheDocument();
})

test("login successfully", async () => {
    // Mocking PostRequest
    const response = {
        'data': {
            'status' : 'SUCCESS',
            'role': 'testRole',
            'userID': 'testID'
        }
    }

    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });

    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
      route: '/',
      push: jest.fn(),
    }));


    // Test
    render(<Login />)

    const loginButton = screen.getByRole("button");
    fireEvent.click(loginButton);
    expect(useRouter).toHaveBeenCalled();
})

test("login unrecognized", async () => {
    // Mocking PostRequest
    const response = {
        'data': {
            'status' : 'UNRECOGNIZED',
        }
    }

    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });

    // Mocking useState
    const setState = jest.fn();
    const useStateSpy = jest.spyOn(React, 'useState');

    useStateSpy.mockImplementation((init) => [init, setState]);

    // Test
    render(<Login />)

    const loginButton = screen.getByRole("button");
    fireEvent.click(loginButton);
    expect(useStateSpy).toHaveBeenCalled();
})