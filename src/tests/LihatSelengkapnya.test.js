import React from "react";
import { expect, test} from '@jest/globals'
import { render } from "@testing-library/react";
import '@testing-library/jest-dom'

import LihatSelengkapnya from "../components/LihatSelengkapnya";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
library.add(fas);


test("renders link to berkas penting", () => {
    const { container } = render(<LihatSelengkapnya url = "/berkasPenting" testId="link-berkas-penting"/>)
    expect(
        container.querySelector('[data-testid="link-berkas-penting"]').getAttribute("href")
      ).toEqual("/berkasPenting");
})
