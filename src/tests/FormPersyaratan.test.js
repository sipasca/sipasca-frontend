import Administrasi from "../pages/mataKuliahSpesial/[mataKuliahID]/s/administrasi/index"
import {expect, test} from '@jest/globals'
import { render, screen,  fireEvent, waitFor } from "@testing-library/react";

import '@testing-library/jest-dom'
import * as nextRouter from "next/router";
import { getServerSideProps } from "../pages/mataKuliahSpesial/[mataKuliahID]/s/administrasi/index";
import { downloadFilesHandler } from "../pages/mataKuliahSpesial/[mataKuliahID]/s/administrasi/index";

const data = {"list_form_persyaratan": [["Form Persyaratan 1"], ["Form Persyaratan 2"], ["Form Persetujuan Pembimbing Akademis"]]}
const mataKuliahID = "dummy"
const detailMK_data = {'detail_mk':["CSGE802097", "2022-2", "Publikasi Ilmiah", 30, 2, 2, "03.00.12.01-2020", false]}

const AdministrasiPage = <Administrasi data={data} mataKuliahID={mataKuliahID} detailMK_data={detailMK_data}  />;

import axios from 'axios';
jest.mock('axios');

// Mocking getting userID and role with axios
const res = {
  data : {
    userID: 'staf123',
    role: 'staf'
  }
}
axios.get.mockResolvedValue(res);

jest.spyOn(window.localStorage.__proto__, 'getItem');
window.localStorage.__proto__.getItem = jest.fn(() => {
    return 's'
});

describe("getServerSideProps", () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ status:'success' })
      })
    );
  
    beforeEach(() => {
      fetch.mockClear();
    });
  
    it("should call api", async () => {
        const context = {
            query: { mataKuliahID: mataKuliahID}
        };
      await getServerSideProps(context);
      expect(fetch).toHaveBeenCalledTimes(2)
    });
  });

test("renders text title in Administrasi", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: "CSGE" } }));
    render(AdministrasiPage)
    const title = screen.getByText("Administrasi");
    expect(title).toBeInTheDocument();
})

test("renders text unduh in Administrasi", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: "CSGE" } }));
    render(AdministrasiPage)
    const unduh = screen.getAllByText("Unduh Semua");
    expect(unduh.length).toBeGreaterThan(0);
})

test("renders text surat  in Administrasi", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: "CSGE" } }));
    render(AdministrasiPage)
    const surat1 = screen.getByText("Form Persetujuan Pembimbing Akademis");
    expect(surat1).toBeInTheDocument();
})

test("renders text surat not in Administrasi", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: "CSGE" } }));
    render(AdministrasiPage)
    const surat2 = screen.queryByText("Bukan Surat");
    expect(surat2).not.toBeInTheDocument();
})

test("function unduh", async ()=>{
  global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve({ detail_form_persyaratan:[['Choi Chaehun', '1906293045', 'production/FormPersyaratan/CSGE802097_2022-2/Test/1906293045_Test.pdf', '1906293045_Test.pdf']] })
  })
  );
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: "CSGE802097_2022-2" } }));  
    render(AdministrasiPage)
    const {container} = render(AdministrasiPage)
    const buttonElement = container.querySelector('[class="unduhSemua"]')
    fireEvent.click(buttonElement)
})

test("function unduhh", async ()=>{
  global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve({ detail_form_persyaratan:[] })
  })
  );
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: "CSGE802097_2022-2" } }));  
    render(AdministrasiPage)
    const {container} = render(AdministrasiPage)
    const buttonElement = container.querySelector('[class="unduhSemua"]')
    fireEvent.click(buttonElement)
})

// describe("downloadFilesHandler", () => {
//   global.fetch = jest.fn(() =>
//     Promise.resolve({
//       json: () => Promise.resolve({ status:'success' })
//     })
//   );

//   beforeEach(() => {
//     fetch.mockClear();
//   });

//   it("should call api", async () => {
//     // nextRouter.useRouter = jest.fn();
//     nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: 'CSGE802097_2022-2' } }));  
//     render(AdministrasiPage)
//     const {container} = render(AdministrasiPage)
//     const buttonElement = container.querySelector('[class="unduhSemua"]')
//     const t = fireEvent.click(buttonElement)
//     await downloadFilesHandler(t,'CSGE802097_2022-2','Form Persetujuan Pembimbing Akademis');
//     expect(fetch).toHaveBeenCalledTimes(2)
//   });
// });

