import { expect, test } from '@jest/globals'
import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom';
import userEvent from "@testing-library/user-event"
import { library, text } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import Edit from '../pages/berkasPenting/edit';
import * as nextRouter from "next/router";
import Heading from '../components/Heading';

library.add(fas);
import { getServerSideProps } from "../pages/berkasPenting/edit";

const berkasPentingUmumDummy = { 'berkas_penting_list': [[1, 'dummy.firebase.com', 'Umum', 'dummy']] };
const berkasPentingTemplateDummy = { 'berkas_penting_list': [[2, 'dummy.firebase.com', 'Template', 'dummy']] };
const berkasPentingLainLainDummy = { 'berkas_penting_list': [[3, 'dummy.firebase.com', 'Lain-lain', 'dummy']] };
const berkasPentingEditPage = <Edit berkasUmum={berkasPentingUmumDummy} berkasTemplate={berkasPentingTemplateDummy} berkasLainLain={berkasPentingLainLainDummy} />;

describe("getServerSideProps", () => {
    global.fetch = jest.fn(() =>
        Promise.resolve({
            json: () => Promise.resolve({ status: 'success' })
        })
    );

    beforeEach(() => {
        fetch.mockClear();
    });

    it("should call api", async () => {
        await getServerSideProps();
        expect(fetch).toHaveBeenCalledTimes(3)
    });
});

test("Page contains Component Heading with text '| Berkas Penting'", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));
  
    render(<Heading text = "| Berkas Penting" type="pusatInformasi"></Heading>)
    const text = screen.getByText("| Berkas Penting");
    expect(text).toBeInTheDocument()
})


test("renders umum section in Berkas Penting Page", () => {
    render(berkasPentingEditPage)

    const umum = screen.getByText("Umum");
    expect(umum).toBeInTheDocument();
})

test("renders template section in Berkas Penting Page", () => {
    render(berkasPentingEditPage)

    const template = screen.getByText("Template");
    expect(template).toBeInTheDocument();
})

test("renders lain-lain section in Berkas Penting Page", () => {
    render(berkasPentingEditPage)

    const lainLain = screen.getByText("Lain-lain");
    expect(lainLain).toBeInTheDocument();
})

test("renders button Batal in Edit Berkas Penting Page", () => {
    render(berkasPentingEditPage)

    const batalButton = screen.getByRole("button", { name: "Batal" });
    expect(batalButton).toBeInTheDocument();
})

test("renders text button unggah in Berkas Penting Page", () => {
    render(berkasPentingEditPage)
    const unggahFile = screen.getAllByText("Unggah File");
    unggahFile.forEach(element => {
        expect(element).toBeInTheDocument();
    });
})

test("render button unggah in Berkas Penting Page", () => {
    render(berkasPentingEditPage)
    const buttonElement = screen.getAllByRole("button", { name: "Unggah File" })
    buttonElement.forEach(element => {
        expect(element).toBeInTheDocument();
    })
})

test("render title in pop up unggah in Berkas Penting Page", () => {
    render(berkasPentingEditPage)
    const buttonElement = screen.getAllByRole("button", { name: "Unggah File" })
    userEvent.click(buttonElement[0])

    const title = screen.getByText("Unggah Berkas Penting")
    expect(title).toBeInTheDocument()
})

test("render click unggah on template section in Berkas Penting Page", () => {
    render(berkasPentingEditPage)
    const buttonElement = screen.getAllByRole("button", { name: "Unggah File" })
    userEvent.click(buttonElement[1])

    const title = screen.getByText("Unggah Berkas Penting")
    expect(title).toBeInTheDocument()
})

test("render click unggah on lain-lain section in Berkas Penting Page", () => {
    render(berkasPentingEditPage)
    const buttonElement = screen.getAllByRole("button", { name: "Unggah File" })
    userEvent.click(buttonElement[2])

    const title = screen.getByText("Unggah Berkas Penting")
    expect(title).toBeInTheDocument()
})


test("render confirmation text in pop up unggah in Berkas Penting Page", () => {
    render(berkasPentingEditPage)
    const buttonElement = screen.getAllByRole("button", { name: "Unggah File" })
    userEvent.click(buttonElement[0])

    const confirmText = screen.getByText("Apakah Anda yakin ingin mengunggah berkas ini?")
    expect(confirmText).toBeInTheDocument()
})

test("render input text in pop up unggah in Berkas Penting Page", () => {
    render(berkasPentingEditPage)
    const buttonElement = screen.getAllByRole("button", { name: "Unggah File" })
    userEvent.click(buttonElement[0])

    const input = screen.getByLabelText("Save As")
    expect(input).toBeInTheDocument()
})

test("render label Attachment in pop up unggah in Berkas Penting Page", () => {
    render(berkasPentingEditPage)
    const buttonElement = screen.getAllByRole("button", { name: "Unggah File" })
    userEvent.click(buttonElement[0])

    const Attachment = screen.getByText("Attachment")
    expect(Attachment).toBeInTheDocument()
})

test("render label Save As in pop up unggah in Berkas Penting Page", () => {
    render(berkasPentingEditPage)
    const buttonElement = screen.getAllByRole("button", { name: "Unggah File" })
    userEvent.click(buttonElement[0])
    const saveAs = screen.getByText("Save As")
    expect(saveAs).toBeInTheDocument()

})

test("function cancel handler", () => {
    render(berkasPentingEditPage)
    const buttonElement = screen.getAllByRole("button", { name: "Unggah File" })
    userEvent.click(buttonElement[0])
    const { container } = render(berkasPentingEditPage)
    const buttonElement2 = container.querySelector('[id="cancelUnggah"]')
    userEvent.click(buttonElement2)
})

// test("function upload handler",()=>{
// 
//     render(berkasPentingEditPage)
//     const buttonElement = screen.getAllByRole("button",{name:"Unggah File"})
//     userEvent.click(buttonElement[0])
//     const {container} = render(berkasPentingEditPage)
//     const buttonElement2 = container.querySelector('[id="yesUnggah"]')
//     console.log(buttonElement2)
//     const file = new File(['dummy'], 'values.json', {
//         type: 'application/JSON'})
//       File.prototype.text = jest.fn().mockResolvedValueOnce("dummy")
//     uploadBytes.push = jest.fn(("filepath", file))
//     userEvent.click(buttonElement2)

// })

test("function file handler if file not exist", () => {
    render(berkasPentingEditPage)
    const buttonElement = screen.getAllByRole("button", { name: "Unggah File" })
    userEvent.click(buttonElement[0])
    const { container } = render(berkasPentingEditPage)
    const buttonElement2 = container.querySelector('[id="berkas"]')
    userEvent.upload(buttonElement2)
})

test("function file handler if file exist ", () => {
    render(berkasPentingEditPage)
    const buttonElement = screen.getAllByRole("button", { name: "Unggah File" })
    userEvent.click(buttonElement[0])
    const { container } = render(berkasPentingEditPage)
    const buttonElement2 = container.querySelector('[id="berkas"]')
    const file = new File(['dummy'], 'values.json', {
        type: 'application/JSON'
    })
    File.prototype.text = jest.fn().mockResolvedValueOnce("dummy")
    userEvent.upload(buttonElement2, file)
})

test("function handleDelete run when click Ya in popup delete", () => {
    render(berkasPentingEditPage);

    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
      route: '/berkasPenting/edit/',
      push: jest.fn(),
    }));

    const { container } = render(berkasPentingEditPage);
    const buttonElement2 = container.querySelector('[id="tombolX"]');
    userEvent.click(buttonElement2);
    const buttonElement3 = container.querySelector('[id="tombolYa"]')
    userEvent.click(buttonElement3)
    expect(useRouter).toHaveBeenCalled();
});

test("function handleDelete run when click Tidak in popup", () => {
    render(berkasPentingEditPage);
    const { container } = render(berkasPentingEditPage);
    const buttonElement2 = container.querySelector('[id="tombolX"]');
    userEvent.click(buttonElement2);
    const buttonElement3 = container.querySelector('[id="tombolTidak"]')
    userEvent.click(buttonElement3)
});
