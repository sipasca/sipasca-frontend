

import Navbar from "../components/Navbar/Navbar";  
import { expect, test } from "@jest/globals";
import { render } from "@testing-library/react";
import * as nextRouter from "next/router";
import "@testing-library/jest-dom";

import { library } from "@fortawesome/fontawesome-svg-core";
import { far } from "@fortawesome/free-regular-svg-icons";
import { fas } from "@fortawesome/free-solid-svg-icons";
library.add(far, fas);

import { TermProvider } from "../context/termContext"

// const activeTerm = '2022-2'
// const contextValues = { activeTerm: activeTerm, updateTerm: jest.fn() };
// jest.spyOn(TermContext, 'useTerm').mockImplementation(() => contextValues);

global.fetch = jest.fn(() =>
        Promise.resolve({
            json: () => 
                Promise.resolve({
                    term_aktif: [['2022-2']]
                })
        })
)

test("Route to '/pusatInformasi' (role: Pengguna) activates Pusat Informasi style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/pusatInformasi" }));
  
    const { container } = render(<TermProvider><Navbar role="Pengguna" isAuth={false}/></TermProvider>);
    const styles = container.querySelector("[class*='active']")
    expect(styles).not.toBeNull();
});

test("Route to '/pusatInformasi' (role: Dosen) activates Pusat Informasi style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/pusatInformasi" }));
  
    const { container } = render(<TermProvider><Navbar role="Dosen" isAuth={false}/></TermProvider>);
    const styles = container.querySelector("[class*='active']")
    expect(styles).not.toBeNull();
});

test("Route to '/pusatInformasi' (role: Mahasiswa) activates Pusat Informasi style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/pusatInformasi" }));
  
    const { container } = render(<TermProvider><Navbar role="Mahasiswa" isAuth={false}/></TermProvider>);
    const styles = container.querySelector("[class*='active']")
    expect(styles).not.toBeNull();
});

test("Route to '/pusatInformasi' (role: Staf) activates Pusat Informasi style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/pusatInformasi" }));
  
    const { container } = render(<TermProvider><Navbar role="Staf" isAuth={false}/></TermProvider>);
    const styles = container.querySelector("[class*='active']")
    expect(styles).not.toBeNull();
});

test("Route to '/daftarDosen' (role: Dosen) activates Daftar Dosen style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/daftarDosen" }));
  
    const { container } = render(<TermProvider><Navbar role="Dosen" isAuth={false}/></TermProvider>);
    const styles = container.querySelector("[class*='active']")
    expect(styles).not.toBeNull();
});

test("Route to '/daftarDosen' (role: Mahasiswa) activates Daftar Dosen  style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/daftarDosen" }));
  
    const { container } = render(<TermProvider><Navbar role="Mahasiswa" isAuth={false}/></TermProvider>);
    const styles = container.querySelector("[class*='active']")
    expect(styles).not.toBeNull();
});

test("Route to '/daftarDosen' (role: Staf) activates Daftar Dosen style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/daftarDosen" }));
  
    const { container } = render(<TermProvider><Navbar role="Staf" isAuth={false}/></TermProvider>);
    const styles = container.querySelector("[class*='active']")
    expect(styles).not.toBeNull();
});

test("Route to '/administrasi' (role: Mahasiswa) activates Administrasi style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi" }));
  
    const { container } = render(<TermProvider><Navbar role="Mahasiswa" isAuth={false}/></TermProvider>);
    const styleActivated = container.querySelector("[class*='active']")
    expect(styleActivated).not.toBeNull();
});
  
test("Route to '/administrasi' (role: Dosen) activates Administrasi style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi" }));
  
    const { container } = render(<TermProvider><Navbar role="Dosen" isAuth={false}/></TermProvider>);
    const styleActivated = container.querySelector("[class*='active']")
    expect(styleActivated).not.toBeNull();
});
  
test("Route to '/administrasi' (role: Staf) activates Administrasi style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi" }));
  
    const { container } = render(<TermProvider><Navbar role="Staf" isAuth={false}/></TermProvider>);
    const styleActivated = container.querySelector("[class*='active']")
    expect(styleActivated).not.toBeNull();
});

test("Route to '/logout' activates Logout style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/logout" }));
  
    const { container } = render(<TermProvider><Navbar role="Staf" isAuth={true}/></TermProvider>);
    const styleActivated = container.querySelector("[class*='active']")
    expect(styleActivated).not.toBeNull();
});


test("Route to '/topik/cariTopik' (role: Mahasiswa) activates Cari Topik style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/topik/cariTopik" }));
  
    const { container } = render(<TermProvider><Navbar role="Mahasiswa" isAuth={false}/></TermProvider>);
    const styleActivated = container.querySelector("[class*='active']")
    expect(styleActivated).not.toBeNull();
});


test("Route to '/cariDosen' (role: Mahasiswa) activates Cari Dosen style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/cariDosen" }));
  
    const { container } = render(<TermProvider><Navbar role="Mahasiswa" isAuth={false}/></TermProvider>);
    const styleActivated = container.querySelector("[class*='active']")
    expect(styleActivated).not.toBeNull();
});


test("Route to '/topik/daftarTopik' (role: Dosen) activates Daftar Topik style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/topik/daftarTopik" }));
  
    const { container } = render(<TermProvider><Navbar role="Dosen" isAuth={false}/></TermProvider>);
    const styleActivated = container.querySelector("[class*='active']")
    expect(styleActivated).not.toBeNull();
});



test("Route to '/profil' (role: Mahasiswa) activates Profil style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/profile/profileMahasiswa/" }));
  
    const { container } = render(<TermProvider><Navbar role="Mahasiswa" isAuth={true}/></TermProvider>);
    const styleActivated = container.querySelector("[class*='active']")
    expect(styleActivated).not.toBeNull();
});

test("Route to '/profil' (role: Dosen) activates Profil style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/profile/profileDosen/" }));
  
    const { container } = render(<TermProvider><Navbar role="Dosen" isAuth={true}/></TermProvider>);
    const styleActivated = container.querySelector("[class*='active']")
    expect(styleActivated).not.toBeNull();
});

test("Route to other than '/profil' (role: Mahasiswa) do not activates Profil style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));
  
    const { container } = render(<TermProvider><Navbar role="Mahasiswa" isAuth={true}/></TermProvider>);
    const styleActivated = container.querySelector("[class*='active']")
    expect(styleActivated).toBeNull();
});

test("Route to other than '/' (role: Dosen) do not activates Profil style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));
  
    const { container } = render(<TermProvider><Navbar role="Dosen" isAuth={true}/></TermProvider>);
    const styleActivated = container.querySelector("[class*='active']")
    expect(styleActivated).toBeNull();
});


test("Route to '/login' (role: Pengguna) activates Login style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/loginSSO" }));
  
    const { container } = render(<TermProvider><Navbar role="Pengguna" isAuth={false}/></TermProvider>);
    const styleActivated = container.querySelector("[class*='active']");
    expect(styleActivated).not.toBeNull();
});

test("Route to '/login' (role: Mahasiswa) activates Login style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/loginSSO" }));
  
    const { container } = render(<TermProvider><Navbar role="Mahasiswa" isAuth={false}/></TermProvider>);
    const styleActivated = container.querySelector("[class*='active']");
    expect(styleActivated).not.toBeNull();
});

test("Route to '/login' (role: Dosen) activates Login style", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/loginSSO" }));
  
    const { container } = render(<TermProvider><Navbar role="Dosen" isAuth={false}/></TermProvider>);
    const styleActivated = container.querySelector("[class*='active']");
    expect(styleActivated).not.toBeNull();
});

