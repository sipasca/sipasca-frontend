import PopUp from "../components/PopUp";
import {describe, expect, test} from '@jest/globals'
import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom'

test("PopUp contains Ya", () => {
    render(<PopUp />)
    const popUpYa = screen.getByText("Ya");
    expect(popUpYa).toBeInTheDocument();
})

test("PopUp contains Tidak", () => {
    render(<PopUp />)
    const popUpTidak = screen.getByText("Tidak");
    expect(popUpTidak).toBeInTheDocument();
})

