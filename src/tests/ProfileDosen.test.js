


import {expect, test} from '@jest/globals'
import { render, screen, fireEvent, getByTestId } from "@testing-library/react";
import { JSDOM } from "jsdom";
import '@testing-library/jest-dom';
import * as nextRouter from "next/router";
import { getServerSideProps } from "../pages/profile/profileDosen";
import React, { useState } from "react";
import ProfileDosen from "../pages/profile/profileDosen";
import { GetCookie } from "./Helper";
import userEvent from "@testing-library/user-event";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";

library.add(fas);

// Mocking axios for CSRF Token
const axios = require('axios');
jest.mock('axios');
const csrf = {
    'data': {'csrftoken': 'test'}
}
axios.get.mockResolvedValue(csrf);

// Mocking setting cookie for CSRF Token
const cookie = 'csrftoken=test;';
global.document.cookie = cookie;

nextRouter.useRouter = jest.fn();
nextRouter.useRouter.mockImplementation(() => ({ 
    pathname: "/profile/profileDosen", 
    req: { 
      cookies: { "userID" : cookieUserDosen }
    },
    query: { id: undefined},
    prefetch: async () => {}
}));


const data_daftarProfile = {
  "profile_dosen": [[
      'dummy',
      'dummy',
      'dummy',
      'Available',
      'dummy',
      'dummy',
      'dummy',
      'dummy',
      null,
      'dummy',
      'dummy'
    ]],
  "list_publikasi": [['dummy'],['dummy']],
  "list_url": [['dummy','dummy'],['dummy','dummy']],
  "list_pendidikan": [[ 'dummy']],
  "list_kepakaran": [ [ 'dummy' ] ]
}

const data_daftarProfile_unav = {
  "profile_dosen": [[
      'dummy',
      'dummy',
      'dummy',
      'Unavailable',
      'dummy',
      'dummy',
      'dummy',
      'dummy',
      null,
      'dummy',
      'dummy'
    ]],
  "list_publikasi": [['dummy'],['dummy']],
  "list_url": [['dummy','dummy'],['dummy','dummy']],
  "list_pendidikan": [[ 'dummy']],
  "list_kepakaran": [ [ 'dummy' ] ]
}

const data_daftarTopik = {"list_topik_by_dosen": [[28,'dummy','dummy','dummy']]}

it("check on getServerSideProps with context id undefined", async () => {
  const context = {
      req: { 
          cookies: { "userID" : cookieUserDosen }
      },
      query: { id: undefined}
  };
  global.fetch = jest.fn(() =>
  Promise.resolve({
          json: () => Promise.resolve({})
      })
  );

  await getServerSideProps(context);
});

// Mock cookie in getServerSideProps
const { cookieUserID: cookieUserMahasiswa } = GetCookie('mahasiswa123', 'mahasiswa')
const { cookieUserID: cookieUserDosen } = GetCookie('dosen123', 'dosen')
const { cookieUserID: cookieUserStaf } = GetCookie('staf123', 'staf')

it("check on getServerSideProps with context id", async () => {
  const context = {
      req: { 
          cookies: { "userID" : cookieUserMahasiswa }
      },
      query: { id: "D2432150536"}
  };
  global.fetch = jest.fn(() =>
  Promise.resolve({
          json: () => Promise.resolve({})
      })
  );
  
  await getServerSideProps(context);
});

it("check on getServerSideProps with context id staf", async () => {
  const context = {
      req: { 
          cookies: { "userID" : cookieUserStaf }
      },
      query: { id: "D2432150536"}
  };
  global.fetch = jest.fn(() =>
  Promise.resolve({
          json: () => Promise.resolve({})
      })
  );
  
  await getServerSideProps(context);
});

describe("check on getServerSideProps with context id undefined called fetch 2", () => {
  const context = {
    req: { 
        cookies: { "userID" : cookieUserDosen }
    },
    query: { id: undefined}
  };
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve({})
    })
  );

  beforeEach(() => {
    fetch.mockClear();
  });

  it("should call api", async () => {
    await getServerSideProps(context);
    expect(fetch).toHaveBeenCalledTimes(2)
  });
});

describe("check on getServerSideProps with context id called fetch 2", () => {
  const context = {
    req: { 
        cookies: { "userID" : cookieUserMahasiswa }
    },
    query: { id: "D2432150536"}
  };
  
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve({})
    })
  );

  beforeEach(() => {
    fetch.mockClear();
  });

  it("should call api", async () => {
    await getServerSideProps(context);
    expect(fetch).toHaveBeenCalledTimes(2)
  });
});

describe("check on getServerSideProps with context id called fetch 2 as staff", () => {
  const context = {
    req: { 
        cookies: { "userID" : cookieUserStaf }
    },
    query: { id: "D2432150536"}
  };
  
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve({})
    })
  );

  beforeEach(() => {
    fetch.mockClear();
  });

  it("should call api", async () => {
    await getServerSideProps(context);
    expect(fetch).toHaveBeenCalledTimes(2)
  });
});

test("Profil is in Profile Dosen Pages", () => {
    // Mocking getting userID and role with axios
    const res = {
      data : {
        userID: 'test',
        role: 'dosen'
      }
    }
    axios.get.mockResolvedValue(res);

    // Mocking useState for set role
    const state = 'mahasiswa';
    React.useState = (initial) => [state, () => {}]
    

    render(<ProfileDosen data_daftarProfile={data_daftarProfile} data_daftarTopik = {data_daftarTopik}/>)
    const profil= screen.getByText("| Profil");
    expect(profil).toBeInTheDocument();
})

test("Profil is in Profile Dosen Pages", () => {
  // Mocking getting userID and role with axios
  const res = {
    data : {
      userID: 'test',
      role: 'dosen'
    }
  }
  axios.get.mockResolvedValue(res);

  // Mocking useState for set role
  const state = 'staf';
  React.useState = (initial) => [state, () => {}]
  

  render(<ProfileDosen data_daftarProfile={data_daftarProfile} data_daftarTopik = {data_daftarTopik}/>)
  const profil= screen.getByText("| Profil");
  expect(profil).toBeInTheDocument();
})

test("nama, email, bidang kepakaran, biografi, pendidikan, minat penelitian, publikasi, dan topik is in profile dosen page", () => {
    // Mocking getting userID and role with axios
    const res = {
      data : {
        userID: 'test',
        role: 'dosen'
      }
    }
    axios.get.mockResolvedValue(res);

    // Mocking useState for set role
    const state = 'dosen';
    React.useState = (initial) => [state, () => {}]

    render(<ProfileDosen data_daftarProfile={data_daftarProfile} data_daftarTopik = {data_daftarTopik}/>)
    const nama = screen.getByText("Nama");
    expect(nama).toBeInTheDocument();
    
    const email = screen.getByText("E-Mail");
    expect(email).toBeInTheDocument();

    const bidang = screen.getByText("Bidang Kepakaran");
    expect(bidang).toBeInTheDocument();

    const biografi = screen.getByText("Biografi");
    expect(biografi).toBeInTheDocument();

    const minat = screen.getByText("Minat Penelitian");
    expect(minat).toBeInTheDocument();

    const pendidikan = screen.getByText("Pendidikan");
    expect(pendidikan).toBeInTheDocument();

    const publikasi = screen.getByText("Publikasi");
    expect(publikasi).toBeInTheDocument();

    const topik = screen.getByText("Topik");
    expect(topik).toBeInTheDocument();
})

test("Test for Role Dosen in Profile Dosen Click Switch Status Change Value", async () => {
    // Mocking getting userID and role with axios
    const res = {
      data : {
        userID: 'test',
        role: 'dosen'
      }
    }
    axios.get.mockResolvedValue(res);

    // Mocking useState for set role
    const state = 'dosen';
    React.useState = (initial) => [state, () => {}]

    const { container } = render(<ProfileDosen data_daftarProfile={data_daftarProfile} data_daftarTopik = {data_daftarTopik}/>);
    const changeStatus = getByTestId(container, "switch");
    expect(changeStatus.checked).toBe(true);
    fireEvent.click(changeStatus);
})

test("Test for Profile Dosen Click Switch Change Status Called Function statusHandler", async () => {

    const statusHandler = jest.fn();
    const { container } = render(<input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" data-testid="switch" data-bs-toggle="tooltip" data-bs-placement="top" title="Change Availability Status" onChange={statusHandler}></input>);
    
    const calledFunction = getByTestId(container, "switch");
    fireEvent.click(calledFunction);
    expect(statusHandler).toBeCalled();
})

test("Test for Function handleAction Run when Click Ya in Popup When Status Available", async () => {
  // Mocking getting userID and role with axios
  const res = {
    data : {
      userID: 'test',
      role: 'dosen'
    }
  }
  axios.get.mockResolvedValue(res);

  // Mocking useRouter
  const useRouter = jest.spyOn(require("next/router"), "useRouter");

  useRouter.mockImplementation(() => ({
    route: '/profile/profileDosen/',
    req: { 
      cookies: { "userID" : cookieUserDosen }
    },
    query: { id: undefined},
    push: jest.fn(),
    prefetch: async () => {}
  }));

  const { container } = render(<ProfileDosen data_daftarProfile={data_daftarProfile} data_daftarTopik = {data_daftarTopik}/>);
  const buttonElement2 = getByTestId(container, "switch");
  fireEvent.click(buttonElement2);
  const buttonElement3 = container.querySelector('[id="tombolYa"]')
  fireEvent.click(buttonElement3)
  expect(useRouter).toHaveBeenCalled();
})

test("Test for Function handleAction Run when Click Ya in Popup When Status Unavailable", async () => {
  // Mocking getting userID and role with axios
  const res = {
    data : {
      userID: 'test',
      role: 'dosen'
    }
  }
  axios.get.mockResolvedValue(res);

  // Mocking useRouter
  const useRouter = jest.spyOn(require("next/router"), "useRouter");

  useRouter.mockImplementation(() => ({
    route: '/profile/profileDosen/',
    req: { 
      cookies: { "userID" : cookieUserDosen }
    },
    query: { id: undefined},
    push: jest.fn(),
    prefetch: async () => {}
  }));

  const { container } = render(<ProfileDosen data_daftarProfile={data_daftarProfile_unav} data_daftarTopik = {data_daftarTopik}/>);
  const buttonElement2 = getByTestId(container, "switch");
  fireEvent.click(buttonElement2);
  const buttonElement3 = container.querySelector('[id="tombolYa"]')
  fireEvent.click(buttonElement3)
  expect(useRouter).toHaveBeenCalled();
})

test("user can click button lihat profile selengkapnya", () => {
  const { container } = render(<ProfileDosen data_daftarProfile={data_daftarProfile_unav} data_daftarTopik = {data_daftarTopik}/>)
  const buttonLihatProfile = container.querySelector('[id="lihatProfilSelengkapnya"]')
  userEvent.click(buttonLihatProfile);
});