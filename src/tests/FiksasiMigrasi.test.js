import Fiksasi from "../pages/mataKuliahSpesial/[mataKuliahID]/s/migrasi";
import {expect, test} from '@jest/globals'
import {render, screen, fireEvent } from "@testing-library/react";
import '@testing-library/jest-dom'
import React from "react";
import * as nextRouter from "next/router";
import { migrasiFile } from "../pages/mataKuliahSpesial/[mataKuliahID]/s/migrasi";

import axios from 'axios';
import { getServerSideProps } from '../pages/mataKuliahSpesial/[mataKuliahID]/s/migrasi';
import { async } from "@firebase/util";
jest.mock('axios');

// Mocking getting userID and role with axios
const res = {
  data : {
    userID: 'staf123',
    role: 'staf'
  }
}
axios.get.mockResolvedValue(res);

// Mocking setting cookie for CSRF Token
const cookie = 'csrftoken=test;';
global.document.cookie = cookie;


const allRequestMigrasi = {"detail_migrasi": [[47, "CSGE802098_2022-2", "M9636831678", "2022-2", "2023-1", "REQ", "Choi Chaehun", null,['urlfile1lama'],['urlfile1baru']]]}
const detailMK = {'detail_mk':["CSGE802098_2022-2", "2022-2", "Publikasi Ilmiah", 30, 2, 2, "03.00.12.01-2020", false]}
const requestMigrasiZero = {"detail_migrasi": []};
const mataKuliahID = "CSGE802098_2022-2";
const nextTermExists = {'is_matkul_next_term_exist': true}
const nextTermNotExists = {'is_matkul_next_term_exist': false}

test("Migrasi Pages contains Pengajuan Migrasi title", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/migrasi", query: { mataKuliahID: "CSGE" }}));
    render( <Fiksasi detailMK={detailMK} allRequestMigrasi={allRequestMigrasi} matkulNextTermExists={nextTermExists}/>)
    const title = screen.getByText("Pengajuan Migrasi");
    expect(title).toBeInTheDocument();
})

test("check when there are no request migrasi", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/migrasi", query: { mataKuliahID: "CSGE" }}));
  render( <Fiksasi detailMK={detailMK} allRequestMigrasi={requestMigrasiZero} matkulNextTermExists={nextTermExists}/>)
  const title = screen.getByText("Belum ada mahasiswa yang mengajukan migrasi");
  expect(title).toBeInTheDocument();
})

test("check when matkul next term is not exists", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/migrasi", query: { mataKuliahID: "CSGE" }}));
  render( <Fiksasi detailMK={detailMK} allRequestMigrasi={requestMigrasiZero} matkulNextTermExists={nextTermNotExists}/>)
  const title = screen.getByText("Untuk melakukan migrasi, mata kuliah ini perlu didaftarkan untuk term selanjutnya.");
  expect(title).toBeInTheDocument();
})

test("accept request migrasi successfully", () => {
  const response = {
    'data': {
      'status' : 'SUCCESS',
      'id_mk': 'CSGE802100_2022-2',
      'id_mahasiswa': 'M9636831678'
  }
  }
  const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });

    render( <Fiksasi mataKuliahID={mataKuliahID} detailMK={detailMK} allRequestMigrasi={allRequestMigrasi} matkulNextTermExists={nextTermExists}/>)
    fireEvent.click(screen.getByRole("button", {name:"Terima"}))
    const title = screen.getByText("Pengajuan Migrasi");
    expect(title).toBeInTheDocument();
})

test("reject request migrasi successfully", () => {
  const response = {
    'data': {
      'status' : 'SUCCESS',
      'id_mk': 'CSGE802100_2022-2',
      'id_mahasiswa': 'M9636831678'
  }
  }
  const postRequest = require('../pages/api/post-request');
  postRequest.default = jest.fn(() => {
        return response
    });

    render( <Fiksasi mataKuliahID={mataKuliahID} detailMK={detailMK} allRequestMigrasi={allRequestMigrasi} matkulNextTermExists={nextTermExists}/>)
    fireEvent.click(screen.getByRole("button", {name:"Tolak"}))
    const title = screen.getByText("Pengajuan Migrasi");
    expect(title).toBeInTheDocument();
})

it("check on getServerSideProps with context", async () => {
    const context = {
        query: { mataKuliahID: "CSGE"}
    };
    global.fetch = jest.fn(() =>
    Promise.resolve({
            json: () => Promise.resolve({})
        })
    );
    
    await getServerSideProps(context);
  });

  test("migrate matkul successfully", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ reload: jest.fn(),pathname: "/migrasi", query: { mataKuliahID: "CSGE" }}));
        
    const response = {
      'data': {
        'status' : 'SUCCESS',
        'id_mk': 'CSGE802098_2022-2',
    }
    }
    const postRequest = require('../pages/api/post-request');
      postRequest.default = jest.fn(() => {
          return response
      });
  
      render( <Fiksasi matkulNextTermExists={nextTermNotExists} mataKuliahID={mataKuliahID} detailMK={detailMK} allRequestMigrasi={requestMigrasiZero}/>)
      fireEvent.click(screen.getByRole("button", {name:"Daftarkan Mata Kuliah di Term Selanjutnya"}))
      expect(nextRouter.useRouter).toHaveBeenCalled();
  
    });