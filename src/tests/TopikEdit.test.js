import Edit, { deleteAllDokumenFromStorageHandler, listDeleteFromStorageHandler } from "../pages/topik/edit/[topikID]";
import { expect, test} from '@jest/globals';
import { render, screen, fireEvent } from "@testing-library/react";
import '@testing-library/jest-dom';
import userEvent from "@testing-library/user-event";
import React from "react";
import { getServerSideProps } from "../pages/topik/edit/[topikID]";

const id_topik = 28
const data = {"detail_topik_dosen": [[28, "D1234567890", "topik 1", "deskripsi 1", "prasyarat 1", "prasyarat 2", "prasyarat 3", "belum diambil", "-1"], []]}
const data2 = {"detail_topik_dosen": [[245, "D1234567890", "nyoba edit", "fshhfh", "", "", "", "belum diambil", "3"], [["local/dokumenTopik/245/dokumen_3", "Dokumen Topik 3.pdf", "3"], ["local/dokumenTopik/245/dokumen_1", "quiz tahun lalu.pdf", 1]]]}

const topikEditPage = <Edit id_topik={id_topik} data={data} />
const topikEditPage2 = <Edit id_topik={id_topik} data={data2} />
test("renders edit topik title in edit topik page", () => {
    render(topikEditPage)
    const editTitle = screen.getByText("| Edit Topik");
    expect(editTitle).toBeInTheDocument(); 
})
test("function handleDelete run when click Ya in popup delete", () => {
    render(topikEditPage2)
    const { container } =  render(topikEditPage2);
    const buttonX = container.querySelector('[id="tombolX"]');
    userEvent.click(buttonX);
    const buttonYa = container.querySelector('[id="tombolYa"]')
    userEvent.click(buttonYa)
});
test("function handleDeleteDokumen run when click Tidak in popup", () => {
    render(topikEditPage2)
    const { container } = render(topikEditPage2);
    const buttonX = container.querySelector('[id="tombolX"]');
    userEvent.click(buttonX);
    const buttonTidak= container.querySelector('[id="tombolTidak"]')
    userEvent.click(buttonTidak);
});
describe("getServerSideProps", () => {
    const context = {
        query: { topikID: "245"}
    
    };
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ status:'success' })
      })
    );
  
    beforeEach(() => {
      fetch.mockClear();
    });
  
    it("should call api", async () => {
      await getServerSideProps(context);
      expect(fetch).toHaveBeenCalledTimes(1)
    });
  });

const cookie = 'csrftoken=test;';
global.document.cookie = cookie;

test("Edit topik dosen successfully", async () => {
    // Mocking PostRequest
    const response = {
        'data': {
            'status' : 'Success',
        }
    }
    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });
    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");
    useRouter.mockImplementation(() => ({
      route: '/daftarTopik',
      push: jest.fn(),
    }));
    const { container } = render(topikEditPage)
    const simpanButton = container.querySelector('[id="submit-button"]')
    fireEvent.click(simpanButton);
    expect(useRouter).toHaveBeenCalled();
})
test("function file handler if file exist ", () => {
    // Mocking PostRequest
    const response = {
        'data': {
            'status' : 'Success',
        }
    }
    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });
    // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");
    useRouter.mockImplementation(() => ({
      route: '/daftarTopik',
      push: jest.fn(),
    }));
    const { container } = render(topikEditPage)
    const buttonInputFile= container.querySelector('[id="1"]')
    const file = new File(['dummy'], 'values.json', {
        type: 'application/JSON'
    })
    File.prototype.text = jest.fn().mockResolvedValueOnce("dummy")
    userEvent.upload(buttonInputFile, file)
    const simpanButton = container.querySelector('[id="submit-button"]')
    fireEvent.click(simpanButton);
    expect(useRouter).toHaveBeenCalled();
})
test("function handleDeleteTopik run when click Ya in popup delete", () => {
    render(topikEditPage)
       // Mocking useRouter
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
        route: 'topik/daftarTopik',
        push: jest.fn(),
    }));
    const { container } =  render(topikEditPage);
    const buttonDeleteTopik = container.querySelector('[id="delete-topik-button"]');
    userEvent.click(buttonDeleteTopik);
    const buttonYa = container.querySelector('[id="tombolYa"]')
    userEvent.click(buttonYa);
    expect(useRouter).toHaveBeenCalled();
});
test("function handleDeleteTopik run when click Tidak in popup delete", () => {
    const { container } = render(topikEditPage);
    const buttonDeleteTopik = container.querySelector('[id="delete-topik-button"]');
    userEvent.click(buttonDeleteTopik);
    const buttonTidak = container.querySelector('[id="tombolTidak"]')
    userEvent.click(buttonTidak);
});

test("onchange in Batas Terima tidak terbatas works", () => {
    const { container } =  render(topikEditPage)
    const buttonElement1 = container.querySelector('[id="button1"]')
    expect(buttonElement1).toBeChecked();
    const buttonElement2 = container.querySelector('[id="button2"]')
    expect(buttonElement2).not.toBeChecked();
    userEvent.click(buttonElement2)
});

test("onchange in Batas Terima dibatasi works", () => {
    const { container } =  render(topikEditPage2)
    const buttonElement3 = container.querySelector('[id="button3"]')
    expect(buttonElement3).not.toBeChecked();
    const buttonElement4 = container.querySelector('[id="button4"]')
    const buttonElement5 = container.querySelector('[id="button5"]')
    userEvent.click(buttonElement5)
});

describe("deleteAllDokumenFromStorageHandler", () => {
    it("should delete dokumen", ( )=> {
        const allDokumen = [['dummyurl', 'dummy', 1]]
        deleteAllDokumenFromStorageHandler(allDokumen)
    });
  })

 describe("listDeleteFromStorageHandler", () => {
    it("should get deleted url correctly", ( )=> {
        const listDeletedFile = [1,2]
        const dictDokumen = {'dokumen_1': [['dummyUrl1', 'dummy1', 1],true,null],
                            'dokumen_2':[['dummyUrl2', 'dummy2', 2],true,null],
                            'dokumen_3':[['dummyUrl3', 'dummy3', 3],true,null]
                            }
        const listUrlDelete = listDeleteFromStorageHandler(listDeletedFile,dictDokumen)
        expect(listUrlDelete).toEqual(['dummyUrl1','dummyUrl2'])
    });
   
  })