import React from "react";
import { expect, test } from '@jest/globals'
import { render, screen, fireEvent } from "@testing-library/react";
import '@testing-library/jest-dom'
import FilterMahasiswa from "../components/FilterMahasiswa";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import * as nextRouter from "next/router";

library.add(fas);
const propsData = [{'Nama Mahasiswa': 'Choi Chaehun', 'NPM': '1906293045', 'Status': 'Menunggu persetujuan dosen', urlId: '/detail' }];

jest.spyOn(window.localStorage.__proto__, 'getItem');
    window.localStorage.__proto__.getItem = jest.fn(() => {
        return 'staf'
    });
nextRouter.useRouter = jest.fn();
nextRouter.useRouter.mockImplementation(() => ({ pathname: "/administrasi", query: { mataKuliahID: "CSGE" }}));

test("render List Mahasiswa Title", () => {
    render(<FilterMahasiswa dataMahasiswaMK={propsData}/>)
    const title = screen.getByText("List Mahasiswa");
    expect(title).toBeInTheDocument();
})

test("search name in List Mahasiswa", () => {
    render(<FilterMahasiswa dataMahasiswaMK={propsData}/>)
    const searchButton = screen.getByPlaceholderText("Nama Mahasiswa");
    fireEvent.change(searchButton, {target: {value: 'D'}})
})

test("filter by Semua in List Mahasiswa", () => {
    render(<FilterMahasiswa dataMahasiswaMK={propsData}/>)
    fireEvent.change(screen.getByTestId("validationCustom"), {
        target: { value: "Semua" },
    });
    expect(screen.getByText("Filter by")).toBeInTheDocument();
})

test("filter by Belum mendapatkan pembimbing in List Mahasiswa", () => {
    render(<FilterMahasiswa dataMahasiswaMK={propsData}/>)
    fireEvent.change(screen.getByTestId("validationCustom"), {
        target: { value: "Belum mendapatkan pembimbing" },
    });
    expect(screen.getByText("Filter by")).toBeInTheDocument();
})