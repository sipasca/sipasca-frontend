import {describe, expect, test} from '@jest/globals'
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import '@testing-library/jest-dom'
import Bimbingan from "../pages/mataKuliahSpesial/[mataKuliahID]/d/bimbingan/index";
import * as nextRouter from "next/router";
import { library } from "@fortawesome/fontawesome-svg-core";
import React from "react";
import { getServerSideProps } from "../pages/mataKuliahSpesial/[mataKuliahID]/d/bimbingan/index";

import { GetCookie } from './Helper';
import axios from 'axios';
jest.mock('axios');

import { fas } from "@fortawesome/free-solid-svg-icons";
library.add(fas);

nextRouter.useRouter = jest.fn();
nextRouter.useRouter.mockImplementation(() => ({ pathname: "/mataKuliahSpesial/CSGE802097/d/bimbingan", query: { mataKuliahID: "CSGE" }, prefetch: async () => {}}));

const dummyData = {
    'list_status_topik_mahasiswa':[],
    'daftarCalon':[],
    'daftarMahasiswa':[],
    'mataKuliahID' : 'dummy',
    'namaMK' : 'dummy'
}

const dummyMenungguPersetujuan = {
    'list_status_topik_mahasiswa':[['dummyNama','dummy','dummyTopik','dummyStatus','dummyNPM']],
    'daftarCalon':[],
    'daftarMahasiswa':[],
    'mataKuliahID' : 'dummy',
    'namaMK' : 'dummy'
}

const dummyDisetujui = {
    'list_status_topik_mahasiswa':[],
    'daftarCalon':[['dummySetuju','dummy','dummyTopikSetuju','dummyStatus','dummyNPM']],
    'daftarMahasiswa':[],
    'mataKuliahID' : 'dummy',
    'namaMK' : 'dummy'
}

const dummyBimbingan = {
    'list_status_topik_mahasiswa':[],
    'daftarCalon':[],
    'daftarMahasiswa':[['dummyNamaBimbingan','dummy','dummyTopikBimbingan','dummyStatus','dummyNPM']],
    'mataKuliahID' : 'dummy',
    'namaMK' : 'dummy'
}

const dummyAll = {
    'list_status_topik_mahasiswa':[['dummyNama','dummy','dummyTopik','dummyStatus','dummyNPM']],
    'daftarCalon':[['dummyNama1','dummy1','dummyTopik1','dummyStatus1','dummyNPM1']],
    'daftarMahasiswa':[['dummyNama2','dummy2','dummyTopik2','dummyStatus2','dummyNPM2']],
    'mataKuliahID' : 'dummy',
    'namaMK' : 'dummy'
}

const dummyMatkul = {
    'list_status_topik_mahasiswa':[['dummyNama','dummy','dummyTopik','dummyStatus','dummyNPM']],
    'daftarCalon':[['dummyNama1','dummy1','dummyTopik1','dummyStatus1','dummyNPM']],
    'daftarMahasiswa':[['dummyNama2','dummy2','dummyTopik2','dummyStatus2','dummyNPM']],
    'mataKuliahID' : 'dummy',
    'namaMK' : 'dummy'
}

const { cookieUserID: cookieUserDosen } = GetCookie('dosen123', 'dosen')

describe("getServerSideProps", () => {
    const context = {
        req: { 
            cookies: { "userID" : cookieUserDosen }
        },
        query: { mataKuliahID: "CSGE802097"}
    };

    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ list_status_topik_mahasiswa:[['dummyNama','dummy','dummyTopik','dummyStatus']], detail_mk : [["CSGE802097", "2022-2", "Publikasi Ilmiah", 30, 2, 2, "03.00.12.01-2020", false]], mataKuliahID : 'dummy',
        namaMK : 'dummy'})
      })
    );
  
    beforeEach(() => {
      fetch.mockClear();
    });
  
    it("should call api", async () => {
      await getServerSideProps(context);
      expect(fetch).toHaveBeenCalledWith("https://simkuspa-backend.herokuapp.com/daftar/Menunggu persetujuan dosen/dosen123/CSGE802097/");
      expect(fetch).toHaveBeenCalledWith("https://simkuspa-backend.herokuapp.com/daftar/Disetujui dosen pembimbing/dosen123/CSGE802097/");
      expect(fetch).toHaveBeenCalledWith("https://simkuspa-backend.herokuapp.com/daftar/Masa bimbingan/dosen123/CSGE802097/");
    });
});

// Mocking getting userID and role with axios
const res = {
    data : {
      userID: 'test',
      role: 'dosen'
    }
  }
axios.get.mockResolvedValue(res);

test("Page Contains Subjudul Daftar Permohonan", () => {
    render(<Bimbingan data={dummyData} />)
    const daftarPermohonan = screen.getByText("Daftar Permohonan Bimbingan");
    expect(daftarPermohonan).toBeInTheDocument();
    
    const daftarCalon = screen.getByText("Daftar Calon Mahasiswa Bimbingan");
    expect(daftarCalon).toBeInTheDocument();

    const daftarMahasiswa = screen.getByText("Daftar Mahasiswa Bimbingan");
    expect(daftarMahasiswa).toBeInTheDocument();
})

test("Page when all data length is 0", () => {
    render(<Bimbingan data={dummyData} />)

    const daftarPermohonanText = screen.getByText("Belum Ada Permohonan Bimbingan");
    expect(daftarPermohonanText).toBeInTheDocument();

    const daftarCalonText = screen.getByText("Belum Ada Calon Mahasiswa Bimbingan");
    expect(daftarCalonText).toBeInTheDocument();

    const daftarMahasiswaText = screen.getByText("Belum Ada Mahasiswa Bimbingan");
    expect(daftarMahasiswaText).toBeInTheDocument();
})

test("Page when only Data Permohonan exists", () => {
    render(<Bimbingan data={dummyMenungguPersetujuan} />)

    const nama = screen.getByText("dummyNama");
    expect(nama).toBeInTheDocument();

    const NPM = screen.getByText("dummyNPM");
    expect(NPM).toBeInTheDocument();

    const topik = screen.getByText("dummyTopik");
    expect(topik).toBeInTheDocument();

    const status = screen.getByText("dummyStatus");
    expect(status).toBeInTheDocument();

    const daftarCalonText = screen.getByText("Belum Ada Calon Mahasiswa Bimbingan");
    expect(daftarCalonText).toBeInTheDocument();

    const daftarMahasiswaText = screen.getByText("Belum Ada Mahasiswa Bimbingan");
    expect(daftarMahasiswaText).toBeInTheDocument();
})

test("Page when only Data Calon Mahasiswa exists", () => {
    render(<Bimbingan data={dummyDisetujui} />)

    const nama = screen.getByText("dummySetuju");
    expect(nama).toBeInTheDocument();

    const NPM = screen.getByText("dummyNPM");
    expect(NPM).toBeInTheDocument();

    const topik = screen.getByText("dummyTopikSetuju");
    expect(topik).toBeInTheDocument();

    const status = screen.getByText("dummyStatus");
    expect(status).toBeInTheDocument();

    const daftarPermohonanText = screen.getByText("Belum Ada Permohonan Bimbingan");
    expect(daftarPermohonanText).toBeInTheDocument();

    const daftarMahasiswaText = screen.getByText("Belum Ada Mahasiswa Bimbingan");
    expect(daftarMahasiswaText).toBeInTheDocument();
})

test("Page when only Data Mahasiswa exists", () => {
    render(<Bimbingan data={dummyBimbingan} />)

    const nama = screen.getByText("dummyNamaBimbingan");
    expect(nama).toBeInTheDocument();

    const NPM = screen.getByText("dummyNPM");
    expect(NPM).toBeInTheDocument();

    const topik = screen.getByText("dummyTopikBimbingan");
    expect(topik).toBeInTheDocument();

    const status = screen.getByText("dummyStatus");
    expect(status).toBeInTheDocument();

    const daftarPermohonanText = screen.getByText("Belum Ada Permohonan Bimbingan");
    expect(daftarPermohonanText).toBeInTheDocument();

    const daftarCalonText = screen.getByText("Belum Ada Calon Mahasiswa Bimbingan");
    expect(daftarCalonText).toBeInTheDocument();
})

test("Page when All Data exists", () => {
    render(<Bimbingan data={dummyAll} />)

    const nama = screen.getByText("dummyNama");
    expect(nama).toBeInTheDocument();

    const nama1 = screen.getByText("dummyNama1");
    expect(nama1).toBeInTheDocument();

    const nama2 = screen.getByText("dummyNama2");
    expect(nama2).toBeInTheDocument();

    const NPM = screen.getByText("dummyNPM");
    expect(NPM).toBeInTheDocument();

    const NPM1 = screen.getByText("dummyNPM");
    expect(NPM1).toBeInTheDocument();

    const NPM2 = screen.getByText("dummyNPM");
    expect(NPM2).toBeInTheDocument();

    const topik = screen.getByText("dummyTopik");
    expect(topik).toBeInTheDocument();

    const topik1 = screen.getByText("dummyTopik1");
    expect(topik1).toBeInTheDocument();

    const topik2 = screen.getByText("dummyTopik2");
    expect(topik2).toBeInTheDocument();

    const status = screen.getByText("dummyStatus");
    expect(status).toBeInTheDocument();

    const status1 = screen.getByText("dummyStatus1");
    expect(status1).toBeInTheDocument();

    const status2 = screen.getByText("dummyStatus2");
    expect(status2).toBeInTheDocument();
})

test("Detail button run when it is clicked", () => {
    render(<Bimbingan data={dummyMenungguPersetujuan} />)
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
        pathname: "/mataKuliahSpesial/CSGE802097/d/bimbingan/detail", 
        query: { mataKuliahID: "CSGE" },
        push: jest.fn(),
    }));

    const { container } =  render(<Bimbingan data={dummyMenungguPersetujuan} />);
    const buttonDetail = container.querySelector('[data-test-id="button-detail"]');
    userEvent.click(buttonDetail);
    expect(useRouter).toHaveBeenCalled();
});
