import React from "react";
import { expect, test } from '@jest/globals'
import { render, screen} from "@testing-library/react";
import '@testing-library/jest-dom'
import userEvent from "@testing-library/user-event";
import Dokumen from "../components/Dokumen";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import storage from "../components/storage";
library.add(fas);
import { ref, getDownloadURL } from "firebase/storage";

test("nama appear in dokumen", () => {
  const { container } = render(<Dokumen url='production/berkasPenting/DummyFileForTestingPurpose' nama='Test' />)
    const nama = screen.getByText("Test");
    expect(nama).toBeInTheDocument();
})

test("user can click the file to download", () => {
    const { container } = render(<Dokumen url='production/berkasPenting/DummyFileForTestingPurpose' nama='Test' />)
    const buttonElement2 = container.querySelector('[data-testid="url"]')
    userEvent.click(buttonElement2);
});

test("get firebase link when click the file", async() => {
    const { container } = render(<Dokumen url='production/berkasPenting/DummyFileForTestingPurpose' nama='Test' />)
    const buttonElement2 = container.querySelector('[data-testid="url"]')
    userEvent.click(buttonElement2);
    const downloadurl = await getDownloadURL(ref(storage,'production/berkasPenting/DummyFileForTestingPurpose'));
    expect(downloadurl).toEqual("https://firebasestorage.googleapis.com/v0/b/simkuspa.appspot.com/o/production%2FberkasPenting%2FDummyFileForTestingPurpose?alt=media&token=15c796d3-0c46-4480-8d1b-149d434c525d")
});