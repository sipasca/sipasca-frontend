import Navbar from "../components/Navbar/Navbar"
import { expect, test } from "@jest/globals";
import { render, screen, fireEvent } from "@testing-library/react";
import * as nextRouter from "next/router";

import "@testing-library/jest-dom";
import { library } from "@fortawesome/fontawesome-svg-core";
import { far } from "@fortawesome/free-regular-svg-icons";
import { fas } from "@fortawesome/free-solid-svg-icons";
library.add(far, fas);

import * as TermContext from "../context/termContext"

const activeTerm = '2022-2'
const contextValues = { activeTerm: activeTerm, updateTerm: jest.fn() };
jest.spyOn(TermContext, 'useTerm').mockImplementation(() => contextValues);

test("Navbar contains SIMKUSPA for all roles", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Navbar role="Mahasiswa" isAuth={false} />);
  const title = screen.getByText("SIPASCA");
  expect(title).toBeInTheDocument();

  render(<Navbar role="Dosen" isAuth={false}/>);
  expect(title).toBeInTheDocument();

  render(<Navbar role="Staf" isAuth={false}/>);
  expect(title).toBeInTheDocument();

  render(<Navbar role="Pengguna" isAuth={false} />);
  expect(title).toBeInTheDocument();
});

test("Navbar with role: Pengguna contains 'Pusat Informasi'", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Navbar role="Pengguna" isAuth={false}/>);

  const pusatInformasi = screen.getByText("Pusat Informasi");
  expect(pusatInformasi).toBeInTheDocument();
});

test("Navbar with role: Mahasiswa contains 'Cari Topik', 'Cari Dosen', 'Administrasi', 'Pusat Informasi'", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Navbar role="Mahasiswa" isAuth={false}/>);

  const cariDosen = screen.getByText("Cari Dosen");
  const cariTopik = screen.getByText("Cari Topik");
  const daftarDosen = screen.getByText("Daftar Dosen");
  const administrasi = screen.getByText("Administrasi");
  const pusatInformasi = screen.getByText("Pusat Informasi");

  expect(cariDosen).toBeInTheDocument();
  expect(cariTopik).toBeInTheDocument();
  expect(daftarDosen).toBeInTheDocument();
  expect(administrasi).toBeInTheDocument();
  expect(pusatInformasi).toBeInTheDocument();
});

test("Navbar with role Dosen contains: 'Daftar Topik', 'Administrasi', 'Pusat Informasi'", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Navbar role="Dosen" isAuth={false}/>);
  const daftarDosen = screen.getByText("Daftar Dosen");
  const daftarTopik = screen.getByText("Daftar Topik");
  const administrasi = screen.getByText("Administrasi");
  const pusatInformasi = screen.getByText("Pusat Informasi");

  expect(daftarDosen).toBeInTheDocument();
  expect(daftarTopik).toBeInTheDocument();
  expect(administrasi).toBeInTheDocument();
  expect(pusatInformasi).toBeInTheDocument();
});

test("Navbar with role Staf contains: 'Administrasi', 'Pusat Informasi'", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Navbar role="Staf" isAuth={false}/>);
  const administrasi = screen.getByText("Administrasi");
  const daftarDosen = screen.getByText("Daftar Dosen");
  const pusatInformasi = screen.getByText("Pusat Informasi");

  expect(daftarDosen).toBeInTheDocument();
  expect(administrasi).toBeInTheDocument();
  expect(pusatInformasi).toBeInTheDocument();
});

test("Icon user appear in navbar", () => {
  const { container } = render(<Navbar role="Pengguna" isAuth={false}/>);
  const icon = container.querySelector("[class*='fa-user']")
  expect(icon).not.toBeNull()
})

test("Icon logout appear in navbar", () => {
  const { container } = render(<Navbar role="Mahasiswa" isAuth={true}/>);
  const icon = container.querySelector("[class*='icon']")
  expect(icon).not.toBeNull()
})

test("Logout works", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/profil" }));

  const { container } = render(<Navbar role="Dosen" isAuth={true}/>);
  const logout = container.querySelector("[id='logout']")
  fireEvent.click(logout);
})




