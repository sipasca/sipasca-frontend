import { sign } from "jsonwebtoken";

export function GetCookie(id, role) {
    // Test for getting user and role from cookie
    const secret = process.env.SECRET

    const cookieUserID = sign(
    {
        exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24, // 1 day
        userID: id
    },
    secret
    );

    const cookieRole = sign(
    {
        exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24,
        role: role
    },
    secret
    );

    return { cookieUserID, cookieRole };
}