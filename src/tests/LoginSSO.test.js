import LoginSSO, { getStaticProps } from '../pages/loginSSO';
import handleSSO from '../pages/api/handleSSO';
import SSO from '../pages/api/auth/SSO';

import * as nextRouter from "next/router";

import { expect, test} from '@jest/globals'
import { render, screen, fireEvent } from "@testing-library/react";
import '@testing-library/jest-dom'

// Mocking axios for CSRF Token
const axios = require('axios');
jest.mock('axios');
const csrf = {
    'data': {'csrftoken': 'test'}
}
axios.get.mockResolvedValue(csrf);

// Mocking setting cookie for CSRF Token
const cookie = 'csrftoken=test;';
global.document.cookie = cookie;

test("renders simkuspa title in Login SSO", () => {
    render(<LoginSSO />)
    const simkuspaTittle = screen.getByText("SIPASCA");
    expect(simkuspaTittle).toBeInTheDocument();
})

test("login SSO works", () => {
    const wndOpen = jest.spyOn(window, 'open')
    wndOpen.mockImplementation((str) => jest.fn())

    const wndEventListener = jest.spyOn(window, 'addEventListener')
    wndEventListener.mockImplementation(() => Promise.resolve({
        data: 'SSO'
    }))

    render(<LoginSSO />)
    const loginButton = screen.getAllByRole("button");
    fireEvent.click(loginButton[0]);
})

test("go to login admin works", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ push: jest.fn() }));

    render(<LoginSSO />)
    const loginButton = screen.getAllByRole("button");
    fireEvent.click(loginButton[1]);
})

test("handle SSO successfully", () => {
    const req = {
        query: {
            ticket: 'ticket'
        }
    }
    
    const res = {
        status: (statusCode) => ({
            send: (str) => jest.fn(),
            end: () => jest.fn()
        })
    }

    global.fetch = jest.fn(() =>
    Promise.resolve({
            text: () => Promise.resolve(`
                <cas:serviceResponse xmlns:cas='http://www.yale.edu/tp/cas'>
                    <cas:authenticationSuccess>
                        <cas:user>user.mahasiswa</cas:user>
                        <cas:attributes>
                                        <cas:ldap_cn>User Mahasiswa</cas:ldap_cn>
                                        <cas:kd_org>09.00.12.01</cas:kd_org>
                                        <cas:peran_user>staff</cas:peran_user>
                                        <cas:nama>User Mahasiswai</cas:nama>
                                        <cas:npm>1112223334</cas:npm>
                        </cas:attributes>
                    </cas:authenticationSuccess>
                </cas:serviceResponse>
            `)
        })
    );

    handleSSO(req, res)
})

test("set cookie from SSO successfully", () => {
    const req = {
        body: {
            userID: 'userID',
            role: 'role'
        },
    }
    
    const res = {
        setHeader: (str, ser) => jest.fn(),
        status: (statusCode) => ({
            json: (obj) => jest.fn(),
            end: () => jest.fn()
        })
    }

    SSO(req, res);
})

it("check on getStaticProps", async () => {
    const res = await getStaticProps();
    const originSite = res.props.originSite;
    expect(originSite).toContain('localhost:3000')
});