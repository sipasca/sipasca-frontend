import Rekap from '../pages/mataKuliahSpesial/[mataKuliahID]/s/rekap';
import {expect, test} from '@jest/globals'
import {render, screen } from "@testing-library/react";
import '@testing-library/jest-dom'
import React from "react";
import * as nextRouter from "next/router";
import userEvent from "@testing-library/user-event"
import { getServerSideProps } from "../pages/mataKuliahSpesial/[mataKuliahID]/s/rekap";

import FileSaver from "file-saver";
jest.mock('file-saver', ()=>({saveAs: jest.fn()}))
global.Blob = function (content, options){return  ({content, options})}

const detailMK_data = {'detail_mk':["CSGE802097", "2022-2", "Publikasi Ilmiah"]}
const data_daftarMahasiswaMK = {'list_all_mahasiswa_mk':[["M1906293046", "Choi Chaehun", "1906293045", "Choi Chaehun, S.Kom., M.Sc., Ph.D.", "Masa bimbingan", "2022-05-20T16:11:22.798"]]}
const rekapPage = <Rekap mataKuliahID={"mataKuliahID"} detailMK_data={detailMK_data} data_daftarMahasiswaMK={data_daftarMahasiswaMK}/>

import axios from 'axios';
jest.mock('axios');

// Mocking getting userID and role with axios
const res = {
  data : {
    userID: 'staf123',
    role: 'staf'
  }
}
axios.get.mockResolvedValue(res);

test("Rekap Pages contains List mahasiswa title", () => {
    jest.spyOn(window.localStorage.__proto__, 'getItem');
    window.localStorage.__proto__.getItem = jest.fn(() => {
        return 'staf'
    });
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/rekap", query: { mataKuliahID: "CSGE" }}));
    render(rekapPage)
    const title = screen.getByText("List Mahasiswa");
    expect(title).toBeInTheDocument();
})

test("Unduh Rekap Mahasiswa in Rekap Pages", () => {
    jest.spyOn(window.localStorage.__proto__, 'getItem');
    window.localStorage.__proto__.getItem = jest.fn(() => {
        return 'staf'
    });
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/rekap", query: { mataKuliahID: "CSGE" }}));    
    const { container } =  render(rekapPage)
    const buttonUnduhRekap = container.querySelector('[id="unduhRekap"]')
    userEvent.click(buttonUnduhRekap)
    expect(FileSaver.saveAs).toHaveBeenCalledWith(
        {content: [new ArrayBuffer()], options: { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8' }}, 
        'Rekap_Mahasiswa_Publikasi Ilmiah.xlsx'
        )
})

describe("getServerSideProps", () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ status:'success' })
      })
    );
  
    beforeEach(() => {
      fetch.mockClear();
    });
  
    it("should call api", async () => {
        const context = {
            query: { mataKuliahID: 'mataKuliahID'}
        };
      await getServerSideProps(context);
      expect(fetch).toHaveBeenCalledTimes(2)
    });
  });



