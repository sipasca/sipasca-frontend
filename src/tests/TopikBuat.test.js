import Buat from "../pages/topik/buat";
import {expect, test} from '@jest/globals'
import {render, screen, fireEvent } from "@testing-library/react";
import '@testing-library/jest-dom'
import React from "react";
import userEvent from "@testing-library/user-event"

const axios = require('axios');
jest.mock('axios');
const result = {
    'data': {'csrftoken': 'test'},
    data: {
        userID: '1234567890'
    }
}
axios.get.mockResolvedValue(result);

const cookie = 'csrftoken=test;';
global.document.cookie = cookie;

test("Buat Topik Pages contains title", () => {
    render(<Buat />)
    const title= screen.getByText("| Buat Topik");
    expect(title).toBeInTheDocument();
})

test("Buat Topik Pages contains 'Deskripsi Topik'", () => {
    render(<Buat />)
    const deskripsiTopik = screen.getByText("Deskripsi Topik");
    expect(deskripsiTopik).toBeInTheDocument();
})

test("Buat Topik Pages contains 'Tambahkan Prasyarat'", () => {
    render(<Buat />)
    const tambahkanPrasyarat = screen.getByText("Tambahkan Prasyarat");
    expect(tambahkanPrasyarat).toBeInTheDocument();
})

test("Buat Topik Pages contains 'Tambahkan Dokumen Pendukung'", () => {
    render(<Buat />)
    const tambahkanDokumenPendukung = screen.getByText("Tambahkan Dokumen Pendukung");
    expect(tambahkanDokumenPendukung).toBeInTheDocument();
})

test("Buat Topik Pages contains title Button", () => {
    render(<Buat />)
    const button = screen.getByRole("button");
    expect(button).toBeInTheDocument()
})

test("Buat topik dosen successfully", async () => {
    const response = {
        'data': {
            'status' : 'Success',
        }
    }
    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });
    const useRouter = jest.spyOn(require("next/router"), "useRouter");
    useRouter.mockImplementation(() => ({
      route: '/daftarTopik',
      push: jest.fn(),
    }));
    render(<Buat />)
    const simpanButton = screen.getByRole("button");
    fireEvent.click(simpanButton);
    expect(useRouter).toHaveBeenCalled();
})
test("function file handler if file exist ", () => {
    const response = {
        'data': {
            'status' : 'Success',
        }
    }
    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });
    const useRouter = jest.spyOn(require("next/router"), "useRouter");
    useRouter.mockImplementation(() => ({
      route: '/daftarTopik',
      push: jest.fn(),
    }));
    const { container } = render(<Buat />)
    const buttonInputFile= container.querySelector('[id="dokumen1"]')
    const file = new File(['dummy'], 'values.json', {
        type: 'application/JSON'
    })
    File.prototype.text = jest.fn().mockResolvedValueOnce("dummy")
    userEvent.upload(buttonInputFile, file)
    const simpanButton = screen.getByRole("button");
    fireEvent.click(simpanButton);
    expect(useRouter).toHaveBeenCalled();
})

const result2 = {
    'data': {'csrftoken': 'test'},
    data: {
        userID: ['1234567890']
    }
}
axios.get.mockResolvedValue(result2);
test("Buat Topik Pages contains title login without SSO", () => {
    render(<Buat />)
    const title= screen.getByText("| Buat Topik");
    expect(title).toBeInTheDocument();
})