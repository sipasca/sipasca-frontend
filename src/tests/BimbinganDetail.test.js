
import {describe, expect, test} from '@jest/globals'
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import '@testing-library/jest-dom'
import Detail from "../pages/mataKuliahSpesial/[mataKuliahID]/d/bimbingan/detail/index";
import * as nextRouter from "next/router";
import { library } from "@fortawesome/fontawesome-svg-core";
import React from "react";
import { getServerSideProps } from "../pages/mataKuliahSpesial/[mataKuliahID]/d/bimbingan/detail/index";

import axios from 'axios';
jest.mock('axios');

import { fas } from "@fortawesome/free-solid-svg-icons";
library.add(fas);

import * as TermContext from "../context/termContext"

const activeTerm = '2022-2'
const contextValues = { activeTerm: activeTerm };
jest.spyOn(TermContext, 'useTerm').mockImplementation(() => contextValues);

nextRouter.useRouter = jest.fn();
nextRouter.useRouter.mockImplementation(() => ({ 
    pathname: "/mataKuliahSpesial/CSGE802097/d/bimbingan", 
    query: { mataKuliahID: "CSGE" },
    prefetch: async () => {}
}));


const dataTopik ={"topik_mahasiswa": [[1, "Test", "Test","2022-12-15","motivasi"]]}
const detailMKData = {"detail_mk": ["CSGE802097", "2022-2", "Publikasi Ilmiah", 29, 2, 2, "03.00.12.01-2020", false]}
const dataProfileMahasiswa = {"profile_mahasiswa": [["M9636831678", "Choi Chaehun", "cachochaehun@gmail.com", "Magister Ilmu Komputer", "1906293045", null, null, null, null]]}
const status1 = 'Menunggu persetujuan dosen'
const status2 = 'Disetujui dosen pembimbing'

const pageStatusMenunggu = <Detail dataTopik = {dataTopik} dataProfileMahasiswa = {dataProfileMahasiswa} detailMKData = {detailMKData} status={status1}/>
const pageStatusNotMenunggu = <Detail dataTopik = {dataTopik} dataProfileMahasiswa = {dataProfileMahasiswa} detailMKData = {detailMKData} status={status2}/>

describe("getServerSideProps", () => {
    const context = {
        query: { mataKuliahID: "CSGE802097", mahasiswaID: "M9636831678", status:'Menunggu persetujuan dosen'}
    };

    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ status: 'success'})
      })
    );
  
    beforeEach(() => {
      fetch.mockClear();
    });
  
    it("should call api", async () => {
      await getServerSideProps(context);
      expect(fetch).toHaveBeenCalledTimes(3);   
    });
});

// Mocking getting userID and role with axios
const res = {
    data : {
      userID: 'test',
      role: 'dosen'
    }
  }
axios.get.mockResolvedValue(res);

test("Page contains nama mata kuliah", () => {
    render(pageStatusMenunggu)
    const namaMataKuliah = screen.getByText('| Mata Kuliah Publikasi Ilmiah');
    expect(namaMataKuliah).toBeInTheDocument()
})

test("Page contains button terima when status menunggu persetujuan dosen", () => {
    const { container } =  render(pageStatusMenunggu);
    const buttonTerima = container.querySelector('[id="terima"]');
    expect(buttonTerima).toBeInTheDocument()
})

test("Page contains button tolak when status menunggu persetujuan dosen", () => {
    const { container } =  render(pageStatusMenunggu);
    const buttonTolak = container.querySelector('[id="tolak"]');
    expect(buttonTolak).toBeInTheDocument()
})

test("Page doesn't contain button terima when status not menunggu persetujuan dosen", () => {
    const { container } =  render(pageStatusNotMenunggu);
    const buttonTerima = container.querySelector('[id="terima"]');
    expect(buttonTerima).toBeNull()
})

test("Page doesn't contain button tolak when status not menunggu persetujuan dosen", () => {
    const { container } =  render(pageStatusNotMenunggu);
    const buttonTolak= container.querySelector('[id="tolak"]');
    expect(buttonTolak).toBeNull()
})

test("Accept run when tombol Ya is clicked", () => {
    render(pageStatusMenunggu)
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
        pathname: "/mataKuliahSpesial/CSGE802097/d/bimbingan/", 
        query: { mataKuliahID: "CSGE802097" },
        push: jest.fn(),
    }));

    const { container } =  render(pageStatusMenunggu);
    const buttonTerima = container.querySelector('[id="terima"]');
    userEvent.click(buttonTerima);
    const buttonYa = container.querySelector('[id="tombolYa"]')
    userEvent.click(buttonYa)
    expect(useRouter).toHaveBeenCalled();
});

test("Reject run when tombol Ya is clicked", () => {
    // Mocking PostRequest
    const response = {
        'data': {
            'status' : 'SUCCESS',
        }
    }

    const postRequest = require('../pages/api/post-request');
    postRequest.default = jest.fn(() => {
        return response
    });

    render(pageStatusMenunggu)
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
        pathname: "/mataKuliahSpesial/CSGE802097/d/bimbingan/", 
        query: { mataKuliahID: "CSGE802097" },
        push: jest.fn(),
    }));

    const { container } =  render(pageStatusMenunggu);
    const buttonTolak = container.querySelector('[id="tolak"]');
    userEvent.click(buttonTolak);
    const buttonYa = container.querySelector('[id="submitReject"]')
    userEvent.click(buttonYa)
    expect(useRouter).toHaveBeenCalled();
});

test("user can cancel accept", () => {
    const { container } =  render(pageStatusMenunggu);
    const buttonTerima = container.querySelector('[id="terima"]');
    userEvent.click(buttonTerima);
    const buttonTidak = container.querySelector('[id="tombolTidak"]')
    userEvent.click(buttonTidak)
});

test("user can cancel reject", () => {
    const { container } =  render(pageStatusMenunggu);
    const buttonTolak = container.querySelector('[id="tolak"]');
    userEvent.click(buttonTolak);
    const buttonTidak = container.querySelector('[id="cancelReject"]')
    userEvent.click(buttonTidak)
});

test("user can see detail profile of mahasiswa", () => {
    render(pageStatusMenunggu)
    const useRouter = jest.spyOn(require("next/router"), "useRouter");

    useRouter.mockImplementation(() => ({
        pathname: "/profile/profileMahasiswa", 
        query: { id: "M9636831678" },
        push: jest.fn(),
    }));

    const { container } =  render(pageStatusMenunggu);
    const lihatProfil = container.querySelector('[id="lihatProfilSelengkapnya"]');
    userEvent.click(lihatProfil)
    expect(useRouter).toHaveBeenCalled();
});
