import { expect, test } from '@jest/globals'
import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom';
import userEvent from "@testing-library/user-event"
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import EditProfileMahasiswa from '../pages/profile/editProfileMahasiswa';

library.add(fas);
import { getServerSideProps } from "../pages/profile/editProfileMahasiswa";
import { GetCookie } from "./Helper";

// Mock cookie in getServerSideProps
const { cookieUserID: cookieUserMahasiswa } = GetCookie('mahasiswa123', 'mahasiswa')

const profile_mahasiswa = {"profile_mahasiswa": [["M9636831678", "Choi Chaehun", "caturyudishtira@gmail.com", "Magister Ilmu Komputer", "1906293045", null, null, null]]}
const edit_profile_page = <EditProfileMahasiswa data = {profile_mahasiswa} />

test("renders title in edit profile mahasiswa Page", () => {
    render(edit_profile_page)
    const title = screen.getByText("| Profil");
    expect(title).toBeInTheDocument();
  })

it("check on getServerSideProps with context", async () => {
    const context = {
        req: { 
            cookies: { "userID" : cookieUserMahasiswa }
        },
    };
    global.fetch = jest.fn(() =>
    Promise.resolve({
            json: () => Promise.resolve({})
        })
    );
    
    await getServerSideProps(context);
  });

test("function file handler if file not exist", () => {
    render(edit_profile_page)
    const buttonElement = screen.getAllByRole("button", { name: "Change File" })
    userEvent.click(buttonElement[0])
    const { container } = render(edit_profile_page)
    const buttonElement2 = container.querySelector('[id="berkas"]')
    userEvent.upload(buttonElement2)
})

test("function file handler if file exist ", () => {
    render(edit_profile_page)
    const buttonElement = screen.getAllByRole("button", { name: "Change File" })
    userEvent.click(buttonElement[0])
    const { container } = render(edit_profile_page)
    const buttonElement2 = container.querySelector('[id="berkas"]')
    const file = new File(['dummy'], 'values.json', {
        type: 'application/JSON'
    })
    File.prototype.text = jest.fn().mockResolvedValueOnce("dummy")
    userEvent.upload(buttonElement2, file)
})

test("function cancel handler", () => {
    render(edit_profile_page)
    const buttonElement = screen.getAllByRole("button", { name: "Change File" })
    userEvent.click(buttonElement[0])
    const { container } = render(edit_profile_page)
    const buttonElement2 = container.querySelector('[id="cancelUnggah"]')
    userEvent.click(buttonElement2)
})

test("renders text button unggah in Profile Mahasiswa Page", () => {
    render(edit_profile_page)
    const unggahFile = screen.getAllByText("Change File");
    unggahFile.forEach(element => {
        expect(element).toBeInTheDocument();
    });
})

test("render button unggah in Profile Mahasiswa Page", () => {
    render(edit_profile_page)
    const buttonElement = screen.getAllByRole("button", { name: "Change File" })
    buttonElement.forEach(element => {
        expect(element).toBeInTheDocument();
    })
})

test("renders text button unggah photo in Profile Mahasiswa Page", () => {
    render(edit_profile_page)
    const unggahFile = screen.getAllByText("Change Photo");
    unggahFile.forEach(element => {
        expect(element).toBeInTheDocument();
    });
})

test("render button unggah photo in Profile Mahasiswa Page", () => {
    render(edit_profile_page)
    const buttonElement = screen.getAllByRole("button", { name: "Change Photo" })
    buttonElement.forEach(element => {
        expect(element).toBeInTheDocument();
    })
})

test("render title in pop up unggah in Profile Mahasiswa Page", () => {
    render(edit_profile_page)
    const buttonElement = screen.getAllByRole("button", { name: "Change File" })
    userEvent.click(buttonElement[0])

    const title = screen.getByText("Unggah CV")
    expect(title).toBeInTheDocument()
})
