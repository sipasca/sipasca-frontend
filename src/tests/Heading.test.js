import Heading from "../components/Heading";
import { expect, test } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import * as nextRouter from "next/router";
import "@testing-library/jest-dom";

test("Heading contains Text for Type pengumuman", () => {
    nextRouter.useRouter = jest.fn();
    nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));
  
    render(<Heading text = "| Pengumuman" type="pengumuman"></Heading>);
    const title = screen.getByText("| Pengumuman");
    expect(title).toBeInTheDocument();

  });

test("Heading contains Text for Type pusatInformasi", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Heading text = "| Pengumuman" type="pusatInformasi"></Heading>);
  const pengumuman = screen.getByText("| Pengumuman");
  expect(pengumuman).toBeInTheDocument();

});

test("Heading contains Text for Type login", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Heading text = "| Login" type="login"></Heading>);
  const login = screen.getByText("| Login");
  expect(login).toBeInTheDocument();

});

test("Heading contains Text for Type landingPage", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Heading text = "| Pengumuman Terkini" type="landingPage"></Heading>);
  const terkini = screen.getByText("| Pengumuman Terkini");
  expect(terkini).toBeInTheDocument();

});

test("Heading contains Text for Type besar", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Heading text = "| Buat Pengumuman" type="besar"></Heading>);
  const buat = screen.getByText("| Buat Pengumuman");
  expect(buat).toBeInTheDocument();

});

test("Heading contains Text for Type kecil", () => {
  nextRouter.useRouter = jest.fn();
  nextRouter.useRouter.mockImplementation(() => ({ pathname: "/" }));

  render(<Heading text = "| Detail Pengumuman" type="kecil"></Heading>);
  const detail = screen.getByText("| Detail Pengumuman");
  expect(detail).toBeInTheDocument();

});

    

    

  

    



    