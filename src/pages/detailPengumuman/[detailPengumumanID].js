import Head from "next/head";
import { Fragment } from "react";
import styles from "../../styles/DetailPengumuman.module.css";
import Heading from "../../components/Heading";
import ButtonWithIcon from "../../components/ButtonWithIcon";
import FormatTanggal from "../../components/FormatTanggal";

export async function getServerSideProps(context){
    const idPengumuman = context.query.detailPengumumanID
    const detailPengumumanRes = await fetch("https://simkuspa-backend.herokuapp.com/pengumumanDetail/" + idPengumuman);
    const detailPengumuman = await detailPengumumanRes.json();
    return { props: { detailPengumuman } }  
}

function DetailPengumuman(props) {
    const arrayDetailPengumuman = props.detailPengumuman.pengumuman_detail[0]

    const judul = arrayDetailPengumuman[1];
    const konten = arrayDetailPengumuman[2];
    const lastModified = arrayDetailPengumuman[3];

    return (
        <Fragment> 
            <Head>
                <title>Detail Pengumuman</title>
                <meta name="description" content="Sistem Informasi Mata Kuliah Pasca" />
            </Head>
            <div className={styles.isian}>
                <Heading text = "| Detail Pengumuman" type="kecil"></Heading>
                <h4 className={styles.judul}>{judul} </h4>
                <p> {konten} </p>
                <p lassName={styles.tanggal}> Terakhir dimodifikasi: <FormatTanggal tanggal={lastModified}></FormatTanggal> </p>
                <div style={{ textAlign:"right" }}>
                    <ButtonWithIcon text="Kembali" to={"../pengumuman"}></ButtonWithIcon>
                </div>
            </div>

        </Fragment>
    )
}

export default DetailPengumuman;
