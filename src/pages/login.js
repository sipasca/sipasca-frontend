import { Fragment, useRef, useState } from "react";
import CSRFToken from "../lib/CSRFToken";
import Cookies from 'js-cookie';
import postRequest from "./api/post-request";
import Head from "next/head";

import { useRouter } from 'next/router'
import Heading from "../components/Heading"
import styles from "../styles/Login.module.css";
import axios from "axios";

function Login() {
  const username = useRef();
  const password = useRef();
  const router = useRouter();

  const [unrecognizedMessage, setUnrecognizedMessage] = useState(false);

  async function loginHandler(event) {
    setUnrecognizedMessage(false);
    event.preventDefault();
    
    const body = {
      'username': username.current.value,
      'password': password.current.value,
      'csrfmiddlewaretoken': Cookies.get('csrftoken')
    }

    const res = await postRequest('/login', body);
    const data = res.data;
    
    if (data.status === 'UNRECOGNIZED') {
      setUnrecognizedMessage(true);
    }
    else if (data.status === 'SUCCESS') {
      await axios.post('/api/auth/SSO', data)
      router.push('/');
    }   
  }
  
  return (
    <Fragment>
      <Head>
        <title>Login</title>
      </Head>
      <section className={styles.login}>
        <p className={styles.simkuspa}>SIPASCA</p>
        <p className={styles.garis}></p>
        <div className={styles.loginContainer}>
          <Heading text = "| Login" type="login"></Heading>
          <form onSubmit={loginHandler}>
            <CSRFToken type="PRODUCTION" />
            <label>Username</label>
            <input type="username" required placeholder="Type username" ref={username}/>
            <label>Password</label>
            <input type="password" required placeholder="Type password" ref={password}/>
            <button>Login</button>
            {unrecognizedMessage && <p>Username atau password yang dimasukkan salah.</p>}
          </form>
        </div>
      </section>
    </Fragment>
  );
}

export default Login;