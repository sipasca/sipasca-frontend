import Head from "next/head";
import { Fragment, useRef, useReducer, useState, useEffect } from "react";
import { useRouter } from 'next/router';
import Cookies from 'js-cookie';
import Heading from "../../components/Heading";
import CSRFToken from "../../lib/CSRFToken";
import postRequest from "../api/post-request";
import styles from "../../styles/RequestDosenPembimbing.module.css";
import axios from "axios";
import { verify } from "jsonwebtoken";

export async function getServerSideProps(context) {
    const userID = context.req.cookies["userID"]
    const secret = process.env.SECRET;
    const verifiedData = verify(userID, secret);
    const userIDVerified = verifiedData.userID;

    const idDosen = context.query.id; 
    const idTopik = context.query.idTopik; 
    const judulTopik = context.query.judulTopik;
    const deskripsiTopik = context.query.deskripsiTopik;

    const administrasiRes = await fetch("https://simkuspa-backend.herokuapp.com/mata_kuliah_can_be_requested/" + userIDVerified)
    const data = await administrasiRes.json();
    data['idDosen'] =idDosen
    data['idTopik'] =idTopik
    data['judulTopik'] = judulTopik
    data['deskripsiTopik'] = deskripsiTopik

    const mahasiswaRes = await fetch("https://simkuspa-backend.herokuapp.com/getProfileMahasiswa/" + userIDVerified)
    const detailMahasiswa = await mahasiswaRes.json()
    data['detailMahasiswa'] = detailMahasiswa.profile_mahasiswa[0]

    const dosenRes = await fetch("https://simkuspa-backend.herokuapp.com/getProfileDosen/" + idDosen)
    const detailDosen = await dosenRes.json()
    data['detailDosen'] = detailDosen.profile_dosen[0]
    return { props: {data} }
}

const initialRequest ={
    judul: '',
    deskripsi: '',
    motivasi: '',
    batasWaktu: '',
    pesan: '',
    mataKuliah:''
}
  
const reducer = (state, action) => {
    switch (action.type) {
        case "JUDUL":
            return { ...state, judul: action.data }
        case "DESKRIPSI":
            return { ...state, deskripsi: action.data }
        case "MOTIVASI":
            return { ...state, motivasi: action.data }
        case "BATASWAKTU":
            return { ...state, batasWaktu: action.data }
        case "PESAN":
            return { ...state, pesan: action.data }
        case "MATAKULIAH":
            return { ...state, mataKuliah: action.data }
    }
};

function RequestDospem(props) {
    const arrayMk = props.data.mk_list
    const [request, dispatch] = useReducer(reducer, initialRequest);
    const [requiredAlert, setRequiredAlert] = useState(false);
    const router = useRouter();
    const idMK = useRef('')

    useEffect(() => {
        if(props.data.mk_list[0] != undefined) {
            dispatch({type:"MATAKULIAH", data: props.data.mk_list[0][0]})
        }
        dispatch({type:"JUDUL", data: props.data.judulTopik})
        dispatch({type:"DESKRIPSI", data: props.data.deskripsiTopik})
    }, [])
    

    async function submitHandler(event) {
        event.preventDefault();

        if (request.judul.trim() === '' || request.deskripsi.trim() === '' || request.motivasi.trim() === '' || 
            request.batasWaktu.trim() === '' || request.mataKuliah.trim() === '') {
            setRequiredAlert(true)
            return
        }

        const creds = await axios.get('/api/creds', { withCredentials: true })
        const userID = creds.data.userID
        const body = {
            'id_mk': idMK.current.value,
            'id_dosen': props.data.idDosen,
            'id_mahasiswa' : userID,
            'judul' : request.judul,
            'deskripsi' : request.deskripsi,
            'motivasi' : request.motivasi,
            'batas_waktu' : request.batasWaktu,
            'pesan' : request.pesan,
            'id_topik': props.data.idTopik,
            'csrfmiddlewaretoken': Cookies.get('csrftoken')
        }
          
        await postRequest('/insertRequestDosenPembimbing/', body);
        window.localStorage.setItem("requestSuccess", "Success")
          
        router.push("/cariDosen")
    }

    return (
        <Fragment>
            <Head>
                <title>Request Dosen Pembimbing</title>
            </Head>
            <div className={styles.container}>
                <Heading text="| Request Dosen Pembimbing" type="kecil" />
                <h6>Dosen Pembimbing yang sedang diajukan: {props.data.detailDosen[9]} </h6>

                <form onSubmit={submitHandler} >
                    <CSRFToken type="PRODUCTION" />

                    <div className={styles.inputBox + " form-group"}>
                        <label htmlFor="mk">Mata Kuliah <span className={styles.req}>*</span></label>
                        <select 
                            className="form-select" 
                            aria-label="Default select example" 
                            id="mk" 
                            onChange={(e) => dispatch({type:"MATAKULIAH", data:e.target.options[e.target.selectedIndex].text})}
                            ref={idMK}
                            required
                        >
                            <option value="" hidden selected>Pilih Mata Kuliah</option>
                        {arrayMk.map(item =>   
                            <option value={item[2]} key={item[2]}>{item[0]}</option>
                        )} 
                        </select>
                    </div>

                    <div className={styles.inputBox + " form-group"}>
                        <label htmlFor="judulTopik">Judul Topik <span className={styles.req}>*</span></label>
                        <input 
                            type="text" 
                            className="form-control" 
                            id="judulTopik" 
                            placeholder="Judul topik yang ingin Anda ajukan kepada dosen pembimbing" 
                            onChange={(event) => dispatch({type:"JUDUL", data:event.target.value})}
                            defaultValue={props.data.judulTopik}
                        />
                    </div>

                    <div className={styles.inputBox + " form-group"}>
                        <label htmlFor="deskripsiTopik">Deskripsi Topik <span className={styles.req}>*</span></label>
                        <textarea 
                            className="form-control" 
                            id="deskripsiTopik" 
                            rows="6" 
                            placeholder="Deskripsi rinci dari topik Anda" 
                            onChange={(event) => dispatch({type:"DESKRIPSI", data:event.target.value})}
                            defaultValue={props.data.deskripsiTopik}
                        ></textarea>
                    </div>

                    <div className={styles.inputBox + " form-group"}>
                        <label htmlFor="motivasiTopik">Motivasi Pengambilan Topik <span className={styles.req}>*</span></label>
                        <textarea 
                            className="form-control" 
                            id="motivasiTopik" 
                            rows="4" 
                            placeholder="Motivasi Anda mengambil topik ini" 
                            onChange={(event) => dispatch({type:"MOTIVASI", data:event.target.value})}
                        ></textarea>
                    </div>

                    <div className={styles.inputBox + " form-group"}>
                        <label htmlFor="batasWaktu">Batas Waktu Penyelesaian Yang Diharapkan <span className={styles.req}>*</span></label>
                        <input 
                            type="date" 
                            id="batasWaktu" 
                            onChange={(event) => dispatch({type:"BATASWAKTU", data:event.target.value})}
                            className="date"
                        ></input>
                    </div>

                    <div className={styles.inputBox + " form-group"}>
                        <label htmlFor="pesan">Pesan untuk dosen</label>
                        <textarea 
                            className="form-control" 
                            id="pesan" 
                            rows="4" 
                            placeholder="Pesan tambahan kepada dosen" 
                            onChange={(event) => dispatch({type:"PESAN", data:event.target.value})}
                        ></textarea>
                    </div>

                    <p><span className={styles.req}>* Kolom dengan tanda ini wajib diisi</span></p>

                    {requiredAlert && 
                        <div className="alert alert-danger alert-dismissible">
                            <p className={styles.info}>Mata kuliah, judul topik, deskripsi topik, motivasi topik, dan usulan batas waktu wajib disii.</p>
                        </div>
                    }

                    <div className={styles.button}>
                        <button 
                            className="btn btn-danger" 
                            type="button" 
                            data-bs-toggle="modal" 
                            data-bs-target="#modalRequest"
                            id="popupButton"
                        >Lihat Overview Request</button>
                    </div>
                    
                    {/* Modal */}
                    <div className="modal fade" id="modalRequest" data-bs-backdrop="static" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div className="modal-dialog modal-lg">
                            <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title px-2">Request Dosen Pembimbing</h5>
                            </div>
                            <div className="modal-body p-4">
                                <div className="alert alert-primary ">
                                    <p className={styles.info}>SIPASCA akan mengirimkan email kepada dosen pembimbing berisi detail permohonan yang Anda ajukan. Jika Dosen membalas email permohonan tersebut, maka balasan Dosen akan langsung masuk ke email Anda.</p>
                                </div>
                                
                                <h6 className={styles.mailKeyHeader}>Sender</h6>
                                <p>sipasca.cs@gmail.com</p>

                                <div className={styles.replyBox}>
                                    <div>
                                        <h6 className={styles.mailKeyHeader}>To</h6>
                                        <p>{props.data.detailDosen[4]}</p>
                                    </div>
                                    <div>
                                        <h6 className={styles.mailKeyHeader}>Reply-To</h6>
                                        <p>{props.data.detailMahasiswa[2]}</p>
                                    </div>
                                </div>

                                <h6 className={styles.mailKeyHeader}>Content</h6>
                                <div className={styles.content}>
                                    {getDateFormatted(new Date())} <br />
                                    Kepada Yth. Bapak/Ibu {props.data.detailDosen[9]} <br />
                                    Di Tempat <br />
                                    <br />                                    
                                    Saya mahasiswa {props.data.detailMahasiswa[3]} atas nama: <br />

                                    <table>
                                        <tr>
                                            <td><b>Nama</b></td>
                                            <td>: {props.data.detailMahasiswa[1]}</td>
                                        </tr>
                                        <tr>
                                            <td><b>NPM</b></td>
                                            <td>: {props.data.detailMahasiswa[4]}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Program Studi</b></td>
                                            <td>: {props.data.detailMahasiswa[3]}</td>
                                        </tr>
                                    </table>
                                    <br />
                                    Dengan ini mengajukan permohonan bimbingan kepada Bapak/Ibu untuk <b>Mata Kuliah Spesial {request.mataKuliah} </b>dengan detail topik sebagai berikut: <br />
                                    <div className={styles.topik}>
                                        <b>Judul Topik:</b> {request.judul} <br />
                                        <b>Deskripsi Topik:</b> <br />
                                        {request.deskripsi} <br />
                                        <br />
                                        <b>Usulan batas waktu penyelesaian topik:</b> {getDateFormatted(new Date(request.batasWaktu))} <br />
                                        <b>Motivasi pengambilan topik:</b> {request.motivasi} <br />
                                        <br />
                                    </div>
                                    
                                    <b>Pesan tambahan:</b> <br />
                                    {request.pesan} <br />
                                    <br />
                                    Demikian surat permohonan ini saya sampaikan. Mohon maaf apabila ada kesalahan. Atas perhatian dan pertimbangan Bapak/Ibu, saya ucapkan terima kasih. <br />
                                    <br />
                                    Salam, <br />
                                    Mahasiswa Pascasarjana Fakultas Ilmu Komputer Universitas Indonesia, <br />
                                    {props.data.detailMahasiswa[1]}
                    </div>
                    
                                <h6 className="mt-4">Apakah Anda yakin untuk mengirimkan request permohonan dosen pembimbing ini?</h6>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn link-danger" data-bs-dismiss="modal">Batal</button>
                                <button type="submit" data-bs-dismiss="modal" className="btn btn-danger" >Request</button>
                            </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </Fragment>
    )
}

export default RequestDospem;

const dayIndo = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"]
const monthIndo = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]

function getDateFormatted(dates){
    if (dates.toString() === 'Invalid Date'){
        return ''
    }
    const date = dates.getDate();
    const day = dayIndo[dates.getDay()]
    const month = monthIndo[dates.getMonth() - 1];
    const year = dates.getFullYear();
    return `${day}, ${date} ${month} ${year}`;
}