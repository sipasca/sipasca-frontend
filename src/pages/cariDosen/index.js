import Head from "next/head";
import Heading from "../../components/Heading";
import MataKuliahBox from "../../components/MataKuliahBox";
import { useRouter } from "next/router"

import styles from "../../styles/CariDosen.module.css"
import { useEffect, useState } from "react";
import SuccessAlert from "../../components/SuccessAlert";

export async function getServerSideProps() {
    const bidangKepakaranRes = await fetch("https://simkuspa-backend.herokuapp.com/getAllBidangKepakaran");
    const data = await bidangKepakaranRes.json();
    return { props: { data } };
}

function CariDosen(props) {
    const router = useRouter();
    const listBidang = props.data.list_bidang_kepakaran;

    const bidangKepakaranHandler = (id, nama) => {
        router.push({
            pathname: 'cariDosen/bidangKepakaran/' + id,
            query: {namaBidang: nama}
        })
    }

    const [alertSuccess, setAlertSuccess] = useState(false)
    useEffect(() => {
        if (window.localStorage.getItem("requestSuccess")) {
            setAlertSuccess(true)
            window.localStorage.removeItem("requestSuccess")
        }
    })

    return (
        <div className={styles.container}>
            <Head>
                <title>Cari Dosen</title>
            </Head>
            <Heading text="| Cari Dosen" type="besar" />
            <h4>Daftar Dosen Berdasarkan Bidang Kepakaran</h4>
            <div className="row row-cols-lg-3 g-4">
              {listBidang.map(bidang => {
                return <MataKuliahBox 
                            periode={bidang[0]} 
                            nama={bidang[1]} 
                            onClick={() => bidangKepakaranHandler(bidang[0], bidang[1])}
                            key={bidang[0]}
                        />
                })}  
            </div>
            {alertSuccess && <SuccessAlert text="Request berhasil dikirimkan" />}
        </div>
    )
}

export default CariDosen