import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Head from "next/head";
import Link from "next/link";
import { useState } from "react";
import { useRouter } from "next/router";
import Heading from "../../../components/Heading";
import Table from "../../../components/Table";

import styles from "../../../styles/DaftarDosenBidang.module.css"

export async function getServerSideProps(context) {
    const bidangID = context.query.bidangKepakaranID
    const namaBidang = context.query.namaBidang
    const listDosenRes = await fetch("https://simkuspa-backend.herokuapp.com/getDosenByBidangKepakaran/" + bidangID);
    const data = await listDosenRes.json();
    data['bidangKepakaran'] = namaBidang
    return { props: { data } };
}

function BidangKepakaran(props) {
    const router = useRouter();
    let listAllDosen = props.data.list_dosen

    function requestHandler(event, id) {
        event.preventDefault();
        router.push({
            pathname: '/cariDosen/requestDospem',
            query: { id: id, judulTopik: null, deskripsiTopik: null, idTopik:null }
        })

    }
    listAllDosen = listAllDosen.map(dosen => {
            return {
                'Nama Dosen' : <Link href={"/profile/profileDosen?id=" + dosen[0]} passHref><span className={styles.namaDosen} >{dosen[1]}</span></Link>, 
                'ID': dosen[0], 
                'Kontak': dosen[2],
                'Action': <button 
                            onClick={event => requestHandler(event, dosen[0])}
                            className={"btn btn-outline-danger " + styles.requestButton}
                        >Request</button>
            }
        }
    )

    const bidangKepakaran = props.data.bidangKepakaran
    const cols = ["Nama Dosen", "Kontak", "Action"]
    const [listDosen, setListDosen] = useState(listAllDosen)

    const searchHandler = (event) => {
        const keyword = event.target.value.toLowerCase()

        setListDosen(listAllDosen.filter(
            dosen => dosen["Nama Dosen"].props.children.props.children.toLowerCase().includes(keyword)
        ))
    }

    return (
        <div className={styles.container}>
            <Head>
                <title>Daftar Dosen {bidangKepakaran}</title>
            </Head>
            <Heading text="| Daftar Dosen" type="kecil" />
            <div className={styles.subContainer}>
                <h5>Bidang Kepakaran {bidangKepakaran} </h5>
                <div className={styles.search}>
                    <input type="text" placeholder="Cari nama dosen" onChange={searchHandler} id="requestDosen" />
                    <FontAwesomeIcon icon="fa-solid fa-magnifying-glass" className="icon"/>
                </div>
            </div>
            <Table
                columns={cols}
                data={listDosen}
            />

            
        </div>
    )
}

export default BidangKepakaran;

