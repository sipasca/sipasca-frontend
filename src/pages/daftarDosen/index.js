import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Head from "next/head";
import Link from "next/link";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Heading from "../../components/Heading";
import Table from "../../components/Table";
import styles from "../../styles/DaftarDosen.module.css"
import axios from "axios";

export async function getServerSideProps(context) {
    const listAllDosen = await fetch("https://simkuspa-backend.herokuapp.com/getAllDosen/");
    const dataListAllDosen = await listAllDosen.json();
    return { props: { dataListAllDosen } };
}

function DaftarDosen(props) {

    const router = useRouter();
    let listAllDosen = props.dataListAllDosen.list_all_dosen

    function requestHandler(event, id) {
        event.preventDefault();
        router.push({
            pathname: '/cariDosen/requestDospem',
            query: { id: id, judulTopik: null, deskripsiTopik: null, idTopik:null }
        })

    }

    const [role, setRole] = useState('pengguna');
    useEffect( () => {  
        async function setRoleData() {
            const res = await axios.get('/api/creds', { withCredentials: true })
            const newRole = res.data.role
            setRole(newRole);
        }
        setRoleData();
    })

    let cols;

    cols = ["Nama Dosen", "Kontak", "Bidang Kepakaran","Action"]

    function getRequestButton(dosen){
        return (
            <button data-test-id="request-button" 
                onClick={event => requestHandler(event, dosen)}
                className={"btn btn-outline-danger " + styles.requestButton}
                >Request
            </button>
        );
    }

    if (role === 'mahasiswa') {
        cols = ["Nama Dosen", "Kontak", "Bidang Kepakaran","Action"]
        listAllDosen = listAllDosen.map(dosen => {
                return {
                    'Nama Dosen' : <Link href={"/profile/profileDosen?id=" + dosen[0]}><span className={styles.namaDosen} >{dosen[1]}</span></Link>, 
                    'ID': dosen[0], 
                    'Kontak': dosen[2],
                    'Bidang Kepakaran' : dosen[4],
                    'Action': getRequestButton(dosen[0])
                }
            }
        )
    }
    else if (role==='dosen' || role==='staf') {
        cols = ["Nama Dosen", "Kontak", "Bidang Kepakaran"]
        listAllDosen = listAllDosen.map(dosen => {
            return {
                'Nama Dosen' : <Link href={"/profile/profileDosen?id=" + dosen[0]}><span className={styles.namaDosen} >{dosen[1]}</span></Link>, 
                'ID': dosen[0], 
                'Kontak': dosen[2],
                'Bidang Kepakaran' : dosen[4]
            }
            }
        )
    }

    const [listDosen, setListDosen] = useState(listAllDosen)

    useEffect( () => {  
        setListDosen(listAllDosen)
    }, [role]);
    
    const searchHandler = (event) => {
        const keyword = event.target.value.toLowerCase()
        setListDosen(listAllDosen.filter(
            dosen => dosen["Nama Dosen"].props.children.props.children.toLowerCase().includes(keyword)
        ))
    }

    return (
        <div className={styles.container}>
            <Head>
                <title>Daftar Dosen</title>
            </Head>
            <Heading text="| Daftar Dosen" type="kecil" />
            <div className={styles.subContainer}>
                <h5>Daftar Semua Dosen</h5>
                <div className={styles.search}>
                    <input type="text" placeholder="Cari nama dosen" onChange={searchHandler}  id="requestDosen" />
                    <FontAwesomeIcon icon="fa-solid fa-magnifying-glass" className="icon"/>
                </div>
            </div>
            <Table
                columns={cols}
                data={listDosen}
            />
        </div>

    )
}

export default DaftarDosen