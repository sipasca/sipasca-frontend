import { verify, JsonWebTokenError } from "jsonwebtoken";

const secret = process.env.SECRET;

export default async function creds(req, res) {
  const { userID, role } = req.cookies

  if (userID === undefined && role === undefined) {
    res.end()
  }

  try {
    let data = {};
    if (userID) {
      const userData = verify(userID, secret);
      data = { ...data, userID: userData.userID }
    }

    if (role) {
      const roleData = verify(role, secret);
      data = {...data, role: roleData.role }
    }
    
    res.status(200).json(data);
  }
  catch (err){
    if (err instanceof JsonWebTokenError) {
      console.error("Token has probably been tampered! Will logout...")
    }
  }

  res.status(401);
  
}