import axios from "axios";
import Cookies from 'js-cookie';

const local = "http://localhost:8000"
const production = "https://simkuspa-backend.herokuapp.com"

async function postRequest(url, data, type='PRODUCTION') {
  let backendURL = production;
  if (type.toUpperCase() === 'LOCAL') {
    backendURL = local
  }

  axios.defaults.withCredentials = true;

  const config = {
    withCredentials: true,
    credentials: 'include',
    headers: {
      'Accept': "application/json",
      "Content-Type": "application/json",
      "X-CSRFToken": Cookies.get("csrftoken"),
    },
  };

  return axios.post(`${backendURL}${url}`, JSON.stringify(data), config);
}

export default postRequest;
