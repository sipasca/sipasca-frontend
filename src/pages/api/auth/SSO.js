import { sign } from "jsonwebtoken";
import { serialize } from "cookie";

const secret = process.env.SECRET;

export default async function SSO(req, res) {
  const { userID, role } = req.body;
  
  const tokenUserID = sign(
    {
      exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24, // 1 day
      userID: userID
    },
    secret
  );

  const tokenRole = sign(
    {
      exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24,
      role: role
    },
    secret
  );

  const cookieSetting = {
    secure: process.env.NODE_ENV !== "development",
    sameSite: "lax",
    maxAge: 60 * 60 * 24,
    path: '/'
  }

  const serializedUserID = serialize("userID", tokenUserID, cookieSetting);
  const serializedRole = serialize("role", tokenRole, cookieSetting);

  res.setHeader("Set-Cookie", [serializedUserID, serializedRole]);
  res.status(200).end();
}