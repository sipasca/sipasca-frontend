import { XMLParser } from "fast-xml-parser";

const originSite = process.env.ORIGINSITE

async function handleSSO(req, res) {  
  const { ticket } = req.query;
  const originURL = originSite + "/loginSSO"
  const serviceURL = encodeURIComponent(originSite + '/api/handleSSO')
  
  const response = await fetch(
    `https://sso.ui.ac.id/cas2/serviceValidate?ticket=${ticket}&service=${serviceURL}`
  );
  const data = await response.text()
  const parser = new XMLParser();
  const dataSSO = parser.parse(data);

  const payload = {data: dataSSO}
  const wait = `
  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <title>Please Wait</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
    </head>
    <body>
      <div id="wrap">
        <h3>An Error Occured</h3>
      </div>
      <script type="application/javascript">
        (() => {
          if (window.open) {
            window.opener.postMessage(${ JSON.stringify(payload) }, "${originURL}");
          } else {
            console.log("An error occured");
          }
        })();
      </script>
    </body>
  </html>
  `;
  res.status(200).send(wait);
}

export default handleSSO;