import Head from "next/head";
import { useEffect, useRef, useState } from "react";
import styles from "../../../../../../styles/DetailBimbingan.module.css";
import { useRouter } from "next/dist/client/router";
import MKLayout from "../../../../../../components/MKLayout";
import SubJudul from "../../../../../../components/SubJudul";
import DeletePopUp from "../../../../../../components/DeletePopUp";
import axios from "axios";
import postRequest from "../../../../../../pages/api/post-request";
import CSRFToken from "../../../../../../lib/CSRFToken";
import Cookies from "js-cookie";
import { useTerm } from "../../../../../../context/termContext";

export async function getServerSideProps(context) {
  const mkID = context.query.mataKuliahID
  const detailMKRes = await fetch('https://simkuspa-backend.herokuapp.com/detailMataKuliah/'+mkID);
  const detailMKData = await detailMKRes.json()
  const status = context.query.statusMahasiswa
  
  const mhsID = context.query.mahasiswaID
  const userMK = mkID + '/' + mhsID + '/' 
  const topik = await fetch('https://simkuspa-backend.herokuapp.com/topikMahasiswa/'+userMK)
  const dataTopik = await topik.json()

  const profileMahasiswa = await fetch('https://simkuspa-backend.herokuapp.com/getProfileMahasiswa/' + mhsID)
  const dataProfileMahasiswa = await profileMahasiswa.json()
  
  return { props: { dataTopik, detailMKData, dataProfileMahasiswa, status } }
}

function checkTerm(activePeriode, thisPeriode) {
  const activeYear = parseInt(activePeriode.slice(0,4))
  const year = parseInt(thisPeriode.slice(0,4))

  const activeTerm = parseInt(activePeriode.slice(-1))
  const term = parseInt(thisPeriode.slice(-1))

  if (year < activeYear) {
    return false
  } else {
    if (year === activeYear && term < activeTerm) {
      return false
    }
    return true
  }
}

function Detail(props) {
    const namaMK = props.detailMKData.detail_mk[2]
    const mkId = props.detailMKData.detail_mk[0]
    const dataMahasiswa = props.dataProfileMahasiswa.profile_mahasiswa[0]
    const mhsId = dataMahasiswa[0]
    const dataTopik = props.dataTopik.topik_mahasiswa[0]
    const status = props.status
    const router = useRouter();
    const feedback = useRef()

    const { activeTerm } = useTerm()
    const term = props.detailMKData.detail_mk[1]

    const [activeMode, setActiveMode] = useState(false)

    const [dosenId, setDosenId] = useState('');
    const [showPopUp, setShowPopUp] = useState(false);
    useEffect(() => {
      async function setIDData() {
        const creds = await axios.get('/api/creds', { withCredentials: true })
        const userID = creds.data.userID
        setDosenId(userID);
      }
      setIDData();

      if (activeTerm !== null) {
        setActiveMode(checkTerm(activeTerm, term))
      }
    },[])

  async function handlePersetujuan(event) {
    event.preventDefault()
    setShowPopUp(true)
  }
    
  async function handleAction(choose) {
    const body = {
      'id_mk': mkId ,
      'id_mahasiswa': mhsId,
      'id_dosen': dosenId,
      'judul': dataTopik[1],
      'deskripsi': dataTopik[2],
      'batas_waktu': dataTopik[3],
      'csrfmiddlewaretoken': Cookies.get('csrftoken')
    }

    if(choose){
      await postRequest('/acceptMahasiswaBimbingan/', body)
      router.push("/mataKuliahSpesial/"+mkId+"/d/bimbingan/")
    }
    setShowPopUp(false)
  }

  function handleLihatProfilMahasiswa(event){
    event.preventDefault();
    const url = "/profile/profileMahasiswa"
    router.push({
      pathname: url,
      query: {id: mhsId}
    })
  }
  
  async function tolakHandler() {
    const creds = await axios.get('/api/creds', { withCredentials: true })
    const userID = creds.data.userID

    const body = { 
      'id_mk': router.query.mataKuliahID, 
      'id_mahasiswa': dataMahasiswa[0], 
      'id_dosen': userID,
      'judul': dataTopik[1],
      'deskripsi': dataTopik[2],
      'batas_waktu': dataTopik[3],
      'feedback': feedback.current.value,
      'csrfmiddlewaretoken': Cookies.get('csrftoken') 
    }
    
    await postRequest('/rejectMahasiswaBimbingan/', body)
    router.push("/mataKuliahSpesial/"+mkId+"/d/bimbingan/")
  }
    
  return (
    <div className={styles.container}>
    <MKLayout namaMK={namaMK}>
      <Head>
        <title>Detail Bimbingan</title>
      </Head>
      <div style={{padding:"30px"}}>
        <SubJudul text="Profil" />
        <div className={styles.section}>
            <div className="row">
                <div className="col-sm-4">
                    <div className={styles.subSection}>
                    <div className={styles.label}>Nama Mahasiswa</div>
                        {dataMahasiswa[1]}
                    </div>
                </div>
                <div className="col-sm-8">
                    <div className={styles.subSection}>
                        <div className={styles.label}>NPM</div>
                        {dataMahasiswa[4]}
                    </div>                   
                </div>
            </div>
            <button className={styles.lihatProfilSelengkapnya}  onClick={(event) => handleLihatProfilMahasiswa(event)} id="lihatProfilSelengkapnya">Lihat Profil Selengkapnya</button>
        </div>
        <div className={styles.section}>
            <SubJudul text="Topik" />
            <div className={styles.subSection}>
                <div className={styles.label}>Nama Topik</div>
                {dataTopik[1]}
            </div>  
            <div className={styles.subSection}>
                <div className={styles.label}>Deskripsi Topik</div>
                {dataTopik[2]}
            </div> 
            <div className={styles.subSection}>
                <div className={styles.label}>Motivasi Pengambilan Topik</div>
                {dataTopik[4]}
            </div> 
            <div className={styles.subSection}>
                <div className={styles.label}>Batas Waktu Penyelesaian Yang Diharapkan</div>
                {dataTopik[3]}
            </div> 
        </div>
        {(status == 'Menunggu persetujuan dosen' && activeMode) && 
        <div>
          <SubJudul text="Persetujuan Pembimbingan" />
          <div className={styles.section}>
          <CSRFToken type="PRODUCTION" />
            <button className={styles.terima} onClick={handlePersetujuan} id="terima">Terima</button>
            <button 
              className={styles.tolak} 
              id="tolak" 
              data-bs-toggle="modal" 
              data-bs-target="#modalTolak">Tolak</button>
          </div>

          <div className="modal fade" id="modalTolak" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title">Penolakan Permohonan Bimbingan</h5>
                </div>
                
                <div className="modal-body">
                  <input type="text" className="form-control" placeholder="Feedback kepada mahasiswa" ref={feedback}/>
                </div>
                <div className="modal-footer">
                  <button id="cancelReject" type="button" data-bs-dismiss="modal" className={styles.cancelReject}>Tidak</button>
                  <button id="submitReject" type="button" data-bs-dismiss="modal" className={styles.confirmReject} onClick={tolakHandler}>Ya</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        }
      </div>
    </MKLayout>
    {showPopUp && <DeletePopUp onDialog={handleAction} message='Apakah Anda yakin untuk menerima?' />} 
    </div>
  );
}
export default Detail;
