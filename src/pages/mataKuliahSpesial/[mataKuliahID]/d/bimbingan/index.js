import MKLayout from"../../../../../components/MKLayout"
import styles from "../../../../../styles/DaftarBimbingan.module.css";
import Head from "next/head";
import Table from "../../../../../components/Table";
import SubJudul from "../../../../../components/SubJudul";
import { useRouter } from "next/dist/client/router";
import { verify } from "jsonwebtoken";

export async function getServerSideProps(context) {
    const userID = context.req.cookies["userID"]

    const secret = process.env.SECRET;
    const verifiedData = verify(userID, secret);
    const userIDVerified = verifiedData.userID;

    const mkID = context.query.mataKuliahID
    const user_mk = userIDVerified + '/' + mkID + '/'

    const daftarPermohonan = await fetch("https://simkuspa-backend.herokuapp.com/daftar/Menunggu persetujuan dosen/" + user_mk)
    const data = await daftarPermohonan.json()

    const daftarCalon = await fetch("https://simkuspa-backend.herokuapp.com/daftar/Disetujui dosen pembimbing/" + user_mk)
    const data_daftarCalon = await daftarCalon.json()

    const daftarMahasiswa = await fetch("https://simkuspa-backend.herokuapp.com/daftar/Masa bimbingan/" + user_mk)
    const data_daftarMahasiswa = await daftarMahasiswa.json()

    const detailMK = await fetch("https://simkuspa-backend.herokuapp.com/detailMataKuliah/"+ mkID)
    const data_detailMK = await detailMK.json()

    data['daftarCalon'] = data_daftarCalon.list_status_topik_mahasiswa
    data['daftarMahasiswa'] = data_daftarMahasiswa.list_status_topik_mahasiswa
    data['mataKuliahID'] = mkID
    data['namaMK'] = data_detailMK.detail_mk[2]
    
    return { props: { data } }
}

function Bimbingan(props) {
    const cols = ["Nama", "NPM", "Topik", "Status", "Action"];
    const isAction = false;
    const dataDaftarPermohonan = [];
    const dataDaftarCalon = [];
    const dataDaftarMahasiswa = [];

    const router = useRouter();
    function HandleDetail(event,id, status){
        event.preventDefault();
        const url = "/mataKuliahSpesial/"+props.data.mataKuliahID+"/d/bimbingan/detail/"
        router.push({
            pathname: url,
            query: { mahasiswaID: id, statusMahasiswa: status}
        })
    }

    function getDetailButton(id,status){
        return (
            <button data-test-id="button-detail"
              style={{ border: "white" }}
              type="button"
              className="btn btn-outline-danger"
              onClick={(event) => HandleDetail(event,id,status)}
            >
              Detail
            </button>
        );
    }
    

    for (let item of props.data.list_status_topik_mahasiswa) {
        let row = {
          "Nama": item[0],
          "NPM": item[4],
          "Topik": item[2],
          "Status": item[3],
          "Action": getDetailButton(item[1],item[3]),
        };
        dataDaftarPermohonan.push(row);
    }

    for (let item of props.data.daftarCalon) {
        let row = {
          "Nama": item[0],
          "NPM": item[4],
          "Topik": item[2],
          "Status": item[3],
          "Action": getDetailButton(item[1],item[3]),
        };
        dataDaftarCalon.push(row);
    }

    for (let item of props.data.daftarMahasiswa) {
        let row = {
          "Nama": item[0],
          "NPM": item[4],
          "Topik": item[2],
          "Status": item[3],
          "Action": getDetailButton(item[1],item[3]),
        };
        dataDaftarMahasiswa.push(row);
    }

    return(
        <div className={styles.container}>
            <Head>
            <title>Daftar Bimbingan</title>
            </Head>
            <MKLayout namaMK = {props.data.namaMK}>
                <div className={styles.containerTable}>
                    <SubJudul text="Daftar Permohonan Bimbingan" />
                    {dataDaftarPermohonan.length == 0 && (
                            <h5 style={{ color: "#163269", textAlign: "center", marginTop: "2em" }}>
                                Belum Ada Permohonan Bimbingan
                            </h5>
                        )}

                    {dataDaftarPermohonan.length != 0 && (
                        <div className={styles.tableTitle}>
                            <Table
                                columns={cols}
                                data={dataDaftarPermohonan}
                                isAction={isAction}
                                isAnotherAction={false}
                                anotherButtonLabel={null}
                            ></Table>
                        </div>
                    )}
                    
                    <div className={styles.titleSecond}></div>

                    <SubJudul text="Daftar Calon Mahasiswa Bimbingan" />
                    <div className={styles.tableTitle}>
                        {dataDaftarCalon.length == 0 && (
                                <h5 style={{ color: "#163269", textAlign: "center", marginTop: "2em" }}>
                                    Belum Ada Calon Mahasiswa Bimbingan
                                </h5>
                        )}
                            
                        {dataDaftarCalon.length != 0 && (
                            <Table
                                columns={cols}
                                data={dataDaftarCalon}
                                isAction={isAction}
                                isAnotherAction={false}
                                anotherButtonLabel={null}
                            ></Table>
                        )}
                    </div>
                    
                    <div className={styles.titleSecond}></div>

                    <SubJudul text="Daftar Mahasiswa Bimbingan" />
                    <div className={styles.tableTitle}>
                        {dataDaftarMahasiswa.length == 0 && (
                            <h5 style={{ color: "#163269", textAlign: "center", marginTop: "2em" }}>
                                Belum Ada Mahasiswa Bimbingan
                            </h5>
                        )}
                            
                        {dataDaftarMahasiswa.length != 0 && (
                            <Table
                                columns={cols}
                                data={dataDaftarMahasiswa}
                                isAction={isAction}
                                isAnotherAction={false}
                                anotherButtonLabel={null}
                            ></Table>
                        )}
                    </div>
                </div>
                   
            </MKLayout>
        </div>
    )
}
export default Bimbingan;
