import MKLayout from"../../../../../components/MKLayout"
import styles from "../../../../../styles/AdministrasiPersyaratan.module.css" ;
import Head from "next/head";
import FilterMahasiswa from "../../../../../components/FilterMahasiswa";
import * as FileSaver from "file-saver";
import * as XLSX from "xlsx";

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Tooltip,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Tooltip,
);

export async function getServerSideProps({ query }) {    
    const mataKuliahID = query.mataKuliahID
    const [detailMK_res] = await Promise.all([
        fetch('https://simkuspa-backend.herokuapp.com/detailMataKuliah/'+mataKuliahID)
    ])
    const [detailMK_data] = await Promise.all([
        detailMK_res.json()
    ])

    const daftarMahasiswaMK = await fetch("https://simkuspa-backend.herokuapp.com/daftarAllMahasiswaMK/" + mataKuliahID)
    const data_daftarMahasiswaMK = await daftarMahasiswaMK.json()

    return { props: { mataKuliahID, detailMK_data, data_daftarMahasiswaMK } }
  }

export function unduhRekapHandler(event,apiData,namaMK){
    event.preventDefault();
    const selectedColumn = ['Nama Mahasiswa', 'NPM', 'Dosen Pembimbing', 'Status', 'Last Modified'];
    const redux = array => array.map(previousObect => selectedColumn.reduce((currentObject, key) => {
        currentObject[key] = previousObect[key];
        return currentObject;
    }, {}));
    const finalData = redux(apiData);
    const fileName = "Rekap_Mahasiswa_" + namaMK;
    const fileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const fileExtension = ".xlsx";
    const ws = XLSX.utils.json_to_sheet(finalData, { origin: 'A2', skipHeader: true });
    var wscols = [
        { width: 25 },  
        { width: 15 }, 
        { width: 35 }, 
        { width: 30 }, 
        { width: 25 }
      ];
    ws["!cols"] = wscols;
    XLSX.utils.sheet_add_aoa(ws, [['Nama Mahasiswa', 'NPM', 'Dosen Pembimbing', 'Status', 'Waktu Perubahan Terakhir']]);
    const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
    const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
    const data = new Blob([excelBuffer], { type: fileType });
    FileSaver.saveAs(data, fileName + fileExtension);
}


function Rekap(props) {
    const mataKuliahID = props.mataKuliahID
    const namaMK = props.detailMK_data.detail_mk[2]

    const dataMahasiswaMK =  []
    const dataPerStatus = {
        'Belum mendapatkan pembimbing': 0, 
        'Menunggu persetujuan dosen': 0, 
        'Disetujui dosen pembimbing': 0, 
        'Masa bimbingan': 0
    }

    for (let item of props.data_daftarMahasiswaMK.list_all_mahasiswa_mk) {
        let row = {
          "Nama Mahasiswa": item[1],
          "NPM": item[2],
          "Dosen Pembimbing": item[3],
          "Status": item[4],
          "Last Modified" :item[5].slice(0,10)+" "+item[5].slice(11,-7),
          urlId : "../s/administrasi/detailStatus/"+item[0]+"/?mataKuliahID="+mataKuliahID,
          idMahasiswa: item[0]
        };
        dataPerStatus[item[4]] += 1
        dataMahasiswaMK.push(row);
    }

    const options = {
        responsive: true,
        indexAxis: 'y',
        scales: {
            x : {
                ticks : {
                    stepSize: 10
                }
            },
            y : {
                ticks : {
                    font: {
                        weight: 600,
                        size: 13,
                        color: '#ff0000'
                    }
                }
            }
        },
    };

    const labels = ['Belum mendapatkan pembimbing', 'Menunggu persetujuan dosen', 'Disetujui dosen pembimbing', 'Masa bimbingan'];

    const data = {
        labels,
        datasets: [
          {
            data: labels.map(label => dataPerStatus[label]),
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
          }
        ],
    };
    
    
    return(
        <div className={styles.container}>
            <Head>
            <title>Administrasi</title>
            </Head>
            <MKLayout namaMK= {namaMK} >
              <h4 className={styles.titleText}>Persebaran Data Mahasiswa</h4>
              <div className={styles.chart}>
                <Bar options={options} data={data}/>
              </div>

              <br></br><br></br>
              <FilterMahasiswa dataMahasiswaMK={dataMahasiswaMK} mataKuliahID={mataKuliahID}></FilterMahasiswa>
              <button className={styles.unduhRekap} onClick={(event) => unduhRekapHandler(event,dataMahasiswaMK,namaMK)} id="unduhRekap">Unduh Rekap</button>
            </MKLayout>
        </div>
    )
}
export default Rekap;