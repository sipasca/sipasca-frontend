import Head from "next/head";
import {Fragment, React, useRef, useState, useEffect} from "react";
import MKLayout from "../../../../../components/MKLayout";
import SubJudul from "../../../../../components/SubJudul";
import Table from "../../../../../components/Table";
import CSRFToken from "../../../../../lib/CSRFToken";
import Cookies from "js-cookie";
import postRequest from "../../../../api/post-request";
import { useRouter } from "next/router";
import SuccessAlert from "../../../../../components/SuccessAlert";
import { ref, getDownloadURL, uploadBytes } from "firebase/storage";
import storage from "../../../../../components/storage";

export async function getServerSideProps({query}){
    const mataKuliahID = query.mataKuliahID;
    const matkulNextTermExistsRes = await fetch('https://simkuspa-backend.herokuapp.com/isMatkulNextTermExist/'+mataKuliahID);
    const matkulNextTermExists = await matkulNextTermExistsRes.json();
    const detailMKRes = await fetch('https://simkuspa-backend.herokuapp.com/detailMataKuliah/'+mataKuliahID);
    const detailMK = await detailMKRes.json();
    const allRequestMigrasiRes = await fetch("https://simkuspa-backend.herokuapp.com/getAllRequestMigrasi/" + mataKuliahID);
    const allRequestMigrasi = await allRequestMigrasiRes.json();

    return { props: {mataKuliahID, allRequestMigrasi , matkulNextTermExists, detailMK} }  
}

export async function migrasiFile(listUrlFileLama,listUrlFileBaru){
    if (listUrlFileLama.length !== 0) {
        const downloadUrls = await Promise.all(listUrlFileLama.map(file =>getDownloadURL(ref(storage,file))))
        const downloadedFiles = await Promise.all(downloadUrls.map(url => fetch(url).then(res => res.blob())));
        for (let i = 0; i < downloadedFiles.length; i++) {
            const berkasDirectoryBaru = listUrlFileBaru[i]
            const filePathBaru = ref(storage, berkasDirectoryBaru)
            await uploadBytes(filePathBaru,downloadedFiles[i])
        }
    }
}

function MigrasiButton(idMK, idMahasiswa, isMatkulNextTermExist, listUrlFileLama, listUrlFileBaru){
    const idMKRef = useRef();
    const idMahasiswaRef = useRef();
    
    const [hideButton, setHideButton] = useState(false);
    const [isAccept, setIsAccept] = useState(false);

    async function migrasiHandler(requestType, idMKReq, idMahasiswaReq, listUrlFileLama, listUrlFileBaru){
        const body = {
            'id_mk' : idMKReq,
            'id_mahasiswa': idMahasiswaReq,        
            'csrfmiddlewaretoken': Cookies.get('csrftoken')
        }
        let res;
        if (requestType == "acc") {
            migrasiFile(listUrlFileLama,listUrlFileBaru)
            res = await postRequest('/acceptMigrasi', body, 'PRODUCTION');
        }
        else {
            res = await postRequest('/rejectMigrasi', body, 'PRODUCTION');
        }
        const responseData = res.data;
        if (responseData.status === 'SUCCESS') {
            hideButtonHandler(requestType);
        }
    }
    
    async function acceptMigrasiHandler(event){
        event.preventDefault();
        migrasiHandler("acc", idMKRef.current.value, idMahasiswaRef.current.value, listUrlFileLama, listUrlFileBaru);
    }

    async function rejectMigrasiHandler(event){
        event.preventDefault();
        migrasiHandler("rej", idMKRef.current.value, idMahasiswaRef.current.value);
    }

    function hideButtonHandler(type) {
        setHideButton(!hideButton)
        if (type == 'acc') {
            setIsAccept(!isAccept)
        }
    }

    const keteranganMigrasi = (
        isAccept ?
        <p style={{ color:'#198754', fontWeight:"bold", marginRight:'auto', marginLeft:'auto', marginTop:'5px', marginBottom:'5px' }}> Migrasi diterima </p>
        : <p style={{ color:'#dc3545', fontWeight:"bold", marginRight:'auto', marginLeft:'auto', marginTop:'5px', marginBottom:'5px'}}> Migrasi ditolak </p>
    )

    return (
        <>{!hideButton ? 
            <div className="row">
                <div className="col">
                    <form onSubmit={acceptMigrasiHandler}>
                    <CSRFToken type="PRODUCTION" />
                    <input type="hidden" value={idMahasiswa} ref={idMahasiswaRef}></input>
                    <input type="hidden" value={idMK} ref={idMKRef}></input>
                    <button className="btn btn-success" type="submit" disabled={!isMatkulNextTermExist}> Terima </button>
                    </form>
                </div>
                <div className="col">
                    <form onSubmit={rejectMigrasiHandler}>
                    <CSRFToken type="PRODUCTION" />
                    <input type="hidden" value={idMahasiswa} ref={idMahasiswaRef}></input>
                    <input type="hidden" value={idMK} ref={idMKRef}></input>
                    <button className="btn btn-danger" type="submit" disabled={!isMatkulNextTermExist}> Tolak </button>
                    </form>
                </div>
            </div>
            : keteranganMigrasi
         }</>
    )
}


function Fiksasi(props) {
    const namaMK = props.detailMK.detail_mk[2];
    const matkulID = props.mataKuliahID;
    const belumMigrasi = "Untuk melakukan migrasi, mata kuliah ini perlu didaftarkan untuk term selanjutnya.";
    const sudahMigrasi = "Mata kuliah ini sudah terdaftar untuk term selanjutnya.";
    const keteranganMahasiswa = "Mahasiswa yang belum menyelesaikan mata kuliah spesial pada term sekarang dapat mengajukan migrasi mata kuliah  ke term berikutnya.";
    const arrayRequestMigrasi = props.allRequestMigrasi.detail_migrasi
    const judul = ["NPM", "Nama Mahasiswa", "Action"];
    const matkulNextTerm = props.matkulNextTermExists.is_matkul_next_term_exist;
    const [nextTermExists, setNextTermExists] = useState(false);
    const [isRegistered, setIsRegistered] = useState(false);

    const router = useRouter();
    useEffect( () => {
        if (matkulNextTerm) {
            setNextTermExists(true)
        }
    })
    var data = [];
    for (let item of arrayRequestMigrasi) {
        if (item[5] === 'REQ') {
            let row = {
                "NPM": item[7],
                "Nama Mahasiswa": item[6],
                "Action": MigrasiButton(item[1], item[2], nextTermExists, item[8], item[9]),
            }; data.push(row);
        }
    }
    const listMahasiswa = (
        <>{arrayRequestMigrasi.length == 0
            ? <p style={{ color: "#163269", fontWeight:"bold"}}> Belum ada mahasiswa yang mengajukan migrasi</p>
            : <Table columns={judul} data={data} />
        }</>
      );

    async function migrateMatkulHandler(event){
        event.preventDefault();

        const body = {
            'id_mk': matkulID,
            'csrfmiddlewaretoken': Cookies.get('csrftoken')
        }

        const res = await postRequest('/createMataKuliahNextTerm', body);
        const responseData = res.data;  

        if (responseData.status === 'SUCCESS') {
            router.reload();
            setIsRegistered(true);
          }
    }

    const buttonDaftarMatkul = ( isRegistered ?
        <SuccessAlert text={"Mata kuliah berhasil didaftarkan"} />
        : <form onSubmit={migrateMatkulHandler}>
        <CSRFToken type="PRODUCTION" />
        <input type="hidden" value={matkulID}></input>
        <button className="btn btn-danger" type="submit" style={{ marginTop:'15px'}}> Daftarkan Mata Kuliah di Term Selanjutnya </button>
        </form>
    );

    return(
        <Fragment>
            <Head>
                <title>Fiksasi Migrasi</title>
                <meta name="description" content="Sistem Informasi Mata Kuliah Pasca" />
            </Head>
            <div style={{ minHeight: '100vh' }}>
                <MKLayout namaMK= {namaMK} >
                    <SubJudul text="Pengajuan Migrasi">  </SubJudul>
                    <div className="col-lg-6">
                    {listMahasiswa}
                    <p style={{ color:"#163269" }}> 
                    {nextTermExists ? 
                    sudahMigrasi : 
                    <>{belumMigrasi}
                    <br></br>
                    {buttonDaftarMatkul}
                    </>
                    }
                    <p style={{ marginTop:'10px' }}> {keteranganMahasiswa}</p>
                    </p>
                    </div>
                </MKLayout>
            </div>
        </Fragment>
    )
}

export default Fiksasi;