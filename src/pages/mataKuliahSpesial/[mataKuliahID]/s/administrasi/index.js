import {useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import MKLayout from"../../../../../components/MKLayout"
import Link from "next/link";
import styles from "../../../../../styles/AdministrasiPersyaratan.module.css" ;
import Head from "next/head";
import storage from "../../../../../components/storage";
import { ref, getDownloadURL } from "firebase/storage";
import JSZip from 'jszip';
import { saveAs } from 'file-saver';
import SuccessAlert from "../../../../../components/SuccessAlert"
import ErrorAlert from "../../../../../components/ErrorAlert"

export async function getServerSideProps({ query }) {    
    const mataKuliahID = query.mataKuliahID
    const [detailMK_res, res] = await Promise.all([
        fetch('https://simkuspa-backend.herokuapp.com/detailMataKuliah/'+mataKuliahID),
        fetch('https://simkuspa-backend.herokuapp.com/formPersyaratan/'+mataKuliahID)
    ])
    const [detailMK_data,data] = await Promise.all([
        detailMK_res.json(),
        res.json()
    ])
    return { props: { data , mataKuliahID, detailMK_data} }
  }

export async function downloadFilesHandler(event,mataKuliahID,urlForm){
    event.preventDefault();
    const listFileRes =  await fetch('https://simkuspa-backend.herokuapp.com/detailFormPersyaratan/'+mataKuliahID+'/'+urlForm);
    const listFile = await listFileRes.json()
    const listDetailFile = listFile.detail_form_persyaratan

    if (listDetailFile.length == 0) {
        return false
    }
    else{
        const downloadUrls = await Promise.all(listDetailFile.map(file =>getDownloadURL(ref(storage,file[2]))))
        const downloadedFiles = await Promise.all(downloadUrls.map(url => fetch(url).then(res => res.blob())));
        const jszip = new JSZip();
        downloadedFiles.forEach((file, i) => jszip.file(listDetailFile[i][3], file));
        jszip.generateAsync({ type: "blob" })
        .then(function (content) {
        saveAs(content, mataKuliahID+"-"+urlForm);
        });
        return true
    }
}

function Administrasi(props) {
    const [showSuccessAlert, setshowSuccessAlert] = useState(false)
    const [textSuccessAlert, setTextSuccessAlert] = useState('')

    const [showErrorAlert, setshowErrorAlert] = useState(false)
    const [textErrorAlert, setTextErrorAlert] = useState('')

    useEffect( () => {
        if (typeof window !== 'undefined') {
            const SuccessCreateForm = window.localStorage.getItem('SuccessCreateForm')
            window.localStorage.removeItem("SuccessCreateForm");
        if (SuccessCreateForm === 'Success') {
            setshowSuccessAlert(true);
            setTextSuccessAlert("Form persyaratan baru berhasil dibuat")
            }
        else if(SuccessCreateForm == "Failed"){
            setshowErrorAlert(true);
            setTextErrorAlert("Form persyaratan dengan nama sama sudah ada, tidak berhasil membuat form baru")
        }   
        }
      },[])

    function setStatusAlert(status){
        if (status) {
         setshowSuccessAlert(true)
         setTextSuccessAlert("Berkas berhasil diunduh")
        }
        else{
        setshowErrorAlert(true)
        setTextErrorAlert("Berkas gagal diunduh karena folder kosong")
        }
    }

    const mataKuliahID = props.mataKuliahID
    const namaMK = props.detailMK_data.detail_mk[2]
    const daftarForm = props.data.list_form_persyaratan
    const isAnyForm = daftarForm.length > 0
    
    return(
        <div className={styles.container}>
            <Head>
            <title>Administrasi</title>
            </Head>
            <MKLayout namaMK= {namaMK} >
                <h4 className={styles.titleText}>Administrasi</h4>
                <h5>Daftar Form Persyaratan</h5>
                {isAnyForm ? 
                    <table>
                    {daftarForm.map(form =>
                        <tr>
                        <Link href={"../s/administrasi/detailAdministrasi/"+form[0]+"?mataKuliahID="+mataKuliahID} passHref={true}><td className={styles.formPersyaratan}>{form[0]}</td></Link>
                        <td>
                        <Link href={"../s/administrasi/"+"?mataKuliahID="+mataKuliahID}><a className={styles.unduhSemua}  onClick={e => downloadFilesHandler(e,mataKuliahID,form[0]).then(status => setStatusAlert(status))} >Unduh Semua</a></Link>
                        </td>
                        </tr>
                    )}
                    </table>
                    :
                    <p>Form Persyaratan belum ada</p>
                }
                <Link href={"administrasi/BuatForm?mataKuliahID="+mataKuliahID}>
                    <a type='button' className={styles.buttonAdministrasi} style={{marginBottom: '1%'}}>
                    <p style={{marginBottom:'0'}}><FontAwesomeIcon icon="fa-solid fa-plus" />  Tambah</p>
                    </a>
                </Link>
            </MKLayout>
            {showSuccessAlert && <SuccessAlert text={textSuccessAlert} />}
            {showErrorAlert && <ErrorAlert text={textErrorAlert} />}
        </div>
    )
}
export default Administrasi;