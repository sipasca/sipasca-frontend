import {useRef } from "react";
import {useRouter} from 'next/router';
import MKLayout from"../../../../../components/MKLayout"
import CSRFToken from "../../../../../lib/CSRFToken";
import styles from "../../../../../styles/AdministrasiPersyaratan.module.css";
import Head from "next/head";
import postRequest from "../../../../api/post-request"
import Cookies from "js-cookie";

export async function getServerSideProps({ query }) {    
    const mataKuliahID = query.mataKuliahID
    const detailMK_res = await fetch('https://simkuspa-backend.herokuapp.com/detailMataKuliah/'+mataKuliahID);
    const detailMK_data = await detailMK_res.json()
    return { props: {  mataKuliahID, detailMK_data} }
    ;
  }

function BuatForm(props) {
    const mataKuliahID = props.mataKuliahID
    const namaMK = props.detailMK_data.detail_mk[2]
    const nama = useRef();
    const jenisPersyaratan = useRef();
    const router = useRouter();
    
    async function buatHandler(event) {
        event.preventDefault();
        const body = { 
          'jenis_form': nama.current.value,
          'jenis_persyaratan': jenisPersyaratan.current.value,
          'csrfmiddlewaretoken': Cookies.get('csrftoken') 
        }
        const res = await postRequest('/tambahFormPersyaratan/'+mataKuliahID, body)
        const data = res.data;

        if (typeof window !== 'undefined') {
            window.localStorage.setItem('SuccessCreateForm', data.Success)
          }
        
        router.push({
          pathname: '../administrasi/',
          query: { mataKuliahID: mataKuliahID },
        })
      }

    return(
        <div className={styles.container}>
            <Head>
            <title>Administrasi</title>
            </Head>
            <MKLayout namaMK= {namaMK}>
                <h4 className={styles.titleText + " px-3"}>Administrasi</h4>
                <form id="formBuat" onSubmit={buatHandler} className="p-3">
                  <CSRFToken type="PRODUCTION" />
                  <div className=" mb-3">
                      <label htmlFor="inputJenis" className = {styles.subTitleText+" form-label"}><h6>Nama Form <span style={{color:'#ff0000'}}>*</span></h6></label>
                      <input type='text' className = {"mb-3 form-control"} id="inputJenis"  placeholder="Form Persetujuan Pembimbing Akademis" rows="2" ref={nama} required></input>
                  </div>
                  <div className=" mb-3">
                      <label htmlFor="inputPersyaratan" className = {styles.subTitleText+" form-label"}><h6>Jenis Form</h6></label>
                      <select className="form-select" ref={jenisPersyaratan} aria-describedby="jenisHelp">
                        <option value={null}>Tidak Ada</option>
                        <option value="Prasyarat">Prasyarat</option>
                        <option value="Keluaran">Keluaran</option>
                      </select>
                      <div id="jenisHelp" className="form-text">
                        <p className="mb-0">Opsi Prasyarat menandakan bahwa form ini perlu dilengkapi mahasiswa di awal perkuliahan.</p>
                        <p className="mb-0">Opsi Keluaran menandakan bahwa form ini perlu dilengkapi mahasiswa saat mata kuliah yang bersangkutan berhasil diselesaikan.</p>
                        <p className="mb-0">Jika tidak termasuk keduanya, silahkan pilih opsi Tidak Ada.</p>
                      </div>
                  </div>
                  <span style={{color:'#ff0000'}}>* Kolom dengan tanda ini wajib diisi</span>
                  <div>
                      <a><button id="simpanButton" className={styles.buttonSimpan} type="submit">Simpan</button></a>
                      <a id="batalButton" href="../administrasi" type="button" className={styles.buttonBatal}>Batal</a>
                  </div>
                </form>
            </MKLayout>
        </div>
    )
}
export default BuatForm;