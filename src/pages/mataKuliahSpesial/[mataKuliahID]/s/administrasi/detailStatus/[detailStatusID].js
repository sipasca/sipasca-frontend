import Head from "next/head";
import MKLayout from "../../../../../../components/MKLayout";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "../../../../../../styles/AdministrasiPersyaratan.module.css";
import styless from "../../../../../../styles/DetailTopik.module.css";
import BerkasPentingBox from "../../../../../../components/BerkasPentingBox";
import { useRouter } from "next/router"
import { useState, Fragment } from "react";
import SuccessAlert from "../../../../../../components/SuccessAlert";

export async function getServerSideProps({ query }) {
    const mataKuliahID = query.mataKuliahID;
    const detailStatusID = query.detailStatusID;
    const detailMK_res = await fetch("https://simkuspa-backend.herokuapp.com/detailMataKuliah/" + mataKuliahID);
    const detailMK_data = await detailMK_res.json();

    const userID = mataKuliahID + "/" + detailStatusID;
    const detail_res = await fetch("https://simkuspa-backend.herokuapp.com/readDetailMahasiswaMK/" + userID);
    const data = await detail_res.json();

    return {
        props: {
            mataKuliahID, detailStatusID, detailMK_data, data
        },
    };
}

function DetailStatus(props) {
    const mkId = props.mataKuliahID
    const detailId = props.detailStatusID
    const namaMK = props.detailMK_data.detail_mk[2]
    const arrayDetail = props.data.detail_mahasiswa_mk[0]
    const arrayFile = props.data.detail_mahasiswa_mk[1]
    const arrayDosen = props.data.detail_mahasiswa_mk[2]
    const [update, setUpdate] = useState(false)
    const router = useRouter();

    const handleChange = () => {
        handleUpdate()
        setUpdate(true)
    }

    async function handleUpdate() {
        await fetch("https://simkuspa-backend.herokuapp.com/updateStatusToMasaBimbingan/" + mkId + "/" + detailId)
        router.push("../detailStatus/" + detailId + "/?mataKuliahID=" + mkId)
        setUpdate(false)
    }

    return (
        <Fragment>
            <div className={styles.container}>
                <Head><title>Detail Mahasiswa</title></Head>
                    <MKLayout namaMK={namaMK}>
                        <Link href={"../../../s/administrasi/" + "?mataKuliahID=" + mkId}>
                            <a style={{ textDecoration: "none" }}>
                                <p className={styles.subTitleText}>
                                    <FontAwesomeIcon icon="fa-solid fa-angle-left" /> Kembali ke halaman Administrasi
                                </p>
                            </a>
                        </Link>
                        <h4 className={styles.titleText}>Administrasi</h4>
                        <table>
                            <colgroup>
                                <col span="1" className={styless.judul} />
                                <col span="1" className={styless.isi} />
                            </colgroup>
                            <tr style={{ margin: "10px" }}>
                                <td className={styless.judulKonten}>Nama Mahasiswa</td>
                                <td className={styless.konten} style={{ borderBottom: "1px solid #163269" }}>{arrayDetail[1]}</td>
                            </tr>
                            <tr>
                                <td className={styless.judulKonten}>NPM</td>
                                <td className={styless.konten} style={{ borderBottom: "1px solid #163269" }}>{arrayDetail[2]}</td>
                            </tr>
                            {arrayDosen.length != 0 && (
                                <tr>
                                    <td className={styless.judulKonten}>Dosen Pembimbing</td>
                                    <td className={styless.konten} style={{ borderBottom: "1px solid #163269" }}>{arrayDosen[0]}</td>
                                </tr>
                            )}
                            <br />
                            <tr>
                                {arrayFile.length != 0 && (
                                    <>
                                        <td className={styless.judulKonten}>Dokumen</td>
                                        <td>
                                            <div>
                                                {arrayFile.map(item =>
                                                    <BerkasPentingBox style={{ borderColor: "white !important" }} nama={item[1]} url={item[0]} />
                                                )}
                                            </div>
                                        </td>
                                    </>
                                )}
                            </tr>
                            <tr>
                                <td className={styless.judulKonten}>Status</td>
                                {arrayDetail[3] == "Disetujui dosen pembimbing" ? (
                                    <>
                                        <td>
                                            <select className="form-select" data-testid ="validationCustom04" onChange={handleChange} style={{ borderColor: "#163269" }}>
                                                <option selected disabled value="">Disetujui dosen pembimbing</option>
                                                <option value="MasaBimbingan">Masa bimbingan</option>
                                            </select>
                                        </td>
                                    </>
                                ) : (
                                    <>
                                        <div style={{ color: "black", borderColor: "#163269", marginBottom: "1%" }} className={"card"}>
                                            <div className={"card-body"}> {arrayDetail[3]} </div>
                                        </div>
                                    </>
                                )}
                            </tr>
                        </table>
                        {update && <SuccessAlert text="Status berhasil diubah" />}
                        <br />
                        <p className={styless.judulKonten}>Filtered by: {arrayDetail[3]}</p>
                    </MKLayout>
                )
            </div>
        </Fragment>
    );
}
export default DetailStatus;