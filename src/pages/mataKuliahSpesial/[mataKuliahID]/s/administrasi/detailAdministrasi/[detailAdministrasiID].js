import {useState } from "react";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import MKLayout from"../../../../../../components/MKLayout"
import styles from "../../../../../../styles/AdministrasiPersyaratan.module.css" ;
import Table from"../../../../../../components/Table";
import Head from "next/head";
import storage from "../../../../../../components/storage";
import { ref, getDownloadURL } from "firebase/storage";
import { downloadFilesHandler } from "../index";
import SuccessAlert from "../../../../../../components/SuccessAlert"
import ErrorAlert from "../../../../../../components/ErrorAlert"

export async function getServerSideProps({ query }) { 
    const mataKuliahID = query.mataKuliahID
    const detailAdministrasiID = query.detailAdministrasiID
    const detailMK_res = await fetch('https://simkuspa-backend.herokuapp.com/detailMataKuliah/'+mataKuliahID);
    const detailMK_data = await detailMK_res.json()

    const res = await fetch('https://simkuspa-backend.herokuapp.com/detailFormPersyaratan/'+mataKuliahID+'/'+detailAdministrasiID);
    const data = await res.json()
    
    
    return { props: {data, mataKuliahID, detailMK_data, detailAdministrasiID} }
  } 

function DetailAdministrasi(props) {
    const mataKuliahID = props.mataKuliahID
    const namaMK = props.detailMK_data.detail_mk[2]
    const detailAdministrasiID = props.detailAdministrasiID

    function setStatusAlert(status){
      if (status) {
       setshowSuccessAlert(true)
       setTextSuccessAlert("Berkas berhasil diunduh")
      }
      else{
      setshowErrorAlert(true)
      setTextErrorAlert("Berkas gagal diunduh karena folder kosong")
      }
  }

  const [showSuccessAlert, setshowSuccessAlert] = useState(false)
  const [textSuccessAlert, setTextSuccessAlert] = useState('')

  const [showErrorAlert, setshowErrorAlert] = useState(false)
  const [textErrorAlert, setTextErrorAlert] = useState('')

    let dataRow = []
    for (let item of props.data.detail_form_persyaratan) {
        console.log(item[0])
        let row = {
          "Nama Mahasiswa": item[0],
          "NPM": item[1],
          "": <FontAwesomeIcon className={styles.iconFile} onClick={e=> downloadFileHandler(e,item[2])} icon="fa-solid fa-file-arrow-down" />
        };
        dataRow.push(row);
      }
    async function downloadFileHandler(event,url){
      const downloadurl = await getDownloadURL(ref(storage,url))
      window.open(downloadurl, '_blank');
      
  }

    const columns = ["Nama Mahasiswa","NPM",""]
    const isAction = false
    const isAnotherAction = false
    
    const isAnyRow = dataRow.length > 0

    return(
        <div className={styles.container}>
            <Head>
            <title>Administrasi</title>
            </Head>
            <MKLayout namaMK= {namaMK}>
                <h4 className={styles.titleText}>Administrasi</h4>
                <h6 className={styles.subTitleText}>{detailAdministrasiID}</h6>
               
              {isAnyRow ?
               <div className={styles.detailFormContainer} >
                 <button type='button' className={styles.buttonAdministrasi} style={{ marginBottom: '1%', alignSelf:'right' }} onClick={e => downloadFilesHandler(e,mataKuliahID,detailAdministrasiID).then(status => setStatusAlert(status))}>
                <p style={{ marginBottom: '0' }}>Unduh Semua</p>
                </button>
                <Table columns={columns} data={dataRow} isAction={isAction} isAnotherAction={isAnotherAction}>
              </Table>
              </div>
              : 
              <p>Belum ada data</p>
              }
                
                <Link href={"../../administrasi/?mataKuliahID="+mataKuliahID} passHref={false}>
                <a type='button' className={styles.buttonAdministrasi} style={{marginBottom: '1%'}} >
                <p style={{marginBottom:'0'}}>Kembali</p>
                </a>
              </Link>
            </MKLayout>
            {showSuccessAlert && <SuccessAlert text={textSuccessAlert} />}
            {showErrorAlert && <ErrorAlert text={textErrorAlert} />}
        </div>
        
    )
}
export default DetailAdministrasi;