import Head from "next/head";
import React, { useEffect, useState } from "react";
import DosenPembimbing from "../../../../components/DosenPembimbing";
import MKLayout from "../../../../components/MKLayout";
import SubJudul from "../../../../components/SubJudul";
import CSRFToken from "../../../../lib/CSRFToken";
import Cookies from "js-cookie";

import styles from "../../../../styles/RingkasanMKMahasiswa.module.css";
import postRequest from "../../../api/post-request";
import { useRouter } from "next/router";
import axios from "axios";
import { verify } from "jsonwebtoken";
import { useTerm } from "../../../../context/termContext";

export async function getServerSideProps(context) {
  const userID = context.req.cookies["userID"]
  const secret = process.env.SECRET;
  const verifiedData = verify(userID, secret);
  const userIDVerified = verifiedData.userID;

  const mkID = context.query.mataKuliahID
  const MK = await fetch('https://simkuspa-backend.herokuapp.com/detailMataKuliah/'+ mkID)
  const dataMK = await MK.json()

  const userMK = mkID + '/' + userIDVerified + '/'
  
  const topik = await fetch('https://simkuspa-backend.herokuapp.com/topikMahasiswa/' + userMK)
  const data = await topik.json()

  const dospem = await fetch('https://simkuspa-backend.herokuapp.com/getDosenPembimbingMK/' + userMK)
  const data_dospem = await dospem.json()
  data['dospem_list'] = data_dospem.dospem_list
  data['mataKuliahID'] = mkID
  data['detail_mk'] = dataMK.detail_mk
  
  return { props: { data } }
}

/**
 * Mengembalikan status atau nama dosen pembimbing
 * @param {array} dospem - Array berisi [status, namaDosen]
 * @returns Status jika statusnya adalah "Menunggu persetujuan dosen" dan jika tidak maka mengembalikan Nama Dosen
 */
function setDospem(dospem) {
  if (dospem != null) {
    if (dospem[0] === 'Menunggu persetujuan dosen') {
      return 'Menunggu Persetujuan Dosen: ' + dospem[1]
    }
    return dospem[1]
  }
  return null
}

function checkTerm(activePeriode, thisPeriode) {
  const activeYear = parseInt(activePeriode.slice(0,4))
  const year = parseInt(thisPeriode.slice(0,4))

  const activeTerm = parseInt(activePeriode.slice(-1))
  const term = parseInt(thisPeriode.slice(-1))

  if (year < activeYear) {
    return false
  } else {
    if (year === activeYear && term < activeTerm) {
      return false
    }
    return true
  }
}

function Ringkasan(props) {
  const router = useRouter();
  const [topik, setTopik] = useState([
    'Anda belum memiliki topik',
    'Anda belum memiliki topik',
    'Anda belum memiliki topik',
    'Anda belum memiliki topik',
  ])
  const [dospem1, setDospem1] = useState(null)
  const [idDosen, setIdDosen] = useState(null)

  const [editMode, setEditMode] = React.useState(false)
  
  const [judulTopik, setJudulTopik] = useState(topik[0])
  const [deskripsiTopik, setDeskripsiTopik] = useState(topik[1])
  const [batasWaktu, setBatasWaktu] = useState('');

  const { activeTerm } = useTerm()
  const term = props.data.detail_mk[1]

  const [activeMode, setActiveMode] = useState(false)

  useEffect(() => {
    const dataTopik = props.data.topik_mahasiswa[0]
    if(dataTopik !== undefined) {
      setTopik([...dataTopik])
      setJudulTopik(dataTopik[1])
      setDeskripsiTopik(dataTopik[2])
    }

    const dospem = props.data.dospem_list

    setDospem1(setDospem(dospem[0]))

    if (dospem[0] !== undefined) {
      setIdDosen(dospem[0][2])
    }

    
    if (activeTerm !== null) {
      setActiveMode(checkTerm(activeTerm, term))
    }
    
  }, [props.data.dospem_list, props.data.topik_mahasiswa])

  async function submitTopikHandler(event) {
    event.preventDefault();
    setEditMode(false);

    const creds = await axios.get('/api/creds', { withCredentials: true })
    const userID = creds.data.userID

    const body = {
      'id_topik': topik[0],
      'id_mahasiswa': userID,
      'judul': judulTopik,
      'deskripsi': deskripsiTopik,
      'batas_waktu': batasWaktu,
      'csrfmiddlewaretoken': Cookies.get('csrftoken')
    }

    setTopik([
      topik[0],
      judulTopik,
      deskripsiTopik,
      batasWaktu
    ])
    
    await postRequest('/editTopikMahasiswa/', body)
    router.reload()
  }

  function editModeActive() {
    setBatasWaktu(topik[3])
    setEditMode(true)
  }

  function cancelHandler() {
    setEditMode(false)
  }

  return (
    <MKLayout namaMK={props.data.detail_mk[2]}>
      <Head>
        <title>Ringkasan Mata Kuliah {props.data.detail_mk[2]}</title>
      </Head>
      
      <div className={styles.ringkasan}>
        {editMode ?
        <div className={styles.topik}>
          <SubJudul text="Topik" />
          <form onSubmit={submitTopikHandler}>
            <CSRFToken type='PRODUCTION' />
            <label htmlFor="judulTopik">Judul Topik</label>
            <input className="form-control" id="judulTopik" onChange={e => setJudulTopik(e.target.value)} defaultValue={topik[1]} required />

            <label htmlFor="deskripsiTopik">Deskripsi Topik</label>
            <textarea className="form-control" id="deskripsiTopik" rows="6" onChange={e => setDeskripsiTopik(e.target.value)} defaultValue={topik[2]} required />

            <label htmlFor="batasWaktu">Batas Waktu Penyelesaian Yang Diharapkan:</label>
            <input type="date" id="batasWaktu" onChange={e => setBatasWaktu(e.target.value)} value={batasWaktu}></input>

            <div>
              <button className={styles.edit} onSubmit={submitTopikHandler} id="submitButton"> Simpan </button>
              <button className={styles.cancel} onClick={cancelHandler} id="cancelButton"> Batal </button>
            </div>
            
          </form>
        </div>
        :
        <div className={styles.topik}>
          <SubJudul text="Topik" />
            <h3 className={styles.h3 + " " + styles.h3_first}>Judul Topik</h3>
            <p>{topik[1]}</p>

            <h3 className={styles.h3}>Deskripsi Topik</h3>
            <p>{topik[2]}</p>
            
            <h3 className={styles.h3}>Batas Waktu Penyelesaian Yang Diharapkan</h3>
            <p>{topik[3]}</p>

            {(topik[0] !== "Anda belum memiliki topik" && activeMode) && 
              <button className={styles.edit} onClick={() => editModeActive()} id="activatingEdit">Edit Topik</button>
            }
        </div>
        }
        <DosenPembimbing dospem1={dospem1} idMK={router.query.mataKuliahID} idDosen={idDosen} activeMode={activeMode}/>
      </div>
    </MKLayout>
  );
}

export default Ringkasan;
