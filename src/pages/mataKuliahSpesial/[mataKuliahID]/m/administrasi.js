import Head from "next/head";
import React, {useEffect, useState} from "react";
import MKLayout from "../../../../components/MKLayout";
import SubJudul from "../../../../components/SubJudul";
import Table from "../../../../components/Table";
import CSRFToken from "../../../../lib/CSRFToken";
import styles from "../../../../styles/AdministrasiMKMahasiswa.module.css";
import Cookies from "js-cookie";
import storage from "../../../../components/storage";
import { ref, uploadBytes } from "firebase/storage";
import { useRouter } from "next/router";
import postRequest from "../../../api/post-request";
import { verify } from "jsonwebtoken";
import Dokumen from "../../../../components/Dokumen";
import SuccessAlert from "../../../../components/SuccessAlert";
import DeletePopUp from "../../../../components/DeletePopUp";
import { useTerm } from "../../../../context/termContext";
import deleteDokumenFromStorage from "../../../../lib/DeleteDokumenFromStorage";

export async function getServerSideProps(context) {
    const mkID = context.query.mataKuliahID
    const mhsID = context.req.cookies["userID"]

    const secret = process.env.SECRET;
    const verifiedData = verify(mhsID, secret);
    const mhsIDVerified = verifiedData.userID;

    const detailMKRes = await fetch('https://simkuspa-backend.herokuapp.com/detailMataKuliah/'+ mkID)
    const detailMK = await detailMKRes.json()  

    const listFormPersyaratanRes = await fetch('https://simkuspa-backend.herokuapp.com/formPersyaratan/' + mkID)
    const listFormPersyaratan = await listFormPersyaratanRes.json()

    const profileMahasiswaRes = await fetch('https://simkuspa-backend.herokuapp.com/getProfileMahasiswa/' + mhsIDVerified)
    const profileMahasiswa = await profileMahasiswaRes.json()

    const dosenPembimbingRes = await fetch('https://simkuspa-backend.herokuapp.com/getDosenPembimbingMK/' + mkID + '/' + mhsIDVerified)
    const dosenPembimbing = await dosenPembimbingRes.json()

    const urlFormPersyaratan = 'https://simkuspa-backend.herokuapp.com/fileFormPersyaratan/'+ mkID + '/' + mhsIDVerified +'/'
    const dataFormPersyaratan = await (getDataFormPersyaratan(listFormPersyaratan,urlFormPersyaratan))

    const detailMigrasiMahasiswa = await fetch('https://simkuspa-backend.herokuapp.com/getDetailMigrasi/' + mkID + '/' + mhsIDVerified)
    const dataDetailMigrasiMahasiswa = await detailMigrasiMahasiswa.json()

  return { props: { mkID, mhsIDVerified, detailMK, profileMahasiswa, dataFormPersyaratan, dosenPembimbing, dataDetailMigrasiMahasiswa, listFormPersyaratan}}
}

export async function getDataFormPersyaratan(listFormPersyaratan,urlFormPersyaratan){
 
  const dataFormPersyaratan = {}

  for (let listForm of Object.values(listFormPersyaratan)){
    for (let form of listForm){
      const res = await fetch(urlFormPersyaratan+form[0])
      const resJson = await res.json()
      dataFormPersyaratan[form[0]]= resJson.detail_file
    }
  }
  return dataFormPersyaratan
}

function OutputKeluaran(dataForm, formMahasiswa) {
  const colsKeluaran = ["Keluaran","Keterangan","Status"]

  dataForm = dataForm.list_form_persyaratan.filter(form => form[1] === 'Keluaran')
  let dataKeluaran = []
  for (let item of dataForm) {
    const [keterangan, status] = formMahasiswa[item[0]].length === 0 ? 
      ["Anda belum melengkapi dokumen ini", <span className="badge rounded-pill bg-danger">Belum Terpenuhi</span>] 
      : 
      ["Anda sudah melengkapi dokumen ini", <span className="badge rounded-pill bg-success">Sudah Terpenuhi</span>]

    dataKeluaran.push({
      "Keluaran": item[0],
      "Keterangan": keterangan,
      "Status": status
    })
  }

  return dataKeluaran.length === 0 ? 
    <p className={styles.keluaran}>Mata kuliah ini tidak memiliki keluaran</p>
    :
    <div className={styles.tabel + ' mb-5'}>
      <Table
        columns={colsKeluaran}
        data={dataKeluaran}
        isAction={false}
        buttonLabel={null}
        isAnotherAction={false}
        anotherButtonLabel={null}
      />
    </div>
}

function OutputPrasyarat(dosenPembimbing, dataForm, formMahasiswa) {
  const colsPrasyarat = ["Prasyarat","Keterangan","Status"]
  dataForm = dataForm.list_form_persyaratan.filter(form => form[1] === 'Prasyarat')

  const [keterangan, status]  = (dosenPembimbing !== undefined && dosenPembimbing[0] !== "Menunggu persetujuan dosen") ? 
    [dosenPembimbing[1], <span className="badge rounded-pill bg-success">Sudah Terpenuhi</span>]
    :
    ["Anda belum memiliki Dosen Pembimbing", <span className="badge rounded-pill bg-danger">Belum Terpenuhi</span>]

  let dataPrasyarat = [{
    "Prasyarat": "Memiliki Dosen Pembimbing",
    "Keterangan": keterangan,
    "Status": status
  }]

  for (let item of dataForm) {
    const [keteranganForm, statusForm] = formMahasiswa[item[0]].length === 0 ? 
      ["Anda belum melengkapi dokumen ini", <span className="badge rounded-pill bg-danger">Belum Terpenuhi</span>] 
      : 
      ["Anda sudah melengkapi dokumen ini", <span className="badge rounded-pill bg-success">Sudah Terpenuhi</span>]

    dataPrasyarat.push({
      "Prasyarat": item[0],
      "Keterangan": keteranganForm,
      "Status": statusForm
    })
  }

  return dataPrasyarat.length === 0 ? 
    <p className={styles.keluaran}>Mata kuliah ini tidak memiliki keluaran</p>
    :
    <div className={styles.tabel + " mb-5"}>
      <Table
        columns={colsPrasyarat}
        data={dataPrasyarat}
        isAction={false}
        buttonLabel={null}
        isAnotherAction={false}
        anotherButtonLabel={null}
      />
    </div>
}

function OutputMigrasi(dataMigrasi, statusMigrasi, namaMK, handlerMigrasi) {
  return (
    <div>
      <br></br>
      <div data-testid="migrasiText1"><SubJudul text="Migrasi ke Semester Selanjutnya" /></div>
      <p 
        className={styles.tulisan} 
        data-testid="migrasiText2">Migrasi data-data mata kuliah ini ke semester selanjutnya jika Anda belum menyelesaikannya di semester ini.
      </p>
      {dataMigrasi.length != 0 && statusMigrasi=="REQ" && (
        <p 
          className={styles.tulisanProses} 
          data-testid="prosesText">Proses pengajuan migrasi Mata Kuliah {namaMK} sedang berjalan
        </p>
      )}

      {dataMigrasi.length != 0 && statusMigrasi=="Disetujui" && (
        <p 
          className={styles.tulisanProses} 
          data-testid="setujuText">Permohonan migrasi Mata Kuliah {namaMK} disetujui
        </p>
      )}

      <CSRFToken type="PRODUCTION" />
      {dataMigrasi.length == 0 && (
        <div className={styles.buton}>
          <a><button 
                className = {" btn btn-danger"} 
                data-testid="migrasiButton1" 
                onClick={handlerMigrasi} 
                type="submit">Migrasi
              </button>
            </a>
        </div>
      )}
      <br></br>
    </div>
  )
}

function checkListFormPersyaratan(listFormPersyaratan, formPersyaratan, setJenisForm, handleIdDelete, activeMode) {
  if(Object.keys(formPersyaratan).length !=0){
    listFormPersyaratan = Object.keys(formPersyaratan).map((key) => 
    <div>
      <h3 className={styles.h3 + ' mt-3 mb-3'}>{key}</h3>
      {formPersyaratan[key].length === 0 ? 
        <div>
          {activeMode && 
            <button 
              className={styles.unggahDokumen} 
              onClick={() => {setJenisForm(key)}}  
              data-bs-toggle="modal" 
              data-bs-target="#modalUpload" 
              id="unggahDokumen"
            >Unggah Dokumen</button>
          }
        </div>
        : 
        <div>
          <Dokumen nama={formPersyaratan[key][0][1]} url={formPersyaratan[key][0][0]}></Dokumen>
          <button className={styles.hapusDokumen} onClick={()=> handleIdDelete(key,formPersyaratan[key][0][0])} id="hapusDokumen">Hapus Dokumen</button>
        </div>
      }
    </div>
    ) 
  }
  return listFormPersyaratan
}

function checkTerm(activePeriode, thisPeriode) {
  const activeYear = parseInt(activePeriode.slice(0,4))
  const year = parseInt(thisPeriode.slice(0,4))

  const activeTerm = parseInt(activePeriode.slice(-1))
  const term = parseInt(thisPeriode.slice(-1))

  if (year < activeYear) {
    return false
  } else {
    if (year === activeYear && term < activeTerm) {
      return false
    }
    return true
  }
}

function Administrasi(props) {
    const router = useRouter();

    const { activeTerm } = useTerm()
    const [activeMode, setActiveMode] = useState(false)
    const term = props.detailMK.detail_mk[1]
    useEffect(() => {
      if (activeTerm !== null) {
        setActiveMode(checkTerm(activeTerm, term))
      }
    })

    const npm = props.profileMahasiswa.profile_mahasiswa[0][4]
    const mkID = props.mkID
    let mhsID = props.mhsIDVerified
    if (Array.isArray(mhsID)){
      mhsID = props.mhsIDVerified[0]
    }

    let statusMigrasi
    const dataMigrasi = props.dataDetailMigrasiMahasiswa.detail_migrasi_per_mahasiswa
    if(dataMigrasi.length!=0) {
      statusMigrasi = props.dataDetailMigrasiMahasiswa.detail_migrasi_per_mahasiswa[0][5]
    }
    const namaMatkul = props.detailMK.detail_mk[2]
    const periodeMatkul = props.detailMK.detail_mk[1]
    const [messagePopUpMigrasi, setMessagePopUpMigrasi] = useState('')
    const [showPopUpKonfirmasiMigrasi, setPopUpKonfirmasiMigrasi] = useState(false)
    const dosenPembimbing = props.dosenPembimbing.dospem_list[0]
    const [jenisForm, setJenisForm] = useState(null)
    const [dokumen, setDokumen] = useState(null)
    const [dokumenName, setDokumenName] = useState(null)
    const [urlDeletedDokumen, setUrlDeletedDokumen] = useState(null)
    const [deletedDokumen, setDeletedDokumen] = useState(null)
    const [showPopUpDelete, setShowPopUpDelete] = useState(false)
    const [showAlert, setShowAlert] = useState(false)
    const [textAlert, setTextAlert] = useState('')
    const [typeFileAlert, setTypeFileAlert] = useState(false)
    const [showAlertBerhasilMigrasi, setShowAlertBerhasilMigrasi] = useState(false)
    const [textAlertMigrasi, setTextAlertMigrasi] = useState('')  

    const formPersyaratan = props.dataFormPersyaratan
    const listForm = props.listFormPersyaratan
    const outputPrasyarat = OutputPrasyarat(dosenPembimbing, listForm, formPersyaratan)
    const outputKeluaran = OutputKeluaran(listForm, formPersyaratan)

    async function uploadHandler(event){
      event.preventDefault();
      if(dokumen!==null){
        const url = "production/FormPersyaratan/" + mkID + "/" +  jenisForm + "/" + dokumenName
        const jenis = jenisForm
        const body = { 'id_mk': mkID, 'id_mahasiswa': mhsID, 'url': url,  'nama_file':dokumenName, 'jenis_form':jenis, 'csrfmiddlewaretoken': Cookies.get('csrftoken') }
        await postRequest('/uploadFormPersyaratan/', body)
        const dokumenPath = ref(storage, url)
        await uploadBytes(dokumenPath, dokumen)
        router.reload()
        setTextAlert('Dokumen berhasil diunggah')
        setShowAlert(true)
      }
      else{
        alert("Harap masukkan dokumen yang ingin Anda unggah.")
      }
    }

    function cancelUploadHandler(resetTypeFileAlert){
      document.getElementById("dokumen").value = null
      setDokumen(null)
      setDokumenName(null)
      if(resetTypeFileAlert){
        setTypeFileAlert(false)
        setJenisForm(null)
      }
    }

    function dokumenHandler(event) {
      event.preventDefault()
      setDokumen(null)
      setDokumenName(null)
      setTypeFileAlert(false)
      if (event.target.files[0] != null) {
        if (event.target.files[0].type != "application/pdf"){
          setTypeFileAlert(true)
          cancelUploadHandler(false)
        }
        else{
          setDokumen(event.target.files[0])
          const name = npm + "_" + jenisForm + ".pdf"
          setDokumenName(name)
        }
      }
    }

    function handleIdDelete(idDelete,urlDelete) {
      setUrlDeletedDokumen(urlDelete)
      setDeletedDokumen(idDelete)
      setShowPopUpDelete(true)
    }

    let listFormPersyaratan = <p className={styles.tulisanProses}>Tidak ada form persyaratan.</p>
    listFormPersyaratan = checkListFormPersyaratan(listFormPersyaratan, formPersyaratan, setJenisForm, handleIdDelete, activeMode)
  
    function handleDelete(choose) {
      if (choose) {
        deleteDokumen()
      }
      setDeletedDokumen(null)
      setUrlDeletedDokumen(null)
      setShowPopUpDelete(false)
    }

    async function deleteDokumen(){
      deleteDokumenFromStorage(urlDeletedDokumen)
      const urlDelete = 'https://simkuspa-backend.herokuapp.com/hapusFormPersyaratan/' + mkID + '/' + mhsID + '/' + deletedDokumen
      await fetch(urlDelete)
      router.reload()
      setTextAlert('Dokumen berhasil dihapus')
      setShowAlert(true)
    }

    function handlerMigrasi(event) {
      event.preventDefault()
      let textPopUpMigrasi = "Apakah Anda yakin untuk melakukan migrasi mata kuliah " + namaMatkul +" ?"
      setMessagePopUpMigrasi(textPopUpMigrasi)
      setPopUpKonfirmasiMigrasi(true)
    }

    async function migrasiHandler(choose){
      if(choose){
        const body = { 'id_mk': mkID, 'id_mahasiswa': mhsID, 'periode_lama': periodeMatkul, 'csrfmiddlewaretoken': Cookies.get('csrftoken') }
        await postRequest('/migrasi', body)
        router.reload()
        let textAlertBerhasilMigrasi = "Pengajuan migrasi mata kuliah " + namaMatkul + " berhasil"
        setTextAlertMigrasi(textAlertBerhasilMigrasi)
        setShowAlertBerhasilMigrasi(true)
      }
      setPopUpKonfirmasiMigrasi(false)
    }

  const outputMigrasi = OutputMigrasi(dataMigrasi, statusMigrasi, props.detailMK.detail_mk[2], handlerMigrasi)

  return (
    <MKLayout namaMK={props.detailMK.detail_mk[2]}>
      <Head>
        <title>Administrasi Mata Kuliah {props.detailMK.detail_mk[2]}</title>
      </Head>
      
      <div className={styles.ringkasan}>
            <SubJudul text="Prasyarat Mata Kuliah" />
              {outputPrasyarat}
            <SubJudul text="Keluaran Mata Kuliah" />
              {outputKeluaran}
            <SubJudul text="Form Administrasi" />
              {listFormPersyaratan}

            {outputMigrasi}
      </div>

      <div className={" modal fade"} id="modalUpload" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <CSRFToken type="PRODUCTION" />
                <div className={" modal-dialog"}>
                    <div className={" modal-content"}>
                        <div className={" modal-header"}>
                            <h5 className={" modal-title"}>Unggah {jenisForm}</h5>
                        </div>
                        <div className={" modal-body"}>
                            <input id="dokumen" type="file"  onChange={dokumenHandler}></input>
                            {typeFileAlert &&<div style={{color:'red', marginTop:'2%', marginBottom:'0'}}>File yang Anda unggah tidak berekstensi .pdf</div>}
                            <br></br><br></br>
                            Apakah Anda yakin ingin mengunggah dokumen ini? (File harus berekstensi .pdf)
                        </div>
                        <div className={" modal-footer"}>
                            <button id="cancelUnggah" type="button" data-bs-dismiss="modal" onClick={e=> cancelUploadHandler(true)} className={styles.cancelUnggah}>Tidak</button>
                            <button id="yesUnggah" type="button"  onClick={uploadHandler} data-bs-dismiss="modal" className={styles.yesUnggah}>Ya</button>
                        </div>
                    </div>
                </div>
      </div>

      {showPopUpDelete && <DeletePopUp onDialog={handleDelete} message="Apakah Anda yakin untuk menghapus dokumen ini?" />}
      {showAlert && <SuccessAlert text={textAlert} />}

      {showPopUpKonfirmasiMigrasi && <DeletePopUp onDialog={migrasiHandler} message={messagePopUpMigrasi}/>}
      {showAlertBerhasilMigrasi && <SuccessAlert text={textAlertMigrasi} />}
    </MKLayout>
  );
}

export default Administrasi;