import PopUp from "../../components/PopUp";
import Heading from "../../components/Heading";
import { Fragment, useRef } from "react";
import { useRouter } from 'next/router';
import styles from "../../styles/TambahMataKuliahSpesial.module.css";
import Head from "next/head";
import CSRFToken from "../../lib/CSRFToken";
import Cookies from 'js-cookie';
import postRequest from "../api/post-request";

function Tambah() {
  const kode = useRef();
  const nama = useRef();
  const periode = useRef();
  const kredit = useRef();
  const kurikulum = useRef();
  const kapasitas = useRef();
  const router = useRouter();
  
  async function tambahHandler(event) {
    event.preventDefault();

    const body = {
      'kode': kode.current.value,
      'nama' : nama.current.value,
      'periode' : periode.current.value,
      'kredit' : parseInt(kredit.current.value),
      'jumlah_mahasiswa' : parseInt(kapasitas.current.value),
      'kurikulum' : kurikulum.current.value,
      'csrfmiddlewaretoken': Cookies.get('csrftoken')
    }
    console.log(body)

    const res = await postRequest('/tambahMataKuliah', body);
    const data = await res.data;  

    if (typeof window !== 'undefined') {
      window.localStorage.setItem('SuccessCreateForm', data.status)
      window.localStorage.setItem('matKul', body["nama"])
    }

    router.push("/administrasi/");
  }

  return (
    <Fragment>
      <Head>
        <title>Tambah Mata Kuliah Spesial</title>
      </Head>
      <section className={styles.tambah + " container"}>
        <div className={styles.tambahContainer}>
          <Heading text = "| Tambah Mata Kuliah Spesial" type="kecil"></Heading>
          
          <form onSubmit={tambahHandler} className = {" needs-validation row g-3"} >
                <CSRFToken type="PRODUCTION" />

                <div className={styles.line + " col-md-6 mb-3"}>
                  <label htmlFor=" kodeMataKuliah" className = {" form-label"}>Kode Mata Kuliah<span className={styles.req}>*</span></label>
                  <input type = "text" className = {"  form-control"} placeholder="Kode Mata Kuliah" ref={kode} required></input>
                  <div id="kodeHelp" className="form-text">contoh : CSGE802097</div>
                </div>
                
                <div className={styles.line + " col-md-6 mb-3"}>
                  <label htmlFor=" namaMataKuliah" className = {" form-label"}>Nama Mata Kuliah<span className={styles.req}>*</span></label>
                  <input type = "text" className = {"   form-control"} placeholder="Nama Mata Kuliah" ref={nama} required></input>
                  <div id="namaHelp" className="form-text">contoh : Publikasi Ilmiah</div>
                </div>

                <div className={styles.line + " form-group col-md-4 mb-3"}>
                  <label htmlFor="validationCustom04" className="form-label">Periode<span className={styles.req}>*</span></label>
                  <select className="form-select" id="validationCustom04" ref={periode} required>
                    <option selected disabled value="">Pilih Periode Mata Kuliah Spesial</option>
                    <option>2022-2</option>
                    <option>2023-1</option>
                    <option>2023-2</option>
                  </select>
                </div>
              
                <div className={styles.line + " form-group col-md-4 mb-3"}>
                  <label htmlFor="validationCustom04" className="form-label">Kredit<span className={styles.req}>*</span></label>
                  <select className="form-select" id="validationCustom04" ref={kredit} required>
                    <option selected disabled value="">Pilih Jumlah Kredit Mata Kuliah Spesial</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                  </select>
                </div>

                <div className={styles.line + " form-group col-md-6 mb-3"}>
                  <label htmlFor=" kurikulumMataKuliah" className = {" form-label"}>Kurikulum<span className={styles.req}>*</span></label>
                  <input type = "text" className = {"   form-control"} placeholder="Kurikulum Mata Kuliah" ref={kurikulum} required></input>
                  <div id="kurikulumHelp" className="form-text">contoh : 03.00.12.01-2020</div>
                </div>

                <div className={styles.line + " form-group col-md-6 mb-3"}>
                  <label htmlFor=" kurikulumMataKuliah" className = {" form-label"}>Kapasitas<span className={styles.req}>*</span></label>
                  <input type = "number" className = {"   form-control"} placeholder="Kapasitas Mata Kuliah" min="1" max="60" ref={kapasitas} required></input>
                  <div id="kurikulumHelp" className="form-text">contoh : 30</div>
                </div>
              
              <p><span className={styles.req}>* Kolom dengan tanda ini wajib diisi</span></p>

              <div className={styles.buton}>
                <a><button data-testid="simpanButton" className = {" btn btn-danger"} type="submit">Simpan</button></a>
                <p data-testid="button" type="button" id="tombolBatal" className={"btn btn-link"} data-bs-toggle="modal" data-bs-target="#staticBackdrop">Batal</p>
              </div>
            </form>
        </div>

       <PopUp text="Perubahan belum disimpan. Apakah Anda yakin untuk batal?" url="/administrasi/"></PopUp>
        
      </section>
    </Fragment>
  );
}

export default Tambah;
