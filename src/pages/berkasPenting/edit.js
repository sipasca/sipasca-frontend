import Link from "next/link";
import { Fragment, useState } from "react";
import styles from "../../styles/PusatInformasi.module.css";
import BerkasPentingBox from "../../components/BerkasPentingBox";
import storage from "../../components/storage"
import { ref, uploadBytes } from "firebase/storage";
import postRequest from "../api/post-request";
import Cookies from "js-cookie";
import CSRFToken from "../../lib/CSRFToken";
import Head from 'next/head'
import SuccessAlert from "../../components/SuccessAlert";
import DeletePopUp from "../../components/DeletePopUp";
import { useRouter } from "next/router"
import ButtonWithIcon from "../../components/ButtonWithIcon";

import Heading from "../../components/Heading";
import deleteDokumenFromStorage from "../../lib/DeleteDokumenFromStorage";

export async function getServerSideProps() {
  const [template, umum, lainlain] = await Promise.all([
    fetch('https://simkuspa-backend.herokuapp.com/berkasPentingTemplate'),
    fetch('https://simkuspa-backend.herokuapp.com/berkasPentingUmum'),
    fetch('https://simkuspa-backend.herokuapp.com/berkasPentingLainlain')
  ]);
  const [berkasUmum, berkasTemplate, berkasLainLain] = await Promise.all([
    umum.json(),
    template.json(),
    lainlain.json()
  ]);
  return { props: { berkasUmum, berkasTemplate, berkasLainLain } };
}

function Edit(props) {
  let [file, setFile] = useState(null)
  let [berkasName, setBerkasName] = useState('');
  let [berkasType, setBerkasType] = useState(null)
  const [berkasId, setBerkasId] = useState(0)
  const [berkasUrl, setBerkasUrl] = useState(null)
  const [showPopUp, setShowPopUp] = useState(false)
  const [showAlert, setshowAlert] = useState(false)
  const [textAlert, setTextAlert] = useState('')
  const router = useRouter();

  function handleId(idDelete,urlDelete) {
    setBerkasId(idDelete)
    setBerkasUrl(urlDelete)
    setShowPopUp(true)
    setshowAlert(false)
    setTextAlert("Berkas berhasil dihapus")
  }

  function handleDelete(choose) {
    if (choose) {
      deleteTrue(berkasId,berkasUrl)
      setBerkasId(0)
      setShowPopUp(false)
    } else {
      setBerkasId(0)
      setBerkasUrl(null)
      setShowPopUp(false)
    }
  }

  async function deleteTrue(nomor,urlDelete) {
    deleteDokumenFromStorage(urlDelete)
    await fetch(`https://simkuspa-backend.herokuapp.com/deleteBerkasPenting/${nomor}`)
    router.push('/berkasPenting/edit')
    setshowAlert(true)
  }

  function fileHandler(event) {
    if (event.target.files[0] == null) {
      setBerkasName("")
    } else {
      setFile(event.target.files[0])
      setBerkasName(event.target.files[0].name)
    }
  }

  function cancelFileHandler() {
    document.getElementById("berkas").value = null
    setBerkasName("")
  }

  async function uploadFileHandler(event) {
    event.preventDefault();
    const berkas_directory = "production/berkasPenting/" + berkasName
    const filePath = ref(storage, berkas_directory)
    const uploaded = await uploadBytes(filePath, file)
    if (uploaded != null) {
      const body = { 'url': berkas_directory, 'type': berkasType, 'name': berkasName, 'csrfmiddlewaretoken': Cookies.get('csrftoken') }
      const res = await postRequest('/createBerkasPenting', body)
      const data = res.data;
      if (data.Success != "Success") {
        alert("Terjadi Kesalahan saat mengunggah data")
      }
    }
    else {
      alert("Terjadi Kesalahan saat mengunggah data")
    }
    document.getElementById("berkas").value = null
    setBerkasName("")
    router.push('/berkasPenting/edit')
    setshowAlert(true)
    setTextAlert("Berkas berhasil disimpan")
  }
  const berkasUmumList = props.berkasUmum.berkas_penting_list
  const berkasTemplateList = props.berkasTemplate.berkas_penting_list
  const berkasLainLainList = props.berkasLainLain.berkas_penting_list

  const berkasUmum = berkasUmumList.map((post) => < BerkasPentingBox handleId={handleId} mode='edit' nama={post[3]} url={post[1]} id={post[0]}></BerkasPentingBox>);
  const berkasTemplate = berkasTemplateList.map((post) => < BerkasPentingBox handleId={handleId} mode='edit' nama={post[3]} url={post[1]} id={post[0]}></BerkasPentingBox>);
  const berkasLainLain = berkasLainLainList.map((post) => < BerkasPentingBox handleId={handleId} mode='edit' nama={post[3]} url={post[1]} id={post[0]}></BerkasPentingBox>);
  const role = 'staf'

  return (
    <Fragment>
    <Head>
      <title>Berkas Penting</title>
    </Head>
    <main>
      {role == 'staf' &&
      <div className="container-fluid" style={{marginTop:"2%"}}>
        <div className="row">
        `<Heading text = "| Berkas Penting" type="pusatInformasi"></Heading>
        </div>
            <div className={styles.containerBerkasPerJenis}>
              <div className="d-flex justify-content-between">
                <p className={styles.subtitle}>Umum</p>
                <div style={{fontSize:"1rem"}}>
                  <ButtonWithIcon to="/berkasPenting" icon="fa-solid fa-xmark" text="Batal" />
                </div>
              </div>
              {berkasUmum}
              <ButtonWithIcon icon="fa-solid fa-plus" text="Unggah File" data-bs-toggle="modal" data-bs-target="#modalUnggah" onClick={e => { setBerkasType("Umum"); }} />
            </div>

            <div className={styles.containerBerkasPerJenis}>
              <p className={styles.subtitle}>Template</p>
              {berkasTemplate}
              <ButtonWithIcon icon="fa-solid fa-plus" text="Unggah File" data-bs-toggle="modal" data-bs-target="#modalUnggah" onClick={e => { setBerkasType("Template"); }} />
            </div>

            <div className={styles.containerBerkasPerJenis}>
              <p className={styles.subtitle}>Lain-lain</p>
              {berkasLainLain}
              <ButtonWithIcon icon="fa-solid fa-plus" text="Unggah File" data-bs-toggle="modal" data-bs-target="#modalUnggah" onClick={e => { setBerkasType("Lain-Lain"); }} />
            </div>
            <div className={" modal fade"} id="modalUnggah" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <CSRFToken type="PRODUCTION" />
              <div className={" modal-dialog"}>
                <div className={" modal-content"}>
                  <div className={" modal-header"}>
                    <h5 className={" modal-title"}>Unggah Berkas Penting</h5>
                  </div>
                  <div className={" modal-body"}>
                    <table>
                      <thead></thead>
                      <tbody>
                        <tr>
                          <td><label htmlFor="berkas">Attachment</label></td>
                          <td><input id="berkas" type="file" onChange={fileHandler}></input> <br /></td>
                        </tr>
                        <tr>
                          <td><label htmlFor="berkas-name">Save As</label></td>
                          <td><input id="berkas-name" value={berkasName} onChange={e => { setBerkasName(e.currentTarget.value); }} type="text"></input></td>
                        </tr>
                      </tbody>
                    </table>
                    <br /><br />
                    Apakah Anda yakin ingin mengunggah berkas ini?
                  </div>
                  <div className={" modal-footer"}>
                    <button id="cancelUnggah" type="button" className={" btn btn-link"} data-bs-dismiss="modal" onClick={cancelFileHandler}>Tidak</button>
                    <Link href="/berkasPenting/edit" passHref={true} ><button id="yesUnggah" type="button" onClick={uploadFileHandler} data-bs-dismiss="modal" className={" btn btn-primary"}>Ya</button></Link>
                  </div>
                </div>
              </div>
            </div>
            {showPopUp && <DeletePopUp onDialog={handleDelete} message="Apakah Anda yakin untuk menghapus berkas ini?" />}
            {showAlert && <SuccessAlert text={textAlert} />}
          </div>}
      </main>
    </Fragment>
  );
}
export default Edit;