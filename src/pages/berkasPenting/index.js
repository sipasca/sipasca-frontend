import { Fragment, useEffect, useState } from "react";
import Head from "next/head";

import styles from "../../styles/PusatInformasi.module.css";
import BerkasPentingBox from "../../components/BerkasPentingBox";
import ButtonWithIcon from "../../components/ButtonWithIcon";
import Heading from "../../components/Heading";
import axios from "axios";

export async function getServerSideProps() {
  const [umumRes, templateRes, lainlainRes] = await Promise.all([
    fetch('https://simkuspa-backend.herokuapp.com/berkasPentingUmum'), 
    fetch('https://simkuspa-backend.herokuapp.com/berkasPentingTemplate'),
    fetch('https://simkuspa-backend.herokuapp.com/berkasPentingLainlain')
  ]);
  const [umum, template, lainlain] = await Promise.all([
    umumRes.json(), 
    templateRes.json(),
    lainlainRes.json()
  ]);
  return { props: { umum, template, lainlain } };
}

function BerkasPenting(props) {
  const [role, setRole] = useState('pengguna');
  useEffect(() => {
    async function setRoleData() {
      const res = await axios.get('/api/creds', { withCredentials: true })
      const newRole = res.data.role
      setRole(newRole);
    }
    setRoleData();
  })
  const listBerkasUmum = props.umum.berkas_penting_list
  const listBerkasTemplate = props.template.berkas_penting_list
  const listBerkasLainLain = props.lainlain.berkas_penting_list

  const berkasUmum = listBerkasUmum.map((post) =><BerkasPentingBox nama={post[3]} url={post[1]} id={post[0]}></BerkasPentingBox>);
  const berkasTemplate = listBerkasTemplate.map((post) =><BerkasPentingBox nama={post[3]} url={post[1]} id={post[0]}></BerkasPentingBox>);
  const berkasLainLain = listBerkasLainLain.map((post) =><BerkasPentingBox nama={post[3]} url={post[1]} id={post[0]}></BerkasPentingBox>);

  return (
    <Fragment>
    <Head>
      <title>Berkas Penting</title>
      <meta name="description" content="Sistem Informasi Mata Kuliah Pasca" />
    </Head>
    <main>
      <div className="container-fluid" style={{marginTop:"2%"}}>
        <div className="row">
          <Heading text = "| Berkas Penting" type="pusatInformasi"></Heading>
        </div>   
        <div className={styles.containerBerkasPerJenis}>
          <div className="d-flex justify-content-between">
            <p className={styles.subtitle} id="umum">Umum</p>
            <div style={{fontSize:"1rem"}}>
              {role == 'staf' &&
                <ButtonWithIcon to="/berkasPenting/edit" icon="fas fa-edit" text="Edit" />
              }
            </div>
            
            </div>
            {berkasUmum}
          </div>
          <div className={styles.containerBerkasPerJenis}>
            <p className={styles.subtitle} id="template">Template</p>
              {berkasTemplate}
          </div>
          <div className={styles.containerBerkasPerJenis}>
              <p className={styles.subtitle} id="lainLain">Lain-lain</p>
              {berkasLainLain}
          </div>
        </div>
    </main>
  </Fragment>
  );
}

export default BerkasPenting;