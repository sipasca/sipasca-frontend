import SuccessAlert from "../../components/SuccessAlert";
import styles from "../../styles/PengumumanPage.module.css";
import {useState , useEffect} from "react";
import PengumumanBox from "../../components/pengumumanBox";
import ButtonWithIcon from "../../components/ButtonWithIcon";
import Head from "next/head";
import Heading from "../../components/Heading"
import axios from "axios";
import FormatTanggal from "../../components/FormatTanggal";

export async function getServerSideProps(){
  const res = await fetch("https://simkuspa-backend.herokuapp.com/pengumumanAll");

  const data = await res.json();

  return { props: { data } }

}

function Pengumuman(props) {
  const [role, setRole] = useState('staf');
  useEffect(() => {  
    if (typeof window !== 'undefined'){
      async function setRoleData() {
        const res = await axios.get('/api/creds', { withCredentials: true })
        const newRole = res.data.role
        setRole(newRole);
      }
      setRoleData();
    }
  })
  const arrayPengumuman = props.data.pengumuman_list

  const [tanda_hapus, setTandaHapus] = useState(false);

  useEffect( () => {
    if (typeof window !== 'undefined') {
        const lokal_hapus = window.localStorage.getItem('tanda_hapus')
        window.localStorage.removeItem("tanda_hapus");
    if (lokal_hapus === 'SUCCESS') {
        setTandaHapus(true);
        }   
    }
  },[])

  return (
    
    <div className={styles.container}>
      <Head>
        <title>Pengumuman</title>
      </Head>
      {tanda_hapus && <SuccessAlert text="Pengumuman berhasil dihapus" />}

      
        <div className={styles.row}>
          <Heading text = "| Pengumuman" type="pengumuman"></Heading>
          <div className={styles.buttons}>
           {role === 'staf' && 
            <div className={styles.cols}> 
                <ButtonWithIcon icon="fa-solid fa-pen-to-square" to="pengumuman/pengumumanEdit" text="Edit"/>
            </div>
            }
            </div>
        </div>
    
      {arrayPengumuman.map(item =>   
       <div className="row">
          <div className="col-sm-11"> 
              <PengumumanBox 
              judul = {item[1]}
              tanggal = {<FormatTanggal tanggal={item[3]}></FormatTanggal>}
              konten = {item[2]}
              selengkapnya = {"detailPengumuman/" + item[0] +"/" }

              />
            </div>
        </div>

      ) }

      
    </div>
  );
}

export default Pengumuman;

