import Link from "next/link";
import Head from "next/head";

import Heading from "../../../components/Heading"
import { Fragment, useRef} from "react";
import postRequest from "../../api/post-request";
import Cookies from 'js-cookie';
import CSRFToken from "../../../lib/CSRFToken";
import styles from "../../../styles/BuatPengumuman.module.css";
import { useRouter } from "next/router";

export async function getServerSideProps(context){
    const id = context.query.pengumumanID
    const res = await fetch("https://simkuspa-backend.herokuapp.com/pengumumanDetail/" + id);
  
    const data = await res.json();
  
    return { props: { data } }
  
}

function Edit(props) {
     
    
    const arrayPengumuman = props.data.pengumuman_detail[0]
    
    const judul = useRef();
    const konten = useRef();
    const router = useRouter();
    async function formHandler(event){
        event.preventDefault();

        const judulInput = judul.current.value
        const kontenInput = konten.current.value
        if (judulInput  === null || String(judulInput).match(/^ *$/) !== null || kontenInput  === null || String(kontenInput).match(/^ *$/) !== null){
            alert("Harap isi semua bidang");
        }
        else {
        const body = {
            'judul' : judul.current.value,
            'konten' : konten.current.value,
            'csrfmiddlewaretoken' : Cookies.get('csrftoken')
        }
        
        const res = await postRequest('/editPengumuman/'+ arrayPengumuman[0], body, 'PRODUCTION');
        const data = res.data;
        
        if(data.Success == "Pengumuman Berhasil Diedit"){
            window.localStorage.setItem('tandaEdit', data.Success)
        }
        router.push('/pengumuman/pengumumanEdit')
    }
        
        
    }
  return (
      
    <Fragment>
        <Head>
            <title>Edit Pengumuman</title>
        </Head>
      <section className={styles.buat + " container"}>
        <div className={styles.buatContainer}>
            <Heading text = "| Edit Pengumuman" type="kecil"></Heading>
          
            <form className = {" needs-validation"} onSubmit={formHandler}>
                <CSRFToken type="PRODUCTION"/>
                <div className={styles.line + " mb-3"}>
                    <label htmlFor=" exampleFormControlInput1" className = {" form-label"} >Judul</label>
                    <textarea className = {"   form-control"} id="exampleFormControlTextarea1"  placeholder="Required judul" rows="2" required ref={judul}>{arrayPengumuman[1]}</textarea>
                </div>

                <div className={styles.line + " mb-3"}>
                    <label htmlFor="exampleFormControlTextarea1" className = {" form-label"}>Konten</label>
                    <textarea className = {" form-control"} id="exampleFormControlTextarea1" rows="10"  placeholder="Required konten" required ref={konten}>{arrayPengumuman[2]}</textarea>
                </div>

                <div className={styles.buton}>
                    <a><button className = {" btn btn-danger"} type="submit">Simpan</button></a>
                    <p type="button" className={"btn btn-link"} data-bs-toggle="modal" data-bs-target="#staticBackdrop">Batal</p>
                </div>
            
            </form>

            
        </div>

        {/* <!-- Modal --> */}
        <div className = {" modal fade"} id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div className = {" modal-dialog"}>
                <div className = {" modal-content"}>
                    <div className = {" modal-header"}>
        
                    </div>
                    <div className = {" modal-body"}>
                        Perubahan belum disimpan. Apakah Anda yakin untuk batal?
                    </div>
                    <div className = {" modal-footer"}>
                        <p type="button" className = {" btn btn-link"} data-bs-dismiss="modal">Tidak</p>
                        <Link href="/pengumuman/pengumumanEdit"><button type="button" className = {" btn btn-danger"} data-bs-dismiss="modal">Ya</button></Link>
                    </div>
                </div>
            </div>
        </div>

        
      </section>

    </Fragment>
  );
}

export default Edit;



