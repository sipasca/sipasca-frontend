import PopUp from "../../components/PopUp";
import { Fragment, useRef } from "react";
import styles from "../../styles/BuatPengumuman.module.css";
import Heading from "../../components/Heading"
import CSRFToken from "../../lib/CSRFToken";
import Cookies from 'js-cookie';
import postRequest from "../api/post-request";
import { useRouter } from 'next/router';
import Head from "next/head";

function Buat() {
  const judul = useRef();
  const konten = useRef();
  const router = useRouter();

  async function buatHandler(event) {
    event.preventDefault();

    const judulInput = judul.current.value
    const kontenInput = konten.current.value
    if (judulInput  === null || String(judulInput).match(/^ *$/) !== null || kontenInput  === null || String(kontenInput).match(/^ *$/) !== null){
      alert("Harap isi semua bidang");
    }
    else {
    const body = {
      'judul': judul.current.value,
      'konten': konten.current.value,
      'csrfmiddlewaretoken': Cookies.get('csrftoken')
    }

    const res = await postRequest('/buatPengumuman', body);
    const data = res.data;  

    if (data.status === 'SUCCESS') {
      if (typeof window !== 'undefined') {
        window.localStorage.setItem('tanda', res.data.status)
      }
    }

    router.push('pengumumanEdit');
  }
}

  return (
    <Fragment>
      <Head>
        <title>Buat Pengumuman</title>
      </Head>
      <section className={styles.buat + " container"}>
        <div className={styles.buatContainer}>
          <Heading text = "| Buat Pengumuman" type="kecil"></Heading>
          
            <form onSubmit={buatHandler} className = {" needs-validation"} >
                <CSRFToken type="PRODUCTION" />

                <div className={styles.line + " mb-3"}>
                    <label htmlFor=" exampleFormControlInput1" className = {" form-label"}>Judul</label>
                    <textarea className = {"   form-control"} id="exampleFormControlTextarea1"  placeholder="Required judul" rows="2" ref={judul} required></textarea>
                </div>

                <div className={styles.line + " mb-3"}>
                    <label htmlFor="exampleFormControlTextarea1" className = {" form-label"}>Konten</label>
                    <textarea className = {" form-control"} id="exampleFormControlTextarea1" rows="10"  placeholder="Required konten" ref={konten} required></textarea>
                </div>

                <div className={styles.buton}>
                    <a><button className = {" btn btn-danger"} type="submit">Simpan</button></a>
                    <p type="button" className={"btn btn-link"} data-bs-toggle="modal" data-bs-target="#staticBackdrop">Batal</p>
                </div>
            
            </form>    
        </div>

       <PopUp text="Apakah Anda yakin untuk batal?" url="pengumumanEdit"></PopUp>
        
      </section>
    </Fragment>
  );
}

export default Buat;