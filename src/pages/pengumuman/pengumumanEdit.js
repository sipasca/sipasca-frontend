import SuccessAlert from "../../components/SuccessAlert";
import styles from "../../styles/PengumumanPage.module.css";
import { useState , useEffect, Fragment } from "react";
import Heading from "../../components/Heading";
import PengumumanBox from "../../components/pengumumanBox";
import DeletePopUp from "../../components/DeletePopUp";
import { useRouter } from 'next/router'
import ButtonWithIcon from "../../components/ButtonWithIcon";
import Head from "next/head";
import axios from "axios";

export async function getServerSideProps(){
    const res = await fetch("https://simkuspa-backend.herokuapp.com/pengumumanAll");
  
    const data = await res.json();
  
    return { props: { data } }
  
}

function PengumumanEdit(props){
    const [role, setRole] = useState('staf');
    const [tanda, setTanda] = useState(false);
    const [berkasId, setBerkasId] = useState(0)
    const [showPopUp, setShowPopUp] = useState(false)
    const [tandaEdit, setTandaEdit] = useState(false)

    const arrayPengumuman = props.data.pengumuman_list
    const router = useRouter();

    useEffect(() => {  
        async function setRoleData() {
            const res = await axios.get('/api/creds', { withCredentials: true })
            const newRole = res.data.role
            setRole(newRole);
          }
        setRoleData();
    })
    
    useEffect( () => {
        if (typeof window !== 'undefined') {
            const lokal_buat = window.localStorage.getItem('tanda')
            window.localStorage.removeItem("tanda");
            if (lokal_buat === 'SUCCESS') {
                setTanda(true);
            }

            if (window.localStorage.getItem('tandaEdit')) {
                setTandaEdit(true)
                window.localStorage.removeItem('tandaEdit')
            }
        }
        
    },[])

    function handleId(idDelete) {
        setBerkasId(idDelete)
        setShowPopUp(true)
      }
    
    function handleDelete(choose) {
    if (choose) {
        hapusHandler(berkasId)
        setBerkasId(0)
        setShowPopUp(false)
    } else {
        setBerkasId(0)
        setShowPopUp(false)
        }
    }

    async function hapusHandler(id) {

        const res = await fetch(`https://simkuspa-backend.herokuapp.com/hapusPengumuman/${id}`);
        const data = await res.json();  
    
        if (data.status === 'SUCCESS') {
            if (typeof window !== 'undefined') {
              window.localStorage.setItem('tanda_hapus', data.status)
            }
        }
    
        router.push('/pengumuman/pengumumanEdit');
    }

    return(
    <Fragment>
        <Head>
            <title>Edit Pengumuman</title>
        </Head>
        
    <div className={styles.container}>    
        {tanda && <SuccessAlert text="Pengumuman berhasil dibuat" />}
        {tandaEdit && <SuccessAlert text="Pengumuman berhasil diedit" />}
        
        
        <div className={styles.row}>
            <Heading text = "| Pengumuman" type="pengumuman"></Heading>
            <div className={styles.buttons}>
            {role === 'staf' && 
            <div className={styles.cols}>
                    <ButtonWithIcon icon="fa-solid fa-plus" to="/pengumuman/buat/" text="Tambah" />
                </div>
            }
            {role === 'staf' && 
                <div className={styles.cols}>
                    <ButtonWithIcon icon="fa-solid fa-xmark" to="/pengumuman" text="Batal" />
                </div>
            }
            </div>

            
        </div>
    
        {arrayPengumuman.map(item => 
           
        <div className="row">
        <div className="col-sm-11">
                <PengumumanBox 
                judul = {item[1]}
                tanggal = {item[3]}
                konten = {item[2]}
                selengkapnya = {"/detailPengumuman/" + item[0] +"/" }
              
                />
        </div>
        {role === 'staf' && 
        <div className="col-sm-1">

            <ButtonWithIcon to={"/pengumuman/edit/" + item[0]} icon="fa-solid fa-pen-to-square" text="" />
            <br></br>
            <br></br>
            <ButtonWithIcon onClick={()=> handleId(item[0])} id="tombolX" icon="fa-solid fa-trash-can" text="" />
        </div>
        }

        </div>
        ) }

        {showPopUp && <DeletePopUp onDialog={handleDelete} message="Apakah Anda yakin untuk menghapus pengumuman ini?" />}
    </div>
    </Fragment>
    );
}

export default PengumumanEdit;


