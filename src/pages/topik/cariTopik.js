import styles from "../../styles/Daftar.module.css";
import Head from "next/head";
import Table from "../../components/Table";
import Heading from "../../components/Heading";
import { Fragment, useState } from "react";
import ReactPaginate from "react-paginate";

export async function getServerSideProps() {
  const cariTopikRes = await fetch("https://simkuspa-backend.herokuapp.com/daftarTopik");
  const dataCariTopik = await cariTopikRes.json();
  return { props: { dataCariTopik } };
}

function CariTopik(props) {
  const judul = ["Nama Topik", "Dosen Pengaju", "Deskripsi Topik",  "Batas Terima", "Jumlah Diterima"];
  const data = props.dataCariTopik.daftar_topik_list;
  const isAction = true;
  const dataCari = [];
  const buttonLabel = (
    <button
      style={{ border: "white" }}
      type="button"
      className="btn btn-outline-danger"
    >
      Detail
    </button>
  );

  for (let item of data) {
    let textDeskripsi = ""
    if (item[3].length > 120) {
      textDeskripsi = item[3].substring(0, 120) + "...."
    } else {
      textDeskripsi = item[3]
    }
    let batasTerima = ""
    if(item[4] == -1){
      batasTerima = "Tidak Terbatas"
    }else{
      batasTerima = item[4]
    }
    let row = {
      "Nama Topik": item[1],
      "Dosen Pengaju": item[2],
      "Deskripsi Topik": textDeskripsi,
      "Batas Terima" : batasTerima,
      "Jumlah Diterima" : item[5],
      urlId: "/topik/detail/" + item[0],
    };
    dataCari.push(row);
  }

  const [pageNumber, setPageNumber] = useState(0);
  const rowsPerPage = 10;
  const pageVisited = pageNumber * rowsPerPage;
  const displayRows = dataCari.slice(pageVisited, pageVisited + rowsPerPage);
  const pageCount = Math.ceil(dataCari.length / rowsPerPage);

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  const rowCari = (
    <Table
      columns={judul}
      data={displayRows}
      isAction={isAction}
      buttonLabel={buttonLabel}
    />
  );

  return (
    <Fragment>
      <Head>
        <title>Cari Topik</title>
      </Head>
        <div className="container mt-5 pt-10 mb-5" style={{minHeight:"100vh"}}>
          <Heading text="| Daftar Topik" type="kecil"></Heading>
          {dataCari.length == 0 && (
            <h3 style={{ color: "#163269", textAlign: "center", marginTop: "200px"}}>
              Belum Ada Topik Yang Tersedia
            </h3>
          )}
          {dataCari.length != 0 && (
            <div>
              {rowCari}
              {dataCari.length > 10 && (
                <ReactPaginate
                  previousLabel={"Prev"}
                  nextLabel={"Next"}
                  pageCount={pageCount}
                  onPageChange={changePage}
                  containerClassName={styles.paginationButtons}
                  previousLinkClassName={"previousButton"}
                  nextLinkClassName={"nextButton"}
                  disabledClassName={"paginationDisabled"}
                  activeClassName={styles.paginationActive}
                />
              )}
            </div>
          )}
        </div>
      )
    </Fragment>
  );
}
export default CariTopik;
