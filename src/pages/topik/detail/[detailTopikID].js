import Head from "next/head";
import {Fragment, useState , useEffect} from "react";
import styles from "../../../styles/DetailTopik.module.css";
import ButtonWithIcon from "../../../components/ButtonWithIcon";
import BerkasPentingBox from "../../../components/BerkasPentingBox";
import { useRouter } from "next/router";
import axios from "axios";

export async function getServerSideProps(context){
    const idTopik = context.query.detailTopikID;
    const detailTopikRes = await fetch("https://simkuspa-backend.herokuapp.com/detailTopikDosen/" + idTopik);
    const detailTopik = await detailTopikRes.json();
    return { props: { detailTopik } }  
}

function DetailTopik(props) {
    const [role, setRole] = useState('pengguna');
    useEffect(() => {  
        async function setRoleData() {
            const res = await axios.get('/api/creds', { withCredentials: true })
            const newRole = res.data.role
            setRole(newRole);
        }
        setRoleData();
    })
    
    const arrayDetailTopikDosen = props.detailTopik.detail_topik_dosen
    const arrayPrasyarat = []
    const arrayDokumen = arrayDetailTopikDosen[1]
    const topikId = arrayDetailTopikDosen[0][0]

    for (let i=4; i < 7; i++) {
        if (arrayDetailTopikDosen[0][i] != null && arrayDetailTopikDosen[0][i] != '') {
            arrayPrasyarat.push(arrayDetailTopikDosen[0][i])
        }
    }

    const router = useRouter();
    function requestHandler() {
        router.push({
            pathname: '/cariDosen/requestDospem',
            query: { 
                id: arrayDetailTopikDosen[0][1], 
                judulTopik: arrayDetailTopikDosen[0][2], 
                deskripsiTopik: arrayDetailTopikDosen[0][3],
                idTopik: arrayDetailTopikDosen[0][0]
            }
        })
    }
    return (
        
        <Fragment> 
            <Head>
                <title>Detail Topik</title>
                <meta name="description" content="Sistem Informasi Mata Kuliah Pasca" />
            </Head>
            <div className={styles.isian}>
                { role === 'dosen' && 
                    <div style={{ textAlign:"right" }}>
                        <ButtonWithIcon icon="fas fa-edit" text="Edit" to={"../edit/" + topikId + "/"}></ButtonWithIcon>
                    </div>
                } 
                <h2 className={styles.title}>| Detail Topik </h2><br></br>
                <table>
                    <colgroup> 
                        <col span="1" className={styles.judul}/>
                        <col span="1" className={styles.isi}/>
                    </colgroup>
                    <tr style={{ margin:"10px" }}> 
                        <td className={styles.judulKonten}>Nama Topik</td>
                        <td className={styles.konten} style={{ borderBottom:"1px solid #163269" }}>  {arrayDetailTopikDosen[0][2]} </td>
                    </tr >
                    <tr style={{ margin:"10px" }}> 
                        <td className={styles.judulKonten}>Dosen Pengaju</td>
                        <td className={styles.konten} style={{ borderBottom:"1px solid #163269" }}>  {arrayDetailTopikDosen[0][10]} </td>
                    </tr >
                    <tr>
                        <td className={styles.judulKonten}> Deskripsi Topik </td>
                        <td className={styles.konten} style={{ borderBottom:"1px solid #163269", whiteSpace:'pre-wrap' }}> {arrayDetailTopikDosen[0][3]} </td>
                    </tr>
                    <tr>
                        <td className={styles.judulKonten}> Dokumen Pendukung </td>
                        { (arrayDokumen.length == 0)
                            ? <td> Tidak ada </td>
                            : <td> <br></br>
                                <div > {arrayDokumen.map (dokumenItem =>
                                    <BerkasPentingBox style={{ borderColor:"white !important" }}  nama={dokumenItem[1]} url={dokumenItem[0]} id={dokumenItem[2]}></BerkasPentingBox>
                                )}</div>
                            </td> }
                    </tr>
                    <tr>
                        <td className={styles.judulKonten}> Prasyarat </td>
                        { (arrayPrasyarat.length == 0)
                            ? <td> Tidak ada </td>
                            : <td>
                                <div>
                                    {arrayPrasyarat.map (prasyaratItem =>
                                        <div><div style={{ color: "black", borderColor:"#163269", marginBottom: "1%" }} className={"card"}>
                                            <div className={"card-body"}> {prasyaratItem} </div>
                                        </div></div>      
                                )} </div>
                                </td> }
                    </tr>
                </table>
                <br></br>
                <div className="container">
                    <div className="row">
                        <div className="col"> 
                            { role === 'mahasiswa' && 
                                <ButtonWithIcon text="Request" id="request" onClick={requestHandler}></ButtonWithIcon>
                            }
                        </div>
                        <div className="col" style={{ textAlign:"right" }}>
                            { role === 'mahasiswa' 
                            ? <ButtonWithIcon text="Kembali" to={"../cariTopik/"}></ButtonWithIcon>
                            : <ButtonWithIcon text="Kembali" to={"../daftarTopik/"}></ButtonWithIcon>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default DetailTopik;