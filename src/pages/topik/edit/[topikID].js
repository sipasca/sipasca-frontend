import PopUp from "../../../components/PopUp";
import { Fragment, useRef, useState } from "react";
import styles from "../../../styles/PusatInformasi.module.css";
import styles2 from "../../../styles/BuatPengumuman.module.css";
import { useRouter } from "next/router";
import DeletePopUp from "../../../components/DeletePopUp";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import CSRFToken from "../../../lib/CSRFToken";
import Cookies from 'js-cookie';
import editTopikHandler from "../../../lib/EditTopikHandler";
import Head from "next/head";
import deleteDokumenFromStorage from "../../../lib/DeleteDokumenFromStorage";

export async function getServerSideProps(context) {
    const id = context.query.topikID
    const res = await fetch("https://simkuspa-backend.herokuapp.com/detailTopikDosen/" + id + "/");

    const data = await res.json();
    return { props: { 'idTopik': id, data } }
}

export function listDeleteFromStorageHandler(listDeletedFile,dictDokumen){
    const listUrlDeletedFile = []
    for (let deletedFile of listDeletedFile) {
        const key = 'dokumen_' + deletedFile
        const urlFolder = dictDokumen[key][0][0]
        listUrlDeletedFile.push(urlFolder)
      }
      return listUrlDeletedFile
    }

export function deleteAllDokumenFromStorageHandler(allDokumen){
    for (let deletedFile of allDokumen) {
        const urlFolder=deletedFile[0]
        deleteDokumenFromStorage(urlFolder)
      }
}

function Edit(props) {
    const idTopik = props.idTopik
    const judul = useRef();
    const deskripsi = useRef();
    const prasyarat1 = useRef();
    const prasyarat2 = useRef();
    const prasyarat3 = useRef();
    const router = useRouter();
    const [showPopUp, setShowPopUp] = useState(false)
    const [showPopUpDeleteTopik, setShowPopUpDeleteTopik] = useState(false)

    function handleDeleteTopikPopUp(event) {
        event.preventDefault();
        setShowPopUpDeleteTopik(true)
    }

    function giveAlertSuccess(query) {
        window.localStorage.setItem(query, 'Success')
    }

    function editTopikHandler2(event) {
        event.preventDefault();
        const listDeletedFile = [...listDeletedDokumenDb]
        const listUrlDeletedFile = listDeleteFromStorageHandler(listDeletedFile,dictDokumen)
  
        const body = {
            'id_topik': idTopik,
            'judul': judul.current.value,
            'deskripsi': deskripsi.current.value,
            'prasyarat_1': prasyarat1.current.value,
            'prasyarat_2': prasyarat2.current.value,
            'prasyarat_3': prasyarat3.current.value,
            'deleted_file': listDeletedFile,
            'batas_diterima': pilihan,
            'csrfmiddlewaretoken': Cookies.get('csrftoken')
        }
        editTopikHandler(router, listAllDokumen, body, listUrlDeletedFile)
    }

    async function handleDeleteTopik(choose) {
        if (choose) {
            deleteAllDokumenFromStorageHandler(allDokumen)
            await fetch('https://simkuspa-backend.herokuapp.com/deleteTopikDosen/' + idTopik + '/')
            giveAlertSuccess('delete')
            router.push('/topik/daftarTopik')
        }
        setShowPopUpDeleteTopik(false)
    }

    function handleNoDokumen(event, no_dokumen) {
        event.preventDefault();
        setDeletedDokumen(no_dokumen)
        setShowPopUp(true)
    }

    function handleDelete(choose) {
        if (!choose) {
            setDeletedDokumen(0)
            setShowPopUp(false)
            return;
        }

        const keyDokumen = 'dokumen_' + deletedDokumen
        if (listAllDokumen[keyDokumen][0][1]) {
            setListDeletedDokumenDb(listDeletedDokumenDb.add(listAllDokumen[keyDokumen][0][2]))
        }
        listAllDokumen[keyDokumen][2] = null
        listAllDokumen[keyDokumen][0] = []
        setListAllDokumen({ ...listAllDokumen })
        setDeletedDokumen(0)
        setShowPopUp(false)
    }

    function fileHandler(event) {
        const detailDokumen = [null, null, null]
        detailDokumen[0] = 'production/dokumenTopik/' + idTopik + '/dokumen_' + event.target.id
        detailDokumen[1] = event.target.files[0].name
        detailDokumen[2] = parseInt(event.target.id)
        const keyDokumen = 'dokumen_' + event.target.id
        listAllDokumen[keyDokumen][0] = detailDokumen
        listAllDokumen[keyDokumen][2] = event.target.files[0]
        setListAllDokumen({ ...listAllDokumen })
    }
    const detailTopik = props.data.detail_topik_dosen[0]
    const allDokumen = props.data.detail_topik_dosen[1]
    const [pilihan, setPilihan] = useState(detailTopik[8]);
    const dictDokumen = { 'dokumen_1': null, 'dokumen_2': null, 'dokumen_3': null }
    dictDokumen.dokumen_1 = allDokumen.filter(function (dokumen) { return dokumen[2] == 1; })
    dictDokumen.dokumen_2 = allDokumen.filter(function (dokumen) { return dokumen[2] == 2; })
    dictDokumen.dokumen_3 = allDokumen.filter(function (dokumen) { return dokumen[2] == 3; })

    for (let key in dictDokumen) {
        dictDokumen[key].push(false)
        dictDokumen[key].push(null)
        if (dictDokumen[key][0].length == 3) {
            dictDokumen[key][1] = true
        }
    }

    const [listAllDokumen, setListAllDokumen] = useState(dictDokumen)
    const [deletedDokumen, setDeletedDokumen] = useState(0)
    const [listDeletedDokumenDb, setListDeletedDokumenDb] = useState(new Set())
    const listDokumen = Object.keys(listAllDokumen).map((key, index) =>
        <div>
            <label className={"form-label"} style={{ fontSize: '15px' }}>Dokumen {index + 1}</label>
            <br></br>
            {listAllDokumen[key][0].length == 3 ?
                <div className="d-flex justify-content-between form-control" style={{ border: '1px solid #163269', outline: 'none', paddingLeft: '10 px', paddingRight: '0px', letterSpacing: '1 px' }} >
                    {listAllDokumen[key][0][1]}
                    <button id="tombolX" className={styles.buttonDelete} onClick={(event) => handleNoDokumen(event, listAllDokumen[key][0][2])}><FontAwesomeIcon icon="fa-solid fa-xmark" /></button>
                </div> :
                <input id={index + 1} type="file" onChange={fileHandler}></input>
            }
            <br></br>
            <br></br>
        </div>
    );
    return (
        <Fragment>
            <Head>
                <title>Edit Topik</title>
            </Head>
            <div className={styles2.buat + " container"}>
                <div className={styles2.buatContainer}>
                    <h4>| Edit Topik</h4>
                    <br></br>
                    <form onSubmit={editTopikHandler2} className={" needs-validation"} >
                        <CSRFToken type="PRODUCTION" />
                        <div className={styles.line + " mb-3"}>
                            <label htmlFor="judul" className={"form-label"}>Topik <div style={{ display: 'inline', color: 'red' }}>*</div></label>
                            <textarea className={"form-control"} id="judul" placeholder="Topik" rows="2" ref={judul} maxLength='100' required>{detailTopik[2]}</textarea>
                            <div id="contohtopik" className="form-text">contoh topik: Intelligent Transport System</div>
                        </div>

                        <br></br>
                        <div className={styles.line + " mb-3"}>
                            <label htmlFor="deskripsi" className={"form-label"}>Deskripsi Topik <div style={{ display: 'inline', color: 'red' }}>*</div></label>
                            <textarea className={"form-control"} id="deskripsi" rows="10" placeholder="Deskripsi topik" ref={deskripsi} required>{detailTopik[3]}</textarea>
                            <div id="contohdeskripsi" class="form-text">contoh deskripsi: penerapan computer vision untuk tracking kendaraan pada malam hari</div>
                        </div>

                        <br></br>
                        <div className={styles.line + " mb-3"}>
                            <label className={"form-label"}>Tambahkan Prasyarat</label>
                            <br></br>
                            <label htmlFor="prasyarat1" className={"form-label"} style={{ fontSize: '15px' }}>Prasyarat 1</label>
                            <textarea className={"form-control"} id="prasyarat1" placeholder="Prasyarat 1" maxLength='100' rows="1" ref={prasyarat1}>{detailTopik[4]}</textarea>
                            <br></br>
                            <label htmlFor="prasyarat2" className={"form-label"} style={{ fontSize: '15px' }}>Prasyarat 2</label>
                            <textarea className={"form-control"} id="prasyarat2" placeholder="Prasyarat 2" maxLength='100' rows="1" ref={prasyarat2}>{detailTopik[5]}</textarea>
                            <br></br>
                            <label htmlFor="prasyarat3" className={"form-label"} style={{ fontSize: '15px' }}>Prasyarat 3</label>
                            <textarea className={"form-control"} id="prasyarat3" placeholder="Prasyarat 3" maxLength='100' rows="1" ref={prasyarat3}>{detailTopik[6]}</textarea>
                            <div id="contohprasyarat" className="form-text">contoh prasyarat: menguasai bahasa python, paham computer vision, dll</div>
                        </div>

                        <br></br>
                        <div className={" mb-3"}>
                            <label>Tambahkan Dokumen Pendukung</label>
                            {listDokumen}
                            <div id="contohdokumen" className="form-text">contoh dokumen: dokumen pdf yang berisi penjelasan lebih lengkap mengenai topik</div>
                        </div>

                        <br></br>
                        <div className={styles.line + " mb-3"}>
                            <label className={"form-label"}>Batas Jumlah Terima Mahasiswa<div style={{ display: 'inline', color: 'red' }}>*</div></label>
                            {pilihan == "-1" ? (
                                <>
                                <div className="form-check">
                                    <input type="radio" id="button1" className="form-check-input" name="pilihan" value="-1" onChange={e => setPilihan(e.target.value)} checked/>Tidak terbatas
                                </div>
                                <div className="form-check">
                                    <input type="radio" id="button2" className="form-check-input" name="pilihan" value="dibatasi" onChange={e => setPilihan(e.target.value)}/>Dibatasi
                                </div>
                                </>
                            ) : (
                                <>
                                <div className="form-check">
                                    <input type="radio" id="button3" className="form-check-input" name="pilihan" value="-1" onChange={e => setPilihan(e.target.value)}/>Tidak terbatas
                                </div>
                                <div className="form-check">
                                    <input type="radio" id="button4" className="form-check-input" name="pilihan" value="dibatasi" onChange={e => setPilihan(e.target.value)} checked/>Dibatasi
                                    <input type="number" id="button5" className={"   form-control"} placeholder="2 orang" min="1" max="60" onChange={e => setPilihan(e.target.value)} value={pilihan}></input>
                                </div>
                                </>
                            )}
                        </div>
                        <div id="contohBatasan" className="form-text">pilih option <b>Dibatasi</b> jika topik hanya bisa diambil oleh beberapa mahasiswa saja</div>

                        <br/>
                        <div className={"row"}>
                            <div className={"col-sm-9"}>
                                <div className={styles2.buton} style={{ margin: 'auto' }}>
                                    <a><button id="submit-button" className={"btn"} style={{ backgroundColor: 'white', color: 'red', borderRadius: '4px', border: '1px solid' }} type="submit">Simpan</button></a>
                                    <p type="button" className={"btn btn-link"} data-bs-toggle="modal" data-bs-target="#staticBackdrop">Batal</p>
                                </div>
                            </div>
                            <div className={"col-sm-3"}>
                                <div className={styles2.buton}>
                                    <a><button id="delete-topik-button" onClick={(event) => handleDeleteTopikPopUp(event)} className={"btn"} >Hapus Topik</button></a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <PopUp text="Apakah Anda yakin untuk batal?" url="/topik/daftarTopik"></PopUp>
                {showPopUp && <DeletePopUp onDialog={handleDelete} message="Apakah Anda yakin untuk menghapus dokumen ini?" />}
                {showPopUpDeleteTopik && <DeletePopUp onDialog={handleDeleteTopik} message="Apakah Anda yakin untuk menghapus topik ini?" />}
            </div>
        </Fragment>
    );
}
export default Edit;