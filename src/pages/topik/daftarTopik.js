import Head from "next/head";
import Table from "../../components/Table";
import Heading from "../../components/Heading";
import { Fragment, useState, useEffect } from "react";
import ButtonWithIcon from "../../components/ButtonWithIcon";
import ReactPaginate from "react-paginate";
import styles from "../../styles/Daftar.module.css";
import SuccessAlert from "../../components/SuccessAlert";
import { verify } from "jsonwebtoken";

export async function getServerSideProps(context) {
  const userID = context.req.cookies["userID"];
  const secret = process.env.SECRET;
  const verifiedData = verify(userID, secret);
  const userIDVerified = verifiedData.userID;

  const daftarTopikRes = await fetch(
    "https://simkuspa-backend.herokuapp.com/daftarTopikByDosen/" + userIDVerified
  );
  const dataDaftarTopik = await daftarTopikRes.json();
  return { props: { dataDaftarTopik } };
}
function DaftarTopik(props) {
  const [showAlert, setShowAlert] = useState(false);
  const [isiAlert, setIsiAlert] = useState("");
  const judul = ["Nama Topik", "Dosen Pengaju", "Deskripsi Topik", "Batas Terima", "Jumlah Diterima"];
  let dataDaftar = props.dataDaftarTopik.list_topik_by_dosen;
  const isAction = true;
  const isAnotherAction = true;
  const buttonLabel = (
    <button
      style={{ border: "white" }}
      type="button"
      className="btn btn-outline-danger"
    >
      Detail
    </button>
  );
  const anotherButtonLabel = (
    <button
      style={{ border: "white" }}
      type="button"
      className="btn btn-outline-danger"
    >
      Edit
    </button>
  );
  useEffect(() => {
    if (typeof window !== "undefined") {
      if (window.localStorage.getItem("buat") === "Success") {
        window.localStorage.removeItem("buat");
        setShowAlert(true);
        setIsiAlert("Topik berhasil dibuat");
      } 
      if (window.localStorage.getItem("edit") === "Success") {
        window.localStorage.removeItem("edit");
        setShowAlert(true);
        setIsiAlert("Topik berhasil diedit");
      } 
      if (window.localStorage.getItem("delete") === "Success") {
        window.localStorage.removeItem("delete");
        setShowAlert(true);
        setIsiAlert("Topik berhasil dihapus");
      }
    }
  });

  dataDaftar = dataDaftar.map((item) => {
    let textDeskripsi = ""
    if (item[3].length > 120) {
      textDeskripsi = item[3].substring(0, 120) + "...."
    } else {
      textDeskripsi = item[3]
    }
    let batasTerima = ""
    if(item[4] == -1){
      batasTerima = "Tidak Terbatas"
    }else{
      batasTerima = item[4]
    }
    return {
      "Nama Topik": item[1],
      "Dosen Pengaju": item[2],
      "Deskripsi Topik": textDeskripsi,
      "Batas Terima" : batasTerima,
      "Jumlah Diterima" : item[5],
      urlId: "/topik/detail/" + item[0],
      anotherUrlId: "/topik/edit/" + item[0],
    };
  });

  const dataLength = dataDaftar.length;
  const [pageNumber, setPageNumber] = useState(0);
  const rowsPerPage = 10;
  const pageVisited = pageNumber * rowsPerPage;
  const displayRows = dataDaftar.slice(pageVisited, pageVisited + rowsPerPage);
  const pageCount = Math.ceil(dataLength / rowsPerPage);
  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  const rowDaftar = (
    <Table
      columns={judul}
      data={displayRows}
      isAction={isAction}
      buttonLabel={buttonLabel}
      isAnotherAction={isAnotherAction}
      anotherButtonLabel={anotherButtonLabel}
    ></Table>
  );

  return (
    <Fragment>
      <Head>
        <title>Daftar Topik</title>
      </Head>
      {showAlert && <SuccessAlert text={isiAlert} />}
        <div
          className="container mt-5 pt-10 mb-5"
          style={{ minHeight: "100vh" }}
        >
          <Heading text="| Daftar Topik" type="kecil"></Heading>
          <div style={{ textAlign: "right", marginBottom: "30px" }}>
            <ButtonWithIcon
              icon="fa-solid fa-plus"
              text="Tambah Topik Baru"
              to="/topik/buat"
            ></ButtonWithIcon>
          </div>
          {dataLength == 0 && (
            <h3
              style={{
                color: "#163269",
                textAlign: "center",
                marginTop: "200px",
              }}
            >
              Belum Ada Topik Yang Ditambahkan
            </h3>
          )}
          {dataLength != 0 && (
            <div>
              {rowDaftar}
              {dataLength > 10 && (
                <ReactPaginate
                  previousLabel={"Prev"}
                  nextLabel={"Next"}
                  pageCount={pageCount}
                  onPageChange={changePage}
                  containerClassName={styles.paginationButtons}
                  previousLinkClassName={"previousButton"}
                  nextLinkClassName={"nextButton"}
                  disabledClassName={"paginationDisabled"}
                  activeClassName={styles.paginationActive}
                />
              )}
            </div>
          )}
        </div>
      )
    </Fragment>
  );
}
export default DaftarTopik;
