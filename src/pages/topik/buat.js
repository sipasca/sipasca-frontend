import PopUp from "../../components/PopUp";
import { Fragment, useRef, useState } from "react";
import styles from "../../styles/BuatPengumuman.module.css";
import CSRFToken from "../../lib/CSRFToken";
import Cookies from 'js-cookie';
import postRequest from "../api/post-request";
import { useRouter } from 'next/router';
import storage from "../../components/storage";
import { ref, uploadBytes } from "firebase/storage";
import axios from "axios";
import Head from "next/head";

function Buat() {
  const judul = useRef();
  const deskripsi = useRef();
  const prasyarat1 = useRef();
  const prasyarat2 = useRef();
  const prasyarat3 = useRef();
  const dokumen1 = useRef();
  const dokumen2 = useRef();
  const dokumen3 = useRef();
  const router = useRouter();
  const [pilihan, setPilihan] = useState('-1');

  async function buatTopikHandler(event) {
    event.preventDefault();
    const creds = await axios.get('/api/creds', { withCredentials: true })
    let idDosen = creds.data.userID
    if (Array.isArray(idDosen)){
      idDosen = idDosen[0]
    }

    const body = {
      'id_dosen': idDosen,
      'judul': judul.current.value,
      'deskripsi': deskripsi.current.value,
      'prasyarat_1': prasyarat1.current.value,
      'prasyarat_2': prasyarat2.current.value,
      'prasyarat_3': prasyarat3.current.value,
      'batas_diterima': pilihan,
      'csrfmiddlewaretoken': Cookies.get('csrftoken')
    }
    const listDokumenTopik = [dokumen1,dokumen2,dokumen3]

    for (let i = 0; i < listDokumenTopik.length; i++) {
      const dokumen = listDokumenTopik[i].current.files[0]
      const noDokumen = i+1
      const keyDokumen = 'dokumen_' + noDokumen
      if (dokumen!=null){
        const nama = dokumen.name
        body[keyDokumen] = {'nama_file':nama}
      }
      else{
        body[keyDokumen] = ""
      }
    }

    const res = await postRequest('/buatTopikDosen/', body);
    const data = res.data;  

    if (data.status === 'Success') {
      for (let i = 0; i < listDokumenTopik.length; i++) {
        const dokumen = listDokumenTopik[i].current.files[0]
        const noDokumen = i+1
        const keyDokumen = 'dokumen_' + noDokumen
        if (dokumen!=null){
          const dokumen_directory = 'production/dokumenTopik/' + data.id_topik +'/' + keyDokumen + '_'+dokumen.name
          const filePath = ref(storage, dokumen_directory)
          await uploadBytes(filePath, dokumen)
        }
      }
      if (typeof window !== 'undefined') {
        window.localStorage.setItem('buat', res.data.status)
      }
      router.push('daftarTopik');
    }
  }

  return (
    <Fragment>
      <Head>
        <title>Buat Topik</title>
      </Head>
      <section className={styles.buat + " container"}>
        <div className={styles.buatContainer}>
            <h4>| Buat Topik</h4>
            <form onSubmit={buatTopikHandler} className = {" needs-validation"}>
              <CSRFToken type="PRODUCTION"/>
                <div className={styles.line + " mb-3"}>
                    <label htmlFor=" exampleFormControlInput1" className = {"form-label"}>Topik <div style={{display:'inline' ,color:'red'}}>*</div></label>
                    <textarea className = {"form-control"} id="exampleFormControlTextarea1"  placeholder="Topik" rows="2" ref={judul} maxLength='100' required></textarea>
                    <div id="contohTopik" className="form-text">contoh topik: Intelligent Transport System</div>
                </div>

                <div className={styles.line + " mb-3"}>
                    <label htmlFor="exampleFormControlTextarea1" className = {"form-label"}>Deskripsi Topik <div style={{display:'inline' ,color:'red'}}>*</div></label>
                    <textarea className = {"form-control"} id="exampleFormControlTextarea1" rows="10"  placeholder="Deskripsi topik (berupa penjelasan singkat mengenai topik, dapat juga menjelaskan output yang diharapkan, mencantumkan jumlah orang yang dibutuhkan, dsb)" ref={deskripsi} required></textarea>
                    <div id="contohDeskripsi" className="form-text">contoh deskripsi: penerapan computer vision untuk tracking kendaraan pada malam hari</div>
                </div>

                <div className={styles.line + " mb-3"}>
                    <label className = {"form-label"}>Tambahkan Prasyarat</label>
                    <br></br>
                    <label htmlFor="exampleFormControlInput1" className = {"form-label"} style={{fontSize:'15px'}}>Prasyarat 1</label>
                    <textarea className = {"form-control"} id="exampleFormControlTextarea1"  placeholder="Prasyarat 1" maxLength='100' rows="1" ref={prasyarat1}></textarea>
                    <br></br>
                    <label htmlFor="exampleFormControlInput1" className = {"form-label"} style={{fontSize:'15px'}}>Prasyarat 2</label>
                    <textarea className = {"form-control"} id="exampleFormControlTextarea1"  placeholder="Prasyarat 2" maxLength='100' rows="1" ref={prasyarat2}></textarea>
                    <br></br>
                    <label htmlFor="exampleFormControlInpu  t1" className = {"form-label"} style={{fontSize:'15px'}}>Prasyarat 3</label>
                    <textarea className = {"form-control"} id="exampleFormControlTextarea1"  placeholder="Prasyarat 3" maxLength='100' rows="1" ref={prasyarat3}></textarea>
                    <div id="contohPrasyarat" className="form-text">contoh prasyarat: menguasai bahasa python, paham computer vision, dll</div>
                </div>
                
                <div className={styles.line + " mb-3"}>
                    <label className = {"form-label"}>Tambahkan Dokumen Pendukung</label>
                    <br></br>
                    <label htmlFor="dokumen1" className = {"form-label"} style={{fontSize:'15px'}}>Dokumen 1</label>
                    <br></br>
                    <input id="dokumen1" type="file" ref={dokumen1}></input> 
                    <br></br>
                    <br></br>
                    <label htmlFor="dokumen2" className = {"form-label"} style={{fontSize:'15px'}}>Dokumen 2</label>
                    <br></br>
                    <input id="dokumen2" type="file" ref={dokumen2}></input> 
                    <br></br>
                    <br></br>
                    <label htmlFor="dokumen3" className = {"form-label"} style={{fontSize:'15px'}}>Dokumen 3</label>
                    <br></br>
                    <input id="dokumen3" type="file" ref={dokumen3}></input> 
                    <br></br>
                    <br></br>
                    <div id="contohDokumen" className="form-text">contoh dokumen: dokumen pdf yang berisi penjelasan lebih lengkap mengenai topik</div>
                </div>

                <div className={styles.line + " mb-3"}>
                  <label className={"form-label"}>Batas Jumlah Terima Mahasiswa<div style={{ display: 'inline', color: 'red' }}>*</div></label>
                  <div className="form-check">
                    <input type="radio" id="button1" className="form-check-input" name="pilihan" value="-1" onChange={e => setPilihan(e.target.value)} /> Tidak terbatas
                  </div>
                  <div className="form-check">
                    <input type="radio" id="button2" className="form-check-input" name="pilihan" value="dibatasi" onChange={e => setPilihan(e.target.value)} /> Dibatasi
                    {pilihan != "-1" && (
                      <input required type="number" id="button3" className={"   form-control"} placeholder="2 orang" min="1" max="60" onChange={e => setPilihan(e.target.value)}></input>
                    )}
                  </div>
                </div>
                <div id="contohBatasan" className="form-text">pilih option <b>Dibatasi</b> jika topik hanya bisa diambil oleh beberapa mahasiswa saja</div>

                <div className={styles.buton}>
                  <a><button className = {"btn btn-primary"} type="submit">Simpan</button></a>
                  <p type="button" className={"btn btn-link"} data-bs-toggle="modal" data-bs-target="#staticBackdrop">Batal</p>
                </div>
            </form>    
        </div>

        <PopUp text="Apakah Anda yakin untuk batal?" url="/topik/daftarTopik"></PopUp>
      </section>
    </Fragment>
  );
}
export default Buat;