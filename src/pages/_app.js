import { useState, useEffect } from "react";
import "@fortawesome/fontawesome-svg-core/styles.css";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";

import { Fragment } from "react";
import Script from "next/script";
import NextNProgress from "nextjs-progressbar";
import Navbar from "../components/Navbar/Navbar";
import Footer from "../components/Footer";
import Cookies from "js-cookie";
import axios from "axios";

import "../styles/globals.css";
import { TermProvider } from "../context/termContext";

library.add(fas, far, fab);

function MyApp({ Component, pageProps }) {
  const [role, setRole] = useState("pengguna");
  const [isAuth, setIsAuth] = useState(false);

  useEffect(() => {
    if (Cookies.get('userID')) {
      async function setRoleData() {
        const res = await axios.get('/api/creds', { withCredentials: true })
        const newRole = res.data.role
        setRole(newRole);
      }
      setRoleData();
      setIsAuth(true);
    }
  });

  return (
    <TermProvider>
    <Fragment>
      {/* Bootstrap */}
      <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossOrigin="anonymous"
      ></link>
      <Script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossOrigin="anonymous"
      ></Script>

      <Navbar role={role} isAuth={isAuth} />
      <NextNProgress color="#ff0000" />
      <Component {...pageProps} />
      <Footer />
    </Fragment>
    </TermProvider>
  );
}

export default MyApp;
