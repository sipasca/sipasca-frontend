import { Fragment, useEffect, useState } from "react";
import styles from "../../styles/AdministrasiPage.module.css";
import MataKuliahBox from "../../components/MataKuliahBox";
import Head from 'next/head'
import SuccessAlert from "../../components/SuccessAlert";
import ErrorAlert from "../../components/ErrorAlert";
import Heading from "../../components/Heading";
import { useRouter } from "next/router";
import { verify } from "jsonwebtoken";
import axios from "axios";
import { useTerm } from "../../context/termContext";
import CSRFToken from "../../lib/CSRFToken";
import Cookies from 'js-cookie';
import postRequest from "../api/post-request";

export async function getServerSideProps(context){
  const userID = context.req.cookies["userID"]
  const role = context.req.cookies["role"]

  const secret = process.env.SECRET;
  const verifiedUserData = verify(userID, secret);
  const verifiedRoleData = verify(role, secret);
  const userIDVerified = verifiedUserData.userID;
  const roleVerified = verifiedRoleData.role;

  if(roleVerified === "staf" || roleVerified === "dosen"){
    const stafRes = await fetch("https://simkuspa-backend.herokuapp.com/daftar_mata_kuliah/")
    const allMK = await stafRes.json()
    let data = {};
    data['mk_list'] = allMK.mk_tersedia.map( mk =>
      [mk[2], mk[1], mk[0]]
    )
    return { props: { data } }  
  }
  else {
    const administrasiRes = await fetch("https://simkuspa-backend.herokuapp.com/mata_kuliah/" + userIDVerified);
    const data = await administrasiRes.json();
    return { props: { data } }  
  }

}

function sortMKByTerm(listMKPerTerm) {
  const termList = Object.keys(listMKPerTerm)
  termList.sort()
  termList.reverse()

  return termList.map((key) => [key, listMKPerTerm[key]])
}

function getListGantiTerm(activePeriode) {
  const year = parseInt(activePeriode.slice(0,4))
  const term = parseInt(activePeriode.slice(-1))

  if (term === 1) {
    return [
      year + "-" + 2,
      activePeriode,
      (year-1) + "-" + 2,
    ]
  }
  else {
    return [
      (year+1) + "-" + 1,
      activePeriode,
      year + "-" + 1,
    ]
  }
}

function goToMataKuliah(router, role, nama, kode) {
  switch (role) {
    case "mahasiswa":
      router.push({
        pathname: "mataKuliahSpesial/" + kode + "/m/ringkasan",
        query: { mk:nama }
      });
      break;

    case "staf":
      router.push("mataKuliahSpesial/" + kode + "/s/rekap")
      break;

    case "dosen":
      router.push("mataKuliahSpesial/" + kode + "/d/bimbingan");
      break;
  }
}

function Administrasi(props){
  const [role, setRole] = useState();
  const router = useRouter();

  const [showSuccessAlert, setshowSuccessAlert] = useState(false)
  const [textSuccessAlert, setTextSuccessAlert] = useState('')

  const [showErrorAlert, setshowErrorAlert] = useState(false)
  const [textErrorAlert, setTextErrorAlert] = useState('')

  const [daftarAlert, setDaftarAlert] = useState(false)

  const { activeTerm } = useTerm()

  let listGantiTerm = []
  if (activeTerm !== null) {
    listGantiTerm = getListGantiTerm(activeTerm)
  }
  
  useEffect(() => {  
    async function setRoleData() {
      const res = await axios.get('/api/creds', { withCredentials: true })
      const newRole = res.data.role
      setRole(newRole);
    }
    setRoleData();    
  })

  useEffect( () => {
    const SuccessCreateForm  = window.localStorage.getItem('SuccessCreateForm')
    window.localStorage.removeItem("SuccessCreateForm");
    const matKul = window.localStorage.getItem('matKul')
    window.localStorage.removeItem("matKul");

        if (SuccessCreateForm === 'SUCCESS') {
            setshowSuccessAlert(true);
            const textSuccess = 'Mata Kuliah ' + matKul + ' berhasil dibuat';
            setTextSuccessAlert(textSuccess)
            }
        else if (SuccessCreateForm === 'FAILED') {
          setshowErrorAlert(true);
          const textError = 'Mata Kuliah ' + matKul + ' tidak berhasil ditambahkan';
          setTextErrorAlert(textError)
        }
        
        if(window.localStorage.getItem('successDaftar')) {
          setDaftarAlert(true)
          window.localStorage.removeItem('successDaftar')
        }
    }
  ,[])

  const arrayMk = props.data.mk_list

  const objMKPerTerm = {}

  for (let mk of arrayMk) {
    let term = mk[1]
    if (objMKPerTerm[term] !== undefined) {
      objMKPerTerm[term].push(mk)
    }
    else {
      objMKPerTerm[term] = [mk]
    }
  }

  const arrayMKPerTerm = sortMKByTerm(objMKPerTerm)

  const [selectedTerm, setSelectedTerm] = useState(null)
  async function termSubmitHandler(e) {
    e.preventDefault()
    const body = {
      'new_term': selectedTerm,
      'csrfmiddlewaretoken' : Cookies.get('csrftoken')
    }

    await postRequest('/updateTermAktif/', body, 'PRODUCTION')
    router.reload()
  }

  return(
    <Fragment>
    <Head>
      <title>Administrasi</title>
    </Head>
    {daftarAlert && <SuccessAlert text="Mata Kuliah berhasil terdaftar" />}
    <main className={styles.general}> 
    <div className="container-fluid" style={{marginTop:"2%"}}>
      <div className="row">
        <Heading text = "| Mata Kuliah Spesial " type="besar"></Heading>
        <br></br>
      </div>
      
      {role === 'mahasiswa' && 
        <div className="mb-4">
          <a className="btn btn-outline-danger" href="administrasi/mendaftarMK" role="button">Daftar Mata Kuliah Lain</a>
        </div>
      } 

      {role === 'staf' && 
      <div className="mb-4">
          <a className="btn btn-outline-danger me-3" href="mataKuliahSpesial/tambah" role="button">Tambah Mata Kuliah Lain</a>
          <a className="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#modalGantiTerm" role="button">Ganti Term Aktif</a>
      </div>
      }

      {arrayMKPerTerm.map(listMK =>
        <div>
          <h2 className={styles.subtitle + " mb-4"}>Term: {listMK[0]} </h2>
          <div className="row row-cols-1 row-cols-md-3 g-4 mb-4">
            {listMK[1].map(mk =>
              <MataKuliahBox 
                periode = {mk[1]}
                nama = {mk[0]}
                onClick={() => goToMataKuliah(router, role, mk[0], mk[2])}
              />
            )}
          </div>
        </div>
      )}      

      </div>

      <div className="modal fade" id="modalGantiTerm" data-bs-backdrop="static" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title px-2">Mengganti Term Aktif</h5>
            </div>
            <form className={styles.form} onSubmit={termSubmitHandler}>
              <div className="modal-body p-4">
                  <CSRFToken type='PRODUCTION' />
                  <select className="form-select" required onChange={(e) => setSelectedTerm(e.target.value)}>
                    <option selected hidden>Pilih Term</option>
                    {listGantiTerm.map(term => <option key={term} value={term}> {term} </option>)}
                  </select>
              </div>

              <h6 className="px-4 mb-3">Apakah Anda yakin untuk mengganti term aktif ini?</h6>

              <div className="modal-footer">
                <button type="button" className="btn link-danger" data-bs-dismiss="modal">Batal</button>
                <button 
                  type="submit" 
                  disabled={selectedTerm === null ? true : false} 
                  data-bs-dismiss="modal" 
                  className="btn btn-danger">
                  Simpan
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </main>
    {showSuccessAlert && <SuccessAlert text={textSuccessAlert} />}
    {showErrorAlert && <ErrorAlert text={textErrorAlert} />}
    </Fragment>
    );
}

export default Administrasi;
