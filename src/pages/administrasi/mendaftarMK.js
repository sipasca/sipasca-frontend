import styles from "../../styles/AdministrasiPage.module.css";
import MendaftarMKBox from "../../components/MendaftarMKBox";
import React,  { useState } from 'react';
import Link from "next/link";
import Head from 'next/head'
import Cookies from 'js-cookie';
import postRequest from "../api/post-request";
import { useRouter } from 'next/router';
import CSRFToken from "../../lib/CSRFToken";
import Heading from "../../components/Heading";
import { verify } from "jsonwebtoken";
import axios from "axios";

export async function getServerSideProps(context){
  const userID = context.req.cookies["userID"]

  const secret = process.env.SECRET;
  const verifiedData = verify(userID, secret);
  const userIDVerified = verifiedData.userID;

  const res = await fetch("https://simkuspa-backend.herokuapp.com/daftar_mata_kuliah_mahasiswa/"+userIDVerified);
  const data = await res.json();
  return { props: { data } }

}

function MendaftarMK(props){
  const arrayDaftarMk = props.data.mk_tersedia
  const router = useRouter();
  const [idMK, setIdMK] = useState("");
  const [emptyMKAlert, setEmptyMKAlert] = useState(false)
  const isAnyMKAvailable = arrayDaftarMk.length > 0
  
  async function buatHandler(event) {
    event.preventDefault();
    if (idMK === '') {
      setEmptyMKAlert(true)
      return
    }
    const creds = await axios.get('/api/creds', { withCredentials: true })
    const userID = creds.data.userID
    const body = {
      id_mk : idMK,
      id_mahasiswa : userID,
      'csrfmiddlewaretoken': Cookies.get('csrftoken')
    }

    const res = await postRequest('/insertMKMahasiswa/', body, 'PRODUCTION');
    const data = res.data; 
    if(data.status == "SUCCESS"){
      window.localStorage.setItem('successDaftar', data.status)
    }
    router.push('/administrasi');
  }
  

    return(
        <div className={styles.general}>
              <Head>
                <title>Administrasi</title>
              </Head>            
            <div className="container-fluid" style={{marginTop:"2%"}}>
            <div className="row">
            <Heading text = "| Daftar Mata Kuliah Spesial " type="kecil"></Heading>
            </div>
            {isAnyMKAvailable ?
            <form onSubmit={buatHandler} className = {" needs-validation"} >
            <CSRFToken type="PRODUCTION" />
            <table className="table table-bordered table-hover">
              <thead className={" thead " + styles.headertable}>
                <tr>
                  <th scope="col"></th>
                  <th scope="col">Mata Kuliah</th>
                  <th scope="col">Kode</th>
                  <th scope="col">Kurikulum</th>
                  <th scope="col">Kredit</th>
                  <th scope="col">Mahasiswa</th>
                </tr>
              </thead>
              <tbody>
             
              {arrayDaftarMk.map(item =>   
                <MendaftarMKBox 
                  nama = {item[2]}
                  kode = {item[0]}
                  kurikulum = {item[5]}
                  kredit = {item[4]}
                  jumlah_mahasiswa = {item[3]}
                  setIdMK = {setIdMK}
                  idMK = {idMK}
                  key={idMK}
                  />
              )}
            </tbody>
            </table>
            <div styles="justify-content: center">
            <Link href="/administrasi" ><a className="btn btn-outline-danger" role="button">Batal</a></Link>
            <a><button className = {" btn btn-outline-primary "} type="submit">Simpan</button></a>
            </div>
            {emptyMKAlert && <p className={styles.alert}>Mohon pilih salah satu mata kuliah.</p>}
            </form> 
            :
            <div styles="justify-content: center">
              <p>Tidak ada kelas yang bisa didaftarkan</p>
              <Link href="/administrasi"><a className="btn btn-outline-danger" role="button">Kembali</a></Link>
            </div>
            }
            
        </div>
      </div>

    );
}

export default MendaftarMK;