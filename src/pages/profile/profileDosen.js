import { Fragment,useEffect, useState } from "react";
import Head from "next/head";
import styles from "../../styles/ProfileDosen.module.css"
import DeletePopUp from "../../components/DeletePopUp";
import Heading from "../../components/Heading";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { verify } from "jsonwebtoken";
import axios from "axios";
import { useRouter } from "next/dist/client/router";

export async function getServerSideProps(context) {    
    const userID = context.req.cookies["userID"]
    const secret = process.env.SECRET;
    const verifiedData = verify(userID, secret);
    const userIDVerified = verifiedData.userID;

    const otherUser = context.query.id

    if(otherUser===undefined){
        const [daftarProfile, daftarTopik] = await Promise.all([
            fetch('https://simkuspa-backend.herokuapp.com/getProfileDosen/'+userIDVerified),
            fetch('https://simkuspa-backend.herokuapp.com/daftarTopikByDosen/'+userIDVerified)
        ])
    
        const [data_daftarProfile, data_daftarTopik] = await Promise.all([
            daftarProfile.json(),
            daftarTopik.json()
        ])
        return { props: { data_daftarProfile, data_daftarTopik } }
    }
    else{
        const [daftarProfile, daftarTopik] = await Promise.all([
            fetch('https://simkuspa-backend.herokuapp.com/getProfileDosen/'+otherUser),
            fetch('https://simkuspa-backend.herokuapp.com/daftarTopikByDosen/'+otherUser)
        ])
    
        const [data_daftarProfile, data_daftarTopik] = await Promise.all([
            daftarProfile.json(),
            daftarTopik.json()
        ])
        return { props: { data_daftarProfile, data_daftarTopik } }
    }
  
  }

function ProfileDosen(props){
    const profileDosen = props.data_daftarProfile.profile_dosen[0]
    const listPublikasi = props.data_daftarProfile.list_publikasi
    const listPendidikan = props.data_daftarProfile.list_pendidikan
    const listKepakaran = props.data_daftarProfile.list_kepakaran
    const listTopik = props.data_daftarTopik.list_topik_by_dosen
    const listURL = props.data_daftarProfile.list_url

    let scopusURL = null
    let sintaURL = null
    if (props.data_daftarProfile.list_url[0]) {
        scopusURL = props.data_daftarProfile.list_url[0][1]
        sintaURL = props.data_daftarProfile.list_url[1][1]
    }

    const publikasiDosen = listPublikasi.map((post) =><a href={post[1]} target="_blank" rel="noreferrer"><span className={styles.fileIcon}><FontAwesomeIcon icon="fa-solid fa-file-arrow-down" /></span><span className={styles.publikasiDosen}>{post[0]}</span></a>);
    const pendidikanDosen = listPendidikan.map((post) =><li className={styles.tulisan}>{post}</li>);
    const kepakaranDosen = listKepakaran.map((post) => <h2><span className={"badge rounded-pill bg-success " + styles.bidangKepakaran}>{post}</span></h2>);
    const topikDosen = listTopik.map((post) => <a href={"../topik/detail/" + post[0]}><li><span className={styles.tulisan}>{post[1]}</span></li></a>)

    const statusBE = profileDosen[3]

    const router = useRouter();

    const [pathFetch, setPathFetch] = useState('');
    const [dosenId, setDosenId] = useState('');
    const [showPopUp, setShowPopUp] = useState(false);
    const [textPopUp, setTextPopUp] = useState('');
    const [statusDosen, setStatusDosen] = useState(true);

    useEffect(() => {
        async function setIDData() {
          const creds = await axios.get('/api/creds', { withCredentials: true })
          const userID = creds.data.userID
          setDosenId(userID);

        }
        setIDData();

        if(statusBE=="Available") {
            setStatusDosen(true)
            setPathFetch('https://simkuspa-backend.herokuapp.com/updateStatusAvailableDosen/'+dosenId+'/0')
        }
        else {
            setStatusDosen(false)
            setPathFetch('https://simkuspa-backend.herokuapp.com/updateStatusAvailableDosen/'+dosenId+'/1')
        }
      })

    const path = "/profile/profileDosen"

    let status;

    status = (statusDosen) ? <span className="badge rounded-pill bg-success">Available</span> : <span className="badge rounded-pill bg-danger">Unavailable</span>   

    function statusHandler(event) {
        event.preventDefault()
        let popUpText = "Apakah Anda yakin untuk mengubah status?"
        setShowPopUp(true)
        setTextPopUp(popUpText)

    }

    async function handleAction(choose) {
        if(choose){
            setStatusDosen(!statusDosen)
            await fetch(pathFetch)
            router.push(path)
        }
        setShowPopUp(false)
      }
     

    const [role, setRole] = useState('pengguna');
    useEffect( () => {  
        async function setRoleData() {
            const res = await axios.get('/api/creds', { withCredentials: true })
            const newRole = res.data.role
            setRole(newRole);
        }
        setRoleData();
    })

    function handleLihatProfilSelengkapnya(event){
        event.preventDefault()
        window.open(profileDosen[1], '_blank');
    }
  
    return(
        <Fragment>
    <Head>
      <title>Profile Dosen</title>
      <meta name="description" content="Sistem Informasi Mata Kuliah Pasca" />
    </Head>
    { (role === 'dosen' || role === 'mahasiswa' || role==='staf') &&
    <main>
        <div className={styles.general}> 
            <div className="container-fluid" style={{marginTop:"2%"}}>
                <div className="row">
                    <Heading text = "| Profil " type="besar"></Heading>
                    <br></br>

                    <div className={styles.profil}>
                        <div className={styles.partsatu}>
                            <div>
                                <img src={profileDosen[10]}/>
                            </div>
                            <div className={styles.komponen}>
                                <p className={styles.subtitle}>Nama</p>
                                <p className={styles.tulisan}>{profileDosen[9]}</p>

                                <p className={styles.subtitledua}>E-Mail</p>
                                <p className={styles.tulisan}>{profileDosen[4]}</p>

                                <div className={styles.partsatu}>
                                    <div className={styles.jarak}>
                                         {status}
                                    </div>
                                    {role === 'dosen' &&
                                        <div className={styles.switch + " form-check form-switch"}>
                                            <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" data-testid="switch" data-bs-toggle="tooltip" data-bs-placement="top" title="Change Availability Status" checked={statusDosen} onChange={statusHandler}></input>
                                        </div>
                                    }       
                                </div>
                            </div>
                        </div>
                        {listKepakaran.length != 0 && (
                        <div className={styles.bidang}>
                            <div className={styles.left}>
                                <p className={styles.subtitle}>Bidang Kepakaran</p>
                                <div className={styles.partsatu}> 
                                    {kepakaranDosen}
                                </div>
                            </div>
                        </div>
                        )}
                        {listTopik.length != 0 && (
                        <div className={styles.topik}>
                            <div className={styles.left}>
                                <p className={styles.subtitle}>Topik</p>
                                {topikDosen}
                            </div>
                        </div>
                        )}
                        <div className={styles.biografi}>
                            <div className={styles.left}>
                                <p className={styles.subtitle}>Biografi</p>
                                <p className={styles.tulisan}>{profileDosen[5]}</p>
                            </div>
                        </div>
                        {listPendidikan.length != 0 && (
                        <div className={styles.pendidikan}>
                            <div className={styles.left}>
                                <p className={styles.subtitle}>Pendidikan</p>
                                {pendidikanDosen}
                            </div>
                        </div>
                        )}
                        <div className={styles.minat}>
                            <div className={styles.left}>
                                <p className={styles.subtitle}>Minat Penelitian</p>
                                <p className={styles.tulisan}>{profileDosen[6]}</p>
                            </div>
                        </div>
                        {listPublikasi.length != 0 && (
                        <div className={styles.publikasi}>
                            <div className={styles.left}>
                                <p className={styles.subtitle}>Publikasi</p>
                                <div className={styles.flexCol}>
                                    {publikasiDosen}
                                </div>
                                {listURL.length !=0  && (
                                <div className={styles.flexRow}>
                                    {scopusURL != null && (
                                        <a href={scopusURL}  target="_blank" rel="noreferrer"><h2><span className={"badge rounded-pill bg-success " + styles.bidangKepakaran}>Scopus Profile</span></h2></a>
                                    )}
                                    {sintaURL != null && (
                                        <a href={sintaURL}  target="_blank" rel="noreferrer"><h2><span className={"badge rounded-pill bg-success " + styles.bidangKepakaran}>Sinta Profile</span></h2></a>
                                    )}
                                </div>
                                )}
                            </div>
                        </div>
                        )}
                    </div>
                    <button onClick={(event) => handleLihatProfilSelengkapnya(event)} className={styles.lihatProfilSelengkapnya} id="lihatProfilSelengkapnya">Lihat Profil Selengkapnya</button>
                </div> 
            </div>
        </div>
        {showPopUp && <DeletePopUp onDialog={handleAction} message={textPopUp} />} 
    </main>
    }
    </Fragment>
    );
}

export default ProfileDosen;