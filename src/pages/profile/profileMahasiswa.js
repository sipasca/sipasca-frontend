import { Fragment, useEffect } from "react";
import Head from "next/head";
import styles from "../../styles/ProfileMahasiswa.module.css";
import BerkasPentingBox from "../../components/BerkasPentingBox";
import ButtonWithIcon from "../../components/ButtonWithIcon";
import Heading from "../../components/Heading";
import storage from "../../components/storage"
import { ref, getDownloadURL } from "firebase/storage";
import { verify } from "jsonwebtoken";




export async function getServerSideProps(context) {
    const userID = context.req.cookies["userID"]
    const secret = process.env.SECRET;
    const verifiedData = verify(userID, secret);
    const userIDVerified = verifiedData.userID;

    const otherUser = context.query.id
    let cvRes
    if(otherUser===undefined){
        cvRes = await fetch('https://simkuspa-backend.herokuapp.com/getProfileMahasiswa/' + userIDVerified);
        const data = await cvRes.json();
        return { props: {data} }
    }
    else{
        cvRes = await fetch('https://simkuspa-backend.herokuapp.com/getProfileMahasiswa/' + otherUser);
        const data = await cvRes.json();
        data['otherUser'] = true
        return { props: {data} }
    }
    
}
    

function ProfileMahasiswa(props){
    const listProfile = props.data.profile_mahasiswa[0]
    let cv = '-'
    if(listProfile[6] != null){
        cv = <BerkasPentingBox nama={listProfile[7]} url={listProfile[6]}></BerkasPentingBox>;  
    }
  
    useEffect(async()=>{
        if(listProfile[5] !== null){
            const foto = await getDownloadURL(ref(storage,listProfile[5]))
            document.getElementById('foto').src = foto
        } 
    })
    return(
        <Fragment>
    <Head>
      <title>Profil</title>
      <meta name="description" content="Sistem Informasi Mata Kuliah Pasca" />
    </Head>
    <main>
      <div className="container-fluid" style={{marginTop:"2%"}}>
        <div className="row">
          <Heading text = "| Profil" type="pusatInformasi"></Heading>
        </div> 
    </div>
    {!props.data.otherUser &&
    <div className={styles.buttonEdit}>
        <ButtonWithIcon to="/profile/editProfileMahasiswa" icon="fas fa-edit" text="Edit" />
    </div>
    }

    <br></br>
    <div class="row">
    <div className="col-sm-3">
    <div className="container-fluid" style={{marginTop:"2%"}}>
    <img 
    style={{ width: 200, height: 200 }}
    id='foto'
    />
    </div>
    </div>

    <div className="col-sm-6">
            <div>
                <p className={styles.subtitle}>Nama</p>
                <p className={styles.tulisan}>{listProfile[1]}</p>
            </div>
            <div>
                <p className={styles.subtitle}>NPM</p>
                <p className={styles.tulisan}>{listProfile[4]}</p>
            </div>
            <div>
                <p className={styles.subtitle}>Email</p>
                <p className={styles.tulisan}>{listProfile[2]}</p>
            </div>
            <div>
                <p className={styles.subtitle}>CV</p>
                <p className={styles.tulisan}>{cv}</p>
            </div>
    </div>
    </div>
    
    </main>
    </Fragment>
    );
}

export default ProfileMahasiswa;