import { Fragment, useEffect, useState } from "react";
import Head from "next/head";
import styles from "../../styles/ProfileMahasiswa.module.css";
import BerkasPentingBox from "../../components/BerkasPentingBox";
import ButtonWithIcon from "../../components/ButtonWithIcon";
import Heading from "../../components/Heading";
import CSRFToken from "../../lib/CSRFToken";
import Link from "next/link";
import { useRouter } from "next/router"
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";
import SuccessAlert from "../../components/SuccessAlert";
import storage from "../../components/storage"
import Cookies from 'js-cookie';
import postRequest from "../api/post-request";
import { verify } from "jsonwebtoken";
import axios from "axios";

export async function getServerSideProps(context) {
    const userID = context.req.cookies["userID"]

    const secret = process.env.SECRET;
    const verifiedData = verify(userID, secret);
    const userIDVerified = verifiedData.userID;

    const cvRes = await fetch('https://simkuspa-backend.herokuapp.com/getProfileMahasiswa/' + userIDVerified);
    const data = await cvRes.json();
    return { props: {data} }
}
function EditProfileMahasiswa(props){
    let [file, setFile] = useState(null)
    let [berkasName, setBerkasName] = useState('');
    const [showAlert, setshowAlert] = useState(false)
    const [textAlert, setTextAlert] = useState('')
    const router = useRouter();

    const listProfile = props.data.profile_mahasiswa[0]
    let cv = '-'
    if(listProfile[6] != null){
        cv = <BerkasPentingBox nama={listProfile[7]} url={listProfile[6]}></BerkasPentingBox>;  
    }

    function fileHandler(event) {
        if (event.target.files[0] == null) {
          setBerkasName("")
        }else{
          setFile(event.target.files[0])
          setBerkasName(event.target.files[0].name)
        }
        
        
    }

    function cancelFileHandler() {
        document.getElementById("berkas").value = null
        setBerkasName("")
    }

    async function uploadFileHandler(event, type) {
        event.preventDefault();
        let berkas_directory;

        const res = await axios.get('/api/creds', { withCredentials: true })
        const userID = res.data.userID;

        berkas_directory = "production/profileImage/mahasiswa/" + userID;
        if(type==='cv'){
          berkas_directory = "production/CV/" + localStorage.getItem("userID")
        }
        berkas_directory = "production/profileImage/mahasiswa/" + localStorage.getItem("userID")
        
        const filePath = ref(storage, berkas_directory)
        const uploaded = await uploadBytes(filePath, file)
        if (uploaded != null) {
          let link = '/updateFotoMahasiswa/'+ userID
          let body = { 'image_url': berkas_directory,'csrfmiddlewaretoken': Cookies.get('csrftoken') }

          if(type==='cv'){
            link = '/updateCvMahasiswa/'+ userID
            body = { 'cv_url': berkas_directory,'cv_name': berkasName, 'csrfmiddlewaretoken': Cookies.get('csrftoken') }
          }

          await postRequest(link, body)
        }

        document.getElementById("berkas").value = null
        setBerkasName("")
        router.push('/profile/profileMahasiswa')
        setshowAlert(true)
        setTextAlert("Berkas berhasil disimpan")
      }

     useEffect(async()=>{
      if(listProfile[5] !== null){
          const foto = await getDownloadURL(ref(storage,listProfile[5]))
          document.getElementById('foto').src = foto
      } 
    })
    return(
        <Fragment>
    <Head>
      <title>Profil</title>
      <meta name="description" content="Sistem Informasi Mata Kuliah Pasca" />
    </Head>
    <main>
      
      <div className="container-fluid" style={{marginTop:"2%"}}>
        <div className="row">
          <Heading text = "| Profil" type="pusatInformasi"></Heading>
        </div> 
    </div>
    <div className={styles.buttonEdit}>
        <ButtonWithIcon to="/profile/profileMahasiswa" icon="fa-solid fa-circle-arrow-left" text="Back" />
    </div>
    <br></br>
    <div class="row">
    <div className="col-sm-3">
        <div className="container-fluid" style={{marginTop:"2%"}}>
        <img 
        style={{ width: 200, height: 200 }}
        id='foto'
        />
        </div>
        
        
        <div className="container-fluid" style={{marginTop:"2%"}}>
        <ButtonWithIcon icon="fa-solid fa-plus" text="Change Photo" data-bs-toggle="modal" data-bs-target="#modalUnggahPhoto" />
        </div>
    </div>

    <div className="col-sm-6">
          <div>
              <p className={styles.subtitle}>Nama</p>
              <p className={styles.tulisan}>{listProfile[1]}</p>
          </div>
          <div>
              <p className={styles.subtitle}>NPM</p>
              <p className={styles.tulisan}>{listProfile[4]}</p>
          </div>
          <div>
              <p className={styles.subtitle}>Email</p>
              <p className={styles.tulisan}>{listProfile[2]}</p>
          </div>
          <div>
              <p className={styles.subtitle}>CV</p>
              <p className={styles.tulisan}>{cv}</p>
              <ButtonWithIcon icon="fa-solid fa-plus" text="Change File" data-bs-toggle="modal" data-bs-target="#modalUnggah" />
              <br></br>
          </div>
      </div>
    </div>

   
    
    <div className={" modal fade"} id="modalUnggah" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <CSRFToken type="PRODUCTION" />
              <div className={" modal-dialog"}>
                <div className={" modal-content"}>
                  <div className={" modal-header"}>
                    <h5 className={" modal-title"}>Unggah CV</h5>
                  </div>
                  <div className={" modal-body"}>
                    <table>
                      <thead></thead>
                      <tbody>
                        <tr>
                          <td><label htmlFor="berkas">Attachment</label></td>
                          <td><input id="berkas" type="file" onChange={fileHandler}></input> <br /></td>
                        </tr>
                      </tbody>
                    </table>
                    <br /><br />
                    Apakah Anda yakin ingin mengunggah berkas ini?
                  </div>
                  <div className={" modal-footer"}>
                    <button id="cancelUnggah" type="button" className={" btn btn-link"} data-bs-dismiss="modal" onClick={cancelFileHandler}>Tidak</button>
                    <Link href="/profile/profileMahasiswa" passHref={true} ><button id="yesUnggah" type="button" onClick={(event) => uploadFileHandler(event, 'cv')} data-bs-dismiss="modal" className={" btn btn-primary"}>Ya</button></Link>
                  </div>
                </div>
              </div>
            </div>
            {showAlert && <SuccessAlert text={textAlert} />}

    
            <div className={" modal fade"} id="modalUnggahPhoto" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <CSRFToken type="PRODUCTION" />
              <div className={" modal-dialog"}>
                <div className={" modal-content"}>
                  <div className={" modal-header"}>
                    <h5 className={" modal-title"}>Unggah Photo</h5>
                  </div>
                  <div className={" modal-body"}>
                    <table>
                      <thead></thead>
                      <tbody>
                        <tr>
                          <td><label htmlFor="berkas">Attachment</label></td>
                          <td><input id="berkas" type="file" onChange={fileHandler}></input> <br /></td>
                        </tr>
                        
                      </tbody>
                    </table>
                    <br /><br />
                    Apakah Anda yakin ingin mengunggah Photo ini?
                  </div>
                  <div className={" modal-footer"}>
                    <button id="cancelUnggah" type="button" className={" btn btn-link"} data-bs-dismiss="modal" onClick={cancelFileHandler}>Tidak</button>
                    <Link href="/profile/profileMahasiswa" passHref={true} ><button id="yesUnggah" type="button" onClick={(event) => uploadFileHandler(event, 'foto')} data-bs-dismiss="modal" className={" btn btn-primary"}>Ya</button></Link>
                  </div>
                </div>
              </div>
            </div>
            {showAlert && <SuccessAlert text={textAlert} />}
   
    </main>
    </Fragment>
    );
}

export default EditProfileMahasiswa;