import { NextResponse } from "next/server";

const SECRET = process.env.SECRET;
const ORIGINSITE = process.env.ORIGINSITE;

const MAHASISWA_ACCESS = ['/cariDosen', '/topik/cariTopik', '/m/', '/profile/editProfileMahasiswa']
const DOSEN_ACCESS = ['/daftarTopik', '/topik/buat', '/topik/edit/', '/d/']
const MAHASISWA_DOSEN_ACCESS = ['/topik/detail']
const STAF_ACCESS = ['/pengumuman/pengumumanEdit','/pengumuman/edit', '/pengumuman/buat', '/s/']

const jwt = require('@tsndr/cloudflare-worker-jwt')

export async function middleware(req) {
    const { cookies, url } = req;

    const userIDSigned = cookies.userID;
    const roleSigned = cookies.role;

    if ((roleSigned !== undefined || userIDSigned !== undefined)) {
        const userVerified = await jwt.verify(userIDSigned, SECRET);
        const roleVerified = await jwt.verify(roleSigned, SECRET);

        if (userVerified && roleVerified) {
            const role = jwt.decode(roleSigned).role;

            if (role!== 'mahasiswa' && role !== 'dosen' && role !== 'staf') {
                return NextResponse.redirect(ORIGINSITE + '/loginSSO')
            }
            return verifyAccess(url, role)
        } 
        return NextResponse.redirect(ORIGINSITE + '/loginSSO')
        
    }
    
    return verifyAccess(url, 'pengguna')
}

function verifyAccess(url, role) {
    if (url.includes('/login') && role !== 'pengguna'){
        return NextResponse.redirect(ORIGINSITE)
    }

    if ((url.includes('/administrasi') || url.includes('/profile/') || url.includes('/daftarDosen')) && role === 'pengguna') {
        return NextResponse.redirect(ORIGINSITE + '/loginSSO')
    }

    if (MAHASISWA_DOSEN_ACCESS.some(mdURL => url.includes(mdURL))){
        return redirectAccess(['mahasiswa', 'dosen'], role)
    }
    else if (MAHASISWA_ACCESS.some(mURL => url.includes(mURL))){
        return redirectAccess('mahasiswa', role)
    }
    else if (DOSEN_ACCESS.some(dURL => url.includes(dURL))){
        return redirectAccess('dosen', role)
    }
    else if (STAF_ACCESS.some(sURL => url.includes(sURL))){
        return redirectAccess('staf', role)
    }
}

function redirectAccess(roles, asRole) {
    if (roles.includes(asRole)){
        return NextResponse.next()
    }
    else {
        return NextResponse.redirect(ORIGINSITE + '/loginSSO')
    }
}