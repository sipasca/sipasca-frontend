import Head from "next/head";
import styles from "../styles/LandingPage.module.css";
import Link from "next/link";
import PengumumanBox from "../components/pengumumanBox";
import {Fragment, useState , useEffect} from "react";
import Heading from "../components/Heading";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import FormatTanggal from "../components/FormatTanggal";
import AlurMahasiswa from "../components/Alur/AlurMahasiswa";
import AlurDosen from "../components/Alur/AlurDosen";
import AlurUmum from "../components/Alur/AlurUmum";

export async function getServerSideProps(){
  const res = await fetch("https://simkuspa-backend.herokuapp.com/pengumumanAll");

  const data = await res.json();

  return { props: { data } }

}

function LandingPage(props) {
  const latestPengumuman = {judul : props.data.pengumuman_list[0][1], 

  tanggal : props.data.pengumuman_list[0][3],

  konten : props.data.pengumuman_list[0][2],

  selengkapnya : "detailPengumuman/" + props.data.pengumuman_list[0][0] +"/" ,  
  }

  const [role, setRole] = useState('pengguna');
  useEffect(() => {  
    async function setRoleData() {
      const res = await axios.get('/api/creds', { withCredentials: true })
      const newRole = res.data.role
      setRole(newRole);
    }
    setRoleData();
  })

  const [openAlur, setOpenAlur] = useState('umum')

  return (
    <Fragment>
      <Head>
        <title>SIPASCA</title>
        <meta name="description" content="Sistem Informasi Mata Kuliah Pasca" />
      </Head>

      <main className={styles.landingPage}>
        <div className={styles.cover + " mb-5"}>
          <center><h1 className={styles.title}>SIPASCA</h1></center>
          <center><p className={styles.subtitle}>Sistem Informasi Mata Kuliah Spesial Pasca</p></center>
          
          { (role !== 'staf' && role !== 'mahasiswa' && role !== 'dosen') && 
          <Link href="/loginSSO" passHref={true}>
          <button className={styles.loginButton}>Login</button>
          </Link>
          }
          <center className="w-fit-content"><a href="#alur" className={styles.linkToAlur}>
            <p className={styles.alur + " mb-0 mt-4"}>Lihat Alur</p>
            <p className={styles.alur + " mt-0"}><FontAwesomeIcon icon="fa-solid fa-angles-down" /></p>
          </a></center>
          
          
        </div>
        <div className="container mb-5">
          <Heading text = "| Pengumuman Terkini" type="besar"></Heading>
          <PengumumanBox 
            judul = {latestPengumuman.judul}
            tanggal = {<FormatTanggal tanggal={latestPengumuman.tanggal}></FormatTanggal>}
            konten = {latestPengumuman.konten}
            selengkapnya = {latestPengumuman.selengkapnya}
          />

          <center id="alur"><FontAwesomeIcon icon="fa-solid fa-scroll" size="2x" className="my-4"/></center>

          <center className={styles.sectionAlur}>
            <h4 className="mb-4">Alur</h4>
            <div className={styles.sectionBox}>
              <h5 onClick={() => setOpenAlur("umum")} className={openAlur === 'umum' ? styles.activated : ''} id='alurUmum'>UMUM</h5>
              <h5 onClick={() => setOpenAlur("mahasiswa")} className={openAlur === 'mahasiswa' ? styles.activated : ''} id='alurMahasiswa'>MAHASISWA</h5>
              <h5 onClick={() => setOpenAlur("dosen")} className={openAlur === 'dosen' ? styles.activated : ''} id='alurDosen'>DOSEN</h5>
            </div>
          </center>

          <div className="shadow p-5 my-4 rounded">
            {openAlur === 'umum' && 
              <div className={styles.animate}>
                <Heading text = "| Alur Umum" type="besar" className="mt-5"></Heading>
                <AlurUmum />
              </div>
            }

            {openAlur === 'mahasiswa' && 
              <div className={styles.animate}>
                <Heading text = "| Alur Mahasiswa" type="besar" className="mt-5"></Heading>
                <AlurMahasiswa />
              </div>
            }

            {openAlur === 'dosen' && 
              <div className={styles.animate}>
                <Heading text = "| Alur Dosen" type="besar" className="mt-5"></Heading>
                <AlurDosen />
              </div>
            }
          </div>

          
        </div>
      </main>
    </Fragment>
  );
}

export default LandingPage;