import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Fragment, useState, useEffect} from "react";
import Heading from "../components/Heading"

import BerkasPentingBox from "../components/BerkasPentingBox";
import PengumumanBox from "../components/pengumumanBox";
import FormatTanggal from "../components/FormatTanggal";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import Head from "next/head";
import LihatSelengkapnya from "../components/LihatSelengkapnya";
import axios from "axios";
library.add(fas);

export async function getServerSideProps() {
    const [berkasPentingRes, pengumumanRes] = await Promise.all([
      fetch('https://simkuspa-backend.herokuapp.com/berkasPentingAll'),
      fetch('https://simkuspa-backend.herokuapp.com/pengumumanAll')
    ]);
    const [berkasPenting, pengumuman] = await Promise.all([
      berkasPentingRes.json(), 
      pengumumanRes.json()
    ]);
    return { props: { berkasPenting, pengumuman } };
  }

function PusatInformasi(props) {
    const [role, setRole] = useState('pengguna');
    useEffect(() => {  
        async function setRoleData() {
            const res = await axios.get('/api/creds', { withCredentials: true })
            const newRole = res.data.role
            setRole(newRole);
        }
        setRoleData();
    })
    const listBerkasPentingAll = props.berkasPenting.berkas_penting_list
    const listBerkasPenting = listBerkasPentingAll.slice(0, 5)
    const berkasPenting = listBerkasPenting.map((post) =><BerkasPentingBox nama={post[3]} url={post[1]} id={post[0]}></BerkasPentingBox>);

    const listPengumumanAll = props.pengumuman.pengumuman_list
    const listPengumuman = listPengumumanAll.slice(0,5)
    const pengumuman = listPengumuman.map((post) =><PengumumanBox judul={post[1]} tanggal={<FormatTanggal tanggal={post[3]}></FormatTanggal>} konten={post[2]} selengkapnya={"detailPengumuman/" + post[0] +"/" }/> );
  return (
    <Fragment>
    <Head>
        <title>Pusat Informasi</title>
        <meta name="description" content="Sistem Informasi Mata Kuliah Pasca" />
    </Head>
    <main>
        <div className="container-fluid" style={{marginTop:"2%"}}>
            <div className="row">
                <div className="col-sm-8" style={{marginBottom:"3%"}}>
                    <div className="d-flex justify-content-between">
                        <Heading text = "| Pengumuman" type="pusatInformasi"></Heading>
                        {role == 'staf' &&
                        <Link href="/pengumuman/pengumumanEdit">
                            <a style={{marginTop:"auto", marginBottom:'2%', color:'red'}}>
                                <FontAwesomeIcon icon="fas fa-edit" />
                            </a>
                        </Link>
                        }
                    </div>
                    {pengumuman}
                    <LihatSelengkapnya url="/pengumuman" testId="link-pengumuman" type="Pengumuman "></LihatSelengkapnya>
                </div>
                <div className="col-sm-4" style={{marginTop:"2%", marginBottom:"3%"}} >
                    <div className="d-flex justify-content-between">
                        <Heading text = "Berkas Penting" type="berkasPenting"></Heading>
                        {role == 'staf' &&
                        <Link href="/berkasPenting/edit">
                            <a  style={{marginTop:"auto", marginBottom:'2%', color:'red'}}>
                                <FontAwesomeIcon  icon="fas fa-edit" />
                            </a>
                        </Link>
                        }
                    </div>
                    {berkasPenting}
                    <LihatSelengkapnya url="/berkasPenting" testId="link-berkas-penting" type="Berkas Penting "></LihatSelengkapnya>
                </div>
                <div className="col-sm-8">
                    
                </div>
                <div className="col-sm-4">
                   
                </div>
            </div>
        </div>
    </main>
  </Fragment>
  );
}

export default PusatInformasi;