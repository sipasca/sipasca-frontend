import { useState } from "react";
import { useRouter } from "next/router";
import Head from "next/head";
import CSRFToken from "../lib/CSRFToken";
import Cookies from 'js-cookie';
import postRequest from "./api/post-request";
import axios from "axios";

import styles from "../styles/LoginSSO.module.css"

export async function getStaticProps() {
  return {
    props: { originSite: process.env.ORIGINSITE }
  }
}

export default function LoginSSO(props) {
  const router = useRouter();
  const [loginSuccess, setLoginSuccess] = useState(false);
  const [waitingLogin, setWaitingLogin] = useState(false);

  async function loginAdminHandler() {
    router.push('/login')
  }

  async function loginSSOHandler() {
    setWaitingLogin(true)
    
    const sso = await popupSSOHandler();
    setLoginSuccess(true)
    setWaitingLogin(false)
    
    const body = {
      'SSO': sso.data,
      'csrfmiddlewaretoken': Cookies.get('csrftoken')
    }
    
    const res = await postRequest('/loginSSO/', body);
    const data = res.data;

    if (data.status === 'SUCCESS'){
      await axios.post('/api/auth/SSO', data)
    }
    
    router.push('/')
  }
  
  const popupSSOHandler = () => {
    const service = encodeURIComponent(props.originSite + '/api/handleSSO')
    const SSOWindow = window.open(
      new URL(
        "https://sso.ui.ac.id/cas2/login?service="+service
      ).toString()
    )
    
    return new Promise((resolve, reject) => {
      window.addEventListener("message", (e) => {
        if (e.origin !== props.originSite) {
          return
        }
        if (SSOWindow) {
          SSOWindow.close();
        }
        
        const data = e.data;
        resolve(data);
      },
        { once: true }
      )
    })
  }

  if (loginSuccess) {
    return (
      <div className={styles.container}>
        <Head>
          <title>Login SSO</title>
        </Head>
        <h4>You will be redirected in a moment...</h4>
      </div>
    )
  }

  if(waitingLogin) {
    return (
    <div className={styles.container}>
      <Head>
        <title>Login SSO</title>
      </Head>
      <h4>Login on process... Please do not close this tab.</h4>
    </div>
    )
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Login SSO</title>
      </Head>
      <CSRFToken type="PRODUCTION" />
      <h1>SIPASCA</h1>
      <div className={styles.login}>
        <button className="mb-4" onClick={loginSSOHandler}>Login Dengan SSO</button>
        <button onClick={loginAdminHandler}>Login Admin</button>
      </div>
    </div>
  )
}