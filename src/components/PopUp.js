import styles from "../styles/PopUp.module.css";

function PopUp(props) {

    return (
        <div className={styles.popUp}>
            <div className = {" modal fade"} id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className = {" modal-dialog"}>
                    <div className = {" modal-content"}>
                        <div className = {" modal-header"}>

                        </div>
                        <div className = {" modal-body"}>
                            {props.text}
                        </div>
                        <div className = {" modal-footer"}>
                            <p type="button" className = {" btn btn-link" } data-bs-dismiss="modal">Tidak</p>
                            <a href={props.url}><button type="button" className = {" btn btn-danger"}>Ya</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}



export default PopUp;
