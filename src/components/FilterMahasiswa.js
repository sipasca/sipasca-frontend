import { React, useState, forwardRef, createRef } from 'react';
import Table from './Table';
import styles from "../styles/FilterMahasiswa.module.css";
import Link from "next/link"
import styles2 from "../styles/Daftar.module.css";
import ReactPaginate from "react-paginate";

const status = [
    { value: "Semua" },
    { value: "Belum mendapatkan pembimbing" },
    { value: "Menunggu persetujuan dosen" },
    { value: "Disetujui dosen pembimbing" },
    { value: "Masa bimbingan" }
];

function TableMahasiswa(props, ref) {
    const detailButton = <button style={{ backgroundColor: "white" }}> Detail</button>
    const judul = ["Nama Mahasiswa", "NPM", "Dosen Pembimbing", "Status", "Perubahan Terakhir"];
    const isAction = true;
    let listMahasiswa;
    listMahasiswa = (
        <>
            <Table
                columns={judul}
                data={props.data}
                isAction={isAction}
                buttonLabel={detailButton} />
            <>{props.data.length == 0
                ? <p style={{ color: "#212529", textAlign: 'center' }}> Belum ada mahasiswa </p>
                : <p></p>}</></>
    );
    return <div ref={ref}> {listMahasiswa} </div>;
}

const TableMahasiswaList = forwardRef(TableMahasiswa);

function FilterMahasiswa(props) {
    const [currentStatus, setCurrentStatus] = useState('Semua');
    const tableMahasiswaRef = createRef();
    const [currentSearch, setCurrentSearch] = useState('');
    var dataMahasiswaMKNew = [];
    
    for (let item of props.dataMahasiswaMK) {
        let row = {
            "Nama Mahasiswa": <Link href={"/profile/profileMahasiswa?id=" + item.idMahasiswa}>
                <span className={styles.namaMahasiswa}> {item['Nama Mahasiswa']}</span>
            </Link>,
            "NPM": item.NPM,
            "Status": item.Status,
            "Dosen Pembimbing": item['Dosen Pembimbing'],
            "Perubahan Terakhir": item['Last Modified'],
            urlId: item.urlId
        };
        dataMahasiswaMKNew.push(row);
    }
    if (currentStatus !== 'Semua') {
        dataMahasiswaMKNew = dataMahasiswaMKNew.filter(data => data['Status'] === currentStatus);
    }
    if (currentSearch !== '') {
        dataMahasiswaMKNew = dataMahasiswaMKNew.filter(data => data["Nama Mahasiswa"].props.children.props.children[1].toLowerCase().includes(currentSearch));
    }

    const dataLength = dataMahasiswaMKNew.length;
    const [pageNumber, setPageNumber] = useState(0);
    const rowsPerPage = 10;
    const pageVisited = pageNumber * rowsPerPage;
    let displayRows = dataMahasiswaMKNew.slice(pageVisited, pageVisited + rowsPerPage);
    const pageCount = Math.ceil(dataLength / rowsPerPage);

    const changePage = ({ selected }) => {
        setPageNumber(selected);
    };

    const changeStatus = (newStatus) => {
        setCurrentStatus(newStatus)
    }

    const handleSearch = (newSearch) => {
        setCurrentSearch(newSearch)
    }

    return (
        <>
            <div className={styles.manageList}>
                <div>
                    <h4 className={styles.titleText}>List Mahasiswa</h4>
                </div>
                <div className='d-flex flex-wrap'>
                    <div style={{ color: "black", borderColor: "#163269", marginRight:"1em", marginTop:"1em" }} className={"card"}>
                        <div style={{ padding: '0px !important' }}>
                            <form className={styles.form} style={{ padding: '0px !important', borderColor: "white" }}>
                                <label className={styles.filterLabel}>
                                    Filter by
                                </label>
                                <select 
                                    data-testid ="validationCustom"
                                    onChange={(event) => changeStatus(event.target.value)}
                                    value={currentStatus} id="selectStatus"
                                    style={{ border: 'none', cursor: 'pointer' }}>
                                    {status.map(item => <option value={item.value}>{item.value}</option>)}
                                </select>
                            </form>
                        </div>
                    </div>
                    <div style={{ color: "black", borderColor: "#163269", marginRight:"1em", marginTop:"1em" }} className={"card"}>
                        <div style={{ padding: '0px !important' }}>
                            <form className={styles.form} style={{ padding: '0px !important', borderColor: "white" }}>
                                <label className={styles.filterLabel}>
                                    Search
                                </label>
                                <input style={{ border: 'none', cursor: 'text' }} placeholder='Nama Mahasiswa' onChange={(event) => handleSearch(event.target.value)}></input>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <br></br>
            <TableMahasiswaList ref={tableMahasiswaRef} status={currentStatus} data={displayRows}></TableMahasiswaList>
            {dataLength > 10 && (
                <ReactPaginate
                    previousLabel={"Prev"}
                    nextLabel={"Next"}
                    pageCount={pageCount}
                    onPageChange={changePage}
                    containerClassName={styles2.paginationButtons}
                    previousLinkClassName={"previousButton"}
                    nextLinkClassName={"nextButton"}
                    disabledClassName={"paginationDisabled"}
                    activeClassName={styles2.paginationActive}
                />
            )}
        </>
    )
}

export default FilterMahasiswa;