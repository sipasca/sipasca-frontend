import styles from "../styles/DeletePopUp.module.css";

function DeletePopUp({ message, onDialog }) {
  return (
    <div className={styles.popup}>
      <div>
      <div className={" modal-dialog"}>
        <div className = {" modal-content"}>
            <div className = {" modal-header"}>
            </div>
            <div className={" modal-body"}>
              {message}
            </div>
            <div className={" modal-footer"}>
              <button
                id="tombolTidak"
                onClick={() => onDialog(false)}
                className={styles.buttonTidak}
              >
                Tidak
              </button>
              <button
                id="tombolYa"
                onClick={() => onDialog(true)}
                className={styles.buttonYa + " btn btn-danger"}
              >
                Ya
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default DeletePopUp;
