import SubJudul from "./SubJudul";
import styles from "../styles/DosenPembimbing.module.css";
import { Fragment, useState } from "react";
import Link from "next/link";
import DeletePopUp from "./DeletePopUp";
import CSRFToken from "../lib/CSRFToken";
import axios from "axios";
import Cookies from "js-cookie";
import postRequest from "../pages/api/post-request";
import { useRouter } from "next/router";

function DosenPembimbing(props) {
  const router = useRouter()
  const dospem1 = props.dospem1 ?? "Anda belum memiliki Dosen Pembimbing 1";
  const cariDosenButton = props.dospem1 === null && props.activeMode

  let activeCancelRequest = false
  if (!cariDosenButton) {
    activeCancelRequest = props.dospem1?.includes("Menunggu Persetujuan Dosen") && props.activeMode
  }

  const [confirmCancel, setConfirmCancel] = useState(false)  
  async function batalRequestHandler(confirm) {
    const creds = await axios.get('/api/creds', { withCredentials: true })
    const userID = creds.data.userID

    if (confirm) {
      const body = { 
        'id_mk': props.idMK, 
        'id_mahasiswa': userID, 
        'id_dosen': props.idDosen,
        'csrfmiddlewaretoken': Cookies.get('csrftoken') 
      }
      await postRequest('/batalRequestDospem', body)
      router.reload()
    }
    setConfirmCancel(false)

  }

  return (
    <Fragment>
      <SubJudul text="Dosen Pembimbing" />
      <div className={styles.container}>
        <div className={styles.dosen}>
          <span>Dosen Pembimbing 1</span>
          <span>{dospem1}</span>
        </div>
      </div>

      <CSRFToken type='PRODUCTION' />
      <div className={styles.buttonContainer}>
        {activeCancelRequest && <button className={styles.button} onClick={() => setConfirmCancel(true)}>Batalkan Permohonan Bimbingan</button>}
        {cariDosenButton && <Link href="/cariDosen" passHref><button className={styles.button}>Cari Dosen</button></Link>}
      </div>
      {confirmCancel && <DeletePopUp onDialog={batalRequestHandler} message="Dosen akan mendapat email terhadap pembatalan bimbingan Anda. Apakah Anda yakin ingin membatalkan permohonan bimbingan?" />}
    </Fragment>
  );
}

export default DosenPembimbing;
