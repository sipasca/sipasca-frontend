import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import axios from "axios";

import styles from "../styles/MataKuliahLayout.module.css";

function MKLayoutItem(props) {
  const router = useRouter();
  let path = router.pathname;
  path = path.split("/")[4];

  const [rolePath, setRolePath] = useState();
  useEffect(() => {
    async function setRole() {
      const res = await axios.get('/api/creds', { withCredentials: true })
      const newRole = res.data.role[0]
      setRolePath(newRole);
    }
    setRole();
  }, [setRolePath])
  
  const itemBasic = styles.item;
  const itemActive = itemBasic + " " + styles.itemActive;

  const linkBasic = styles.itemLink;
  const linkActive = linkBasic + " active " + styles.linkActive;

  const heading = props.path.charAt(0).toUpperCase() + props.path.slice(1);

  return (
    <div className={path === props.path ? itemActive : itemBasic}>
      <Link href={"/mataKuliahSpesial/" + props.mkID + "/" + rolePath + "/" + props.path}>
        <a className={path === props.path ? linkActive : linkBasic}>
          {heading}
        </a>
      </Link>
    </div>
  );
}

export default MKLayoutItem;
