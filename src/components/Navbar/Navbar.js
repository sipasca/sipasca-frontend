import NavItemStaf from "./NavItemStaf";
import NavItemPengguna from "./NavItemPengguna";
import NavItemDosen from "./NavItemDosen";
import NavItemMahasiswa from "./NavItemMahasiswa";
import styles from "../../styles/Navbar.module.css";
import Link from "next/link";

function Navbar(props) {
  let navItem = <NavItemPengguna isAuth={props.isAuth} />;

  switch (props.role.toLowerCase()) {
    case "mahasiswa":
      navItem = <NavItemMahasiswa isAuth={props.isAuth} />;
      break;

    case "dosen":
      navItem = <NavItemDosen isAuth={props.isAuth} />;
      break;

    case "staf":
      navItem = <NavItemStaf isAuth={props.isAuth}/>;
      break;
  }

  return (
    <nav className={styles.navbar + " navbar navbar-expand-lg navbar-light bg-light sticky-top"}>
      <div className="container-fluid">
        <Link href="/">
          <a className={styles.brand + " navbar-brand"}>
            SIPASCA
          </a>
        </Link>

        <button
          className={styles.navbarToggler + " navbar-toggler"}
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          {navItem}
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
