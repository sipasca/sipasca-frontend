
import { useRouter } from "next/router";
import styles from "../../styles/Navbar.module.css";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Cookies from "js-cookie";
import { useTerm } from "../../context/termContext"
import { useEffect } from "react";

const basicStyle = styles.link + " nav-link ";
const activeStyle = basicStyle + styles.active + " active";

function NavItem(props) {
  const router = useRouter();
  const path = router.pathname;

  return (
    <li className="nav-item">
      <Link href={props.url} passHref>
        <a className={path === props.url ? activeStyle : basicStyle}>
          {props.text}
        </a>
      </Link>
    </li>
  );
}

export default NavItem;

export function NavItemIcon(props) {
  const router = useRouter();
  const path = router.pathname;
  
  const { activeTerm, updateTerm } = useTerm()

  useEffect(() => {
    updateTerm()
  },[activeTerm])

  function logoutHandler() {
    if(typeof window !== 'undefined') {
      Cookies.remove('userID');
      Cookies.remove('role');
      window.location.href = '/';
    }

  }

  return (
    <div className={styles.rightIcon} >
      <li className="nav-item">
        <span className={styles.term} > Term Aktif: {activeTerm} </span>
      </li>
      {!props.staf && <li className="nav-item">
        <Link href={props.url} passHref>
          <div>
            <a className={path === props.url ? styles.iconActive : styles.icon}>
              <span>
                <FontAwesomeIcon icon="fa-regular fa-user" />
                <span className={styles.text}> {props.url.includes('/profil') ? 'Profil': 'Login'} </span>
              </span>
            </a>
            <a
              className={path === props.url ? activeStyle : basicStyle}
              id={path === props.url ? styles.pathActive : styles.path}
            >
              {props.text}
            </a>
          </div>
        </Link>
      </li>}
      {(props.url.includes('/profil')) && <li className="nav-item">
          <div className={styles.logout}>
            <a className={path === '/logout' ? styles.iconActive : styles.icon} onClick={logoutHandler} id='logout'>
              <span>
                <FontAwesomeIcon icon="fa-solid fa-arrow-right-from-bracket" />
                <span className={styles.text} > Logout </span>
              </span>
            </a>
            <a
              className={path === '/logout' ? activeStyle : basicStyle}
              id={path === '/logout' ? styles.pathActive : styles.path}
              onClick={logoutHandler}
            >
              Logout
            </a>
          </div>
        </li>
      }
    </div>
    
  );
}