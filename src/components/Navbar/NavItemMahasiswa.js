
import NavItem, { NavItemIcon } from "./NavItem";

function NavItemMahasiswa(props) {
  return (
    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
      <NavItem text="Cari Dosen" url="/cariDosen" />
      <NavItem text="Cari Topik" url="/topik/cariTopik" />
      <NavItem text="Daftar Dosen" url="/daftarDosen" />

      <NavItem text="Administrasi" url="/administrasi" />
      <NavItem text="Pusat Informasi" url="/pusatInformasi" />

      {props.isAuth && <NavItemIcon url="/profile/profileMahasiswa/" text="Profil" staf={false}/>}
      {!props.isAuth && <NavItemIcon url="/loginSSO" text="Login" staf={false} />}
    </ul>
  );
}

export default NavItemMahasiswa;