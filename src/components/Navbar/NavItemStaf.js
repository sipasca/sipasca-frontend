
import NavItem, { NavItemIcon } from "./NavItem";

function NavItemStaf(props) {
  return (
    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
<NavItem text="Daftar Dosen" url="/daftarDosen" />
      <NavItem text="Administrasi" url="/administrasi" />
      <NavItem text="Pusat Informasi" url="/pusatInformasi" />
      
      {!props.isAuth && <NavItemIcon url="/loginSSO" text="Login" staf={true}/>}
      {props.isAuth && <NavItemIcon url="/profil" text="Profil" staf={true} />}
    </ul>
  );
}

export default NavItemStaf;