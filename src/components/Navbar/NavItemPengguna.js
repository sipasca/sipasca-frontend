import NavItem, { NavItemIcon } from "./NavItem";

function NavItemPengguna(props) {
  return (
    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
      <NavItem text="Pusat Informasi" url="/pusatInformasi" />
      {!props.isAuth && <NavItemIcon url="/loginSSO" text="Login" />}
    </ul>
  );
}

export default NavItemPengguna;
