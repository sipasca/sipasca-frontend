

import NavItem, { NavItemIcon } from "./NavItem";

function NavItemDosen(props) {

  return (
    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
<NavItem text="Daftar Dosen" url="/daftarDosen" />
      <NavItem text="Daftar Topik" url="/topik/daftarTopik" />
      <NavItem text="Administrasi" url="/administrasi" />
      <NavItem text="Pusat Informasi" url="/pusatInformasi" />
      
      {props.isAuth && <NavItemIcon url="/profile/profileDosen/" text="Profil" staf={false}/>}

      {!props.isAuth && <NavItemIcon url="/loginSSO" text="Login" staf={false}/>}
    </ul>
  );
}

export default NavItemDosen;