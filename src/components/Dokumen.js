import { Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import storage from "./storage";
import { ref, getDownloadURL } from "firebase/storage";

function Dokumen(props) {
    async function downloadFileHandler(event){
        event.preventDefault();
        const downloadurl = await getDownloadURL(ref(storage,props.url));
        window.open(downloadurl, '_blank');
    }

    let berkasPentingItem = <button data-testid="url" onClick={downloadFileHandler}style={{ backgroundColor:"transparent", color:"black", border:"0px" }}>
    <FontAwesomeIcon icon="fa-solid fa-file-arrow-down" /> {props.nama}</button>;

    return (
        <Fragment>
            <div >
                {berkasPentingItem}
            </div>
        </Fragment>
    )
}

export default Dokumen;