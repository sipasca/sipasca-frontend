import Link from "next/link";
import styles from "../styles/PengumumanPage.module.css";

function PengumumanBox(props){
    return(
        <div className={styles.box}>
                <div  className={styles.cardheader}>
                    <div className="row">
                        <div className="col">
                            <p className="card-header" >{props.judul} </p>
                        </div>
                        <div className="col-md-auto">
                            <div className={styles.tanggal}> 
                            <p className="card-header" >{props.tanggal} </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={styles.cardBody}>
                    <p className="card-text">
                        {props.konten}
                    </p>
                <Link href={props.selengkapnya} className="card-link">Lihat Selengkapnya</Link>
                </div>
        </div>
     

        
    );
}

export default PengumumanBox;