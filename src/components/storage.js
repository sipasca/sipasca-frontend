import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyCw0ubEUL02b7cUXVrjAl9BP5iZ4deaJ-A",
  authDomain: "simkuspa.firebaseapp.com",
  databaseURL: "https://simkuspa-default-rtdb.firebaseio.com/",
  projectId: "simkuspa",
  storageBucket: "gs://simkuspa.appspot.com",
  messagingSenderId: "1041099709012",
  appId: "1:1041099709012:web:c0b837872581508d772bc1",
  measurementId: "G-YMMT94Y4X4",
};

// Initialize Firebase if there are no default app
const firebaseApp = initializeApp(firebaseConfig);
const storage = getStorage(firebaseApp);

export default storage;
