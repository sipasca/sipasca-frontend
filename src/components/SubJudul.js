import styles from "../styles/SubJudul.module.css";

function SubJudul(props) {
    return <h2 className={styles.subjudul}>{props.text}</h2>
}
export default SubJudul;