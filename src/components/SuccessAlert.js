import { Alert } from '@mui/material';
import Snackbar from '@mui/material/Snackbar';
import { useEffect,useState } from "react";

function SuccessAlert(props) {

  const [open, setOpen] = useState(false);

  useEffect( () => {
    setOpen(true);
  },[])

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  return (
    <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
      <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
        {props.text}
      </Alert>
    </Snackbar>
  );
}

export default SuccessAlert;
