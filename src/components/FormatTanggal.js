function FormatTanggal(props){
    const tanggalArray = (props.tanggal).split("-");
    const tanggal = tanggalArray[2];
    const tahun = tanggalArray[0];
    const bulan = new Date(tahun, tanggalArray[1]-1, tanggal).toLocaleString("id-ID", { month: "long" });

    return tanggal.concat(' ', bulan, ' ', tahun);
}

export default FormatTanggal;