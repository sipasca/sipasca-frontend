import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import styles from "../styles/ButtonWithIcon.module.css";

function ButtonWithIcon(props) {

  const additionalAttr = {...props}
  let iconFA = null
  if (props.icon !== undefined ) {
    delete additionalAttr.icon 
    iconFA = <FontAwesomeIcon icon={props.icon} />
  }
  delete additionalAttr.text
  delete additionalAttr.to
  
  const button = (<button type="button" className={styles.button} {...additionalAttr}>
        <p className={styles.text}>
            {iconFA} {props.text}
        </p>
    </button>);

  if (props.to !== undefined) {
      return (
        <Link href={props.to} passHref={true}> 
            {button}
        </Link>
      )
  }
  else {
      return button;
  }
}

export default ButtonWithIcon;
