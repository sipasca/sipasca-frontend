import styles from '../styles/Footer.module.css';

function Footer() {
  return (
    <div className={styles.container}>
      <div className='img-box'>
        <img src="/images/logo-footer-fasilkom.png" alt='logo' width={200}></img>
      </div>
      <div className={styles.contactBox}>
        <p className={styles.contact}>KONTAK</p>

        <div className={styles.socialBox}>
          <p className={styles.social}><a href="https://web.facebook.com/FasilkomUniversitasIndonesia" target="_blank" rel="noreferrer">Facebook</a></p>
          <p className={styles.social}><a href="https://www.linkedin.com/company/faculty-of-computer-science-universitas-indonesia/" target="_blank" rel="noreferrer">LinkedIn</a></p>
          <p className={styles.social}><a href="https://www.instagram.com/fasilkomuiofficial/" target="_blank" rel="noreferrer">Instagram</a></p>
          <p className={styles.social}><a href="https://twitter.com/FASILKOM_UI" target="_blank" rel="noreferrer">Twitter</a></p>
          <p className={styles.social}><a href="https://www.youtube.com/c/FasilkomUIOfficial/videos" target="_blank" rel="noreferrer">Youtube</a></p>
        </div>        
      </div>      
    </div>
  );
}

export default Footer;
