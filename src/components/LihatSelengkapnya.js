import { Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "../styles/PusatInformasi.module.css";
import Link from "next/link";

function LihatSelengkapnya(props){
    return(
        <Fragment>
            <div className={styles.containerLihatSelengkapnya}>
                <Link   href={props.url}>
                    <a data-testid={props.testId} style={{ textDecoration:"none"}}>
                        <div className={styles.lihatSelengkapnyaText}>Lihat Seluruh {props.type}</div>
                        <FontAwesomeIcon icon="fa-solid fa-angle-down" style={{color:"red"}} />
                    </a>
                </Link>
            </div>
        </Fragment>
    )
}

export default LihatSelengkapnya;
