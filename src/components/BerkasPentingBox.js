import { Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "../styles/PusatInformasi.module.css";
import Dokumen from "./Dokumen";

function BerkasPentingBox(props) {
    function handleDeleteId(id,url) {
        props.handleId(id,url)
    }

    let berkasPentingItem = <Dokumen nama={props.nama} url={props.url}></Dokumen>;

    if (props.mode == 'edit') {
        berkasPentingItem = <div className="d-flex justify-content-between">
            <Dokumen nama={props.nama} url={props.url}></Dokumen>
            <button id="tombolX" className={styles.buttonDelete} onClick={() => handleDeleteId(props.id,props.url)}><FontAwesomeIcon icon="fa-solid fa-xmark" /></button>
        </div>;
    }

    return (
        <Fragment>
            <div style={{ textDecoration: "none", color: "black", border: "2px solid #blue !important", marginBottom: "1%" }} className={"card"} >
                <div className={"card-body"}>
                    {berkasPentingItem}
                </div>
            </div>
        </Fragment>
    )
}

export default BerkasPentingBox;