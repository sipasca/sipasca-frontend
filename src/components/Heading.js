import styles from "../styles/Heading.module.css";

function Heading(props) {
    let headingItem = <h4>{props.text}</h4>;

    switch (props.type) {
        case "kecil":
          headingItem = <div className={styles.head}><h4>{props.text}</h4></div>;
          break;

        case "besar":
          headingItem = <div className={styles.head}><h2>{props.text}</h2></div>;
          break;

        case "login":
          headingItem = <div className={styles.login}><h3>{props.text}</h3></div>;
          break;

        case "pusatInformasi":
          headingItem = <div className={styles.pusatInformasi}><h1>{props.text}</h1></div>;
          break;

        case "pengumuman":
          headingItem = <div className={styles.pengumuman}><h1>{props.text}</h1></div>;
          break;

        case "berkasPenting":
          headingItem = <div className={styles.berkasPenting}><h4>{props.text}</h4></div>;
          break;
      }

    return (
        <div className="heading">
            {headingItem}
        </div>   
    );
}



export default Heading;
