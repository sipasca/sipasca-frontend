import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import styles from "../styles/MataKuliahLayout.module.css";
import Heading from "./Heading";
import MKLayoutItem from "./MKLayoutItem";
import axios from "axios";

function MKLayout(props) {
  const [role, setRole] = useState("pengguna");
  const [mkID, setMKID] = useState("")
  const router = useRouter();
  useEffect(() => {
    async function getRole() {
      const res = await axios.get('/api/creds', { withCredentials: true })
      const newRole = res.data.role
      setRole(newRole);
    }
    getRole()
    setMKID(router.query.mataKuliahID)
  }, [setMKID, setRole, router.query.mataKuliahID]);

  return (
    <div className={styles.layout}>
      <Heading text={"| Mata Kuliah " + props.namaMK} type="besar" />
      <div className={styles.subnavbar}>
        {role === 'mahasiswa' && <MKLayoutItem path="ringkasan" mkID={mkID}/>}
        {role === 'staf' && <MKLayoutItem path="rekap" mkID={mkID} />}
        {(role === 'mahasiswa' || role === 'staf') && <MKLayoutItem path="administrasi" mkID={mkID} />}
        {(role === 'dosen') && <MKLayoutItem path="bimbingan" mkID={mkID} />}
        {role === 'staf' && <MKLayoutItem path="migrasi" mkID={mkID} />}
      </div>

      <div className={styles.container}>{props.children}</div>
    </div>
  );
}

export default MKLayout;