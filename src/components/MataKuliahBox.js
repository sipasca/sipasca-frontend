import styles from "../styles/AdministrasiPage.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";

function MataKuliahBox(props){
    return(
       
        <div className="col">
          <div className={" card " + styles.cardstyle} onClick={props.onClick} id="cardClick">
            <div className="card-body d-flex bd-highlight mb-2">
              <div className="p-3 ">
              <FontAwesomeIcon icon="fa-solid fa-bookmark fa-4x" className={styles.tulisanButton} />
              </div>
              <div className="p-2 flex-fill bd-highlight">
              <h5 className={" card-title " + styles.cardjudul} >{props.periode}</h5>
              <h5 className="card-text">{props.nama}</h5>
              </div>
              <div className={styles.icon + " py-2"}>
              <Link href="#" passHref>
              <FontAwesomeIcon icon="fa-solid fa-angle-right fa-4x" />
              </Link>
              </div>
            </div>
          </div>
        </div>
        
    );
}

export default MataKuliahBox;