import AlurBox from "./AlurBox"
import styles from "../../styles/Alur.module.css"

function AlurStep({ listStep, width }) {
  return (
    <div className={styles.row} style={{width: width}}>
      <div className={styles.lineBox}>
        <span className={styles.dot}></span>
      </div>
      <div className={styles.rowContainer}>
        {listStep.map((step, idx) => (
            <AlurBox title={step.title} key={idx}>{step.content}</AlurBox>
        ))}
      </div>
    </div>
  )
}

export default AlurStep;
