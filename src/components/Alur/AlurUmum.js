import AlurStep from "./AlurStep"
import styles from "../../styles/Alur.module.css"
import { Fragment } from "react"

const Step1 = [
  {
    title: "Pusat Informasi",
    content: (
      <p>
        Halaman <a href={"/pusatInformasi"} className={styles.linkAlur}>Pusat Informasi</a> berisi pengumuman dan berkas penting terbaru.
      </p>
    ),
  },
]

const Step2 = [
  {
    title: "Pengumuman",
    content: (
      <p>
        Halaman <a href={"/pengumuman"} className={styles.linkAlur}>Pengumuman</a> berisi seluruh pengumuman mengenai informasi-informasi sehubungan dengan perkuliahan atau informasi lain untuk mahasiswa S2.
      </p>
    ),
  },
  {
    title: "Berkas Penting",
    content: (
      <p>
        Halaman <a href={"/berkasPenting"} className={styles.linkAlur}>Berkas Penting</a> berisi seluruh berkas penting yang dapat diunduh. Berkas penting terbagi menjadi 3 bagian yaitu: <a href={"/berkasPenting#umum"} className={styles.linkAlur}>Umum</a>, <a href={"/berkasPenting#template"} className={styles.linkAlur}>Template</a>, dan <a href={"/berkasPenting#lainLain"} className={styles.linkAlur}>Lain-lain</a>
      </p>
    ),
  },
]

function AlurUmum() {
  return (
    <Fragment>
      <AlurStep listStep={Step1} width='80%' />
      <AlurStep listStep={Step2} width='100%' />
    </Fragment>
  )
}

export default AlurUmum;
