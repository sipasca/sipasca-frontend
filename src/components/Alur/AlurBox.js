import styles from "../../styles/Alur.module.css";

function AlurBox({ title, children }) {
  return (
    <div className={styles.container}>
      <div className={"card " + styles.card} >
        <div className="card-body">
          <h5 className={"card-title " + styles.h5}>{title}</h5>
          <p className="card-text mt-3">{children}</p>
        </div>
      </div>
    </div>
  );
}

export default AlurBox;
