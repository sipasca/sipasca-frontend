import AlurStep from "./AlurStep"
import styles from "../../styles/Alur.module.css"
import { Fragment } from "react"

const Step1 = [
  {
    title: "Topik",
    content: (
      <p>
        Dosen dapat membuka topik yang nantinya dapat diambil oleh mahasiswa dari halaman <a href={"/topik/daftarTopik"} className={styles.linkAlur}>Daftar Topik</a>
        <br />
        <br />
        Topik yang dibuka dosen bisa memiliki batasan terima. Jika mahasiswa yang diterima sudah melewati batas terima, maka topik akan otomatis tertutup dari mahasiswa.
      </p>
    ),
  },
  {
    title: "Status Dosen",
    content: (
      <p>
        Dosen yang dapat direquest oleh mahasiswa hanya dosen yang statusnya <span style={{color: '#008D32', fontWeight: 700}}>Available</span>. Dosen dapat mengubah statusnya dengan mengakses halaman <a href={"/profile/profileDosen"} className={styles.linkAlur}>Profil</a>
      </p>
    ),
  },
]

const Step2 = [
  {
    title: "Permintaan Pembimbingan Mahasiswa",
    content: (
      <p>
        Dosen akan mendapat email dari SIPASCA berisi permintaan dosen pembimbing. Jika dosen memilih untuk membalas email tersebut, maka balasan dosen akan langsung dikirimkan ke email mahasiswa.
        <br />
        <br />
        Permintaan bimbingan mahasiswa akan masuk ke halaman bimbingan di <a href={"/administrasi"} className={styles.linkAlur}>Administrasi</a> pada mata kuliah masing-masing.
      </p>
    ),
  },
  {
    title: "Melihat Profil Mahasiswa",
    content: (
      <p>
        Dosen dapat melihat detail profil mahasiswa yang mengajukan bimbingan serta mahasiswa bimbingannya dari halaman bimbingan di <a href={"/administrasi"} className={styles.linkAlur}>Administrasi</a>.
      </p>
    ),
  },
]

const Step3 = [
  {
    title: "Persetujuan Pembimbingan Mahasiswa",
    content: (
      <p>
        Jika dosen sudah bersedia menjadi dosen pembimbing dari salah seorang mahasiswa, maka dosen dapat menekan tombol <span style={{color: '#008D32', fontWeight: 700}}>Accept</span> pada halaman bimbingan untuk mahasiswa tersebut di mata kuliah terkait. Mahasiswa akan mendapat email terkait persetujuan permintaan bimbingan ini.
        <br />
        <br />
        Catatan:
        <br />
        Mahasiswa yang permintaan bimbingannya disetujui akan masuk ke daftar calon mahasiswa bimbingan. Dalam tahap ini, mahasiswa perlu mengunggah form persetujuan pembimbimbing untuk diverifikasi oleh staf. Kemudian staf akan mengubah status mahasiswa tersebut ke dalam Mahasiswa Bimbingan.
      </p>
    ),
  },
  {
    title: "Penolakan Pembimbingan Mahasiswa",
    content: (
      <p>
        Jika dosen tidak bersedia menjadi dosen pembimbing dari salah seorang mahasiswa, maka dosen dapat menekan tombol <span style={{color: '#ff0000', fontWeight: 700}}>Reject</span> pada halaman bimbingan untuk mahasiswa tersebut di mata kuliah terkait. Mahasiswa akan mendapat email terkait penolakan permintaan bimbingan ini.
      </p>
    ),
  },
  {
    title: "Pembatalan Permintaan Bimbingan Oleh Mahasiswa",
    content: (
      <p>
        Mahasiswa dapat membatalkan permintaan bimbingan. Dosen akan menerima email jika ada mahasiswa yang membatalkan permintaan bimbingan.
      </p>
    ),
  },
]

function AlurDosen() {
  return (
    <Fragment>
      <AlurStep listStep={Step1} width='70%' />
      <AlurStep listStep={Step2} width='90%' />
      <AlurStep listStep={Step3} width='100%' />
    </Fragment>
  )
}

export default AlurDosen;
