import AlurStep from "./AlurStep"
import styles from "../../styles/Alur.module.css"
import { Fragment } from "react"

const Step1 = [
  {
    title: "Mendaftar Salah Satu Mata Kuliah Spesial",
    content: (
      <p>
        Mahasiswa mendaftar salah satu mata kuliah yang diinginkan dari halaman <a href={"/administrasi"} className={styles.linkAlur}>Administrasi</a>
      </p>
    ),
  },
]

const Step2 = [
    {
      title: "Mencari Dosen Pembimbing",
      content: (
        <p>
          Mahasiswa mencari dosen pembimbing untuk mata kuliahnya. Jika mahasiswa sudah memiliki topik, mahasiswa langsung mengajukan permohonan ke salah satu dosen dari halaman <a href={"/cariDosen"} className={styles.linkAlur}>Cari Dosen</a> atau halaman <a href={"/daftarDosen"} className={styles.linkAlur}>Daftar Dosen</a>. Jika mahasiswa belum mendapat topik, mahasiswa bisa mencari topik dari topik yang dibuka dosen pada halaman <a href={"/topik/cariTopik"} className={styles.linkAlur}>Cari Topik</a>         
        </p>
      ),
    },
    {
      title: "Melakukan Request Dosen Pembimbing",
      content: (
        <p>
          Mahasiswa mengajukan permohonan dosen pembimbing dengan menekan tombol “Request” dan mengisi formulir Request Dosen Pembimbing.
          <br />
          <br />
          <span className={styles.linkAlur}>
            Perhatian! <br /> Permintaan yang Anda ajukan dari Request Dosen Pembimbing akan dikirimkan ke email dosen yang bersangkutan. Jika dosen membalas email tersebut, balasan dosen akan otomatis terkirim ke email Anda.
          </span>         
        </p>
      ),
    },
    {
      title: "Persetujuan Dosen Pembimbing",
      content: (
        <p>
          Pada umumnya, dosen pembimbing akan berdiskusi dengan Anda terlebih dahulu terkait pengajuan bimbingan yang Anda kirimkan (melalui email atau media lainnya). Jika dosen pembimbing menyetujui Anda, Anda dapat melihat perubahan status di Tabel Prasyarat pada bagian administrasi di mata kuliah terkait.
          <br />
          <br />
          Jika dosen pembimbing sudah menyetujui permintaan Anda, maka Anda dapat mengunggah form persetujuan dosen pembimbing pada bagian administrasi.       
        </p>
      ),
    },
]

const Step3 = [
    {
      title: "Melengkapi Form Prasyarat Mata Kuliah",
      content: (
        <p>
          Mahasiswa melengkapi form-form yang menjadi prasyarat mata kuliah dengan melakukan upload form pada bagian administrasi dari mata kuliah terkait.
        </p>
      ),
    },
    {
      title: "Menunggu Perubahan Status oleh Admin",
      content: (
        <p>
          Admin akan mengubah status Anda dalam sistem ke Masa Bimbingan setelah Anda mengunggah form persetujuan pembimbing.
        </p>
      ),
    },
]

const Step4 = [
    {
      title: "Melengkapi Tabel Keluaran Mata Kuliah",
      content: (
        <p>
          Jika Anda sudah menyelesaikan suatu mata kuliah, Anda dapat melengkapi tabel keluaran mata kuliah.
        </p>
      ),
    },
    {
      title: "Melakukan Migrasi Mata Kuliah",
      content: (
        <p>
          Jika Anda belum menyelesaikan mata kuliah pada term yang sedang berjalan, maka Anda dapat mengajukan permohonan migrasi untuk mentransfer data-data mata kuliah terkait ke term selanjutnya.
        </p>
      ),
    },
]

function AlurMahasiswa() {
  return (
    <Fragment>
      <AlurStep listStep={Step1} width='50%' />
      <AlurStep listStep={Step2} width='100%'/>
      <AlurStep listStep={Step3} width='90%'/>
      <AlurStep listStep={Step4} width='90%'/>
    </Fragment>
  )
}

export default AlurMahasiswa;
