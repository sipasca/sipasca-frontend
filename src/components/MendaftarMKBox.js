import { Fragment } from "react";
function MendaftarMKBox(props){
    function klik(event){
      props.setIdMK(event.target.value)
    }
    function klik2(event){
      const checkElement = event.target.parentElement.childNodes[0].childNodes[0];
      props.setIdMK(checkElement.value);
      checkElement.checked = (props.kode == props.idMK) ? true : false
    }
    return(
          <Fragment>
                <tr style={{cursor: 'pointer'}}>
                  <th scope="row">
                  <input 
                  onClick = {klik}
                  type="radio" 
                  value={props.kode}
                  id="flexCheckDefault"
                  checked = {(props.kode == props.idMK) ? true : false}
                  />
                  </th>
                  <td className="matkulName" onClick={klik2}>{props.nama}</td>
                  <td onClick={klik2}>{props.kode}</td>
                  <td onClick={klik2}>{props.kurikulum}</td>
                  <td onClick={klik2}>{props.kredit}</td>
                  <td onClick={klik2}>{props.jumlah_mahasiswa}</td>
                </tr>
          </Fragment>
    );
}

export default MendaftarMKBox;
                
                