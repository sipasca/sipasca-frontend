import styles from "../styles/Table.module.css";
import Link from "next/link";

function Table(props) {
    const columns = props.columns
    const dataRow = props.data
    const isAction = props.isAction
    const isAnotherAction = props.isAnotherAction
    const buttonLabel = props.buttonLabel
    const anotherButtonLabel = props.anotherButtonLabel
    return (
        <table className={styles.tabel + " table"}>
            <thead className={styles.judul}>
                {columns.map((col, index) =>
                    <th scope="col" key={index}>{col}</th>
                )}
                {isAction &&
                    <th className={styles.col}>Action</th>
                }
                {isAnotherAction &&
                    <th className={styles.col}></th>
                }
            </thead>
            <tbody>
                {dataRow.map((data, index) =>
                    <tr key={index}>
                        {columns.map((col, index2) =>
                            <td key={index2}>{data[col]}</td>
                        )}
                        {isAction &&
                        <td className={styles.action}>
                            <Link href={data.urlId} passHref={true}>
                                <a>
                                {buttonLabel}
                                </a>
                            </Link>
                        </td>
                        }
                        {isAnotherAction &&
                         <td className={styles.action}>
                            <Link href={data.anotherUrlId} passHref={true}>
                               <a>
                               {anotherButtonLabel}
                               </a>
                            </Link>
                            </td>
                        }
                    </tr>
                )}
            </tbody>
        </table>

    );
}

export default Table;