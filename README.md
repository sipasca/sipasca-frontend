# SIPASCA
## Sistem Informasi Mata Kuliah Pasca
SIPASCA is a web application and information system to help Postgraduate (S2) students took *Mata Kuliah Spesial*.

## Table of Contents
- [Released Features](#released-features)
- [Getting Started](#getting-started)
- [Environment](#environment)
- [Developers](#developers)
- [Acknowledgements](#acknowledgements)

## Released Features

## May 24, 2022 - SIPASCA 1.3, An Integrated System for Postgraduate Special Course
### New Features
1. Pengajuan Migrasi Mata Kuliah <br >
Mahasiswa bisa mengajukan migrasi ke semester selanjutnya jika pada semester sebelumnya belum selesai

2. Fiksasi Migrasi <br >
Staf bisa menerima ataupun menolak migrasi yang diajukan oleh mahasiswa

3. Melihat Halaman Daftar Dosen <br >
Mahasiswa bisa melihat daftar dosen secara langsung tanpa melalui halaman cari dosen. Staf juga bisa melihat daftar dosen tanpa adanya request button

4. Export Data Mahasiswa ke Excel <br >
Staf bisa mengunduh seluruh rekap mahasiswa pada suatu mata kuliah dalam bentuk excel

5. Chart Administrasi<br >
Staf bisa melihat grafik data mahasiswa berdasarkan statusnya

6. Pembatalan Request Permohonan <br >
Mahasiswa dapat membatalkan request.

7. Term Aktif <br >
Menambahkan sistem term aktif. Mahasiswa tidak dapat mengunggah form atau mengedit topik pada mata kuliah yang sudah melewati term aktif. Dosen tidak dapat menerima atau menolak permohonan bimbingan pada mata kuliah yang sudah melewati term aktif. Penggantian term aktif dilakukan dari sisi admin.

8. Tabel Prasyarat dan Tabel Keluaran <br >
Menambahkan jenis persyaratan pada pembuatan form agar form bisa menjadi prasyarat dan keluaran dari suatu mata kuliah.


### Bug Fixes and Improvement
1. Alur <br >
Membuat alur menjadi lebih deskriptif sehingga lebih mudah dimengerti di halaman landing page

2. Pusat Informasi <br >
Merapikan styling detail pengumuman pada halaman Pusat Informasi

3. List Mahasiswa <br >
Menambahkan search box untuk memudahkan pencarian nama mahasiswa, filter berdasarkan status permohonan pembimbing, dan pagination untuk memudahkan staf agar tidak perlu terlalu banyak scroll ketika jumlah mahasiswa banyak. Selain itu menambahkan waktu perubahan terakhir dan dosen pembimbing pada list mahasiswa.

4. Profil Mahasiswa <br >
Mengubah dan merapikan tampilan profil mahasiswa

5. Profile Dosen <br >
Membetulkan crawling ke web CS UI

6. Topik
- Buat dan Edit Topik Dosen <br >
Menambah pilihan batas jumlah terima mahasiswa saat membuat dan mengedit topik dosen
- Daftar Topik <br >
Menambah kolom batas terima dan jumlah diterima
- Detail Topik <br >
Menambah nama dosen pengaju pada detail topik dosen

7. Daftar Mata Kuliah <br >
Mengubah checkbox menjadi radio button dan hanya memunculkan mata kuliah yang aktif pada term terkait.

8. Tambah Mata Kuliah <br >
Menghapus pilihan "Sidang" karena fitur tidak diimplementasikan dan menghapus pilihan "dosen pembimbing" karena seluruh mata kuliah spesial harus memiliki dosen pembimbing. 

9. Request Dosen Pembimbing <br >
Membatasi hanya mata kuliah pada term aktif saja yang dapat di-request dan mengganti tombol "Request" pertama menjadi "Lihat Overview Request"

10. Unggah dan Unduh Form Persyaratan <br >
Menambahkan batasan peringatan untuk mengunggah dengan ekstensi .pdf dan mengubah unduh semua form bergantung dengan list url yang ada di database

11. Bimbingan <br >
Menambahkan opsi untuk memberi pesan (*feedback*) saat dosen ingin menolak mahasiswa serta memperbaiki format email.

12. Required Form <br >
Membetulkan bug pada batasan required di beberapa form

13. Alert <br >
Memperbaiki tampilan alert

14. Membuka link eksternal ke tab baru

15. Mengganti logo SIPASCA


## April 26, 2022 - SIPASCA 1.2, An Integrated System for Postgraduate Special Course

### New Features
1. Request Dosen Pembimbing <br >
Mahasiswa dapat mengirimkan permohonan request dosen pembimbing.

2. Daftar dan Detail Permohonan Bimbingan <br >
Dosen dapat melihat daftar dan detail mahasiswa yang mengajukan permohonan bimbingan serta daftar mahasiswa bimbingannya.

3. List Mahasiswa Pada Administrasi Staf <br >
Staf dapat melihat list mahasiswa dengan status dan detailnya.

4. Mengubah Status Mahasiswa oleh Staf <br >
Staf dapat mengubah status mahasiswa.

5. Profile Dosen dan Mahasiswa <br >


6. Upload Form Persyaratan <br >
Mahasiswa dapat mengupload berkas yang dibutuhkan untuk administrasi dalam mata kuliah tersebut.

### Bug Fixes and Improvement
1. Cari Dosen
Merapikan styling pada halaman Cari Dosen dan menambahkan tombol request.

## April 05, 2022 - SIPASCA 1.1, An Integrated System for Postgraduate Special Course

### New Features

1. Administrasi Mata Kuliah <br >
Mahasiswa dapat enroll beberapa mata kuliah spesial dan melihat ringkasan administrasi dari mata kuliah tersebut.

2. Cari Topik <br >
Mahasiswa dapat mencari topik-topik yang ditawarkan oleh dosen.

3. Cari Dosen <br >
Mahasiswa dapat mencari dosen sebagai dosen pembimbing yang sesuai dengan bidang minatnya.

### Bug Fixes and Improvement

1. Landing Page <br >
Membuat gambar dan tulisan menjadi responsive ketika lebar dan tinggi layar berubah-ubah.

2. Pusat Informasi <br >
Menambahkan popup yang berisi konfirmasi penghapusan sebuah pengumuman atau berkas penting

## March 16, 2022 - SIPASCA 1.0,  An Integrated System for Postgraduate Special Course

### New Features

1. Login <br >
Mengautentikasi user dalam mengakses SIPASCA dan mengotorisasi user SIPASCA sesuai rolenya. 

2. Landing Page & Home <br >
Menjadi halaman pertama yang dilihat pengguna saat mengakses SIPASCA, menginformasikan pemberitahuan terkini terkait mata kuliah spesial Pasca, dan menginformasikan alur Penggunaan aplikasi SIPASCA.

3. Pusat Informasi <br >
Menginformasikan beberapa pemberitahuan terkini terkait mata kuliah Spesial dan berkas-berkas penting terkait mata kuliah Spesial

- Pengumuman <br >
Menginformasikan seluruh pemberitahuan terkait mata kuliah Spesial. Untuk Staff Akademik, dapat membuat dan mengedit pengumuman

- Berkas Penting <br >
Menginformasikan seluruh berkas-berkas penting terkait mata kuliah Spesial. Untuk staf akademik, dapat menambahkan atau menghapus berkas Penting


## Getting Started
SIPASCA's frontend is developed with [NEXTJS Framework](https://nextjs.org). To run this project, make sure to fulfill [NEXTJS system requirements](https://nextjs.org/docs/getting-started#system-requirements). Then, clone the project and do `npm install` inside the project:

    npm install

## Environment

Running the app:

    npm run dev
<br>
Testing the app:

    npm run test

Testing spesific test file:

    npm run test -- <fileName>

For example: running `LandingPage.test.js` will be:

    npm run test -- LandingPage
<br>
Coverage:

    npx jest --coverage
<br>



## Developers
SIPASCA is made by kangenz team:
- **K**aysa Syifa Wijdan Amin
- **A**lisha Yumna Bakri
- **N**abila Dita Putri
- **G**ilang Catur Yudishtira
- **E**tya Resa Fatma
- **N**adilatusifa
- **Z**ahrah Rahmani Putri

## Acknowledgements
* PPL CS UI 2022